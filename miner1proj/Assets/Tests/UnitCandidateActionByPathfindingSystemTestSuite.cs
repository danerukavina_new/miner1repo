﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class UnitCandidateActionByPathfindingSystemTestSuite
	{
		public GameStateEntity gameStateEntity;
		public LevelEntity levelEntity;
		public List<DwarfEntity> dwarfEntities;
		public List<AttackEntity> attackEntities;

		UnitCandidateActionByPathfindingSystem unitCandidateActionByPathfindingSystem;

		public GameObject floorObject;
		public GameObject rockObject;
		public GameObject dwarvesObject;
		public GameObject attacksObject;
		public GameObject partialFogObject;
		public GameObject fullFogObject;

		public FloorTileEntity[] editorFloorTilesPrefabs;
		public Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs;

		public RockTileEntity[] editorRockTilesPrefabs;
		public Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs;

		public DwarfEntity[] editorDwarfPrefabs;
		public Dictionary<DwarfType, DwarfEntity> dwarfPrefabs;

		public AttackEntity[] editorAttackPrefabs;
		public Dictionary<AttackType, AttackEntity> attackPrefabs;

		public PartialFogEntity[] editorPartialFogEntities;
		public Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs;

		public FullFogEntity[] editorFullFogEntities;
		public Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs;

		public DamageOrBuffTextEntity[] editorDamageOrBuffTextEntities;
		public Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs;

		public BuffGiverAreaEntity[] editorBuffGiverAreaPrefabs;
		public Dictionary<BuffGiverAreaType, BuffGiverAreaEntity> buffGiverAreaPrefabs;

		public BuffVisualAnimationControllerData[] editorBuffVisualAnimationControllers;
		public Dictionary<BuffType, BuffVisualAnimationControllerData> buffVisualAnimationControllers;

		public Dictionary<ResourceType, Sprite> resourceSprites;

		public RockDamageOrDeathAnimationEntity[] editorRockDeathAnimationEntities;
		public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDeathAnimationPrefabs;

		public RockDamageOrDeathAnimationEntity[] editorRockDamagedStageAnimationEntities;
		public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationPrefabs;

		public Dictionary<DwarfType, DwarfSpriteData> dwarfSprites;

		int x; int y;
		Vector2Int coord;
		float p;
		int id;

		[SetUp]
		public void SetupObjectsPrefabsSystems()
		{
			gameStateEntity = new GameStateEntity()
			{
				autoMine = true
			};
			levelEntity = new LevelEntity();
			dwarfEntities = new List<DwarfEntity>();
			attackEntities = new List<AttackEntity>();

			SystemManager systemManagerFromWhichToGetPrefabs = MonoBehaviour.Instantiate(Resources.Load<SystemManager>("Prefabs" + System.IO.Path.DirectorySeparatorChar + "SystemManager"));

			// TODO: Refactor this... usage... just use a real system manager object
			EntityHelpers.DictionerifyEditorArrays(
				systemManagerFromWhichToGetPrefabs.editorFloorTilesPrefabs,
				ref floorTilesPrefabs,

				systemManagerFromWhichToGetPrefabs.editorRockTilesPrefabs,
				ref rockTilesPrefabs,

				systemManagerFromWhichToGetPrefabs.editorDwarfPrefabs,
				ref dwarfPrefabs,

				systemManagerFromWhichToGetPrefabs.editorAttackPrefabs,
				ref attackPrefabs,

				systemManagerFromWhichToGetPrefabs.editorPartialFogEntities,
				ref partialFogPrefabs,

				systemManagerFromWhichToGetPrefabs.editorFullFogEntities,
				ref fullFogPrefabs,

				systemManagerFromWhichToGetPrefabs.editorDamageOrBuffTextEntities,
				ref damageOrBuffTextPrefabs,

				systemManagerFromWhichToGetPrefabs.editorResourceSprites,
				ref resourceSprites,

				systemManagerFromWhichToGetPrefabs.editorRockDeathAnimationEntities,
				ref rockDeathAnimationPrefabs,

				systemManagerFromWhichToGetPrefabs.editorRockDamagedStageAnimationEntities,
				ref rockDamagedStageAnimationPrefabs,

				systemManagerFromWhichToGetPrefabs.editorDwarfSprites,
				ref dwarfSprites,

				systemManagerFromWhichToGetPrefabs.editorBuffGiverAreaPrefabs,
				ref buffGiverAreaPrefabs,

				systemManagerFromWhichToGetPrefabs.editorBuffVisualAnimationControllers,
				ref buffVisualAnimationControllers
				);

			GameObject.DestroyImmediate(systemManagerFromWhichToGetPrefabs.gameObject);

			floorObject = new GameObject();
			rockObject = new GameObject();
			dwarvesObject = new GameObject();
			attacksObject = new GameObject();
			partialFogObject = new GameObject();
			fullFogObject = new GameObject();

			unitCandidateActionByPathfindingSystem = new UnitCandidateActionByPathfindingSystem()
			{
				gameStateEntity = gameStateEntity,
				dwarfEntities = dwarfEntities,
				levelEntity = levelEntity
			};
		}

		void CallSystems()
		{
			unitCandidateActionByPathfindingSystem.SystemUpdate();
		}

		[TearDown]
		public void TeardownEverything()
		{
			EntityHelpers.DestroyEverything(levelEntity, dwarfEntities, attackEntities, useImmediateForEditorTests: true);

			gameStateEntity.autoMine = true;
		}

		[Test]
		public void NoPathWhenInitialized()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);
		}

		// NOTE: This is a useful first check when refactoring the DwarfAutoMineTargetSystem
		[Test]
		public void DirectlyFindCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);


			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void DirectlyFindAdjacentCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void DoNotAttackCopperDiagonallyInsteadAttackBlockingRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // 2, 1 determined wrong empirically, and it's deterministic
		}

		[Test]
		public void WhenDiagonalBlockedAttackRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // 2, 1 determined wrong empirically, and it's deterministic
		}

		[Test]
		public void DoNotGoDiagonallyToCopperWhenBlockedByRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// Is this desired behaviour?
		[Test]
		public void StandingNextToNodePathSize()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 4);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(1, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void RockOverNothing()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]); // 2, 1 determined wrong empirically, and it's deterministic
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void FullFogOverNothingDiagonally()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject); // NOTE: navigation stops 1 tile next to full fog, so it has to be 2 moves away

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
        public void CopperOverRock()
        {
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			Assert.AreEqual(dwarfEntities.Count, 1);
			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void FullFogOverRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void CopperOverRockOrFullFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void RockOverBlockedCopperAndBlockedFogByMiningNearIt()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // TODO: Unclear why it chooses this path, instead of going through wall first
		}

		// NOTE: It's better to dig to copper than to explore available fog. Forgot this at some point.
		[Test]
		public void RockOverBlockingCopperOverFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 1), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void RockOverBlockedCopperAndAvoidingFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 1), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // TODO: Unclear why it chooses this path, instead of going through 2, 2 perhaps
		}

		[Test]
		public void FogInSpiteOfRockHiddenUnderIt()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void FogInsteadOfCopperFurtherDownCorridor()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void DoNotGoThroughIndestructibleToCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 5), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(false, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void DoNotGoThroughVoidToCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if (x != 1 || y != 3)
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(1, 3), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 5), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(false, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void DoGoToVoidWithFogOverIt()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if (x != 1 || y != 3)
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(1, 3), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void DoNotGoThroughIndestructibleWithFogAndCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(false, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void RockOverInaccessibleCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);

			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // There was a bug here where value was not checked correctly and it would go for some other node
		}

		[Test]
		public void AroundRocksToCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[5]);

			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // There was a bug here where value was not checked correctly and it would go for some other node
		}

		[Test]
		public void FullFogBlocksPathToCopperAroundRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // There was a bug here where value was not checked correctly and it would go for some other node
		}

		[Test]
		public void ChooseCloserCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 7, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(4, 5), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 3), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);

			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // There was a bug here where value was not checked correctly and it would go for some other node
		}

		[Test]
		public void ChooseDiagonallyCloserCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 6, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);

			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // There was a bug here where value was not checked correctly and it would go for some other node
		}

		[Test]
		public void ChooseCloserCopperAroundRocks()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 10, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(8, 1), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[5]);

			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // There was a bug here where value was not checked correctly and it would go for some other node
		}

		[Test]
		public void TwoDwarvesOneNode()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: Undesired behavior, dwarves getting attack position preference based on ID
		[Test]
		public void TwoDwarvesOneNodeFirstDwarfGetsBetterPosition()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void TwoDwarvesOneNodeWithOnePenetration()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 4), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 4), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(3, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 4), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void TwoDwarvesTwoNodes()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void TwoDwarvesTwoNodesWithPenetration()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 4), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// NOTE: This is the go-to test for "does the path system respect reservation"
		[Test]
		public void TwoDwarvesReservationDivertsOtherDwarf()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 10, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			// Only first-in-order dwarf should go for this node because only one path with 1 tile of penetration is available
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(4, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(4, 5), levelEntity, rockTilesPrefabs, rockObject);

			// Second-in-order dwarf should go for this far away node once the good penetration spot is reserved, since it's 1 tile of penetration
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(7, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(8, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(8, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(5, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(6, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(7, 1), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void ThreeDwarvesTwoNodesWithPenetration()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 4), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 4), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 2;
			Assert.AreEqual(new Vector2Int(2, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 4), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord); // 1, 4 determined wrong empirically, and it's deterministic
		}

		[Test]
		public void TwoDwarvesSameFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void ChooseLowerHPPenetrationCopperWithLongerPathTwoDwarves()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 7, 11);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			// One Copper in near corner surrounded by 2 layers of rock
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(5, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 3), levelEntity, rockTilesPrefabs, rockObject);

			// Preferred copper in far corner, surrounded by 1 layer of rock
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 8), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 8), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 9), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 9), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(1, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(1, 5), dwarfEntities[id].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(1, 6), dwarfEntities[id].candidateTileMovementStateComponent.navPath[5]);
			Assert.AreEqual(new Vector2Int(1, 7), dwarfEntities[id].candidateTileMovementStateComponent.navPath[6]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 8), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(2, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(2, 5), dwarfEntities[id].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(2, 6), dwarfEntities[id].candidateTileMovementStateComponent.navPath[5]);
			Assert.AreEqual(new Vector2Int(3, 7), dwarfEntities[id].candidateTileMovementStateComponent.navPath[6]); // This diagonal transition was determined emirically, and it's deterministic. Consider asserting only the end of the navpath
			Assert.AreEqual(new Vector2Int(3, 8), dwarfEntities[id].candidateTileMovementStateComponent.navPath[7]);
			Assert.AreEqual(new Vector2Int(3, 9), dwarfEntities[id].candidateTileMovementStateComponent.navPath[8]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 9), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void ThreeDwarvesOneAccessibleCopperSpot()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if (x != 1 || y != 3)
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(1, 3), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 1), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 2), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(1, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(1, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 2;
			Assert.IsEmpty(dwarfEntities[id].candidateTileMovementStateComponent.navPath);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(false, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// TODO: this has undesired behavior where dwarves get to mine nodes thanks to ID
		[Test]
		public void MultiDwarfOccupiedNodesAndFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 10, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if (x != 4 || y != 2)
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			// Dwarf id 2 will have to go around this void
			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(4, 2), levelEntity, floorTilesPrefabs, floorObject);

			// Dwarf id 0 can and will mine this pocket node
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);

			// Dwarves id 1 and 2 will mine this node, dwarf id 3 will dig through the rock to it
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(5, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(6, 3), levelEntity, rockTilesPrefabs, rockObject);

			// Dwarf 4 will go for fog
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 5), levelEntity, fullFogPrefabs, fullFogObject);

			// No one will mine this whatever rock
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(8, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(4, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(5, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(6, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(4, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 2;
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(3, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(4, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[5]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(4, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 3;
			Assert.AreEqual(new Vector2Int(5, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(5, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(5, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 4;
			Assert.AreEqual(new Vector2Int(6, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(7, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(7, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(7, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// TODO: Test where dwarves prefer to break rocks closer to either center of map or fog when given no better choice. Once the feature exists / or is better defined.

		// TODO: Add tests where dwarves that don't have anything to do move out of the way. Once the system is implemented...

		// TODO: Consider adding a unit test to ensure that dwarves are not using HP penetration as a way to skip walking checks across voids

		// TODO: Referring to dwarves by "id" in the dwarfEntities list makes it weird/circular to test functionality that changes the sort order of the list based on power level
	}
}
