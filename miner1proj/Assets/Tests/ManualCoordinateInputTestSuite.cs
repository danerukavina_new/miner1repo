﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
	public class ManualCoordinateInputTestSuite
	{
		public GameStateEntity gameStateEntity;
		public LevelEntity levelEntity;
		public List<DwarfEntity> dwarfEntities;
		public List<AttackEntity> attackEntities;

		UnitCandidateActionByPathfindingSystem unitCandidateActionByPathfindingSystem;

		public GameObject floorObject;
		public GameObject rockObject;
		public GameObject dwarvesObject;
		public GameObject attacksObject;
		public GameObject partialFogObject;
		public GameObject fullFogObject;
		public GameObject playerInputIndicatorObject;

		public FloorTileEntity[] editorFloorTilesPrefabs;
		public Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs;

		public RockTileEntity[] editorRockTilesPrefabs;
		public Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs;

		public DwarfEntity[] editorDwarfPrefabs;
		public Dictionary<DwarfType, DwarfEntity> dwarfPrefabs;

		public AttackEntity[] editorAttackPrefabs;
		public Dictionary<AttackType, AttackEntity> attackPrefabs;

		public PartialFogEntity[] editorPartialFogEntities;
		public Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs;

		public FullFogEntity[] editorFullFogEntities;
		public Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs;

		public PlayerInputIndicator playerInputIndicatorPrefab;

		public DamageOrBuffTextEntity[] editorDamageOrBuffTextEntities;
		public Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs;

		public BuffGiverAreaEntity[] editorBuffGiverAreaPrefabs;
		public Dictionary<BuffGiverAreaType, BuffGiverAreaEntity> buffGiverAreaPrefabs;

		public BuffVisualAnimationControllerData[] editorBuffVisualAnimationControllers;
		public Dictionary<BuffType, BuffVisualAnimationControllerData> buffVisualAnimationControllers;

		public Sprite[] editorResourceSprites;
		// public ResourceTypeSpriteData[] editorResourceSprites2;
		public Dictionary<ResourceType, Sprite> resourceSprites;

		public RockDamageOrDeathAnimationEntity[] editorRockDeathAnimationEntities;
		public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDeathAnimationPrefabs;

		public RockDamageOrDeathAnimationEntity[] editorRockDamagedStageAnimationEntities;
		public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationPrefabs;

		public Dictionary<DwarfType, DwarfSpriteData> dwarfSprites;

		int x; int y;
		Vector2Int coord;
		float p;
		int id;

		[SetUp]
		public void SetupObjectsPrefabsSystems()
		{
			gameStateEntity = new GameStateEntity()
			{
				autoMine = false,
				playerCoordinatesChanged = true
			};
			levelEntity = new LevelEntity();
			dwarfEntities = new List<DwarfEntity>();
			attackEntities = new List<AttackEntity>();

			SystemManager systemManagerFromWhichToGetPrefabs = MonoBehaviour.Instantiate(Resources.Load<SystemManager>("Prefabs" + System.IO.Path.DirectorySeparatorChar + "SystemManager"));

			// TODO: Refactor this... usage... just use a real system manager object
			EntityHelpers.DictionerifyEditorArrays(
				systemManagerFromWhichToGetPrefabs.editorFloorTilesPrefabs,
				ref floorTilesPrefabs,

				systemManagerFromWhichToGetPrefabs.editorRockTilesPrefabs,
				ref rockTilesPrefabs,

				systemManagerFromWhichToGetPrefabs.editorDwarfPrefabs,
				ref dwarfPrefabs,

				systemManagerFromWhichToGetPrefabs.editorAttackPrefabs,
				ref attackPrefabs,

				systemManagerFromWhichToGetPrefabs.editorPartialFogEntities,
				ref partialFogPrefabs,

				systemManagerFromWhichToGetPrefabs.editorFullFogEntities,
				ref fullFogPrefabs,

				systemManagerFromWhichToGetPrefabs.editorDamageOrBuffTextEntities,
				ref damageOrBuffTextPrefabs,

				systemManagerFromWhichToGetPrefabs.editorResourceSprites,
				ref resourceSprites,

				systemManagerFromWhichToGetPrefabs.editorRockDeathAnimationEntities,
				ref rockDeathAnimationPrefabs,

				systemManagerFromWhichToGetPrefabs.editorRockDamagedStageAnimationEntities,
				ref rockDamagedStageAnimationPrefabs,

				systemManagerFromWhichToGetPrefabs.editorDwarfSprites,
				ref dwarfSprites,

				systemManagerFromWhichToGetPrefabs.editorBuffGiverAreaPrefabs,
				ref buffGiverAreaPrefabs,

				systemManagerFromWhichToGetPrefabs.editorBuffVisualAnimationControllers,
				ref buffVisualAnimationControllers
				);

			// TODO: this is a one off, probably the input indicators would be in a dictionary anyway
			playerInputIndicatorPrefab = systemManagerFromWhichToGetPrefabs.playerInputIndicatorPrefab;

			GameObject.DestroyImmediate(systemManagerFromWhichToGetPrefabs.gameObject);

			floorObject = new GameObject();
			rockObject = new GameObject();
			dwarvesObject = new GameObject();
			attacksObject = new GameObject();
			partialFogObject = new GameObject();
			fullFogObject = new GameObject();
			playerInputIndicatorObject = new GameObject();

			unitCandidateActionByPathfindingSystem = new UnitCandidateActionByPathfindingSystem()
			{
				gameStateEntity = gameStateEntity,
				dwarfEntities = dwarfEntities,
				levelEntity = levelEntity
			};
		}

		void CallSystems()
		{
			// TODO: consider calling clearThisAndSetFixedUpdateDataSystem.SystemUpdate(); here since there was a bug where unit tests were not catching a bug caused by it

			unitCandidateActionByPathfindingSystem.SystemUpdate();
		}

		[TearDown]
		public void TeardownEverything()
		{
			EntityHelpers.DestroyEverything(levelEntity, dwarfEntities, attackEntities, useImmediateForEditorTests: true);

			gameStateEntity.autoMine = false;
			gameStateEntity.playerCoordinatesChanged = true;
		}

		[Test]
		public void ManualRockOverCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			Assert.AreEqual(dwarfEntities.Count, 1);
			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void ManualFreeSpaceOverCopperAndRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 1), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void ManualFogSpaceOverManualRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 1), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 1), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// NOTE: This tested caught that the complicated if regarding mining node with highest value vs fog isn't leaking
		[Test]
		public void ManualFreeSpaceOverManualRockAndAutoMineFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 7, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 1), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(2, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// TODO: Game design, is this desired - to go to an indestructible wall that can be reached?
		// Does not work, checked April 16th, 2020 did not investigate
		[Test]
		public void ManualReachableIndestructibleWallOverAutoMine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 4, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 1), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void UnreachableFogDownCorridor()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void UnreachableFogFurtherDownCorridor()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 4, 9);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 7), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 7), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 6), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void ManualCopperOverFreeSpaceBehindRock()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 4, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 4), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 5), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(2, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void UnreachableEmptySpotDownCorridorWithOneFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 5), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void GoNearUnreachableFogDownBlockedOffCorridor()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(2, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void GoToUnreachableFogInSpiteOfIndestructibleUnderFogCorridor()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void ImmediatelySuckGoingToUnreachable()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 2), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(1, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void UnreachableEmptyTileDownCorridor()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// TODO: Is this a bug? Won't cut through fog to get to manual reachable... but dwarf WOULD reveal the fog anyway if he got near
		// More evidence to consider reachable and unreachable together
		// TODO: Does not work, checked March 5th, 2020
		[Test]
		public void MineThroughVisibleCopperInsteadOfExploreFogToGetToManualReachable()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(1, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void MultipleManualChooseFreeSpaceOverManualRockAndAutoMineFog()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 1), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // Free space
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(2, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// NOTE: Regression test, for functionality that's missing I believe. Unreachable edges around indestructible being created is bad.
		// TODO: Does not work, checked April 16th, 2020
		[Test]
		public void TwoDwarfInputUnreachableEdgeGetsEliminatedFromNavForSecondDwarf()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 6, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 5), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // rock node that will get reserved
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(4, 5), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // free space

			gameStateEntity.autoMine = true;

			CallSystems();

			id = 0;
			Assert.AreEqual(2, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.IsEmpty(dwarfEntities[id].candidateTileMovementStateComponent.navPath); // TODO: known bug where unreachable edges are being created around indestructible walls instead of on them possibly
			Assert.AreEqual(4, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(4, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(4, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// NOTE: This is a regression from Feb 26th, dwarf intends to go through two indestructible walls and a void to get into fog, rather than going through a rock
		// TODO: Does not work, checked March 16th, 2020
		// TODO: Started working again, checked April 16th, 2020
		// TODO: Stopped working again after the SetLevelEntityEdgesToIndestructibleRockTiles change, May 24th, 2020
		// TODO: Started working on June 11th, 2020 thanks to the fix for dwarves that incorrectly go through indestructible by assigning a high penetration value to them (as a hack)
		[Test]
		public void DwarfPrefersPenetrateAroundVoidOverIndestructibleUnreachablePath()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 10, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if ((x != 3 || y != 5) || (x != 5 || y != 1) 
						|| (x != 7 || y != 2) // This coordinate is key in the regression
						)
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(3, 5), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(5, 1), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(7, 2), levelEntity, floorTilesPrefabs, floorObject); // This coordinate is key in the regression

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(3, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(4, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(5, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 6), levelEntity, rockTilesPrefabs, rockObject); // under fog, but adding based on real scenario that failed
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(5, 6), levelEntity, rockTilesPrefabs, rockObject); // under fog, but adding based on real scenario that failed
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(6, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(6, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(6, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(7, 1), levelEntity, rockTilesPrefabs, rockObject); // under fog, but adding based on real scenario that failed
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(7, 3), levelEntity, rockTilesPrefabs, rockObject); // under fog, but adding based on real scenario that failed
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(8, 2), levelEntity, rockTilesPrefabs, rockObject); // under fog, but adding based on real scenario that failed


			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 1), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 1), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 6), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(4, 2), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(8, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // unreachable in the fog

			gameStateEntity.autoMine = false;

			CallSystems();

			id = 0;
			// Assert.IsEmpty(dwarfEntities[id].candidateTileMovementStateComponent.navPath);
			Assert.AreEqual(2, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(4, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(5, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(6, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void DwarfPrefersPenetrateOverIndestructibleUnreachablePath()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 10, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if ((x != 3 || y != 5) || (x != 5 || y != 1))
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(3, 5), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(5, 1), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(3, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(4, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(5, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 5), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(3, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(6, 1), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(6, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(6, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 1), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 6), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 1), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 6), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(4, 2), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(8, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // unreachable in the fog

			gameStateEntity.autoMine = false;

			CallSystems();

			id = 0;
			Assert.AreEqual(2, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(4, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(5, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(6, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: Does not work, checked April 16th, 2020
		[Test]
		public void MultiDwarfOccupiedNodesAndFogWithManualCoordinates()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 10, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if (x != 4 || y != 2)
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			// Dwarf id 2 will have to go around this void
			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(4, 2), levelEntity, floorTilesPrefabs, floorObject);

			// Dwarf id 0 can and will mine this pocket node
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);

			// Dwarves id 1 and 2 will mine this node, dwarf id 3 will dig through the rock to it
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 3), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(5, 4), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(6, 3), levelEntity, rockTilesPrefabs, rockObject);

			// Dwarf 4 will go for fog
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 5), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 5), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(8, 4), levelEntity, fullFogPrefabs, fullFogObject);

			// No one will mine this whatever rock
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(8, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(3, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(4, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(5, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(6, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // copper
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(4, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // copper
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(5, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // rock
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(8, 5), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // unreachable free space

			gameStateEntity.autoMine = true;

			CallSystems();

			id = 0;
			Assert.AreEqual(1, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(3, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(4, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 2;
			Assert.AreEqual(6, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(3, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(4, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[5]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(4, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 3;
			Assert.AreEqual(2, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(5, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(5, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(5, 3), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 4;
			Assert.IsEmpty(dwarfEntities[id].candidateTileMovementStateComponent.navPath); // TODO: known bug where unreachable edges are being created around indestructible walls instead of on them possibly
			Assert.AreEqual(5, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(6, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(7, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(7, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(7, 4), dwarfEntities[id].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void UnreachableUnwalkableFogOverAutoMine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(3, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void GoAroundFogAndIntendToMoveDiagonallyThroughIt()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 7, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(3, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(5, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(4, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(3, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void LowestVisibleHPPenetrationToReachableOverAutoMine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 7, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 2), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject); // Should not be aware this blocks good penetration
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(5, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(5, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(5, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(5, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void LowestVisibleHPPenetrationAndPathToUnreachableOverAutoMine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 7, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 2), levelEntity, rockTilesPrefabs, rockObject); // Should penetrate here
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 2), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(4, 3), levelEntity, rockTilesPrefabs, rockObject); // Should not be aware this blocks good penetration
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(4, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(4, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(4, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: Game design, is this desired behavior? Or should or do there need to be two categories of fog unreachable and certainly unreachable?
		[Test]
		public void ReachableOverCloserUnreachable()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 9, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // unreachable
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(7, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // reachable

			CallSystems();

			Assert.AreEqual(6, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(5, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(6, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(new Vector2Int(7, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[5]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}


		[Test]
		public void ReachableCopperOverUnreachableOverVoidFreeSpace()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 4, 7);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					if ((x != 1 || y != 3) || (x != 2 || y != 3))
						EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(1, 3), levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(2, 3), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 1), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 1), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 5), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.AreEqual(1, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: Game design, is this desired behavior? Or should or do there need to be two categories of fog unreachable and certainly unreachable?
		[Test]
		public void LowestVisibleHPPenetrationToReachableOverCloserUnreachableAndAutoMine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 8, 8);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(3, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(4, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(5, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 2), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(6, 2), levelEntity, rockTilesPrefabs, rockObject); // Should not be aware this blocks good penetration

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(5, 3), levelEntity, rockTilesPrefabs, rockObject); // Should not be aware this blocks good penetration
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(4, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // unreachable
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(6, 3), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject); // reachable

			gameStateEntity.autoMine = true;

			CallSystems();

			Assert.AreEqual(5, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(3, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(new Vector2Int(4, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[3]);
			Assert.AreEqual(new Vector2Int(5, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[4]);
			Assert.AreEqual(true, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(5, 2), dwarfEntities[0].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: Does not work, perhaps due to SetLevelEntityEdgesToIndestructibleRockTiles change, at least as of May 24th, 2020
		[Test]
		public void SecondCoordinateCloserUnreachableInTwoRegions()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 9, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(7, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			Assert.IsEmpty(dwarfEntities[0].candidateTileMovementStateComponent.navPath);
			Assert.AreEqual(3, dwarfEntities[0].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[0].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[0].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[0].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[0].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[0].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		[Test]
		public void TwoDwarfManualRockOverCopper()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 5, 5);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 3), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: This is an example of the undesireable "which dwarf goes first" behavior. They switch spots for no reason
		[Test]
		public void TwoDwarfManualCopperAndThenRockAutomine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 4, 4);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(2, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		// TODO: This is an example of the undesireable "which dwarf goes first" behavior. They switch spots for no reason
		[Test]
		public void TwoDwarfManualRockAndThenCopperAutomine()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 4, 4);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
			EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(2, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 2), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.autoMine = true;

			CallSystems();

			id = 0;
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);

			id = 1;
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(2, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(true, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
			Assert.AreEqual(new Vector2Int(2, 2), dwarfEntities[id].candidateUnitAttackingStateComponent.intendedAttackTargetCoord);
		}

		[Test]
		public void UnreachableFogDownCorridorTwoDwarves()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 3, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 2), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(3, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);

			id = 1;
			Assert.AreEqual(1, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// TODO: Does not work, perhaps due to SetLevelEntityEdgesToIndestructibleRockTiles change, at least as of May 24th, 2020
		[Test]
		public void CloserDwarfCloserUnreachableCoordTwoRegions()
		{
			EntityHelpers.InitializeLevelEntity(levelEntity, 9, 6);
			EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

			for (x = 0; x < levelEntity.xSize; x++)
				for (y = 0; y < levelEntity.ySize; y++)
					EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 3), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 3), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(1, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(2, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(3, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(5, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(6, 4), levelEntity, fullFogPrefabs, fullFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(7, 4), levelEntity, fullFogPrefabs, fullFogObject);

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(7, 1), dwarfEntities, dwarfPrefabs, dwarvesObject);

			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(7, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, new Vector2Int(1, 4), levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			CallSystems();

			id = 0;
			Assert.AreEqual(3, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(1, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(1, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(1, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);

			id = 1;
			Assert.AreEqual(3, dwarfEntities[id].candidateTileMovementStateComponent.navPath.Count);
			Assert.AreEqual(new Vector2Int(7, 1), dwarfEntities[id].candidateTileMovementStateComponent.navPath[0]);
			Assert.AreEqual(new Vector2Int(7, 2), dwarfEntities[id].candidateTileMovementStateComponent.navPath[1]);
			Assert.AreEqual(new Vector2Int(7, 3), dwarfEntities[id].candidateTileMovementStateComponent.navPath[2]);
			Assert.AreEqual(false, dwarfEntities[id].candidateUnitAttackingStateComponent.hasIntendedAttackTarget);
			Assert.AreEqual(true, dwarfEntities[id].candidateTileMovementStateComponent.hasIntendedMovementTarget);
		}

		// TODO: Manual coords, once implemented

		// TODO: permanently locked in unreachable coord being removed, once it's implemented
	}
}
