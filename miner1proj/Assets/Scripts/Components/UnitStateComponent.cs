﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitState
{
	Idle = 0,
	Moving,
	Attacking
}

public class UnitStateComponent
{
	public UnitState unitState;
	public bool changedToBeClearedOnUpdate;

	public void ResetForNextLevel()
	{
		unitState = UnitState.Idle;
		changedToBeClearedOnUpdate = true;
	}
}
