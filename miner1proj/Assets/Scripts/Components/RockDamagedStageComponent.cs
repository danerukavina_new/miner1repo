﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockDamagedStageComponent
{
	public List<Sprite> rockDamagedStageSprites;
	public List<double> percentualRockDamagedStageThresholds;
	public int thresholdStage;
}
