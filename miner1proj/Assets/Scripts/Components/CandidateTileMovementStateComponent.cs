﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandidateTileMovementStateComponent
{
	// State data
	public bool candidatePrimedForChecking;
	public bool hasIntendedMovementTarget; // Perhaps this should always be used, regardless of attacking?
	public List<Vector2Int> navPath;
	public Vector2Int spotToFreeSpaceStandOnCoord;

	public void ResetForNextLevel()
	{
		candidatePrimedForChecking = false;
	}

	public static bool Equal(TileMovementStateComponent tileMovementStateComponent, CandidateTileMovementStateComponent tileMovementStateCandidateComponent)
	{
		if (tileMovementStateComponent.hasIntendedMovementTarget == tileMovementStateCandidateComponent.hasIntendedMovementTarget
			&& tileMovementStateComponent.navPath.Count == tileMovementStateCandidateComponent.navPath.Count
			&& tileMovementStateComponent.spotToFreeSpaceStandOnCoord == tileMovementStateCandidateComponent.spotToFreeSpaceStandOnCoord)
		{
			bool allTheSame = true;

			for (int i = 0; i < tileMovementStateComponent.navPath.Count; i++)
			{
				allTheSame &= tileMovementStateComponent.navPath[i] == tileMovementStateCandidateComponent.navPath[i];
			}
			return allTheSame;
		}

		return false;
	}
}
