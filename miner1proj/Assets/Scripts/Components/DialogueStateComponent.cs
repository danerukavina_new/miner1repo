﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum DialogueState
//{
//	DialogueInProgiress,
//	DialogueEnded
//}

public class DialogueStateComponent
{
	// State data
	public bool playerInputNextSpeechStep;

	public bool initiateInteractiveDialogue;

	public List<DialogueStepEntity> dialogueStepEntities;
	public int currentDialogueStep;
}
