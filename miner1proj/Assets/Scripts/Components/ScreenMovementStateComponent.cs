﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMovementStateComponent
{
	// Prototype data
	public float speed;
	public float lingerInPlaceDelay;

	// State data
	public Vector3 direction;
	public float movementStartTime;
}
