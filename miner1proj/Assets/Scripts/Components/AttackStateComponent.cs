﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackState
{
	Moving = 0,
	DealingDamage = 1,
	FizzlingOut = 2
}

public class AttackStateComponent
{
	public AttackState attackState;
	public bool changedToBeClearedOnUpdate;
}
