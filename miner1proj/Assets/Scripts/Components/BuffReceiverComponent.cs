﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiverComponent
{
	// TODO: This unit typing should be its own component
	public enum ReceiverUnitTypeTag
	{
		Dwarf,
		Node
	}

	// Prototype
	// public int numberOfTopNBuffsToVisualize;
	// NOTE: Number of buffs to visualize and the said buffs are controlled by the BuffVisualEntity collection o nthe DwarfEntity

	// State
	public HashSet<ReceiverUnitTypeTag> receiverUnitTypeTags;
	public List<PrototypeBuff> receivedPrototypeBuffsThisFixedUpdate;
	public Dictionary<BuffType, BuffEntity> buffEntities;
	public bool buffEntitiesListIsDirtyThisFixedUpdate;
	// public List<BuffEntity> topNBuffsToVisualize; // TODO: Use limited set here?
	public BuffEntity.BuffStatSet accumulatedBuffStats;
	public bool accumulatedBuffStatsIsDirty;
	public bool buffEntitiesListIsDirtyAfterBuffResolver;
}
