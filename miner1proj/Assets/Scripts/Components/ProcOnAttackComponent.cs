﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProcType
{
	OreCryBuffGiverArea,
	OnNodeToughnessArmorReduction,
	OnNodeDamageAmp
}

public struct TemporaryLevelToProcUnlockData
{
	public int unlockLevel;
	public ProcType procType;
}

public class ProcOnAttackComponent
{
	public int highestLevelAttained; // TODO: This is a little janky, but it's insurance that a dwarf doesn't keep regaining the same procs if the level keeps going dirty. Wasn't actually an issue but feels right to have in place. Would be dictated by further designs potentially.
	public List<ProcData> procDataOnAttack;
}
