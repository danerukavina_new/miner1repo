﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovementRangeComponent
{
	public Vector2Int originCoord;
	public float movementRange;
}
