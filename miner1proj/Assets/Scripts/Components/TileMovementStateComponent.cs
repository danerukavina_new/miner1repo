﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileMovementStateComponent
{
	// Prototype data
	public float movementSpeed;

	// State data
	public bool hasIntendedMovementTarget; // Perhaps this should always be used, regardless of attacking?
	public bool readyForNextCoord;
	public List<Vector2Int> navPath;
	public Vector2Int spotToFreeSpaceStandOnCoord;

	public Vector2Int nextCoord;
	public Vector2Int normalizedGridAlignedDirection;
	public bool directionChangeToBeCheckedOnUpdate;
	// TODO: Note, when we implement movement animations, diagonal movements should have a mapping to grid aligned directions. upleft and upright or whatever should show the profle of the dwarf, while downleft and downright should be face on.

	public void ResetForNextLevel()
	{
		hasIntendedMovementTarget = false;
		readyForNextCoord = false;
		navPath.Clear();

		normalizedGridAlignedDirection = Vector2Int.down;
		directionChangeToBeCheckedOnUpdate = false;
	}
}
