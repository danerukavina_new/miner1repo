﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandidateUnitAttackingStateComponent
{
	// State data
	public bool candidatePrimedForChecking;
	public bool hasIntendedAttackTarget;
	public Vector2Int intendedAttackTargetCoord;
	public Vector2Int spotToAttackFromCoord; // TODO: probably ok to remove this and replace with the last nav?

	public void ResetForNextLevel()
	{
		candidatePrimedForChecking = false;
	}

	public static bool Equal(UnitAttackingStateComponent unitAttackingStateComponent, CandidateUnitAttackingStateComponent unitAttackingStateCandidateComponent)
	{
		if (unitAttackingStateComponent.hasIntendedAttackTarget == unitAttackingStateCandidateComponent.hasIntendedAttackTarget
			&& unitAttackingStateComponent.intendedAttackTargetCoord == unitAttackingStateCandidateComponent.intendedAttackTargetCoord
			&& unitAttackingStateComponent.spotToAttackFromCoord == unitAttackingStateCandidateComponent.spotToAttackFromCoord)
			return true;
		return false;
	}
}
