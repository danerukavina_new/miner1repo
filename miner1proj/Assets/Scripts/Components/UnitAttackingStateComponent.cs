﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PossibleAttackingPosition
{
	Melee,
	GridAlignedWithRange,
	RadiusWithRange,
}

[System.Serializable]
public class UnitAttackingStateComponent
{
	[System.Serializable]
	public struct AttackStatSet
	{
		public float attackCreationPointTime;
		public float attackBackswingFinishedPointTime;
		public float attackCompletionTime;
	}

	// Prototype data
	public List<AttackType> attackRotation;
	public PossibleAttackingPosition possibleAttackingPosition;
	public float attackRange; // NOTE: Makes sense to be float for carefully calibrated "distance" circle attacks. For example 3.5 creates a specific kind of circle of ranges
	public AttackStatSet baseAttackStatSet;
	public AttackStatSet currentAttackStatSet;
	public AttackStatSet nextAttackStatSet;

	// State data
	public bool isRefreshedPreviousFixedUpdate; // TODO: attack finish is occuring before the nav system, so it's setting "attackFinishedThisFixedUpdate". Maybe that's wrong...
	public bool hasIntendedAttackTarget;
	public Vector2Int intendedAttackTargetCoord;
	public Vector2Int spotToAttackFromCoord; // TODO: probably ok to remove this and replace with the last nav?

	public bool initiateAttackRotation;
	public int nextAttack;
	public float attackStartTime;
	public bool attackCreationPointPerformed;
	public bool attackBackswingFinishedPointPerformed;

	public void ResetForNextLevel()
	{
		isRefreshedPreviousFixedUpdate = false;
		hasIntendedAttackTarget = false;

		nextAttack = 0;
		initiateAttackRotation = false;
	}
}
