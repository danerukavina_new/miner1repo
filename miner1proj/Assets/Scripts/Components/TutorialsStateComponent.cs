﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialsStateComponent
{
	// Prototype data
	public List<TutorialStepEntity> tutorialStepEntities;

	// State data
	public int currentOrNextTutorialStep;
	public bool activateTutorialStepThisUpdate;
	public bool tutorialStepIsActive;
	public float endTimeofPreviousTutorialStep;
	public bool dialogueEnded;

	public bool hasTutorialCameraBounds;
	public BoundsInt tutorialCameraBounds;
}
