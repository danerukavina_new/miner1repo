﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainTileBouncingComponent
{
	public int maxBounces;
	public bool repeatBounceTargetsAllowed;
	public float bounceRange; // NOTE: Expected maximum implied from size initiation with MAXIUM_CHAIN_TILE_BOUNCE_ANDIDATE_TARGETS. At time of writing on March 8th, 2020, it's 8f
	public List<Vector2Int> candidateBounceTargets;
	

	public bool bouncedThisFixedUpdate;
	public int bounces;
	public List<Vector2Int> bounceHistory; // NOTE: Expected maximum is MAXIMUM_CHAIN_TILE_BOUNCE_HISTORY

}
