﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceAdditionSourceType
{
	DamagedStageOrDeath,
	DamagedStageSmelt
}

public class CandidateResourceAddition
{
	public RockTileType sourceRockTileType; // The system that processes these candidate additions actually determines what's received. This prevents the implementation of a "find which nodes have gems in them" gameplay
	// If you want this discover what's in nodes ahead of time gameplay, create a RockResourcesComponent which stores the available resources at rock creation, rather than randomly determining them in the system.
	public Vector2Int coord; // TODO: This is annoying. Makes me think it's better to pass a reference to the whole rock tile entity...
	public bool isSmeltEffect;
}

public class PlayerResourcesComponent
{
	public Dictionary<ResourceType, ResourceEntity> resources;
	public List<CandidateResourceAddition> candidateResourceAdditions;
	public LimitedQueuedSet<ResourceType> recentlyAddedResources;
}
