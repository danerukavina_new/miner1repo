﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationComponent
{
	public bool levelDataWasDirtied;

	public NavCoord[,] navArray;
	public NavCoord[,] unreachableNavArray; // NOTE: Each dwarf has a full fog nav array, because they can have a manual assignment as well as the overall assignment on the map
	public List<Vector2Int> candidateNavCoords; // TODO: Why is this not in the systems? It's a temp variable
	public List<Vector2Int> foundManualCoords;
	public List<Vector2Int> foundMiningNodes;
	public List<Vector2Int> foundFullFogs;
	public List<Vector2Int> foundUnreachableEdges;
	public bool[,] consideredMiningNodeAlready;
	public bool[,] consideredFullFogAlready;
	public bool[,] consideredUnreachableEdgeAlready;
	public List<Vector2Int> reverseNavPathHolder;
	public List<Vector2Int> attackRangePattern;
}
