﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogSightComponent
{
	public bool fogSightEnabled;
	public int partialFogSightRadius;
	public int fullFogSightRadius;
}
