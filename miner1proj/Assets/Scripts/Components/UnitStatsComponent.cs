﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatsComponent
{
	[System.Serializable]
	public struct UnitStatSet
	{
		public double damageFactor;
		public double critChance;
		public double critBonusDamageFactor;
		public double smeltChance;
		public float movementSpeed;

		public double upgradeCost;

		public float attackSpeedBonus;
		public double damageAmpBonus;
	}

	public UnitStatSet baseStats;
	public UnitStatSet growthStats;

	// State
	public bool statsWereDirtied;
	public int level;
	public UnitStatSet deltaFromUpgradeStats;
	public UnitStatSet levelBasedStats;
	public UnitStatSet finalStats;
	// TODO: add boolean "canBeUpgraded", use it in a system to set the state of DwarfUpgradePanelEntity

	public bool attemptToPerformLevelUpgrade;
	public bool levelIsDirty;
	public bool updateUIAsResultOfRecalculatedLevelBasedStats; // TODO: Check this idea - these should maybe live in the DwarfUpgradePanelEntity?
	public bool showUpgradeStatsTextsAsResultOfSuccessfulLevelIncrease; // TODO: Check this idea - these should maybe live in the DwarfUpgradePanelEntity?

	public UnitStatSet upgradeDeltaComparedToBaseStats;
}
