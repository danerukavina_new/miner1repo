﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TutorialValueHolder
{
	public const int L0_R1OffY = 3;
	public static readonly Vector2Int L0_dwarf0Coord = new Vector2Int(3, L0_R1OffY + 4);
	public static readonly Vector2Int L0_copperCoord = new Vector2Int(13, L0_R1OffY + 5);
	public static readonly Vector2Int L0_coordBeforeExit = new Vector2Int(15, L0_R1OffY + 6);
	public const string EGG_CHAMBER_INITIAL_NAME = "";
	public const string EGG_CHAMBER_KNOWN_NAME = "Egg";

	public const int questionmarkOffestY = 15 + 7;
	public const int R1Width = 10;
	public const int R1Height = 7;
	public const int R2Width = 11;
	public const int R2Height = R1Height + 2;
	public const int R3Width = 10;
	public const int R3Height = 11;
	public const int R4Width = 9;
	public const int R4Height = 9;

	public const int R1offX = 0;
	public const int R1offY = questionmarkOffestY;
	public const int R2offX = 10;
	public const int R2offY = questionmarkOffestY - 2;
	public const int R3offX = R2offX; // 10
	public const int R3offY = R2offY - R3Height; // 9
	public const int R4offX = R3offX - 1;
	public const int R4offY = R3offY - R4Height;

	public static readonly Vector2Int L1_dwarf0Coord = new Vector2Int(R1offX + 1, R1offY + 3);
	public static readonly Vector2Int L1_dwarf1Coord = new Vector2Int(R3offX + 4, R3offY + 10);
	public static readonly Vector2Int L1_dwarf2Coord = new Vector2Int(R4offX + 2, R4offY + 3);
	public static readonly Vector2Int L1_dwarf3Coord = new Vector2Int(R4offX + 3, R4offY + 5);
	public static readonly Vector2Int L1_dwarf4Coord = new Vector2Int(R4offX + 4, R4offY + 2);
	public static readonly Vector2Int L1_dwarf5Coord = new Vector2Int(R4offX + 5, R4offY + 4);
	public static readonly Vector2Int L1_dwarf6Coord = new Vector2Int(R4offX + 0, R4offY + 4);

	public static readonly Vector2Int L1_ritual00Coord = new Vector2Int(R4offX + 3, R4offY + 3);
	public static readonly Vector2Int L1_ritual10Coord = new Vector2Int(R4offX + 4, R4offY + 3);
	public static readonly Vector2Int L1_ritual01Coord = new Vector2Int(R4offX + 3, R4offY + 4);
	public static readonly Vector2Int L1_ritual11Coord = new Vector2Int(R4offX + 4, R4offY + 4);

	public static readonly Vector2Int L1_dwarf2InitialFacingDirection = Vector2Int.right;
	public static readonly Vector2Int L1_dwarf3InitialFacingDirection = Vector2Int.down;
	public static readonly Vector2Int L1_dwarf4InitialFacingDirection = Vector2Int.up;
	public static readonly Vector2Int L1_dwarf5InitialFacingDirection = Vector2Int.left;
	public static readonly Vector2Int L1_dwarf6InitialFacingDirection = Vector2Int.right;

	public const int L2_R1OffY = 3;
	public static readonly Vector2Int L2_dwarf0Coord = new Vector2Int(0, L0_R1OffY + 1);
	public static readonly Vector2Int L2_dwarf1Coord = new Vector2Int(0, L0_R1OffY + 2);
	public static readonly Vector2Int L2_coordBeforeExit = new Vector2Int(0 + 12, L2_R1OffY + 1);

	public const int L3_Width = 7;
	public const int L3_Height = 9;
	public static readonly Vector2Int L3_EntranceCoord = new Vector2Int(0, L3_Height - 2);
	public static readonly Vector2Int L3_ExitCoord = new Vector2Int(L3_Width - 3, 0);

	public const int L4_Width = 17;
	public const int L4_Height = 9; // Don't make levels height 10, awkward for camera... camera makes it look like it's an exit. Would also be a shitty experience to have to shift camera by 1 up just to see an exit
	public const int L4_R1OffX = 0;
	public const int L4_R1OffY = 0;

	public const int L5_Width = 16;
	public const int L5_Height = 9;

	public const int L6_Width = 16;
	public const int L6_Height = 20;
	public const int L6_R1OffX = 0;
	public const int L6_R1OffY = 0;

	public const int L7_Width = 16;
	public const int L7_Height = 9;
	public const int L7_R1OffX = 0;
	public const int L7_R1OffY = 0;

	public const int L8_Width = 10;
	public const int L8_Height = 9;
	public const int L8_R1OffX = 0;
	public const int L8_R1OffY = 0;
	public static readonly Vector2Int L8_eggChamberOpenCoord = new Vector2Int(L8_R1OffX + 7, L8_R1OffY + 3);


	// TODO: work on encoding the tutorial
	public static readonly List<TutorialStepEntity> tutorialStepEntities = new List<TutorialStepEntity>()
	{
		//new TutorialStepEntity(
		//	"TEST - Do nothing step",
		//	new List<TutorialStepComponentTag>() {
		//	},
		//	(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
		//	{
		//		return true;
		//	}
		//	)
		//{
		//},
		//new TutorialStepEntity(
		//	"TEST - Unenterable step",
		//	new List<TutorialStepComponentTag>() {
		//	},
		//	(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
		//	{
		//		return false;
		//	}
		//	)
		//{
		//},

		/// --------------------------------------------
		/// Level 0
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 0, "L0-1 - Disable other dwarves, disable controls, disable fog removal",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.DisableDwarfFogSight,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.DisableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons // NOTE: For consistency, putting this in all tutorial starts
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			teleportCameraPosition = new Vector2(4.5f, 7f),
			disableDwarfFogSightIDs = new List<int>() { 0 },
			disableDwarfCandidateActionProcessingIDs = new List<int> { 0 },
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-1b - Detect absence of start screen",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return !gameStateEntity.onStartScreen;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-1c - After a delay, have the egg chamber say something",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"... bioforming outcome: ACCEPTABLE",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.EggChamberClosed },
					customSpeakerName = EGG_CHAMBER_INITIAL_NAME,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				},
				new DialogueStepEntity("...", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity("Umbilicals: DISENGING", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity("Umbilicals: CLEAR", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity(
					"Subject ejection: COMPLETE",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.EggChamberOpen },
				},
				new DialogueStepEntity("...", new List<DialogueStepComponentTag>()),
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 0, "L0-1c2 - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 0, "L0-1d - Have the egg chamber say some more",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2.5f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Wake up, Maglore.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.EggChamberOpen },
					customSpeakerName = "",
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 0, "L0-1e - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 0, "L0-2 - Clear some fog",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveFullFog,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			fullFogsToBeRemovedCoords = TutorialLevel0R1FogClearingCoordinates(L0_dwarf0Coord),
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-2a - Turn on sight",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReenableAllDwarfFogSight,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-2b - Have the dwarf talk",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 0, new List<string>() { "... ow.", "... hungry..." })
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 0, "L0-2c - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 0, "L0-3 - Turn the dwarf, pan camera",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			// tutorialCameraBounds = new BoundsInt(7, 3, 0, 12, 8, 0),
			tutorialCameraBounds = new BoundsInt(9, 3, 0, 12, 8, 0),
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.right },
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-3b - Have the egg chamber give more instructions",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(DwarfType.EggChamberOpen, EGG_CHAMBER_KNOWN_NAME, new List<string>() { "Mine stone." })
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 0, "L0-3c - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 0, "L0-4 - Enable user input and show tutorial indicator",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return  true;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.TapInput,
			addedTutorialInputIndicatorPosition = new Vector2(5f, 6.5f),
			addedTutorialInputIndicatorOptionalText = "Tap here !"
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-4b - Remove tutorial indicator",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return levelEntity.sharedManualCoordinates.Count > 0;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-5 - Detect progress, and move camera",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return  LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L0_dwarf0Coord.x + 5, L0_dwarf0Coord.y, 0), new Vector3Int(1,1,1)));
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(13, 3, 0, 7, 8, 0),
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-6 - When user sees copper...",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return levelEntity.fullFogTiles[L0_copperCoord.x, L0_copperCoord.y] == null;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-6c - ... tell user to mine it",
			new List<TutorialStepComponentTag>() {
				// TutorialStepComponentTag.ClearManualCoordinates,
				// TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				// TutorialStepComponentTag.DisableManualCoordinateInput,
				// TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int> { 0 }, // The only dwarf
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(DwarfType.EggChamberOpen, EGG_CHAMBER_KNOWN_NAME, new List<string>() { "Seek rare metals and gems." } )
		},
		// TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 0, "L0-6d - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 0, "L0-6e - Enable user input",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.EnableCameraInput
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return  true;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0 }
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-7b - Detect exit area being seen and enough time passing",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// TODO: This exit area tutorial step is a great example of something that should be optional, instead of linear tutorial steps
				return  (levelEntity.fullFogTiles[L0_coordBeforeExit.x, L0_coordBeforeExit.y] == null && gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 40f < Time.time) // exit passage seen
				|| (levelEntity.rockTiles[L0_copperCoord.x, L0_copperCoord.y] == null && gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time) // Copper destroyed
				|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 50f < Time.time // A safety measure
				|| levelEntity.rockTiles[L0_coordBeforeExit.x - 1, L0_coordBeforeExit.y] == null; // In case the user is proceeding too quickly
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-7b - Place tutorial marker for exiting and reveal exit",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText,
				TutorialStepComponentTag.RemoveFullFog
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time
				|| (levelEntity.rockTiles[L0_coordBeforeExit.x - 1, L0_coordBeforeExit.y] == null && gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.375f < Time.time); // In case the user is proceeding too quickly
			}
			)
		{
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.TapInput,
			addedTutorialInputIndicatorPosition = new Vector2(L0_coordBeforeExit.x, L0_coordBeforeExit.y - 0.5f),
			addedTutorialInputIndicatorOptionalText = "Go here !",
			fullFogsToBeRemovedCoords = new List<Vector2Int>() { L0_coordBeforeExit, L0_coordBeforeExit + new Vector2Int(1, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-8 - Detect exiting and stop the dwarf, remove tutorial indication",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// If the dwarf is standing one space before the exit
				// Or if the dwarf is standing two spaces before the exit, and has an intended input towards the exit

				// TODO: This transform.position based check introduced before release of first playable because some other checks didn't help. This whole step is... risky. The issue was when the user clicked on the fog coord 1 above the exit
				// It may be better to disable/enable exiting on the gameStateEntity level as a guarantee these tutorials don't mess up
				return dwarfEntities[0].transform.position.x >= L0_coordBeforeExit.x - 0.25f
				|| LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L0_coordBeforeExit.x, L0_coordBeforeExit.y, 0), new Vector3Int(2,1,1)))
				||
				(
				LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L0_coordBeforeExit.x - 1, L0_coordBeforeExit.y, 0), new Vector3Int(3,1,1)))
				&& (levelEntity.isSharedManualCoordinate[L0_coordBeforeExit.x, L0_coordBeforeExit.y] 
				|| levelEntity.isSharedManualCoordinate[L0_coordBeforeExit.x + 1, L0_coordBeforeExit.y]
				|| levelEntity.isSharedManualCoordinate[L0_coordBeforeExit.x + 1, L0_coordBeforeExit.y - 1]
				|| levelEntity.isSharedManualCoordinate[L0_coordBeforeExit.x + 1, L0_coordBeforeExit.y + 1] // NOTE: This didn't help somehow for preventing a buggy, early exit, even though this is the tile
				)
				)
				;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { L0_coordBeforeExit }
		},
		new TutorialStepEntity(
			gameLevel: 0, "L0-7b - Egg chamber says one more thing",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return dwarfEntities[0].unitStateComponent.unitState == UnitState.Idle;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"And, we beg you...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.CustomSpeakerName,
						// DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.EggChamberOpen },
					customSpeakerName = EGG_CHAMBER_KNOWN_NAME,
					speakerNameIsOnRightSide = false,
					// highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				},
				new DialogueStepEntity(
					"... find the others.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 0, "L0-7c - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 0, "L0-7d - Force exiting",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.AssignManualCoordinates,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int> { L0_coordBeforeExit + new Vector2Int(1, 0) },
		},

		/// --------------------------------------------
		/// Level 1
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 1, "1 - Disable other dwarves, disable camera controls, move dwarf at start of game, weaken a specific rock later in level",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.DisableDwarfFogSight,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.SetRockHPs,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
				TutorialStepComponentTag.DisableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons // NOTE: For consistency, putting this in all tutorial starts
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 1 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },

			teleportCameraPosition = new Vector2(4.5f, 25f),
			disableDwarfCandidateActionProcessingIDs = new List<int> { 1, 2, 3, 4, 5, 6 }, // All but the first dwarf
			disableDwarfFogSightIDs = new List<int> { 1, 2, 3, 4, 5, 6 }, // All but the first dwarf don't clear fog
			rocksToSetHPsCoords = new List<Vector2Int>() { new Vector2Int(R2offX + 4, R2offY + 0) }, // A rock blocking the second dwarf
			rockHPsToBeSetValues = new List<double>() { 20d }, // HP value for that rock
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int> { 2, 3, 4, 5, 6 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int> { L1_dwarf2InitialFacingDirection, L1_dwarf3InitialFacingDirection, L1_dwarf4InitialFacingDirection, L1_dwarf5InitialFacingDirection, L1_dwarf6InitialFacingDirection }
		},
		new TutorialStepEntity(
			gameLevel: 1, "3 - Detect progress, take control dwarf, and pan camera over to next chamber",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(R1offX + 8, R1offY + 2, 0), new Vector3Int(1,2,1)));
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(R2offX + 4, R2offY + 5, 0, 1, 1, 0),
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R2offX + 4, R2offY + 5) }, // A free tile in the second chamber
		},
		new TutorialStepEntity(
			gameLevel: 1, "4 - Stop first dwarf, have second dwarf speak",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(R2offX + 4, R2offY + 5, 0), new Vector3Int(1,1,1)));
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 1, new List<string>() { "Hey you, on the other side !",  "Come help me !" })
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "4b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "5 - Pan camera down, activate second dwarf to drill into room (through the pre-damaged rock), disable resource gains from this dwarf for now, and ensure the target full fog is gone on time (and clear some other fogs to make it natural), turn first dwarf to look downwards",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.DisableResourceGains,
				TutorialStepComponentTag.RemoveFullFog,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time; // 1.5 second delay before camera pans and dwarf starts mining
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(R2offX + 4, R2offY + 3, 0, 1,1,0),
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 1 },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R2offX + 4, R2offY + 3) },
			fullFogsToBeRemovedCoords = new List<Vector2Int>() {
				new Vector2Int(R2offX + 3, R2offY + 1),
				new Vector2Int(R2offX + 3, R2offY + 2),
				new Vector2Int(R2offX + 3, R2offY + 3),
				new Vector2Int(R2offX + 4, R2offY + 0),
				new Vector2Int(R2offX + 4, R2offY + 1),
				new Vector2Int(R2offX + 4, R2offY + 2),
				new Vector2Int(R2offX + 4, R2offY + 3),
				new Vector2Int(R2offX + 5, R2offY + 1),
				new Vector2Int(R2offX + 5, R2offY + 2),
				new Vector2Int(R2offX + 5, R2offY + 3),
			}, // Again, the rock blocking the second dwarf. Making sure it's not in fog which may depend on Update vs FixedUpdate when the dwarf's attack plans are calculated.
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int> { 0 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int> { Vector2Int.down }
		},
		new TutorialStepEntity(
			gameLevel: 1, "6 - Second dwarf talking about tomb",
			new List<TutorialStepComponentTag>() { TutorialStepComponentTag.DialogueSteps },
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(R2offX + 4, R2offY + 3, 0), new Vector3Int(1,1,1)));
			}
			)
		{
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 1, new List<string>() { "*Huff* *huff* I finally found it...", "The Tomb of Lacondre !", "But, I'm stuck. Come -","- let's dig it out, together !" })
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "6b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "7 - Pan camera down, reactivate first dwarf, and walk both dwarves down to the bottom room. Re-enable resource gains and add fog sight to new dwarf, now that they're working together",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.EnableResourceGains,
				TutorialStepComponentTag.ReenableDwarfFogSight
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time; // 0.5 second delay before camera pans and dwarf starts mining
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(new Vector3Int(R3offX + 4, R3offY + 6, 0), new Vector3Int(1,1,0)),
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R3offX + 4, R3offY + 6), new Vector2Int(R3offX + 5, R3offY + 6) },
			reenableDwarfFogSightIDs = new List<int> () { 1 }
		},
		new TutorialStepEntity(
			gameLevel: 1, "8 - Have the dwarves arrive...",
			new List<TutorialStepComponentTag>(),
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(R3offX + 5, R3offY + 6, 0), new Vector3Int(1,1,1)));
			}
			)
		{
		},
		new TutorialStepEntity(
		gameLevel: 1, "8b - ... delay a bit, second dwarf saying dig here, face dwarves down",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.25f < Time.time;
			}
			)
		{
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 1, new List<string>() { "These gray rocks are too tough for me." }),
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int> { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int> { Vector2Int.down, Vector2Int.down }
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "8b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "9 - Pan camera down, further",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time; // delay before camera pans down further
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(new Vector3Int(R3offX + 4, R3offY + 2, 0), new Vector3Int(1,1,0))
		},
		new TutorialStepEntity(
			gameLevel: 1, "9b - Make Gettan demonstrate not doing damage",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time; // delay before Gettan moves
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R3offX + 5, R3offY + 4) },
		},
		new TutorialStepEntity(
			gameLevel: 1, "9b2 - Make Gettan demonstrate not doing damage part 2",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.AssignManualCoordinates,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// TODO: This is a delay before assigning the attack coordinate. For some reason we can't assign movement and attack together. Would think dwarf would go for free space first... but no for some reason.
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.75f < Time.time; 
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R3offX + 4, R3offY + 4) },
		},
		new TutorialStepEntity(
			gameLevel: 1, "9c - Make Gettan stop demonstrating and walk up",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 6f < Time.time; // delay before camera pans down further
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R3offX + 5, R3offY + 5) },
		},
		new TutorialStepEntity(
			gameLevel: 1, "9d - Have Gettan speak, asking the user to help",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(R3offX + 5, R3offY + 5, 0), new Vector3Int(1,1,1))); // delay before camera pans down further
			}
			)
		{
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 1, new List<string>() { "See?  My ice picks aren't made to break these.", "Wanna give it a shot?" }),
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "9e - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "10 - Enable user input (and don't constrain it?) and turn Gettan to face the rocks, and show tutorial input indicator",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return  gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.down },
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.TapAndDragInput,
			addedTutorialInputIndicatorPosition = new Vector2(R3offX + 4f, R3offY + 1.5f),
			addedTutorialInputIndicatorOptionalText = "Swipe !"
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1-10b - Remove tutorial indicator",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return levelEntity.sharedManualCoordinates.Count > 1;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 1, "11 - Next room reached, stop dwarves",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				Vector2Int rockCoord0 = new Vector2Int(R3offX + 3, R3offY);
				Vector2Int rockCoord1 = new Vector2Int(R3offX + 4, R3offY);

				return levelEntity.rockTiles[rockCoord0.x, rockCoord0.y] == null || levelEntity.rockTiles[rockCoord1.x, rockCoord1.y] == null;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 }
		},
		new TutorialStepEntity(
			gameLevel: 1, "12 - Enable both camera and input controls (let the user learn the difference), clear some fog to guide the user, show tap and hold tutorial input indicator, and extend tutorial camera bounds.",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.RemoveFullFog,
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText,
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera

			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 },
			fullFogsToBeRemovedCoords = TutorialR4FogClearingCoordinatesPart1(),
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.PressAndHoldInput,
			addedTutorialInputIndicatorPosition = new Vector2(R3offX + 0f, R3offY + -0.5f),
			addedTutorialInputIndicatorOptionalText = "Tap and hold, then look down !",
			tutorialCameraBounds = new BoundsInt(new Vector3Int(R3offX + 4, R3offY - 4, 0), new Vector3Int(2,8,0))
		},
		new TutorialStepEntity(
			gameLevel: 1, "13 - Camera moved appropriately by user - usurp camera control and lock controls, move dwarves to enrance, and remove tutorial input indicator",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.CameraIsWithinBounds(mainCamera, new BoundsInt(R4offX + 2, R4offY + 2, 0, 6, 6, 0));
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(R4offX + 4, R4offY + 3, 0, 1, 1, 0), // Focused on center of Room 4, the where the ritual is occuring
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(R4offX + 4, R4offY + 7), new Vector2Int(R4offX + 5, R4offY + 7) }, // The top of room 4
		},
		new TutorialStepEntity(
			gameLevel: 1, "13b - When dwarves arrive, clear more fog, and turn dwarves to face down",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveFullFog,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(R4offX + 4, R4offY + 7, 0), new Vector3Int(1,1,1)))
				|| LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(R4offX + 4, R4offY + 7, 0), new Vector3Int(1,1,1)));
			}
			)
		{
			fullFogsToBeRemovedCoords = TutorialR4FogClearingCoordinatesPart2(),
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int> { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int> { Vector2Int.down, Vector2Int.down }
		},
		new TutorialStepEntity(
			gameLevel: 1, "13c - And then some more fog, and turn dwarves to face down again, just in case",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveFullFog,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time; // 2 second delay after dwarves arrive
			}
			)
		{
			fullFogsToBeRemovedCoords = TutorialR4FogClearingCoordinatesPart3(),
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int> { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int> { Vector2Int.down, Vector2Int.down }
		},
		new TutorialStepEntity(
			gameLevel: 1, "13d - Have the ritual dwarves provide fog sight, revealing the ritual fully",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReenableDwarfFogSight
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time; // 3 second delay after dwarves arrive
			}
			)
		{
			reenableDwarfFogSightIDs = new List<int> () { 2, 3, 4, 5 }
		},
		new TutorialStepEntity(
			gameLevel: 1, "14 - Ritual speech after a delay",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time; // 4 second total delay after dwarves arrive
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 },
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 2, new List<string>() { "Frou ett. Cern sevur. Cern zod...." })
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "14b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "15 - Ritual dwarf says something, dwarf 1 says something back",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time; // 1.5 second delay before camera pans down further
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"... Zod taru. Zod dour. Zod ohn...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 3, 4 },
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("... Zod zod."),
				new DialogueStepEntity(
					"Hello there...?",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.HighlightedParticipants,
						DialogueStepComponentTag.SpeakerName
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 3, 4, 1, 0 },
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, false, false, false, null },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = true
				},
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "15b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "15c - Ritual dwarves say something more",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time; // 1.5 second delay before camera pans down further
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Ferra ferra ! Epo zod !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.HighlightedParticipants,
						DialogueStepComponentTag.SpeakerName
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 3, 4, 1, 0 },
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, null, null, false, false },
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "15d - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 16c - Create ritual rock effects part 1",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L1_ritual00Coord, L1_ritual01Coord, L1_ritual10Coord, L1_ritual11Coord },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L1_ritual00Coord, L1_ritual01Coord, L1_ritual10Coord, L1_ritual11Coord },
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 16d - Create ritual rock effects part 2",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveDwarves,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			dwarvesToBeRemovedIDs = new List<int>() { 2, 3, 4, 5 },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L1_dwarf2Coord, L1_dwarf3Coord, L1_dwarf4Coord, L1_dwarf5Coord },
		},
		new TutorialStepEntity(
			gameLevel: 1, "17 - Dwarves comment escaping",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Uhm...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { null, null, null, 1, 0 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = true,
					highlightSingleParticipantScreenID = 4
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "Let's get outta here !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Look, there's an exit !")
			}
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 17b - Create ritual rock effects part 3",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2.5f < Time.time;
			}
			)
		{
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L1_dwarf2Coord + new Vector2Int(-1, -1), L1_dwarf3Coord + new Vector2Int(-1, 1), L1_dwarf4Coord + new Vector2Int(2, 0), L1_dwarf5Coord + new Vector2Int(1, 0) },
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 17b2 - Create ritual rock effects part 4",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L1_dwarf2Coord, L1_dwarf3Coord, L1_dwarf4Coord, L1_dwarf5Coord },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L1_dwarf2Coord, L1_dwarf3Coord, L1_dwarf4Coord, L1_dwarf5Coord },
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 17b3 - Create ritual rock effects part 5",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			rocksToBeCreatedTypes = new List<RockTileType>() {
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
			},
			rocksToBeCreatedCoords = new List<Vector2Int>() {
				L1_dwarf3Coord + new Vector2Int(-2, 0),
				L1_dwarf3Coord + new Vector2Int(0, 2),
				L1_dwarf4Coord + new Vector2Int(-1, 0),
				L1_dwarf4Coord + new Vector2Int(-1, -1),
				L1_dwarf5Coord + new Vector2Int(0, 2),
				L1_dwarf5Coord + new Vector2Int(0, -1),
				L1_dwarf5Coord + new Vector2Int(2, -1)
			},
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 1, "17b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 1, "18 - Enable input and let dwarves escape",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.DisableAllButExitInput,
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 },
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.TapInput,
			addedTutorialInputIndicatorPosition = new Vector2(R4offX + 8f, R4offY + 3.5f),
			addedTutorialInputIndicatorOptionalText = "Tap to escape !"
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 18b - Create ritual rock effects part 6",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() {
				L1_dwarf2Coord + new Vector2Int(-1, -1),
				L1_dwarf3Coord + new Vector2Int(-1, 1),
				L1_dwarf4Coord + new Vector2Int(-1, 0),
				L1_dwarf5Coord + new Vector2Int(1, 0),
				L1_dwarf5Coord + new Vector2Int(0, -1),
				L1_dwarf5Coord + new Vector2Int(2, -1)
			},
			rocksToBeCreatedTypes = new List<RockTileType>() {
				RockTileType.RockRitualDarkness,
				RockTileType.RockRitualDarkness,
				RockTileType.RockRitualDarkness,
				RockTileType.RockRitualDarkness,
				RockTileType.RockRitualDarkness,
				RockTileType.RockRitualDarkness
			},
			rocksToBeCreatedCoords = new List<Vector2Int>() {
				L1_dwarf2Coord + new Vector2Int(-1, -1),
				L1_dwarf3Coord + new Vector2Int(-1, 1),
				L1_dwarf4Coord + new Vector2Int(-1, 0),
				L1_dwarf5Coord + new Vector2Int(1, 0),
				L1_dwarf5Coord + new Vector2Int(0, -1),
				L1_dwarf5Coord + new Vector2Int(2, -1)
			},
		},
		new TutorialStepEntity(
			gameLevel: 1, "18b - Remove tutorial input",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// NOTE: No need to check if it was exit input, since that's the only input allowed
				return levelEntity.sharedManualCoordinates.Count > 0;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 1, "L1 - 18b2 - Create ritual rock effects part 7",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			rocksToBeCreatedTypes = new List<RockTileType>() {
				RockTileType.RockRitualDarknessSpawnEffect,
				RockTileType.RockRitualDarknessSpawnEffect,
			},
			rocksToBeCreatedCoords = new List<Vector2Int>() {
				L1_dwarf5Coord + new Vector2Int(0, 1),
				L1_dwarf5Coord + new Vector2Int(-1, 1)
			},
		},

		/// --------------------------------------------
		/// Level 2
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 2, "2 - 1 - Start off next level, with restricted controls", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.DisableDwarfUpgradeContinueButton,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons // NOTE: For consistency, putting this in all tutorial starts
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 2 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(6.5f, 4.5f),
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L2_dwarf0Coord.x + 3, L2_dwarf0Coord.y), new Vector2Int(L2_dwarf1Coord.x + 3, L2_dwarf1Coord.y) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 1b - Turn dwarves to face here and there",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return dwarfEntities[0].unitStateComponent.unitState == UnitState.Idle && dwarfEntities[1].unitStateComponent.unitState == UnitState.Idle;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.right, Vector2Int.left },
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 1b2 - Create pursuing ritual rock effects",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf1Coord }, // Removal of the inivisible entrance rock
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf1Coord }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 1b3 - Create pursuing ritual rock effects part 2",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.75f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf0Coord }, // Removal of the inivisible entrance rock
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarknessSpawnEffect, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord, L2_dwarf1Coord + new Vector2Int(1, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-1c - Discuss escape and the bridge",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"The shadow ! It's still comin' !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { null, null, null, 1 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = true,
				},
				new DialogueStepEntity(
					"This narrow pass ahead - that might slow it !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "Go. Go !")
			}
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 1c2 - Create pursuing ritual rock effects part 3, while dwarves talk",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.25f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf1Coord },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf1Coord, L2_dwarf0Coord + new Vector2Int(2, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 1c2 - Create pursuing ritual rock effects part 4, while dwarves talk",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf0Coord },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord, L2_dwarf0Coord + new Vector2Int(1, 0) }
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-1d - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 2, "L2-1e - Reenable controls",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.EnableCameraInput,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 2a - Create pursuing ritual rock effects part 4, during play",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(1, 0), L2_dwarf1Coord + new Vector2Int(1, 0)},
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(1, 0), L2_dwarf1Coord + new Vector2Int(1, 0), L2_dwarf1Coord + new Vector2Int(2, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 2a2 - Create pursuing ritual rock effects part 4, during play",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(1, 0), L2_dwarf1Coord + new Vector2Int(1, 0)},
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarkness, /* RockTileType.RockRitualDarknessSpawnEffect*/ },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(1, 0), L2_dwarf1Coord + new Vector2Int(1, 0), /* L2_dwarf1Coord + new Vector2Int(2, 0)*/ }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 2a3 - Create pursuing ritual rock effects part 5, during play",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(2, 0) },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness, RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(2, 0), L2_dwarf0Coord + new Vector2Int(3, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 2a4 - Create pursuing ritual rock effects part 6, during play",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarknessSpawnEffect },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(4, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 2a5 - Create pursuing ritual rock effects part 7, during play",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3.5f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(3, 0) },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf0Coord + new Vector2Int(3, 0) }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2 - 2a6 - Create pursuing ritual rock effects part 8, during play",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveRocks,
				TutorialStepComponentTag.CreateRocks
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3.5f < Time.time;
			}
			)
		{
			rocksToBeRemovedCoords = new List<Vector2Int>() { L2_dwarf1Coord + new Vector2Int(2, 0) },
			rocksToBeCreatedTypes = new List<RockTileType>() { RockTileType.RockRitualDarkness },
			rocksToBeCreatedCoords = new List<Vector2Int>() { L2_dwarf1Coord + new Vector2Int(2, 0) }
		},
		// TODO: This step sometimes has dwarves face oddly
		new TutorialStepEntity(
			gameLevel: 2, "L2-3 - Detect exiting and stop the dwarves",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return levelEntity.rockTiles[L2_coordBeforeExit.x, L2_coordBeforeExit.y] == null;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { L2_coordBeforeExit }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-3b - Discuss hunger",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return dwarfEntities[0].unitStateComponent.unitState == UnitState.Idle;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Hold on... so hungry...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-3c - Wait for user confirmation of text"),
				new TutorialStepEntity(
			gameLevel: 2, "L2-3d - Turn dwarves to face each other (hopefully?)",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return dwarfEntities[1].unitStateComponent.unitState == UnitState.Idle && gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.right, Vector2Int.left },
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-3d2 - Discuss hunger more",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Please, I have to eat !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "Aye, we could take a quick nibble.")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-3e - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 2, "L2-3f - Force exiting",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.HideAllDwarfUpgradeButtons,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int> { L2_coordBeforeExit + new Vector2Int(1, 0) },
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-4 - Gettan gives instructions",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"We got plenty o' stone to go around.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { null, null, null, 1 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = true,
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-4b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 2, "L2-4c - Reenable Gettan's upgrade button",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReshowDwarfUpgradeButtons,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			reshowDwarfUpgradeButtonDwarfIDs = new List<int>() { 1 }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-4d - Gettan gets upgrade, disable button",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.HideAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				if (dwarfEntities[1].unitStatsComponent.level > 2)
					throw new System.Exception("It's expected Gettan goes from level 1 to level 2 for the tutorial.");

				return dwarfEntities[1].unitStatsComponent.level == 2;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-4e - Gettan eats, dwarves discuss eating stone",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"*CRUNCH* Ahhh that hits the spot.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { null, null, null, 1 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = true,
				},
				new DialogueStepEntity(
					"...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.CustomSpeakerName
					}
					)
				{
					customSpeakerName = string.Empty,
					speakerNameIsOnRightSide = false,
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 0, "You just ate that? That stone?"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "Huh? Ya don't like the grey ones?"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Can't be picky now - have some !")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-4f - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 2, "L2-4g - Reenable Malgore's's upgrade button",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReshowDwarfUpgradeButtons,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			reshowDwarfUpgradeButtonDwarfIDs = new List<int>() { 0 }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-4h - Malgore gets upgrade, disable button",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.HideAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				if (dwarfEntities[0].unitStatsComponent.level > 2)
					throw new System.Exception("It's expected Malgore goes from level 1 to level 2 for the tutorial.");

				return dwarfEntities[0].unitStatsComponent.level == 2;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-4i - Maloge eats, dwarves discuss eating stone",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"*CRUNCH*",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("..."),
				DialogueStepEntity.ContinueConversationWithAnotherString("Stones are... delicious !"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "That's the spirit ! Go on, have the rest of 'em !")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-4z - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 2, "L2-3 - Reenable Malgore's upgrade button",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReshowDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			reshowDwarfUpgradeButtonDwarfIDs = new List<int>() { 0 }
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-3b - Empty step to detect enough upgrades performed to Maglore",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// TODO: Would have liked to check playerStateEntity here for running out of resources. But this will do.
				return dwarfEntities[0].unitStatsComponent.level >= 4;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 2, "L2-3c - Outro of dwarf upgade / eating rocks dialogue",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"*CRACK* *GRIND* . . . *burp*",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("I feel... GREAT !"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "That's great. Let's keep movin' !")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 2, "L2-3d - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 2, "2 - 4 - Reenable the upgrades continue button, letting the player finish the level",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,
				TutorialStepComponentTag.ReenableDwarfUpgradeContinueButton
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
		},

		/// --------------------------------------------
		/// Level 3
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 3, "3 - 1 - Start off next level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				// TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing, // TODO: This is implicit in creation of these levels. This divided "tutorial" and "level creation" will cause confusion
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 3 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(4.25f, 4f)
		},
		new TutorialStepEntity(
			gameLevel: 3, "L3 - 2 - Do a quick dialogue",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{

			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"So... what was that?",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 3, "I dunno... some kinda... swirly shadow."),
				DialogueStepEntity.ContinueConversationWithAnotherString("But did ya see those dwarves?"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Beardless dwarves !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("In purple robes !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("... let's keep goin'."),
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 3, "L3 - 2b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 3, "L3 - 2c - Return controls",
			new List<TutorialStepComponentTag>() {
			TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 3, "L3 - 3 - Detect dwarf ore cry buff starting or skip it",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return (dwarfEntities[0].buffReceiverComponent.buffEntities.Count > 0)
				|| gameStateEntity.indexToLoad >= 4 && !gameStateEntity.levelShouldBeLoaded; // A safety precaution
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 3, "L3 - 3b - Detect dwarf buff ended or level ending",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return (dwarfEntities[0].buffReceiverComponent.buffEntities.Count == 0 && dwarfEntities[1].buffReceiverComponent.buffEntities.Count == 0)
				|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 12.5f < Time.time // A safety precaution
				|| gameStateEntity.indexToLoad >= 4 && !gameStateEntity.levelShouldBeLoaded; // A safety precaution
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 3, "L3 - 3c - Do a dialogue regarding buff work OR alternately just mining",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// TODO: Determine that the user mined at all, instead of relying on these safeties, etc.
				return  gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time
				|| gameStateEntity.indexToLoad >= 4 && !gameStateEntity.levelShouldBeLoaded; // A safety precaution
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"That was... amazin' !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 0, null, null, 1 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = true,
					highlightSingleParticipantScreenID = 3
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 0, "Thanks ! Eating those stones was a great idea !")
			}
		},

		/// --------------------------------------------
		/// Level 4
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 4, "4 - 1 - Start off next level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 4 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(6.5f, 4f)
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 2 - Disable everything and inform user of early stopping point",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Thank you for playing ! There's more, but you can stop now - ",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.CustomSpeakerName,
					}
					)
				{
					customSpeakerName = "Note",
					speakerNameIsOnRightSide = false,
				},
				DialogueStepEntity.ContinueConversationWithAnotherString(" - and please provide feedback."),
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 2b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "4 - 2c - Re-enable things", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 4 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(5.5f, 4f)
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 4 - Encountering Tin",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L4_R1OffX + 10, L4_R1OffY + 2, 0), new Vector3Int(1,1,1)))
				|| LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(L4_R1OffX + 10, L4_R1OffY + 2, 0), new Vector3Int(1,1,1)));
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 10, L4_R1OffY + 2), new Vector2Int(L4_R1OffX + 11, L4_R1OffY + 2), new Vector2Int(L4_R1OffX + 11, L4_R1OffY + 1) },
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 4a2 - Detect dwarves arriving on spots",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AllDwarvesAreWithinBounds(dwarfEntities,
					new List<BoundsInt>()
					{
						new BoundsInt( new Vector3Int(L4_R1OffX + 10, L4_R1OffY + 1, 0), new Vector3Int(2, 2, 1))
					})
					|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 4a3 - Turn dwarves",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.up, Vector2Int.up }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 4c - Pan camera, discuss Tin",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,

				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(L4_R1OffX + 14, L4_R1OffY + 4, 0, 1, 1, 0),

			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Look ! It's Tin !" + System.Environment.NewLine + "A whole seam of Tin !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightSingleParticipant
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 1, 0 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightSingleParticipantScreenID = 0
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 1, "That's good right? We like Tin?"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 0, "Ah... usually. But tough as nails, it is..."),
				DialogueStepEntity.ContinueConversationWithAnotherString("Even with your big mining pick, we'd be digging for ages !")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 4d - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 4e - Return controls and loosen camera bounds",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,

				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(L4_R1OffX + 6, L4_R1OffY + 0, 0, 6, 10, 0),
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 5 - Have the smelting hammer dwarf speak",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.StopDwarfAttacksInPlaceAfterBackswing,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 8f < Time.time;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 },
			stopDwarfAttacksInPlaceAfterBackswingIDs = new List<int>() { 0, 1 },
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Hello.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.MeleeHammer },
					customSpeakerName = "???",
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				},
				new DialogueStepEntity(
					"I will try to Smelt this Tin.",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
			}
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 5b2 - Wait for user confirmation of but also force progress if lingering too long",
			new List<TutorialStepComponentTag>(),
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AssessDialogueEndAndResetFlag(gameStateEntity)
				|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 10f < Time.time; // This is a precaution, otherwise it may be possible for the first two dwarves to mine out everything if user doesn't confirm text
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 5b3 - Move dwarves in response to the dialogue",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 11, L4_R1OffY + 1), new Vector2Int(L4_R1OffX + 11, L4_R1OffY + 2) }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 5b3 - Check dwarves being in position",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return (
				LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L4_R1OffX + 11, L4_R1OffY + 1, 0), new Vector3Int(1,1,1)))
				|| LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L4_R1OffX + 11, L4_R1OffY + 2, 0), new Vector3Int(1,1,1)))
				)
				&& (
				LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(L4_R1OffX + 11, L4_R1OffY + 1, 0), new Vector3Int(1,1,1)))
				|| LogicHelpers.DwarfIsWithinBounds(dwarfEntities[1], new BoundsInt(new Vector3Int(L4_R1OffX + 11, L4_R1OffY + 2, 0), new Vector3Int(1,1,1)))
				)
				|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time; // NOTE: A safety measure in case a dwarf is in a long swing and never reaches his spot
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 5b4 - Turn dwarves, and attempt to stop attacking again just in case there's lingering",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
				TutorialStepComponentTag.StopDwarfAttacksInPlaceAfterBackswing
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.right, Vector2Int.right },
			stopDwarfAttacksInPlaceAfterBackswingIDs = new List<int>() { 0, 1 }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 5c - Create the smelting dwarf and move it in",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.CreateDwarves,
				// TutorialStepComponentTag.SetIdleDwarfFacingDirections, // TODO: This still doesn't solve Gonk facing the wrong direction when he spawns
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1 },

			dwarvesToBeCreatedPartyInsertionIndexes = new List<int>() { 0 },
			dwarvesToBeCreatedTypes = new List<DwarfType>() { DwarfType.MeleeHammer },
			dwarvesToBeCreatedCoords = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 13, L4_R1OffY + 0) },
			dwarvesToBeCreatedNames = new List<string>() { "Gonk" },
			dwarvesToBeCreatedExitLevelRequirement = new List<bool>() { true },
			dwarvesToBeCreatedBelongingInPlayersPartisanship = new List<bool>() { true },

			// idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0 },
			// idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.up },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 13, L4_R1OffY + 2) }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 6 - Smelting dwarf says more things",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[0], new BoundsInt(new Vector3Int(L4_R1OffX + 13, L4_R1OffY + 2, 0), new Vector3Int(1,1,1)));
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Hm. Yes.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 1, null, 0 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = true,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, false }
				},
				new DialogueStepEntity(
					"I can Smelt this.",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 6b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 6c - Turn off smelting, and have hammer dwarf starts attacking a vein",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ChangeTutorialSmeltControlSetting,
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return true;
			}
			)
		{
			tutorialSmeltControlSettingToBeChangedTo = TutorialSmeltControlSetting.PreventSmelting,
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 14, L4_R1OffY + 2), new Vector2Int(L4_R1OffX + 14, L4_R1OffY + 3) }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 6d - Dwarves comment lack of performance",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 12f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"(Hey look ! He's... helping...?)",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 1 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, null }
				},
				new DialogueStepEntity(
					"(He's lost his marble, I'd say...)",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { true, false }
				},
			}
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 6e - Stop hammer dwarf attacking during the conversation",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.StopDwarfAttacksInPlaceAfterBackswing
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			stopDwarfAttacksInPlaceAfterBackswingIDs = new List<int>() { 0 }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 6e2 - Stop hammer dwarf attacking during the conversation step 2",
			new List<TutorialStepComponentTag>() {
				 TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				 TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 0 },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 13, L4_R1OffY + 2) }
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 6f - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 6g - Have the hammer dwarf speak",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"I see now.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { null, null, 0 },
					speakerNamingID = 0,
					speakerNameIsOnRightSide = false,
				},
				new DialogueStepEntity(
					"I must hit it... here.",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 6h - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 7 Resume attacking, now with smelting guaranteed",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ChangeTutorialSmeltControlSetting,
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			tutorialSmeltControlSettingToBeChangedTo = TutorialSmeltControlSetting.AlwaysSmeltIfSmeltable,
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 14, L4_R1OffY + 2), new Vector2Int(L4_R1OffX + 14, L4_R1OffY + 3) }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 8 - Discuss being fascinated with smelting happening, and disable smelt, to show variation in success",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
				TutorialStepComponentTag.ChangeTutorialSmeltControlSetting
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 10f < Time.time;
			}
			)
		{
			tutorialSmeltControlSettingToBeChangedTo = TutorialSmeltControlSetting.PreventSmelting,

			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Whoa what was that?",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 1 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				},
				new DialogueStepEntity(
					"Yer hammer hit the rock, and, and - ",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 1 },
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { true, false }
				},
			}
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 8b - Stop hammer dwarf attacking during the conversation and walk him to the other dwarves",
			new List<TutorialStepComponentTag>() {
				 TutorialStepComponentTag.ClearManualCoordinates,
				 TutorialStepComponentTag.StopDwarfAttacksInPlaceAfterBackswing,
				 TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			stopDwarfAttacksInPlaceAfterBackswingIDs = new List<int>() { 2 },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 12, L4_R1OffY + 2) }
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 8c - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 9 - Gonk discusses with dwarves",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.75f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"- and, and a perfect bar of Tin popped out !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 2, 1, null, 0 },
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, false, null, false }
				},
				new DialogueStepEntity(
					"How are ya doing that !?",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
				new DialogueStepEntity(
					"Hm? You want the Tin bars?",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					speakerNamingID = 0,
					speakerNameIsOnRightSide = true,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, null, null, true }
				},
				new DialogueStepEntity(
					"Fair, you found the Tin.",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
				new DialogueStepEntity(
					"Here.",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
				new DialogueStepEntity(
					"Ouch. Still hot...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, true, null, false }
				},
				new DialogueStepEntity("Thank you.", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity(
					"We say:  \"He who finds some, woudln't mind some.\"",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					speakerNamingID = 0,
					speakerNameIsOnRightSide = true,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, false, null, true }
				},
				new DialogueStepEntity(
					"\"He who Smelts it, dealts it.\"",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
				new DialogueStepEntity(
					"...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					customSpeakerName = string.Empty, // TODO: Perhaps create a "NoSpeakerName" tag
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { true, true, null, null }
				},
				new DialogueStepEntity(
					"Hey... There's this... swirly, whirly shadow. It's chasin' us.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, false, null, false }
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("Do ya know a way out?"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 3, "Hm? A Shadow?"),
				DialogueStepEntity.ContinueConversationWithAnotherString("There is a passage ahead.")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 4, "L4 - 9b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 10 - Have hammer dwarf continue on",
			new List<TutorialStepComponentTag>() {
				 TutorialStepComponentTag.ClearManualCoordinates,
				 TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 12, L4_R1OffY + 3), new Vector2Int(L4_R1OffX + 13, L4_R1OffY + 3), new Vector2Int(L4_R1OffX + 14, L4_R1OffY + 3) }
		},
		new TutorialStepEntity(
			gameLevel: 4, "L4 - 10 - Clear the attack coordinates now that the hammer dwarf has probably started attacking, so as not to force Gerran and Maglore to move when unlocking their control",
			new List<TutorialStepComponentTag>() {
				 TutorialStepComponentTag.ClearManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 4, "4 - 10b - Resume control, place tutorial marker", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,
				TutorialStepComponentTag.ChangeTutorialSmeltControlSetting,
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText,
				TutorialStepComponentTag.RemoveFullFog
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			tutorialSmeltControlSettingToBeChangedTo = TutorialSmeltControlSetting.None,
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.TapInput,
			addedTutorialInputIndicatorPosition = new Vector2(L4_R1OffX + 13f, L4_R1OffY + 7.5f),
			addedTutorialInputIndicatorOptionalText = "Exit !",
			fullFogsToBeRemovedCoords = new List<Vector2Int>() { new Vector2Int(L4_R1OffX + 13, L4_R1OffY + 8) }
		},
		new TutorialStepEntity(
			gameLevel: 4, "4 - 10c - Remove tutorial marker", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 7.5f < Time.time;
			}
			)
		{
		},

		/// --------------------------------------------
		/// Level 5
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 5, "L5 - 1 - Start off next level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 5 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(5.5f, 4f)
		},

		/// --------------------------------------------
		/// Level 6
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 1 - Start off next level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 6 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(5.5f, 4f)
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 2 - Detect dwarves moving along", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return dwarfEntities[0].coord.y >= L6_R1OffY + 7
				|| dwarfEntities[1].coord.y >= L6_R1OffY + 7
				|| dwarfEntities[2].coord.y >= L6_R1OffY + 7;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 3 - Disable input and have the ranged dwarf speak",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Wait !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.RangedBasicProjectile },
					customSpeakerName = "???",
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false }
				},
				new DialogueStepEntity(
					"Help !",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
			}
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 3b - Move dwarves into position", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.AssignManualCoordinates,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L6_R1OffX + 2, L6_R1OffY + 6), new Vector2Int(L6_R1OffX + 3, L6_R1OffY + 6), new Vector2Int(L6_R1OffX + 3, L6_R1OffY + 5) },
		},
		//new TutorialStepEntity(
		//	gameLevel: 6, "L6 - 3 - Check dwarves being in position", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
		//	new List<TutorialStepComponentTag>() {
		//	},
		//	(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
		//	{
		//		return LogicHelpers.AllDwarvesAreWithinBounds(dwarfEntities,
		//			new List<BoundsInt>() {
		//				new BoundsInt( new Vector3Int(L6_R1OffX + 2, L6_R1OffY + 6, 0), new Vector3Int(1,1,1)),
		//				new BoundsInt( new Vector3Int(L6_R1OffX + 3, L6_R1OffY + 6, 0), new Vector3Int(1,1,1)),
		//				new BoundsInt( new Vector3Int(L6_R1OffX + 3, L6_R1OffY + 5, 0), new Vector3Int(1,1,1))
		//			}
		//			)
		//			|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time; // NOTE: A safety measure in case a dwarf is in a long swing and never reaches his spot
		//	}
		//	)
		//{
		//},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 6, "L6 - 3b3 - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 3b2 - Try to turn dwarves and move them",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L6_R1OffX + 2, L6_R1OffY + 6), new Vector2Int(L6_R1OffX + 3, L6_R1OffY + 6), new Vector2Int(L6_R1OffX + 3, L6_R1OffY + 5) },
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1, 2 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.down, Vector2Int.down, Vector2Int.down }
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 3c - Create the ranged dwarf and move it in",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.CreateDwarves,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections, // NOTE: Adding a lot of redundant turns because Gonk often takes too long to get to his spot
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1, 2 },

			dwarvesToBeCreatedPartyInsertionIndexes = new List<int>() { 3 },
			dwarvesToBeCreatedTypes = new List<DwarfType>() { DwarfType.RangedBasicProjectile },
			dwarvesToBeCreatedCoords = new List<Vector2Int>() { new Vector2Int(L6_R1OffX + 0, L6_R1OffY + 4) },
			dwarvesToBeCreatedNames = new List<string>() { "Zam" },
			dwarvesToBeCreatedExitLevelRequirement = new List<bool>() { true },
			dwarvesToBeCreatedBelongingInPlayersPartisanship = new List<bool>() { true },

			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.down },
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L6_R1OffX + 1, L6_R1OffX + 4) }
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 3d - Turn dwarves again just in case",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1, 2 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.down, Vector2Int.down, Vector2Int.down }
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 4 - Ranged dwarf and the dwarves talk",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections, // NOTE: Adding a lot of redundant turns because Gonk often takes too long to get to his spot
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				// TODO: This should just be time based, IMO. It's silly to snap the dialogue instantly
				return LogicHelpers.DwarfIsWithinBounds(dwarfEntities[3], new BoundsInt(new Vector3Int(L6_R1OffX + 1, L6_R1OffY + 4, 0), new Vector3Int(1,1,1)))
				|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.down },

			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"... finally someone. *Huff* *Huff*",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 3, null, null, 0, 1, 2 },
					speakerNamingID = 3,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, null, null, false, false, false }
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("... I've been wandering... these caves... for... *Huff*"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 3, "That is a short beard."),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 2, screenID: 5, "Aye, and yer robe is purple..."),
				DialogueStepEntity.ContinueConversationWithAnotherString("A suspicious shade of purple."),
				DialogueStepEntity.ContinueConversationWithAnotherString("Ya know anythin' about a swirly, twirly, whirly shadow?"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 3, screenID: 0, "The Sh-shadow? You've SEEN it?"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 2, screenID: 5, "Aye, and it's comin', so -"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 3, screenID: 0, "Oh no... their ritual worked..."),
				DialogueStepEntity.ContinueConversationWithAnotherString("Please ! There's a blocked-off bridge ahead !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("It might buy us some time."),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 3, "Yes. I will help."),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 2, screenID: 5, "Gonk..."),
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 6, "L6 - 4b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 6, "6 - 5 - Resume control, place tutorial marker", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,
				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			addedTutorialInputIndicatorType = TutorialInputIndicatorType.PressAndHoldInput,
			addedTutorialInputIndicatorPosition = new Vector2(L6_R1OffX + 7f, L6_R1OffY + 6.5f),
			addedTutorialInputIndicatorOptionalText = "Tap and hold, then look up !"
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 5b - Remove tutorial marker", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.CameraIsWithinBounds(mainCamera, new BoundsInt(L6_R1OffX + 0, L6_R1OffY + 9, 0, L6_Height, L6_Width, 0));
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 6, "L6 - 6 - Detect dwarves moving further along, and trigger a bit of dialogue", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return dwarfEntities[0].coord.x >= L6_R1OffX + 13
				|| dwarfEntities[1].coord.x >= L6_R1OffX + 13
				|| dwarfEntities[2].coord.x >= L6_R1OffX + 13
				|| dwarfEntities[3].coord.x >= L6_R1OffX + 13;
			}
			)
		{
			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(3, new List<string>() { "Thank you... you've saved me." })
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 6, "L6 - 6b - Wait for user confirmation of text"),

		/// --------------------------------------------
		/// Level 7
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 1 - Start off next level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,

				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.DisableDwarfFogSight,
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 7 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(5.5f, 4f),

			disableDwarfCandidateActionProcessingIDs = new List<int>() { 4 },
			disableDwarfFogSightIDs = new List<int>() { 4 },
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 4 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.right }
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 2 - Detect dwarves moving along...",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AtLeastKDwarvesAreWithinBounds(
					2,
					dwarfEntities,
					new List<BoundsInt>()
					{
						new BoundsInt( new Vector3Int(L7_R1OffX + 5, L7_R1OffY + 4, 0), new Vector3Int(4, 4, 1))
					});
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 2b - ...and disable coordinate input",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.AssignManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L7_R1OffX + 6, L7_R1OffY + 4), new Vector2Int(L7_R1OffX + 5, L7_R1OffY + 4), new Vector2Int(L7_R1OffX + 5, L7_R1OffY + 5), new Vector2Int(L7_R1OffX + 5, L7_R1OffY + 6) },
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 3 - Check dwarves being in position",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AllDwarvesAreWithinBounds(dwarfEntities,
					new List<BoundsInt>()
					{
						new BoundsInt( new Vector3Int(L7_R1OffX + 5, L7_R1OffY + 4, 0), new Vector3Int(4, 4, 1))
					})
					|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time; // NOTE: A safety measure in case a dwarf is in a long swing and never reaches his spot
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 3b - Turn the chain dwarf",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 4 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.left }
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 3b2 - Have the chain dwarf speak",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Hey !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { null, null, null, null, null, DwarfType.RangedChain },
					customSpeakerName = "???",
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, null, null, null, null, false }
				},
				new DialogueStepEntity(
					"Over here !",
					new List<DialogueStepComponentTag>() {
					}
					)
				{
				},
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 7, "L7 - 3b3 - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 3b2 - Turn dwarves",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetIdleDwarfFacingDirections
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			idleDwarvesToHaveFacingDirectionSetIDs = new List<int>() { 0, 1, 2, 3 },
			idleDwarfFacingDirectionNormalizedGridAlignedDirections = new List<Vector2Int>() { Vector2Int.right, Vector2Int.right, Vector2Int.right, Vector2Int.right }
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 3c - Move chain dwarf left",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DisableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing,
				TutorialStepComponentTag.AssignManualCoordinates
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
			disableDwarfCandidateActionProcessingIDs = new List<int>() { 0, 1, 2, 3 },
			reenableDwarfCandidateActionProcessingIDs = new List<int>() { 4 },

			assignedManualCoordinates = new List<Vector2Int>() { new Vector2Int(L7_R1OffX + 7, L7_R1OffX + 6) }
		},
		new TutorialStepEntity(
			gameLevel: 7, "L7 - 4 - Chain dwarf and the dwarves talk",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Zim !",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 1, 2, 3, null, null, 4 },
					speakerNamingID = 3,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, false, null, null, null, false }
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 4, screenID: 5, "Brother? Where've ya been !"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 3, screenID: 2, "Well those dwarves in robes, I... uhhh - "),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 4, screenID: 5, "That's great ! Anyway, I found Skysilver !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("A giant lode of Skysilver, Zam !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Zam !" + System.Environment.NewLine + "Newest friends of Zam !"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Let's mine Skysilver !"),
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 7, "L7 - 4b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 7, "7 - 5 - Resume control and reveal some fog over Skysilver",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,
				TutorialStepComponentTag.ReenableAllDwarfFogSight,
				TutorialStepComponentTag.RemoveFullFog
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			fullFogsToBeRemovedCoords = new List<Vector2Int>() { new Vector2Int(L7_R1OffX + 10, L7_R1OffY + 5), new Vector2Int(L7_R1OffX + 10, L7_R1OffY + 6), new Vector2Int(L7_R1OffX + 10, L7_R1OffY + 7) }
		},

		/// --------------------------------------------
		/// Level 8
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 1 - Start off next level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 8 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(5.5f, 4f)
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 2 - Check dwarves being in position",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AtLeastKDwarvesAreWithinBounds(
					3,
					dwarfEntities,
					new List<BoundsInt>()
					{
						new BoundsInt( new Vector3Int(L8_R1OffX + 2, L8_R1OffY + 1, 0), new Vector3Int(7, 6, 1))
					})
					|| gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 10f < Time.time; // NOTE: A safety measure
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 3 - Disable controls and discuss the extent of Skysilver found",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.DialogueSteps,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"Aye... that's a lode of Skysilver alright.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 1, 0, 2, 3, 4 },
					speakerNamingID = 2,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, false, null, false, false }
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 4, screenID: 4, "See? I knew it !"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 0, screenID: 1, "I will Smelt it.")
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 8, "L8 - 3b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 7c - Reenable controls",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 4 - Check egg chamber being found",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return levelEntity.fullFogTiles[L8_eggChamberOpenCoord.x, L8_eggChamberOpenCoord.y] == null;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 5 - Disable controls and discuss the egg chamber seen",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.HideOpenDwarfUpgradesButton,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"What is that?",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 1, 0, 2, 3, 4 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, false, false, false, false }
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 2, screenID: 2, "Everybody ! It's a Dwarf Egg!")
			}
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 5b - Pan the camera",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 3f < Time.time;
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(L8_eggChamberOpenCoord.x + 2, L8_eggChamberOpenCoord.y - 2, 0, 1, 1, 0),
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 5b2 - Expand the tutorial camera bounds to return control but keep position",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1f < Time.time;
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(L8_R1OffX + 0, L8_R1OffY + 0, 0, L8_Width, L8_Height, 0),
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 8, "L8 - 5c - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 6 - Discuss the egg chamber further",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"When I woke up... I saw this...",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromIDs,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantPortraitIDsForScreenIDs = new List<int?>() { 1, 0, 2, 3, 4 },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, false, false, false, false }
				},
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 2, screenID: 2, "Oh."),
				DialogueStepEntity.ContinueConversationWithAnotherString("Ya shoulda mentioned yer a hatcher!"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 1, screenID: 0, "..."),
				DialogueStepEntity.ContinueConversationWithAnotherString("Let's dig it out ! There must be another dwarf inside !"),
				DialogueStepEntity.HighlightSpeakerAndHaveThemSaySomething(dwarfID: 4, screenID: 4, "Yea - and let's \"remove\" all this Skysilver !"),
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 8, "L8 - 6b - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 7c - Reenable controls",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.5f < Time.time;
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 7 - Check dwarves being in position",
			new List<TutorialStepComponentTag>() {
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AtLeastKDwarvesAreWithinBounds(
					1,
					dwarfEntities,
					new List<BoundsInt>()
					{
						new BoundsInt( new Vector3Int(L8_eggChamberOpenCoord.x - 1, L8_eggChamberOpenCoord.y, 0), new Vector3Int(3, 1, 1)),
						new BoundsInt( new Vector3Int(L8_eggChamberOpenCoord.x, L8_eggChamberOpenCoord.y - 1, 0), new Vector3Int(1, 3, 1)),
					});
			}
			)
		{
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 8 - Disable movement, stop attacks, the egg chamber speaks",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.ClearManualCoordinates,
				TutorialStepComponentTag.DisableManualCoordinateInput,
				TutorialStepComponentTag.DisableCameraInput,
				TutorialStepComponentTag.DialogueSteps,
				TutorialStepComponentTag.StopDwarfAttacksInPlaceAfterBackswing
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 2f < Time.time;
			}
			)
		{
			stopDwarfAttacksInPlaceAfterBackswingIDs = new List<int>() { 0, 1, 2, 3, 4 },

			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"It's... humming.",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes,
						DialogueStepComponentTag.SpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.MeleeCleave, DwarfType.MeleeHammer, DwarfType.MeleeSpear, DwarfType.EggChamberClosed },
					speakerNamingID = 1,
					speakerNameIsOnRightSide = false,
					highlightedParticipantWithScreenIDs = new List<bool?>() { null, false, false, false }
				},
				new DialogueStepEntity(
					"Proximity sensor: KIN DETECTED",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.CustomSpeakerName,
						DialogueStepComponentTag.HighlightedParticipants
					}
					)
				{
					customSpeakerName = "Egg",
					speakerNameIsOnRightSide = true,
					highlightedParticipantWithScreenIDs = new List<bool?>() { false, null, null, true }
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("Chamber CU: ASSESSING"),
				DialogueStepEntity.ContinueConversationWithAnotherString("--- PROTOCOL-D SUMMARY ---"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Major tissue reassignment: 89%" + System.Environment.NewLine + "High-G endoskeletal remodel: 94%"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Epithelial grain rescale: 71%" + System.Environment.NewLine + "(WARNING - minor SiO2 intolerance)"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Neocortical re-trim: 81%"),
				DialogueStepEntity.ContinueConversationWithAnotherString("Bioforming outcome: ACCEPTABLE"),
				new DialogueStepEntity("...", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity("Umbilicals: DISENGING", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity("Umbilicals: CLEAR", new List<DialogueStepComponentTag>()),
				new DialogueStepEntity(
					"Subject ejection: COMPLETE",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes
					}
					)
				{
					participantDwarfTypesForScreenIDs = new List<DwarfType?>() { DwarfType.MeleeCleave, DwarfType.MeleeHammer, DwarfType.MeleeSpear, DwarfType.EggChamberOpen },
				}
			}
		},
		TutorialStepEntity.EndOfDialogueTutorialStep(gameLevel: 8, "L8 - 8 - Wait for user confirmation of text"),
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 9 - Begin shaking the camera",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 1.5f < Time.time;
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(L8_R1OffX + 0, L8_R1OffY - 500, 0, 100, 100, 0),
		},
		new TutorialStepEntity(
			gameLevel: 8, "L8 - 9b - Pan the camera up, end the first playable",
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera,
				TutorialStepComponentTag.DialogueSteps,
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.tutorialsStateComponent.endTimeofPreviousTutorialStep + 0.0625f < Time.time;
			}
			)
		{
			tutorialCameraBounds = new BoundsInt(L8_R1OffX + 0, L8_R1OffY + 500, 0, 100, 100, 0),
			dialogueStepEntities = new List<DialogueStepEntity>()
			{
				new DialogueStepEntity(
					"THANK YOU FOR PLAYING",
					new List<DialogueStepComponentTag>() {
						DialogueStepComponentTag.CustomSpeakerName,
					}
					)
				{
					customSpeakerName = "Note",
					speakerNameIsOnRightSide = false,
				},
				DialogueStepEntity.ContinueConversationWithAnotherString("Please provide feedback." + System.Environment.NewLine + "You may turn the game off."),
			}
		},

		/// --------------------------------------------
		/// Test Level 100
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 100, "100 - 1 - Start off testing level", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,

				TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 100 && !gameStateEntity.levelShouldBeLoaded;
			}
			)
		{
			teleportCameraPosition = new Vector2(7.5f, 4f),

			addedTutorialInputIndicatorType = TutorialInputIndicatorType.TapAndDragInput,
			addedTutorialInputIndicatorPosition = new Vector2(7.5f, 4.5f),
			addedTutorialInputIndicatorOptionalText = "This is a test!"
		},
		new TutorialStepEntity(
			gameLevel: 100, "100 - 2 - Remove tutorial input", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return levelEntity.sharedManualCoordinates.Count > 0;
			}
			)
		{
		},

		/// --------------------------------------------
		/// Test Level 103
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 103, "103 - 1 - Start off testing level, with dialogue", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons,

				TutorialStepComponentTag.DialogueSteps
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 103;
			}
			)
		{
			teleportCameraPosition = new Vector2(7.5f, 4f),

			dialogueStepEntities = DialogueStepEntity.SingleDwarfSpeaking(dwarfID: 0, new List<string>() { "Test", "Test 2" })
		},

		/// --------------------------------------------
		/// Test Level 104
		/// --------------------------------------------
		new TutorialStepEntity(
			gameLevel: 104, "104 - 1 - Start off testing level, with dialogue", // TODO: Maybe there needs to exist a "go to normal" tag, but the camera position is a little tricky, you can't know it ahead of time. Guess you can input the coords
			new List<TutorialStepComponentTag>() {
				TutorialStepComponentTag.RenableNonExitInput,
				TutorialStepComponentTag.RemoveTutorialCameraBounds,
				TutorialStepComponentTag.TeleportCamera,
				TutorialStepComponentTag.EnableCameraInput,
				TutorialStepComponentTag.EnableManualCoordinateInput,
				TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing,
				TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
				TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton,
				TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons
			},
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return gameStateEntity.indexToLoad >= 104;
			}
			)
		{
			teleportCameraPosition = new Vector2(7.5f, 4f),
		},
	};

	private static List<Vector2Int> TutorialLevel0R1FogClearingCoordinates(Vector2Int centerCoord)
	{
		List<Vector2Int> ret = new List<Vector2Int>();

		for (int x = -1; x <= 1; x++)
			for (int y = -1; y <= 1; y++)
				ret.Add(centerCoord + new Vector2Int(x, y));

		return ret;
	}

	private static List<Vector2Int> TutorialR2FogClearingCoordinates()
	{
		List<Vector2Int> ret = new List<Vector2Int>();

		for (int x = R4offX + 4; x <= R4offX + 5; x++)
			for (int y = R4offY + 4; y <= R4offY + R4Height; y++)
				ret.Add(new Vector2Int(x, y));

		return ret;
	}

	private static List<Vector2Int> TutorialR4FogClearingCoordinatesPart1()
	{
		List<Vector2Int> ret = new List<Vector2Int>();

		for (int x = R4offX + 4; x <= R4offX + 5; x++)
			for (int y = R4offY + 4; y <= R4offY + R4Height; y++)
				ret.Add(new Vector2Int(x, y));

		return ret;
	}

	private static List<Vector2Int> TutorialR4FogClearingCoordinatesPart2()
	{
		List<Vector2Int> ret = new List<Vector2Int>();

		for (int x = R4offX + 3; x <= R4offX + 3; x++)
			for (int y = R4offY + 2; y <= R4offY + R4Height; y++)
				ret.Add(new Vector2Int(x, y));

		return ret;
	}

	private static List<Vector2Int> TutorialR4FogClearingCoordinatesPart3()
	{
		List<Vector2Int> ret = new List<Vector2Int>();

		for (int x = R4offX; x <= R4offX + R4Width; x++)
			for (int y = R4offY; y <= R4offY + R4Height; y++)
				ret.Add(new Vector2Int(x, y));

		return ret;
	}
}
