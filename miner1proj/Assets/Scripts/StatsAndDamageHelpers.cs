﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageOutcomeType
{
	BasicDamage,
	CritDamage,
	Smelt,
	ImmuneNoDamage,
	ToughnessArmorNoDamage
}

public class AttackDamageInstance
{
	public AttackEntity ownerAttackEntity;
	public RockTileEntity damageRockTileEntity;
}

public class DamageOutcome
{
	public DamageOutcomeType damageOutcomeType;
	public double damage;
	public bool canCauseProcs; // TODO: This is redundnat with  triggerRequiredDamageOutcomeTypeConstraints and the other constraints
}

public static class StatsAndDamageHelpers
{
	public static DamageOrBuffTextType DamageOrBuffTextTypeFromDamageOutcomeType(DamageOutcomeType damageOutcomeType)
	{
		switch (damageOutcomeType)
		{
			case DamageOutcomeType.BasicDamage:
				return DamageOrBuffTextType.BasicDamage;
			case DamageOutcomeType.CritDamage:
				return DamageOrBuffTextType.CritDamage;
			case DamageOutcomeType.Smelt:
				return DamageOrBuffTextType.Smelt;
			case DamageOutcomeType.ImmuneNoDamage:
				return DamageOrBuffTextType.ImmuneNoDamage;
			case DamageOutcomeType.ToughnessArmorNoDamage:
				return DamageOrBuffTextType.ToughnessArmorNoDamage;
			default:
				throw new System.Exception("Unexpected DamageOutcomeType in DamageOrBuffTextType converter function.");
		}
	}

	// TODO: Wanted this to be floor. But adding 0.01 crit and flooring it caused it to go to 0 with floor at 2 precision. 0.00999999 * 100 -> 0.99999 - floor -> 0
	// Best fix may be to chance % chance effects to be stored as * 1000, thus allowing for natural 1 decimal precision in storage. So 12.3% is nicely represented as 123.
	public static double RoundWithPrecision(double val, int decimalPlaces)
	{
		return System.Math.Round(val, decimalPlaces, System.MidpointRounding.ToEven);
		//double power = System.Math.Pow(10, decimalPlaces);
		//return System.Math.Floor(val * power) / power;
	}

	public static float RoundWithPrecisionFloatingMultiple(float val, float multiple)
	{
		return Mathf.Round(val / multiple) * multiple;
	}

	public static bool CanAffordUpgradeCost(UnitStatsComponent unitStatsComponent, PlayerResourcesComponent playerResourcesComponent)
	{
		return unitStatsComponent.finalStats.upgradeCost <= playerResourcesComponent.resources[ResourceType.Stone].amount;
	}

	// TODO: The usage of this function made me waste 10-20 minutes. It was not clear what was deleting stats that were not mentioned here because it can fail silently
	public static UnitStatsComponent.UnitStatSet RoundUnitStatSetWithAppropriatePrecisions(UnitStatsComponent.UnitStatSet unitStatSet)
	{
		return new UnitStatsComponent.UnitStatSet()
		{
			damageFactor = RoundWithPrecision(unitStatSet.damageFactor, ValueHolder.UNIT_STAT_DAMAGE_FACTOR_DECIMAL_PRECISION),
			critChance = RoundWithPrecision(unitStatSet.critChance, ValueHolder.UNIT_STAT_CRIT_CHANCE_DECIMAL_PRECISION),
			critBonusDamageFactor = RoundWithPrecision(unitStatSet.critBonusDamageFactor, ValueHolder.UNIT_STAT_CRIT_BONUS_DAMAGE_DECIMAL_PRECISION),
			smeltChance = RoundWithPrecision(unitStatSet.smeltChance, ValueHolder.UNIT_STAT_SMELT_CHANCE_DECIMAL_PRECISION),
			movementSpeed = unitStatSet.movementSpeed, // NOTE: Not flooring  this for now

			attackSpeedBonus = unitStatSet.attackSpeedBonus,
			damageAmpBonus = unitStatSet.damageAmpBonus,

			upgradeCost = RoundWithPrecision(unitStatSet.upgradeCost, ValueHolder.UNIT_STAT_UPGRADE_COST_DECIMAL_PRECISION),
		};
	}

	public static UnitStatsComponent.UnitStatSet DeltaBetweenTwoStatSetsRoundedWithPrecision(UnitStatsComponent.UnitStatSet setInitial, UnitStatsComponent.UnitStatSet setFinal)
	{
		return RoundUnitStatSetWithAppropriatePrecisions(new UnitStatsComponent.UnitStatSet()
		{
			damageFactor = setFinal.damageFactor - setInitial.damageFactor,
			critChance = setFinal.critChance - setInitial.critChance,
			critBonusDamageFactor = setFinal.critBonusDamageFactor - setInitial.critBonusDamageFactor,
			smeltChance = setFinal.smeltChance - setInitial.smeltChance,
			movementSpeed = setFinal.movementSpeed - setInitial.movementSpeed,

			upgradeCost = setFinal.upgradeCost - setInitial.upgradeCost
		});
	}
}
