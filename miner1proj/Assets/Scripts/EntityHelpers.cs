﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EntityHelpers
{
	public static void DictionerifyEditorArrays(
		FloorTileEntity[] editorFloorTilesPrefabs,
		ref Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs,

		RockTileEntity[] editorRockTilesPrefabs,
		ref Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs,

		DwarfEntity[] editorDwarfPrefabs,
		ref Dictionary<DwarfType, DwarfEntity> dwarfPrefabs,

		AttackEntity[] editorAttackPrefabs,
		ref Dictionary<AttackType, AttackEntity> attackPrefabs,

		PartialFogEntity[] editorPartialFogEntities,
		ref Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs,

		FullFogEntity[] editorFullFogEntities,
		ref Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs,

		DamageOrBuffTextEntity[] editorDamageOrBuffTextEntities,
		ref Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs,

		ResourceTypeSpriteData[] editorResourceSprites,
		ref Dictionary<ResourceType, Sprite> resourceSprites,

		RockDamageOrDeathAnimationEntity[] editorRockDeathAnimationEntities,
		ref Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDeathAnimationPrefabs,

		RockDamageOrDeathAnimationEntity[] editorRockDamagedStageAnimationEntities,
		ref Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationPrefabs,

		DwarfSpriteData[] editorDwarfSprites,
		ref Dictionary<DwarfType, DwarfSpriteData> dwarfSprites,

		BuffGiverAreaEntity[] editorBuffGiverAreaPrefabs,
		ref Dictionary<BuffGiverAreaType, BuffGiverAreaEntity> buffGiverAreaPrefabs,

		BuffVisualAnimationControllerData[] editorBuffVisualAnimationControllers,
		ref Dictionary<BuffType, BuffVisualAnimationControllerData> buffVisualAnimationControllers
		)
	{
		floorTilesPrefabs = new Dictionary<FloorTileType, FloorTileEntity>();
		foreach (var item in editorFloorTilesPrefabs)
		{
			floorTilesPrefabs.Add(item.floorTileType, item);
		}

		rockTilesPrefabs = new Dictionary<RockTileType, RockTileEntity>();
		foreach (var item in editorRockTilesPrefabs)
		{
			rockTilesPrefabs.Add(item.rockTileType, item);
		}

		dwarfPrefabs = new Dictionary<DwarfType, DwarfEntity>();
		foreach (var item in editorDwarfPrefabs)
		{
			dwarfPrefabs.Add(item.dwarfType, item);
		}

		attackPrefabs = new Dictionary<AttackType, AttackEntity>();
		foreach (var item in editorAttackPrefabs)
		{
			attackPrefabs.Add(item.attackType, item);
		}

		partialFogPrefabs = new Dictionary<FogShapeType, PartialFogEntity>();
		foreach (var item in editorPartialFogEntities)
		{
			partialFogPrefabs.Add(item.fogShapeType, item);
		}

		fullFogPrefabs = new Dictionary<FogShapeType, FullFogEntity>();
		foreach (var item in editorFullFogEntities)
		{
			fullFogPrefabs.Add(item.fogShapeType, item);
		}

		resourceSprites = new Dictionary<ResourceType, Sprite>();
		foreach (var item in editorResourceSprites)
		{
			resourceSprites.Add(item.resourceType, item.sprite);
		}

		damageOrBuffTextPrefabs = new Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity>();
		foreach (var item in editorDamageOrBuffTextEntities)
		{
			damageOrBuffTextPrefabs.Add(item.damageOrBuffTextType, item);
		}

		rockDeathAnimationPrefabs = new Dictionary<RockTileType, RockDamageOrDeathAnimationEntity>();
		foreach (var item in editorRockDeathAnimationEntities)
		{
			rockDeathAnimationPrefabs.Add(item.originRockTileType, item);
		}

		rockDamagedStageAnimationPrefabs = new Dictionary<RockTileType, RockDamageOrDeathAnimationEntity>();
		foreach (var item in editorRockDamagedStageAnimationEntities)
		{
			rockDamagedStageAnimationPrefabs.Add(item.originRockTileType, item);
		}

		dwarfSprites = new Dictionary<DwarfType, DwarfSpriteData>();
		foreach (var item in editorDwarfSprites)
		{
			dwarfSprites.Add(item.dwarfType, item);
		}

		buffGiverAreaPrefabs = new Dictionary<BuffGiverAreaType, BuffGiverAreaEntity>();
		foreach (var item in editorBuffGiverAreaPrefabs)
		{
			buffGiverAreaPrefabs.Add(item.buffGiverAreaType, item);
		}

		buffVisualAnimationControllers = new Dictionary<BuffType, BuffVisualAnimationControllerData>();
		foreach (var item in editorBuffVisualAnimationControllers)
		{
			buffVisualAnimationControllers.Add(item.buffType, item);
		}
	}

	public static void DictionerifyAdditionalEditorArrays(
		TutorialInputIndicatorAnimationControllerData[] editorTutorialInputIndicatorAnimationControllers,
		ref Dictionary<TutorialInputIndicatorType, TutorialInputIndicatorAnimationControllerData> tutorialInputIndicatorAnimationControllers
		)
	{
		tutorialInputIndicatorAnimationControllers = new Dictionary<TutorialInputIndicatorType, TutorialInputIndicatorAnimationControllerData>();
		foreach (var item in editorTutorialInputIndicatorAnimationControllers)
		{
			tutorialInputIndicatorAnimationControllers.Add(item.tutorialInputIndicatorType, item);
		}
	}

	public static void InitializeLevelEntity(LevelEntity levelEntity)
	{
		InitializeLevelEntity(levelEntity, -1, -1);
	}

	public static void InitializeLevelEntity(LevelEntity levelEntity, int xSize, int ySize)
	{
		levelEntity.xSize = xSize;
		levelEntity.ySize = ySize;

		// TODO: GC can be avoided here
		levelEntity.floorTiles = new FloorTileEntity[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.rockTiles = new RockTileEntity[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.partialFogTiles = new PartialFogEntity[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.fullFogTiles = new FullFogEntity[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.playerInputIndicators = new PlayerInputIndicator[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];

		levelEntity.reservedByAttackerTiles = new TileMovementStateComponent[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.reservedByFreeSpaceMoverTiles = new TileMovementStateComponent[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];

		levelEntity.sharedManualCoordinates = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT);
		levelEntity.isSharedManualCoordinate = new bool[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.sharedUnreachableNavArray = new NavCoord[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
		levelEntity.sharedFoundUnreachableEdges = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2);
		levelEntity.sharedConsideredUnreachableEdgeAlready = new bool[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
	}

	public static void ReinitializeLevel(LevelEntity levelEntity, int xSize, int ySize, List<HPBarEntity> HPBarEntities, List<BuffGiverAreaEntity> buffGiverAreaEntities)
	{
		levelEntity.xSize = xSize;
		levelEntity.ySize = ySize;

		LogicHelpers.EntityRemover(levelEntity.floorTiles);
		LogicHelpers.EntityRemover(levelEntity.rockTiles);
		LogicHelpers.EntityRemover(levelEntity.partialFogTiles);
		LogicHelpers.EntityRemover(levelEntity.fullFogTiles);
		LogicHelpers.EntityRemover(levelEntity.playerInputIndicators);

		LogicHelpers.EntityRemover(HPBarEntities);
		LogicHelpers.EntityRemover(buffGiverAreaEntities);

		LogicHelpers.ComponentRemoverSetNull(levelEntity.reservedByAttackerTiles);
		LogicHelpers.ComponentRemoverSetNull(levelEntity.reservedByFreeSpaceMoverTiles);

		levelEntity.sharedManualCoordinates.Clear();
		LogicHelpers.BoolClearer(levelEntity.isSharedManualCoordinate);
		LogicHelpers.ComponentRemoverSetNull(levelEntity.sharedUnreachableNavArray);
		levelEntity.sharedFoundUnreachableEdges.Clear();
		LogicHelpers.BoolClearer(levelEntity.sharedConsideredUnreachableEdgeAlready);
	}

	// TODO: Get rid of addFloors, and unify how the BasicTestLevel, TutorialLevels, and unit tests use this function
	public static void SetLevelEntityEdgesToIndestructibleRockTiles(LevelEntity levelEntity, Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs, GameObject floorObject, Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs, GameObject rockObject, bool addFloors = true)
	{
		Vector2Int coord;

		for (int x = 0; x < levelEntity.xSize; x++)
		{
			coord = new Vector2Int(x, 0);

			if (addFloors)
				EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, coord, levelEntity, rockTilesPrefabs, rockObject);

			coord = new Vector2Int(x, levelEntity.ySize - 1);

			if (addFloors)
				EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, coord, levelEntity, rockTilesPrefabs, rockObject);
		}

		for (int y = 1; y < levelEntity.ySize - 1; y++)
		{
			coord = new Vector2Int(0, y);

			if (addFloors)
				EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, coord, levelEntity, rockTilesPrefabs, rockObject);

			coord = new Vector2Int(levelEntity.xSize - 1, y);

			if (addFloors)
				EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddRock(RockTileType.IndestructibleWall, coord, levelEntity, rockTilesPrefabs, rockObject);
		}
	}

	public static void DestroyEverything(LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, List<AttackEntity> attackEntities, bool useImmediateForEditorTests = false)
	{
		// TODO: For levelEntity, maybe not needed to set these to null, might not be meaningful

		levelEntity.floorTiles = null;
		levelEntity.rockTiles = null;
		levelEntity.partialFogTiles = null;
		levelEntity.fullFogTiles = null;

		levelEntity.reservedByAttackerTiles = null;
		levelEntity.reservedByFreeSpaceMoverTiles = null;

		levelEntity = new LevelEntity();
		dwarfEntities = new List<DwarfEntity>();
		attackEntities = new List<AttackEntity>();

		DwarfEntity[] temp = Object.FindObjectsOfType<DwarfEntity>();
		foreach (var item in temp)
		{
			if (useImmediateForEditorTests)
				Object.DestroyImmediate(item.gameObject);
			else
				Object.Destroy(item.gameObject);
		}

		FloorTileEntity[] temp2 = Object.FindObjectsOfType<FloorTileEntity>();
		foreach (var item in temp2)
		{
			if (useImmediateForEditorTests)
				Object.DestroyImmediate(item.gameObject);
			else
				Object.Destroy(item.gameObject);
		}

		RockTileEntity[] temp3 = Object.FindObjectsOfType<RockTileEntity>();
		foreach (var item in temp3)
		{
			if (useImmediateForEditorTests)
				Object.DestroyImmediate(item.gameObject);
			else
				Object.Destroy(item.gameObject);
		}

		PartialFogEntity[] temp4 = Object.FindObjectsOfType<PartialFogEntity>();
		foreach (var item in temp4)
		{
			if (useImmediateForEditorTests)
				Object.DestroyImmediate(item.gameObject);
			else
				Object.Destroy(item.gameObject);
		}

		FullFogEntity[] temp5 = Object.FindObjectsOfType<FullFogEntity>();
		foreach (var item in temp5)
		{
			if (useImmediateForEditorTests)
				Object.DestroyImmediate(item.gameObject);
			else
				Object.Destroy(item.gameObject);
		}

		PlayerInputIndicator[] temp6 = Object.FindObjectsOfType<PlayerInputIndicator>();
		foreach (var item in temp6)
		{
			if (useImmediateForEditorTests)
				Object.DestroyImmediate(item.gameObject);
			else
				Object.Destroy(item.gameObject);
		}
	}

	public static void AddFloor(FloorTileType floorTileType, Vector2Int coord, LevelEntity levelEntity, Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs, GameObject floorObject)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		levelEntity.floorTiles[coord.x, coord.y] = GameObject.Instantiate<FloorTileEntity>(floorTilesPrefabs[floorTileType], pos, Quaternion.identity, floorObject.transform);
	}

	public static void RemoveFloor(Vector2Int coord, LevelEntity levelEntity)
	{
		if (levelEntity.floorTiles[coord.x, coord.y] == null)
			return;

		GameObject.Destroy(levelEntity.floorTiles[coord.x, coord.y].gameObject);

		levelEntity.floorTiles[coord.x, coord.y] = null; // TODO: determine if this is necessary
	}

	public static void AddRock(RockTileType rockTileType, Vector2Int coord, LevelEntity levelEntity, Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs, GameObject rockObject)
	{
		if (levelEntity.rockTiles[coord.x, coord.y])
			throw new System.Exception("Attempted to create rock over existing rock at: " + coord);

		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		RockTileEntity rockTileEntity = GameObject.Instantiate<RockTileEntity>(rockTilesPrefabs[rockTileType], pos, Quaternion.identity, rockObject.transform);

		rockTileEntity.coord = coord;
		rockTileEntity.HP = rockTileEntity.maxHP;
		rockTileEntity.HPBarEntity = null;

		rockTileEntity.rockDamagedStageComponent = new RockDamagedStageComponent()
		{
			rockDamagedStageSprites = rockTileEntity.editorRockDestructionStageSprites,
			percentualRockDamagedStageThresholds = new List<double>() { 0.666666d, 0.333333d}, // TODO: Once you decide on smite mechanic and DamagedStage threshold randomization, refactor this as appropriate.
			thresholdStage = 0
		};

		rockTileEntity.spriteRenderer.sprite = rockTileEntity.rockDamagedStageComponent.rockDamagedStageSprites[rockTileEntity.rockDamagedStageComponent.thresholdStage]; // TODO: Does this violate DRY? Setting in multiple places based on threshold

		rockTileEntity.buffGiverAreaCreatorComponent = new BuffGiverAreaCreatorComponent()
		{
			buffGiverAreasToBeCreated = new List<BuffGiverAreaType>(ValueHolder.MAXIMUM_BUFF_GIVER_AREA_CREATIONS_TO_STORE_PER_CREATOR)
		};

		rockTileEntity.buffReceiverComponent = new BuffReceiverComponent()
		{
			receiverUnitTypeTags = new HashSet<BuffReceiverComponent.ReceiverUnitTypeTag>() { BuffReceiverComponent.ReceiverUnitTypeTag.Node },
			receivedPrototypeBuffsThisFixedUpdate = new List<PrototypeBuff>(ValueHolder.MAXIMUM_BUFFS_TO_HAVE),
			buffEntities = new Dictionary<BuffType, BuffEntity>(ValueHolder.MAXIMUM_BUFFS_TO_HAVE),
			buffEntitiesListIsDirtyThisFixedUpdate = true,
		};

		levelEntity.rockTiles[coord.x, coord.y] = rockTileEntity;
	}

	public static void RemoveRock(Vector2Int coord, LevelEntity levelEntity)
	{
		if (levelEntity.rockTiles[coord.x, coord.y] == null)
			return;

		GameObject.Destroy(levelEntity.rockTiles[coord.x, coord.y].gameObject);

		levelEntity.rockTiles[coord.x, coord.y] = null; // TODO: determine if this is necessary
	}

	public static void AddRockDamageOrDeathAnimation(RockTileType rockTileType, Vector2Int coord, List<RockDamageOrDeathAnimationEntity> rockDamageOrDeathAnimationEntities, Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDamageOrDeathAnimationPrefabs, GameObject rockDamageOrDeathAnimationsObject)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		RockDamageOrDeathAnimationEntity rockDeathAnimationEntity = Object.Instantiate(rockDamageOrDeathAnimationPrefabs[rockTileType], pos, Quaternion.identity, rockDamageOrDeathAnimationsObject.transform);

		rockDeathAnimationEntity.dissipationStartTime = Time.time;

		rockDamageOrDeathAnimationEntities.Add(rockDeathAnimationEntity);
	}

	public static void AddPartialFog(FogShapeType fogShapeType, Vector2Int coord, LevelEntity levelEntity, Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs, GameObject partialFogObject)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		levelEntity.partialFogTiles[coord.x, coord.y] = GameObject.Instantiate<PartialFogEntity>(partialFogPrefabs[fogShapeType], pos, Quaternion.identity, partialFogObject.transform);
		levelEntity.partialFogTiles[coord.x, coord.y].coord = coord;
		levelEntity.partialFogTiles[coord.x, coord.y].isFading = false;
	}

	public static void AddFullFog(FogShapeType fogShapeType, Vector2Int coord, LevelEntity levelEntity, Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs, GameObject fullFogObject)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		levelEntity.fullFogTiles[coord.x, coord.y] = GameObject.Instantiate<FullFogEntity>(fullFogPrefabs[fogShapeType], pos, Quaternion.identity, fullFogObject.transform);
		levelEntity.fullFogTiles[coord.x, coord.y].coord = coord;
		levelEntity.fullFogTiles[coord.x, coord.y].isFading = false;
	}

	public static bool RemoveFullFog(Vector2Int coord, LevelEntity levelEntity)
	{
		FullFogEntity fullFogEntity = levelEntity.fullFogTiles[coord.x, coord.y];

		if (fullFogEntity != null)
		{
			GameObject.Destroy(fullFogEntity.gameObject);
			levelEntity.fullFogTiles[coord.x, coord.y] = null;

			return true;
		}

		return false;
	}

	// NOTE: This should not be used outside of this static class - instead add the player's input
	private static void AddPlayerInputIndicator(PlayerClickOrderType playerClickOrderType, Vector2Int coord, LevelEntity levelEntity, PlayerInputIndicator playerInputIndicatorPrefab, GameObject playerInputIndicatorObject)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		levelEntity.playerInputIndicators[coord.x, coord.y] = GameObject.Instantiate<PlayerInputIndicator>(playerInputIndicatorPrefab, pos, Quaternion.identity, playerInputIndicatorObject.transform);
		levelEntity.playerInputIndicators[coord.x, coord.y].coord = coord;
	}

	// NOTE: This should not be used outside of this static class - instead remove the player's input
	private static void RemovePlayerInputIndicator(Vector2Int coord, LevelEntity levelEntity)
	{
		if (levelEntity.playerInputIndicators[coord.x, coord.y] == null)
			return;

		GameObject.Destroy(levelEntity.playerInputIndicators[coord.x, coord.y].gameObject);

		levelEntity.playerInputIndicators[coord.x, coord.y] = null; // TODO: determine if this is necessary
	}

	public static void AddValidatedSharedManualCoordinateAndUpdateGameState(GameStateEntity gameStateEntity, PlayerClickOrderType playerClickOrderType, Vector2Int coord, LevelEntity levelEntity, PlayerInputIndicator playerInputIndicatorPrefab, GameObject playerInputIndicatorObject)
	{
		if (!LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			return;

		if (!levelEntity.isSharedManualCoordinate[coord.x, coord.y])
		{
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, coord, levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

			gameStateEntity.playerCoordinatesChanged = true;
		}
	}

	public static void AddValidatedSharedManualCoordinateWithoutIndicatorAndUpdateGameState(GameStateEntity gameStateEntity, PlayerClickOrderType playerClickOrderType, Vector2Int coord, LevelEntity levelEntity)
	{
		if (!LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			return;

		if (!levelEntity.isSharedManualCoordinate[coord.x, coord.y])
		{
			EntityHelpers.AddSharedManualCoordinate(PlayerClickOrderType.AttackNode, coord, levelEntity, null, null, false);

			gameStateEntity.playerCoordinatesChanged = true;
		}
	}

	// NOTE: At time of writing, only used for unit tests
	public static void AddSharedManualCoordinate(PlayerClickOrderType playerClickOrderType, Vector2Int coord, LevelEntity levelEntity, PlayerInputIndicator playerInputIndicatorPrefab, GameObject playerInputIndicatorObject, bool addIndicator = true)
	{
		levelEntity.isSharedManualCoordinate[coord.x, coord.y] = true;
		levelEntity.sharedManualCoordinates.Add(coord);

		if (addIndicator)
			EntityHelpers.AddPlayerInputIndicator(PlayerClickOrderType.AttackNode, coord, levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
	}

	public static bool RemoveSharedManualCoordinate(Vector2Int coord, LevelEntity levelEntity, int indexHint)
	{
		if (!levelEntity.isSharedManualCoordinate[coord.x, coord.y])
			return false;

		levelEntity.isSharedManualCoordinate[coord.x, coord.y] = false;

		levelEntity.sharedManualCoordinates.RemoveAt(indexHint);

		EntityHelpers.RemovePlayerInputIndicator(coord, levelEntity);

		return true;
	}

	public static bool RemoveSharedManualCoordinate(Vector2Int coord, LevelEntity levelEntity)
	{
		if (!levelEntity.isSharedManualCoordinate[coord.x, coord.y])
			return false;

		levelEntity.isSharedManualCoordinate[coord.x, coord.y] = false;

		bool removalOccurred = false;

		// TODO: This can be optimized by converting isSharedManualCoordinate to references back to sharedManualCoordinates... but no need for now
		for (int i = levelEntity.sharedManualCoordinates.Count - 1; i >= 0 && !removalOccurred; i--)
		{
			if (levelEntity.sharedManualCoordinates[i] == coord)
			{
				levelEntity.sharedManualCoordinates.RemoveAt(i);
				removalOccurred = true;
			}
		}

		EntityHelpers.RemovePlayerInputIndicator(coord, levelEntity);

		return true;
	}

	public static void RemoveSharedManualCoordinates(LevelEntity levelEntity)
	{
		foreach (var sharedManualCoordinate in levelEntity.sharedManualCoordinates)
		{
			EntityHelpers.RemovePlayerInputIndicator(sharedManualCoordinate, levelEntity);
		}

		levelEntity.sharedManualCoordinates.Clear();

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				levelEntity.isSharedManualCoordinate[x, y] = false;
			}
	}

	// TODO: Just tacked on the dwarfName at the end. Probably needs to be something else. This kept me from refactoring all the unit tests - and it's definitely not necessary information.
	// Dwarves should probably be created from prototypes in some more complex manner anyway
	public static void AddDwarf(DwarfType dwarfType, Vector2Int coord, List<DwarfEntity> dwarfEntities, Dictionary<DwarfType, DwarfEntity> dwarfPrefabs, GameObject dwarvesObject, string dwarfName = "", bool isRequiredToExitLevelToProceedToNextLevel = true, bool belongsInPlayersParty = true)
	{
		//Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		//DwarfEntity dwarfEntity = Object.Instantiate(dwarfPrefabs[dwarfType], pos, Quaternion.identity, dwarvesObject.transform);

		//InitializeDwarfEntity(dwarfEntity, dwarfName);

		//ResetDwarfStateForNextLevel(dwarfEntity, coord, isRequiredToExitLevelToProceedToNextLevel, belongsInPlayersParty);

		//dwarfEntities.Add(dwarfEntity);

		AddDwarfAtPartyIndex(dwarfEntities.Count, dwarfType, coord, dwarfEntities, dwarfPrefabs, dwarvesObject, dwarfName, isRequiredToExitLevelToProceedToNextLevel, belongsInPlayersParty);
	}

	// TODO: This is probably temporary
	public static void AddDwarfAtPartyIndex(int index, DwarfType dwarfType, Vector2Int coord, List<DwarfEntity> dwarfEntities, Dictionary<DwarfType, DwarfEntity> dwarfPrefabs, GameObject dwarvesObject, string dwarfName = "", bool isRequiredToExitLevelToProceedToNextLevel = true, bool belongsInPlayersParty = true)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		DwarfEntity dwarfEntity = Object.Instantiate(dwarfPrefabs[dwarfType], pos, Quaternion.identity, dwarvesObject.transform);

		InitializeDwarfEntity(dwarfEntity, dwarfName);

		ResetDwarfStateForNextLevel(dwarfEntity, coord, isRequiredToExitLevelToProceedToNextLevel, belongsInPlayersParty);

		dwarfEntities.Insert(index, dwarfEntity);
	}

	public static void InitializeDwarfEntity(DwarfEntity dwarfEntity, string dwarfName)
	{
		dwarfEntity.unitStateComponent = new UnitStateComponent();

		dwarfEntity.dwarfName = dwarfName;

		dwarfEntity.tileMovementStateComponent = new TileMovementStateComponent()
		{
			// NOTE: Speed for Dwarves is set based on the UpdateUnitStatsSystem

			navPath = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2)
		};

		dwarfEntity.candidateTileMovementStateComponent = new CandidateTileMovementStateComponent()
		{
			navPath = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2)
		};

		switch (dwarfEntity.dwarfType)
		{
			case DwarfType.MeleeSpear:
				dwarfEntity.unitAttackingStateComponent = new UnitAttackingStateComponent()
				{
					attackRotation = new List<AttackType>(ValueHolder.dwarfTypeAttackRotations[dwarfEntity.dwarfType]), // TODO: Dwarf information should not be stored this way, all over the place. It's temporary. NOTE: Copying to a new list just in case
					possibleAttackingPosition = dwarfEntity.possibleAttackingPosition,
					attackRange = dwarfEntity.baseAttackRange,
					baseAttackStatSet = new UnitAttackingStateComponent.AttackStatSet()
					{
						attackCreationPointTime = 0.5f,
						attackBackswingFinishedPointTime = 1.5f, // Ends as the hitting node frame would end, assuming 4 frames and a 2 second attack time
						attackCompletionTime = 2f,
					}
				};
				break;
			case DwarfType.MeleeHammer:
				dwarfEntity.unitAttackingStateComponent = new UnitAttackingStateComponent()
				{
					attackRotation = new List<AttackType>(ValueHolder.dwarfTypeAttackRotations[dwarfEntity.dwarfType]), // TODO: Dwarf information should not be stored this way, all over the place. It's temporary. NOTE: Copying to a new list just in case
					possibleAttackingPosition = dwarfEntity.possibleAttackingPosition,
					attackRange = dwarfEntity.baseAttackRange,
					baseAttackStatSet = new UnitAttackingStateComponent.AttackStatSet()
					{
						attackCreationPointTime = 1.5f,
						attackBackswingFinishedPointTime = 1.75f,
						attackCompletionTime = 2f,
					}
				};
				break;
			case DwarfType.RangedBasicProjectile:
				dwarfEntity.unitAttackingStateComponent = new UnitAttackingStateComponent()
				{
					attackRotation = new List<AttackType>(ValueHolder.dwarfTypeAttackRotations[dwarfEntity.dwarfType]), // TODO: Dwarf information should not be stored this way, all over the place. It's temporary. NOTE: Copying to a new list just in case
					possibleAttackingPosition = dwarfEntity.possibleAttackingPosition,
					attackRange = dwarfEntity.baseAttackRange,
					baseAttackStatSet = new UnitAttackingStateComponent.AttackStatSet()
					{
						attackCreationPointTime = 1f,
						attackBackswingFinishedPointTime = 1f, // TODO: If backswing time is the same as the complation time, the dwarf ignores orders
						attackCompletionTime = 1.5f,
					}
				};
				break;
			case DwarfType.MeleeCleave:
			case DwarfType.RangedChain:
			case DwarfType.Ritual:
			default:
				dwarfEntity.unitAttackingStateComponent = new UnitAttackingStateComponent()
				{
					attackRotation = new List<AttackType>(ValueHolder.dwarfTypeAttackRotations[dwarfEntity.dwarfType]), // TODO: Dwarf information should not be stored this way, all over the place. It's temporary. NOTE: Copying to a new list just in case
					possibleAttackingPosition = dwarfEntity.possibleAttackingPosition,
					attackRange = dwarfEntity.baseAttackRange,
					baseAttackStatSet = new UnitAttackingStateComponent.AttackStatSet()
					{
						attackCreationPointTime = 1f,
						attackBackswingFinishedPointTime = 1.5f, // Ends as the hitting node frame would end, assuming 4 frames and a 2 second attack time
						attackCompletionTime = 2f,
					}
				};
				break;
		}

		dwarfEntity.candidateUnitAttackingStateComponent = new CandidateUnitAttackingStateComponent();

		dwarfEntity.fogSightComponent = new FogSightComponent()
		{
			fogSightEnabled = true,
			partialFogSightRadius = 2, // TODO: Consider how this needs to be tied to attackRange, to prevent shooting into partial fog
			fullFogSightRadius = 3
		};

		dwarfEntity.navigationComponent = new NavigationComponent()
		{
			levelDataWasDirtied = false,

			navArray = new NavCoord[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES],
			unreachableNavArray = new NavCoord[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES],
			candidateNavCoords = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2),
			foundManualCoords = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2),
			foundMiningNodes = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2),
			foundFullFogs = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2),
			foundUnreachableEdges = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2),
			consideredMiningNodeAlready = new bool[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES],
			consideredFullFogAlready = new bool[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES],
			consideredUnreachableEdgeAlready = new bool[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES],
			reverseNavPathHolder = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT / 2),
			attackRangePattern = new List<Vector2Int>(ValueHolder.MAXIMUM_ATTACK_RANGE_PATTERN_TILES)
		};

		dwarfEntity.unitStatsComponent = new UnitStatsComponent()
		{
			baseStats = dwarfEntity.baseStats,
			growthStats = dwarfEntity.growthStats,

			level = 1,
			levelIsDirty = true
		};

		dwarfEntity.buffGiverAreaCreatorComponent = new BuffGiverAreaCreatorComponent()
		{
			buffGiverAreasToBeCreated = new List<BuffGiverAreaType>(ValueHolder.MAXIMUM_BUFF_GIVER_AREA_CREATIONS_TO_STORE_PER_CREATOR)
		};

		dwarfEntity.buffReceiverComponent = new BuffReceiverComponent()
		{
			receiverUnitTypeTags = new HashSet<BuffReceiverComponent.ReceiverUnitTypeTag>() { BuffReceiverComponent.ReceiverUnitTypeTag.Dwarf },
			receivedPrototypeBuffsThisFixedUpdate = new List<PrototypeBuff>(ValueHolder.MAXIMUM_BUFFS_TO_HAVE),
			buffEntities = new Dictionary<BuffType, BuffEntity>(ValueHolder.MAXIMUM_BUFFS_TO_HAVE),
			buffEntitiesListIsDirtyThisFixedUpdate = true,
		};

		dwarfEntity.procOnAttackComponent = new ProcOnAttackComponent()
		{
			highestLevelAttained = 0,
			procDataOnAttack = new List<ProcData>(ValueHolder.MAXIMUM_SIMULTANEOUS_PROC_TYPES)
		};
	}

	public static void ResetDwarfStateForNextLevel(DwarfEntity dwarfEntity, Vector2Int coord, bool isRequiredToExitLevelToProceedToNextLevel = true, bool belongsInPlayersParty = true)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);
		dwarfEntity.transform.position = pos;

		dwarfEntity.tutorialDisabledCandidateActionProcessing = false;
		dwarfEntity.hasExitedLevel = false;
		dwarfEntity.isRequiredToExitLevelToProceedToNextLevel = isRequiredToExitLevelToProceedToNextLevel;
		dwarfEntity.belongsInPlayersParty = belongsInPlayersParty;

		dwarfEntity.coord = coord;
		dwarfEntity.coordWasDirtiedThisFixedUpdate = true;

		dwarfEntity.unitStateComponent.ResetForNextLevel();

		dwarfEntity.tileMovementStateComponent.ResetForNextLevel();
		dwarfEntity.candidateTileMovementStateComponent.ResetForNextLevel();

		dwarfEntity.unitAttackingStateComponent.ResetForNextLevel();
		dwarfEntity.candidateUnitAttackingStateComponent.ResetForNextLevel();

		dwarfEntity.gameObject.SetActive(true);

		foreach (var procData in dwarfEntity.procOnAttackComponent.procDataOnAttack)
		{
			procData.isInternallyCoolingdown = false;
		}
	}

	// NOTE: Not currently used
	public static void RemoveDwarf(DwarfEntity dwarfEntity, List<DwarfEntity> dwarfEntities)
	{
		dwarfEntities.Remove(dwarfEntity);

		GameObject.Destroy(dwarfEntity.gameObject);
	}

	public static bool AttackRequiresTileMovementComponent(AttackType attackType)
	{
		switch (attackType)
		{
			case AttackType.Chain:
			case AttackType.BasicProjectile:
				return true;
			default:
				return false;
		}
	}

	public static bool AttackRequiresTileMovementRangeComponent(AttackType attackType)
	{
		switch (attackType)
		{
			case AttackType.Chain:
			case AttackType.BasicProjectile:
				return true;
			default:
				return false;
		}
	}

	public static bool AttackRequiresChainTileBouncingComponent(AttackType attackType)
	{
		switch (attackType)
		{
			case AttackType.Chain:
				return true;
			default:
				return false;
		}
	}

	public static void AddAttack(AttackType attackType, Vector2Int startCoord, Vector2Int targetCoord, DwarfEntity ownerDwarfEntity, List<AttackEntity> attackEntities, Dictionary<AttackType, AttackEntity> attackPrefabs, GameObject attacksObject)
	{
		Vector3 attackDirection = (LogicHelpers.PositionFromCoord(targetCoord) - LogicHelpers.PositionFromCoord(startCoord)).normalized;
		Vector3 finalAttackStartOffsetFromDwarf;
		switch (attackType)
		{
			case AttackType.Chain:
				finalAttackStartOffsetFromDwarf = attackPrefabs[attackType].attackStartOffsetFromDwarf * Vector3.up;
				break;
			default:
				finalAttackStartOffsetFromDwarf = attackPrefabs[attackType].attackStartOffsetFromDwarf * attackDirection;
				break;
		}

		Vector3 pos = LogicHelpers.PositionFromCoord(startCoord) + finalAttackStartOffsetFromDwarf;

		Quaternion attackDirectionQuaternion = Quaternion.LookRotation(Vector3.forward, attackDirection);

		AttackEntity attackEntity = Object.Instantiate(attackPrefabs[attackType], pos, attackDirectionQuaternion, attacksObject.transform);

		attackEntity.attackStateComponent = new AttackStateComponent()
		{
			attackState = AttackState.Moving,
			changedToBeClearedOnUpdate = true
		};

		attackEntity.coord = startCoord;
		attackEntity.targetCoord = targetCoord;
		attackEntity.owner = ownerDwarfEntity;
		attackEntity.primedToDealDamage = true;

		if (AttackRequiresTileMovementComponent(attackType))
		{
			attackEntity.tileMovementStateComponent = new TileMovementStateComponent()
			{
				// TODO: This will eventually have to be refactored when speed becomes more dynamic in the game. See the dwarf example with how this variable is set
				movementSpeed = attackEntity.baseMovementSpeed,
			};

			// TODO: Make this suck less, it's connected to the SometimesSetDirectionFromStateSystem
			switch (attackEntity.attackType)
			{
				case AttackType.BasicProjectile:
					Vector2Int gridDirectionToTarget = attackEntity.targetCoord - attackEntity.coord;

					if (!LogicHelpers.IsGridAligned(gridDirectionToTarget))
						throw new System.Exception("Attack that's typically grid aligned has a diagonal target.");

					attackEntity.tileMovementStateComponent.normalizedGridAlignedDirection = LogicHelpers.NormalizeGridAlignedDirection(gridDirectionToTarget);
					break;
				default:
					break;
			}

			attackEntity.tileMovementStateComponent.nextCoord = LogicHelpers.NextCoordForAttack(attackEntity); // TODO: this violates DRY
		}
		else
			attackEntity.tileMovementStateComponent = null;

		if (AttackRequiresTileMovementRangeComponent(attackType))
		{
			attackEntity.tileMovementRangeComponent = new TileMovementRangeComponent()
			{
				originCoord = startCoord,
				movementRange = attackEntity.baseMovementRange
			};
		}
		else
			attackEntity.tileMovementRangeComponent = null;

		if (AttackRequiresChainTileBouncingComponent(attackType))
		{
			attackEntity.chainTileBouncingComponent = new ChainTileBouncingComponent()
			{
				maxBounces = attackEntity.baseMaxBounces,
				repeatBounceTargetsAllowed = attackEntity.baseRepeatBounceTargetsAllowed,
				bounceRange = attackEntity.baseBounceRange,
				candidateBounceTargets = new List<Vector2Int>(ValueHolder.MAXIUM_CHAIN_TILE_BOUNCE_ANDIDATE_TARGETS),

				bounces = 0,
				bounceHistory = new List<Vector2Int>(ValueHolder.MAXIMUM_CHAIN_TILE_BOUNCE_HISTORY)
				
			};
		}
		else
			attackEntity.chainTileBouncingComponent = null;

		attackEntity.damageTileCoords = new List<DamageTileCoord>(ValueHolder.MAXIMUM_DAMAGE_TILES);

		attackEntities.Add(attackEntity);
	}


	public static void AddDamageText(DamageOutcomeType damageOrBuffTextType, double damage, float initialImpactfullness, Vector2Int sourceCoord, Vector2Int targetCoord, List<DamageOrBuffTextEntity> damageOrBuffTextEntities, Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs, GameObject damageOrBuffTexts)
	{
		Vector3 fromSourceDirection = LogicHelpers.PositionFromCoord(targetCoord) - LogicHelpers.PositionFromCoord(sourceCoord);
		DamageOrBuffTextEntity damageOrBuffTextEntityPrefab = damageOrBuffTextPrefabs[StatsAndDamageHelpers.DamageOrBuffTextTypeFromDamageOutcomeType(damageOrBuffTextType)];
		Vector3 pos = LogicHelpers.PositionFromCoord(targetCoord) + damageOrBuffTextEntityPrefab.damageOrBuffOffsetFromSource * fromSourceDirection;

		// Quaternion fromSourceDirectionQuaternion = Quaternion.LookRotation(Vector3.forward, fromSourceDirection);

		DamageOrBuffTextEntity damageOrBuffTextEntity = Object.Instantiate(damageOrBuffTextEntityPrefab, pos, Quaternion.identity, damageOrBuffTexts.transform);

		damageOrBuffTextEntity.initialImpactfullness = initialImpactfullness;
		damageOrBuffTextEntity.textComponent.fontSize = damageOrBuffTextEntity.baseFontSize * damageOrBuffTextEntity.initialImpactfullness;

		switch (damageOrBuffTextType)
		{
			case DamageOutcomeType.BasicDamage:
			case DamageOutcomeType.CritDamage:
			case DamageOutcomeType.ImmuneNoDamage:
			case DamageOutcomeType.ToughnessArmorNoDamage:
				damageOrBuffTextEntity.textComponent.text = damage.ToString();
				break;
			case DamageOutcomeType.Smelt:
				damageOrBuffTextEntity.textComponent.text = "SMELT!";
				break;
			default:
				throw new System.Exception("Unexpected damageOrBuffTextType.");
		}

		damageOrBuffTextEntity.isDissipating = true;
		damageOrBuffTextEntity.dissipationStartTime = Time.time;

		damageOrBuffTextEntity.screenMovementStateComponent = new ScreenMovementStateComponent()
		{
			speed = damageOrBuffTextEntity.baseScreenMovementSpeed,
			direction = Vector3.Normalize(2 * Vector3.up + fromSourceDirection), // TODO: an experiment between directional impact and making numbers appear to pop "upwards"
			lingerInPlaceDelay = damageOrBuffTextEntity.baseLingerInPlaceDelay,
			movementStartTime = Time.time
		};

		damageOrBuffTextEntities.Add(damageOrBuffTextEntity);
	}

	public static void AddBuffText(DamageOrBuffTextType damageOrBuffTextType, string buffText, Vector2Int coord, List<DamageOrBuffTextEntity> damageOrBuffTextEntities, Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs, GameObject damageOrBuffTexts)
	{
		Vector3 fromSourceDirection = Vector3.up;
		Vector3 pos = LogicHelpers.PositionFromCoord(coord) + damageOrBuffTextPrefabs[damageOrBuffTextType].damageOrBuffOffsetFromSource * fromSourceDirection;

		// Quaternion fromSourceDirectionQuaternion = Quaternion.LookRotation(Vector3.forward, fromSourceDirection);

		DamageOrBuffTextEntity damageOrBuffTextEntity = Object.Instantiate(damageOrBuffTextPrefabs[damageOrBuffTextType], pos, Quaternion.identity, damageOrBuffTexts.transform);

		damageOrBuffTextEntity.initialImpactfullness = 1f;
		damageOrBuffTextEntity.textComponent.fontSize = damageOrBuffTextEntity.baseFontSize * damageOrBuffTextEntity.initialImpactfullness;

		damageOrBuffTextEntity.textComponent.text = buffText.ToString();

		damageOrBuffTextEntity.isDissipating = true;
		damageOrBuffTextEntity.dissipationStartTime = Time.time;

		damageOrBuffTextEntity.screenMovementStateComponent = new ScreenMovementStateComponent()
		{
			speed = damageOrBuffTextEntity.baseScreenMovementSpeed,
			direction = Vector3.up,
			lingerInPlaceDelay = damageOrBuffTextEntity.baseLingerInPlaceDelay,
			movementStartTime = Time.time
		};

		damageOrBuffTextEntities.Add(damageOrBuffTextEntity);
	}

	public static void AddResourceGainText(ResourceType resourceType, double amount, float initialImpactfullness, Vector2Int coord, Vector3 dynamicOffest, List<ResourceGainTextEntity> resourceGainTextEntities, ResourceGainTextEntity resourceGainTextPrefab, Dictionary<ResourceType, Sprite> resourceSprites, GameObject resourceGainTexts)
	{
		Vector3 fromSourceDirection = Vector3.up;
		Vector3 pos = LogicHelpers.PositionFromCoord(coord) + resourceGainTextPrefab.resourceGainOffsetFromSource * fromSourceDirection + dynamicOffest;

		// Quaternion fromSourceDirectionQuaternion = Quaternion.LookRotation(Vector3.forward, fromSourceDirection);

		ResourceGainTextEntity resourceGainEntity = Object.Instantiate(resourceGainTextPrefab, pos, Quaternion.identity, resourceGainTexts.transform);

		resourceGainEntity.initialImpactfullness = initialImpactfullness;
		resourceGainEntity.textComponent.fontSize = resourceGainEntity.baseFontSize * initialImpactfullness;
		resourceGainEntity.textComponent.text = "+ " + amount.ToString();
		resourceGainEntity.spriteComponent.sprite = resourceSprites[resourceType];

		resourceGainEntity.isDissipating = true;
		resourceGainEntity.dissipationStartTime = Time.time;

		resourceGainEntity.screenMovementStateComponent = new ScreenMovementStateComponent()
		{
			speed = resourceGainEntity.baseScreenMovementSpeed,
			direction = Vector3.up,
			lingerInPlaceDelay = resourceGainEntity.baseLingerInPlaceDelay,
			movementStartTime = Time.time
		};

		resourceGainTextEntities.Add(resourceGainEntity);
	}

	public static void AddHPBar(RockTileEntity rockTileEntity, List<HPBarEntity> HPBarEntities, HPBarEntity HPBarEntityPrefab, GameObject unitProgressBars)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(rockTileEntity.coord);

		HPBarEntity HPBarEntity = Object.Instantiate(HPBarEntityPrefab, pos, Quaternion.identity, unitProgressBars.transform);

		HPBarEntities.Add(HPBarEntity);

		rockTileEntity.HPBarEntity = HPBarEntity;
	}

	public static void RemoveHPBar(HPBarEntity HPBarEntity, List<HPBarEntity> HPBarEntities)
	{
		for (int i = HPBarEntities.Count - 1; i >= 0; i--)
		{
			if (HPBarEntities[i] == HPBarEntity)
			{
				HPBarEntities.RemoveAt(i);
				GameObject.Destroy(HPBarEntity.gameObject);
				return;
			}
		}
	}

	public static void AddBuffGiverAreaEntity(BuffGiverAreaType buffGiverAreaType, Vector2Int coord, List<BuffGiverAreaEntity> buffGiverAreaEntities, Dictionary<BuffGiverAreaType, BuffGiverAreaEntity> buffGiverAreaPrefabs, GameObject buffGiverAreasObject)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);

		BuffGiverAreaEntity buffGiverAreaEntity = Object.Instantiate(buffGiverAreaPrefabs[buffGiverAreaType], pos, Quaternion.identity, buffGiverAreasObject.transform);

		InitializeBuffGiverAreaEntity(buffGiverAreaEntity);

		ResetBuffGiverAreaEntity(buffGiverAreaEntity, coord);

		buffGiverAreaEntities.Add(buffGiverAreaEntity);
	}

	public static void InitializeBuffGiverAreaEntity(BuffGiverAreaEntity buffGiverAreaEntity)
	{
		buffGiverAreaEntity.prototypeBuffTiles = new PrototypeBuff[ValueHolder.MAXIMUM_LEVEL_X_TILES, ValueHolder.MAXIMUM_LEVEL_Y_TILES];
	}

	public static void ResetBuffGiverAreaEntity(BuffGiverAreaEntity buffGiverAreaEntity, Vector2Int coord)
	{
		Vector3 pos = LogicHelpers.PositionFromCoord(coord);
		buffGiverAreaEntity.transform.position = pos;

		buffGiverAreaEntity.dissipationStartTime = Time.time;

		buffGiverAreaEntity.coord = coord;
		//dwarfEntity.coordWasDirtiedThisFixedUpdate = true;

		List<PrototypeBuffGiverAreaTileCoord> buffGiverAreaShapeTiles = ValueHolder.buffGiverAreaShapeTiles[buffGiverAreaEntity.buffGiverAreaType];

		LogicHelpers.ComponentRemoverSetNull(buffGiverAreaEntity.prototypeBuffTiles);

		Vector2Int placementCoord;
		foreach (var buffGiverAreaShapeTile in buffGiverAreaShapeTiles)
		{
			placementCoord = coord + buffGiverAreaShapeTile.relativeCoord;

			if (LogicHelpers.CoordIsInBounds(placementCoord, buffGiverAreaEntity.prototypeBuffTiles)) // TODO: It's likely this causes buffs to appear outside of level bounds
				buffGiverAreaEntity.prototypeBuffTiles[placementCoord.x, placementCoord.y] = new PrototypeBuff(buffGiverAreaShapeTile);
		}

		buffGiverAreaEntity.gameObject.SetActive(true);
	}

	public static void RemoveBuffGiverAreaEntity(BuffGiverAreaEntity buffGiverAreaEntity, List<BuffGiverAreaEntity> buffGiverAreaEntities)
	{
		buffGiverAreaEntities.Remove(buffGiverAreaEntity);

		GameObject.Destroy(buffGiverAreaEntity.gameObject);
	}

	public static void AddBuffEntity(PrototypeBuff receivedPrototypeBuff, Dictionary<BuffType, BuffEntity> buffEntities, Dictionary<BuffType, BuffEntity> buffPseudoprefabs)
	{
		BuffEntity buffPrefab = buffPseudoprefabs[receivedPrototypeBuff.buffType];

		BuffEntity buffEntity = new BuffEntity()
		{
			buffType = buffPrefab.buffType,
			// procBuffApplicationType = buffPrefab.procBuffApplicationType,
			baseStats = buffPrefab.baseStats,
			visualPriorityRating = buffPrefab.visualPriorityRating,

			isDissipating = buffPrefab.isDissipating,
			isCountingDownCharges = buffPrefab.isCountingDownCharges,
			isBuffGiverAreaDependent = buffPrefab.isBuffGiverAreaDependent,

			level = receivedPrototypeBuff.buffLevel,

			dissipationStartTime = Time.time,
		};

		buffEntity.finalStats = buffEntity.baseStats;

		buffEntity.buffAreaDependenceMaintainedThisFixedUpdate = true; // NOTE: Implicitly, if the buff is being added, the area is there
		buffEntity.currentCharges = buffEntity.finalStats.charges;

		buffEntities.Add(buffEntity.buffType, buffEntity);
	}

	public static void RemoveBuffEntity(BuffEntity buffEntity, Dictionary<BuffType, BuffEntity> buffEntities)
	{
		buffEntities.Remove(buffEntity.buffType);

		// TODO: Sanity check this, removal of buff entities is automatically processed by "ProcessBuffVisualEntity"
	}

	public static void RemoveBuffEntities(List<BuffEntity> buffEntitiesForRemoval, Dictionary<BuffType, BuffEntity> buffEntities)
	{
		foreach (var buffEntityForRemoval in buffEntitiesForRemoval)
		{
			buffEntities.Remove(buffEntityForRemoval.buffType);
		}

		// TODO: Sanity check this, removal of buff entities is automatically processed by "ProcessBuffVisualEntity"
	}

	//public static void RemoveBuffEntities(List<BuffEntity> buffEntitiesForRemovalToBeModified, Dictionary<BuffType, BuffEntity> buffEntities)
	//{
	//	for (int i = buffEntitiesForRemovalToBeModified.Count; i >= 0; i--)
	//	{
	//		buffEntities.Remove(buffEntitiesForRemovalToBeModified[i].buffType);

	//		buffEntitiesForRemovalToBeModified.RemoveAt(i);
	//	}

	//	// TODO: Sanity check this, removal of buff entities is automatically processed by "ProcessBuffVisualEntity"
	//}
}
