﻿
using UnityEngine;
using TMPro;

/// <summary>
/// From: https://forum.unity.com/threads/does-the-content-size-fitter-work.484678/
/// By Antistone and then Akrone?
/// No licenses mentioned
/// https://forum.unity.com/threads/license-on-code-in-forum-posts.714107/
/// https://en.wikipedia.org/wiki/Implied_license
/// </summary>

// NOTE: Made improvements to actually respect padding and to be pixle perfect

// TODO: This is a very rare case of the code in this project using Update(). Consider getting that under control.

// TODO: Improve this code to allow for more consistent results in "Both" mode. It jitters sometimes when an extra character is added, making the text go into 2 row mode instead of going wider. Text that's being typed should never go back to being 1 row after it becomes 2 rows. Etc.

[ExecuteInEditMode]
public class TextSizer : MonoBehaviour
{
	public TMP_Text Text;
	public bool ResizeTextObject = true;
	public Vector2 Padding;
	public Vector2 MaxSize = new Vector2(1000, float.PositiveInfinity);
	public Vector2 MinSize;
	public Mode ControlAxes = Mode.Both;

	// NOTE: Additions
	public bool bePixelPerfect;
	public float pixelsPerUnitInBothDimensions = 1f;

	[System.Flags]
	public enum Mode
	{
		None = 0,
		Horizontal = 0x1,
		Vertical = 0x2,
		Both = Horizontal | Vertical
	}

	private string _lastText;
	private Mode _lastControlAxes = Mode.None;
	private Vector2 _lastSize;
	private bool _forceRefresh;
	private bool _isTextNull = true;
	private RectTransform _textRectTransform;
	private RectTransform _selfRectTransform;

	protected virtual float MinX
	{
		get
		{
			if ((ControlAxes & Mode.Horizontal) != 0) return MinSize.x;
			return _selfRectTransform.rect.width - Padding.x;
		}
	}
	protected virtual float MinY
	{
		get
		{
			if ((ControlAxes & Mode.Vertical) != 0) return MinSize.y;
			return _selfRectTransform.rect.height - Padding.y;
		}
	}
	protected virtual float MaxX
	{
		get
		{
			if ((ControlAxes & Mode.Horizontal) != 0) return MaxSize.x;
			return _selfRectTransform.rect.width - Padding.x;
		}
	}
	protected virtual float MaxY
	{
		get
		{
			if ((ControlAxes & Mode.Vertical) != 0) return MaxSize.y;
			return _selfRectTransform.rect.height - Padding.y;
		}
	}

	// NOTE: Addition
	// Snapping with Ceil because we're looking to "maximize" the space needed by the text. If Round is used, sometimes it rounds down. Then the TMP text overflows very badly.
	protected virtual float PixelPerfectSnapWithCeil(float val)
	{
		if (!bePixelPerfect)
			return val;

		if (Mathf.Approximately(pixelsPerUnitInBothDimensions, 0f))
			return val;

		float pixelsMultiple = 1f / pixelsPerUnitInBothDimensions;

		return Mathf.Ceil(val / pixelsMultiple) * pixelsMultiple;
	}

	protected virtual void RecalculateSizeIfNeeded()
	{
		if (!_isTextNull && (Text.text != _lastText || _lastSize != _selfRectTransform.rect.size || _forceRefresh || ControlAxes != _lastControlAxes))
		{
			var preferredSizeWithPadding = Text.GetPreferredValues(MaxX, MaxY);

			preferredSizeWithPadding.x = PixelPerfectSnapWithCeil(Mathf.Clamp(preferredSizeWithPadding.x, MinX, MaxX)); // NOTE: Addition
			preferredSizeWithPadding.y = PixelPerfectSnapWithCeil(Mathf.Clamp(preferredSizeWithPadding.y, MinY, MaxY)); // NOTE: Addition
			var preferredSizeWithoutPadding = preferredSizeWithPadding + 2 * Padding; // NOTE: Addition, previously it used the same preferred size for both the rect and text. Either I don't understand what the padding meant or it was an error in the original code.

			if ((ControlAxes & Mode.Horizontal) != 0)
			{
				_selfRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, preferredSizeWithoutPadding.x);
				if (ResizeTextObject)
				{
					_textRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, preferredSizeWithPadding.x);
				}
			}
			if ((ControlAxes & Mode.Vertical) != 0)
			{
				_selfRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, preferredSizeWithoutPadding.y);
				if (ResizeTextObject)
				{
					_textRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, preferredSizeWithPadding.y);
				}
			}

			_lastText = Text.text;
			_lastSize = _selfRectTransform.rect.size;
			_lastControlAxes = ControlAxes;
			_forceRefresh = false;
		}
	}

	protected virtual void Update()
	{
		RecalculateSizeIfNeeded();
	}

	// Forces a size recalculation on this Update
	public virtual void RefreshThisUpdate()
	{
		Refresh();
		RecalculateSizeIfNeeded();
	}

	// Forces a size recalculation on next Update, hiding this function
	protected virtual void Refresh()
	{
		_forceRefresh = true;

		_isTextNull = Text == null;
		if (Text) _textRectTransform = Text.GetComponent<RectTransform>();
		_selfRectTransform = GetComponent<RectTransform>();
	}

	private void OnValidate()
	{
		Refresh();
	}

	// NOTE: Pasted as advised in the thread this code appeared in
	public virtual void Start()
	{
		Refresh();
	}
}
