﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityDissipationSystem : BaseSystem
{
	public List<AttackEntity> attackEntities;
	public List<DamageOrBuffTextEntity> damageOrBuffTextEntities;
	public List<ResourceGainTextEntity> resourceGainTextEntities;
	public List<RockDamageOrDeathAnimationEntity> rockDeathAnimationEntities;
	public List<RockDamageOrDeathAnimationEntity> rockDamageStageAnimationEntities;
	public List<BuffGiverAreaEntity> buffGiverAreaEntities;

	AttackEntity attackEntity;
	DamageOrBuffTextEntity damageOrBuffTextEntity;
	ResourceGainTextEntity resourceGainTextEntity;
	RockDamageOrDeathAnimationEntity rockDeathAnimationEntity;
	RockDamageOrDeathAnimationEntity rockDamageStageAnimationEntity;
	BuffGiverAreaEntity buffGiverAreaEntity;

	public override void SystemUpdate()
	{
		for (int i = attackEntities.Count - 1; i >= 0; i--)
		{
			attackEntity = attackEntities[i];

			if ((attackEntity.attackStateComponent.attackState == AttackState.DealingDamage && attackEntity.attackDissipationStartTime + attackEntity.attackDealingDamageDissipationDuration <= Time.time) 
				|| (attackEntity.attackStateComponent.attackState == AttackState.FizzlingOut && attackEntity.attackDissipationStartTime + attackEntity.attackFizzlingOutDissipationDuration <= Time.time)
				)
			{
				attackEntities.RemoveAt(i);
				GameObject.Destroy(attackEntity.gameObject);
			}
		}

		for (int i = damageOrBuffTextEntities.Count - 1; i >= 0; i--)
		{
			damageOrBuffTextEntity = damageOrBuffTextEntities[i];

			if (damageOrBuffTextEntity.isDissipating && damageOrBuffTextEntity.dissipationStartTime + damageOrBuffTextEntity.dissipationDuration <= Time.time)
			{
				damageOrBuffTextEntities.RemoveAt(i);
				GameObject.Destroy(damageOrBuffTextEntity.gameObject);
			}
		}

		for (int i = resourceGainTextEntities.Count - 1; i >= 0; i--)
		{
			resourceGainTextEntity = resourceGainTextEntities[i];

			if (resourceGainTextEntity.isDissipating && resourceGainTextEntity.dissipationStartTime + resourceGainTextEntity.dissipationDuration <= Time.time)
			{
				resourceGainTextEntities.RemoveAt(i);
				GameObject.Destroy(resourceGainTextEntity.gameObject);
			}
		}

		for (int i = rockDeathAnimationEntities.Count - 1; i >= 0; i--)
		{
			rockDeathAnimationEntity = rockDeathAnimationEntities[i];

			if (rockDeathAnimationEntity.dissipationStartTime + rockDeathAnimationEntity.dissipationDuration <= Time.time)
			{
				rockDeathAnimationEntities.RemoveAt(i);
				GameObject.Destroy(rockDeathAnimationEntity.gameObject);
			}
		}

		for (int i = rockDamageStageAnimationEntities.Count - 1; i >= 0; i--)
		{
			rockDamageStageAnimationEntity = rockDamageStageAnimationEntities[i];

			if (rockDamageStageAnimationEntity.dissipationStartTime + rockDamageStageAnimationEntity.dissipationDuration <= Time.time)
			{
				rockDamageStageAnimationEntities.RemoveAt(i);
				GameObject.Destroy(rockDamageStageAnimationEntity.gameObject);
			}
		}

		for (int i = buffGiverAreaEntities.Count - 1; i >= 0; i--)
		{
			buffGiverAreaEntity = buffGiverAreaEntities[i];

			if (buffGiverAreaEntity.dissipationStartTime + buffGiverAreaEntity.dissipationDuration <= Time.time)
			{
				buffGiverAreaEntities.RemoveAt(i);
				GameObject.Destroy(buffGiverAreaEntity.gameObject);
			}
		}
	}
}
