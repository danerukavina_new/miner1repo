﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCompletionAndRotationSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;

	public static void CheckAndAdjustAttackSpeed(DwarfEntity dwarfEntity)
	{
		// TODO: This is a hacky way to check if attack speed changed
		if (
			dwarfEntity.unitAttackingStateComponent.currentAttackStatSet.attackCompletionTime != dwarfEntity.unitAttackingStateComponent.nextAttackStatSet.attackCompletionTime
			|| dwarfEntity.animator.speed != (1f + dwarfEntity.unitStatsComponent.finalStats.attackSpeedBonus)
			)
		{
			dwarfEntity.unitAttackingStateComponent.currentAttackStatSet = dwarfEntity.unitAttackingStateComponent.nextAttackStatSet;

			// TODO: This animator control is not granular enough. Should only control the attack animations
			// TODO: This should be controlled from SetAnimationFromStateSystem
			dwarfEntity.animator.speed = (1f + dwarfEntity.unitStatsComponent.finalStats.attackSpeedBonus);
		}
	}

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.unitAttackingStateComponent.initiateAttackRotation)
			{
				dwarfEntity.unitAttackingStateComponent.initiateAttackRotation = false;

				dwarfEntity.unitAttackingStateComponent.attackStartTime = Time.time;
				dwarfEntity.unitAttackingStateComponent.attackCreationPointPerformed = false;
				dwarfEntity.unitAttackingStateComponent.attackBackswingFinishedPointPerformed = false;

				CheckAndAdjustAttackSpeed(dwarfEntity);
			}

			// NOTE: Very specifically, it doesn't matter if the dwarf is still attacking:
			// Instead, as long as the dwarf has "created" an attack, they need to progress their rotation eventually
			// If instead this depended on the dwarf continuing to attack, they can keep creating their last created attack
			// TODO: Consider an alternate implementation that iterates the nextAttack at creation time... is that more representative of what's going on?
			if (dwarfEntity.unitAttackingStateComponent.attackCreationPointPerformed)
			{
				if (dwarfEntity.unitAttackingStateComponent.attackStartTime + dwarfEntity.unitAttackingStateComponent.currentAttackStatSet.attackCompletionTime <= Time.time)
				{
					dwarfEntity.unitAttackingStateComponent.nextAttack = (dwarfEntity.unitAttackingStateComponent.nextAttack + 1) % dwarfEntity.unitAttackingStateComponent.attackRotation.Count;
					dwarfEntity.unitAttackingStateComponent.attackStartTime += dwarfEntity.unitAttackingStateComponent.currentAttackStatSet.attackCompletionTime;
					dwarfEntity.unitAttackingStateComponent.attackCreationPointPerformed = false;
					dwarfEntity.unitAttackingStateComponent.attackBackswingFinishedPointPerformed = false;

					CheckAndAdjustAttackSpeed(dwarfEntity);
				}
			}
		}
	}
}
