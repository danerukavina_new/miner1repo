﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearFixedUpdateFlagsUsedByMultipleSystems : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.coordWasDirtiedThisFixedUpdate)
				dwarfEntity.coordWasDirtiedThisFixedUpdate = false;

			if (dwarfEntity.buffReceiverComponent.buffEntitiesListIsDirtyThisFixedUpdate)
				dwarfEntity.buffReceiverComponent.buffEntitiesListIsDirtyThisFixedUpdate = false;
		}
	}
}
