﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffGiverAreaGiveBuffsSystem : BaseSystem
{
	public List<BuffGiverAreaEntity> buffGiverAreaEntities;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	RockTileEntity rockTileEntity;
	BuffReceiverComponent buffReceiverComponent;
	PrototypeBuff prototypeBuff;
	bool typeTagsMatched;

	void BuffReceiverLogic(BuffReceiverComponent buffReceiverComponent, Vector2Int receiverCoord)
	{
		foreach (var buffGiverAreaEntity in buffGiverAreaEntities)
		{
			typeTagsMatched = false;

			foreach (var intendedReceiverUnitTypeTag in buffGiverAreaEntity.intendedReceiverUnitTypeTags)
				typeTagsMatched |= buffReceiverComponent.receiverUnitTypeTags.Contains(intendedReceiverUnitTypeTag);

			if (typeTagsMatched)
			{
				prototypeBuff = buffGiverAreaEntity.prototypeBuffTiles[receiverCoord.x, receiverCoord.y];

				if (prototypeBuff != null)
				{
					buffReceiverComponent.receivedPrototypeBuffsThisFixedUpdate.Add(prototypeBuff);
				}
			}
		}
	}

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
			BuffReceiverLogic(dwarfEntity.buffReceiverComponent, dwarfEntity.coord);

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				rockTileEntity = levelEntity.rockTiles[x, y];
				if (rockTileEntity != null)
					BuffReceiverLogic(rockTileEntity.buffReceiverComponent, rockTileEntity.coord);
			}
	}
}
