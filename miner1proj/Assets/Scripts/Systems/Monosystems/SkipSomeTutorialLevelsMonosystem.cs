﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Think about this Monosystem concept some more...
public class SkipSomeTutorialLevelsMonosystem
{
	public GameStateEntity gameStateEntity;

	TutorialsStateComponent tutorialsStateComponent;

	public void ExecuteOnce()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevels)
		{
			// NOTE: Do not unflag gameStateEntity.testingSkipSomeTutorialLevels, other systems check it

			gameStateEntity.indexToLoad = gameStateEntity.testingLevelOfTutorialToSkipTo;

			tutorialsStateComponent = gameStateEntity.tutorialsStateComponent;

			while (tutorialsStateComponent.currentOrNextTutorialStep < tutorialsStateComponent.tutorialStepEntities.Count
				&& tutorialsStateComponent.tutorialStepEntities[tutorialsStateComponent.currentOrNextTutorialStep].gameLevel < gameStateEntity.testingLevelOfTutorialToSkipTo)
				tutorialsStateComponent.currentOrNextTutorialStep++;

			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = true;

			// NOTE: Other systems that Load know how to handle this flag
		}
	}
}