﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveCompletedPlayerInputSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	public override void SystemUpdate()
	{
		bool removalOccured = false;

		foreach (var dwarfEntity in dwarfEntities)
		{
			// NOTE: Otherwise, a dwarf that's exited, while disabled, keeps clearing the exit coordinate annoyingly
			if (!dwarfEntity.hasExitedLevel)
				if (EntityHelpers.RemoveSharedManualCoordinate(dwarfEntity.coord, levelEntity))
					removalOccured = true;
		}

		for (int i = levelEntity.sharedManualCoordinates.Count - 1; i >= 0; i--)
		{
			if (!LogicHelpers.CheckTileMayBePassable(levelEntity.sharedManualCoordinates[i], levelEntity))
			{
				// TODO: this will always be true since we know the index exists
				if (EntityHelpers.RemoveSharedManualCoordinate(levelEntity.sharedManualCoordinates[i], levelEntity, i))
					removalOccured = true;
			}
		}

		if (removalOccured)
		{
			foreach (var dwarfEntity in dwarfEntities)
			{
				dwarfEntity.navigationComponent.levelDataWasDirtied = true;
			}
		}
	}
}
