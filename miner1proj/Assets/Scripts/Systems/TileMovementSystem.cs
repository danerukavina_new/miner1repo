﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovementSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public List<AttackEntity> attackEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.unitStateComponent.unitState == UnitState.Moving)
			{
				dwarfEntity.transform.position = Vector3.MoveTowards(
					dwarfEntity.transform.position,
					LogicHelpers.PositionFromCoord(dwarfEntity.tileMovementStateComponent.nextCoord),
					Time.deltaTime * dwarfEntity.tileMovementStateComponent.movementSpeed
					);

				if (LogicHelpers.IsPositionOnCoord(dwarfEntity.transform.position, dwarfEntity.tileMovementStateComponent.nextCoord))
				{
					// Debug.Log("dwarf: " + dwarfEntity.coord + " - nav: " +dwarfEntity.tileMovementStateComponent.navPath.CustomListOutput());
					dwarfEntity.isCoordGridAligned = true;
					dwarfEntity.tileMovementStateComponent.readyForNextCoord = true;
				}
				else
					dwarfEntity.isCoordGridAligned = false;
			}
		}

		foreach (var attackEntity in attackEntities)
		{
			if ((attackEntity.attackStateComponent.attackState == AttackState.Moving || attackEntity.attackStateComponent.attackState == AttackState.FizzlingOut)
				&& attackEntity.tileMovementStateComponent != null)
			{
				attackEntity.transform.position = Vector3.MoveTowards(
					attackEntity.transform.position,
					LogicHelpers.PositionFromCoord(attackEntity.tileMovementStateComponent.nextCoord),
					Time.deltaTime * attackEntity.tileMovementStateComponent.movementSpeed
					);

				if (LogicHelpers.IsPositionOnCoord(attackEntity.transform.position, attackEntity.tileMovementStateComponent.nextCoord))
				{
					attackEntity.isCoordGridAligned = true;
					attackEntity.tileMovementStateComponent.readyForNextCoord = true;
				}
				else
					attackEntity.isCoordGridAligned = false;
			}
		}
	}
}
