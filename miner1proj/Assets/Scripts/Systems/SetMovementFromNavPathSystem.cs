﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMovementFromNavPathSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.tileMovementStateComponent.readyForNextCoord)
			{
				dwarfEntity.tileMovementStateComponent.readyForNextCoord = false;

				dwarfEntity.coord = dwarfEntity.tileMovementStateComponent.nextCoord;
				dwarfEntity.coordWasDirtiedThisFixedUpdate = true;

				if (dwarfEntity.tileMovementStateComponent.navPath.Count > 0)
				{
					
					// TODO: This is crap, obviously. It occurs
					// NOTE: Sometimes, the dwarves navPath gets reset when the dwarf is already on the way to the next tile. In that case, this can occur.
					// Then, the next navPath node needs to simply be cleared and another attempt of selecting a navPath should be taken
					// If this code is removed, animations glitch out, with the dwarf's nextCoord being the same as coord, so direction can't properly be determined
					if (dwarfEntity.tileMovementStateComponent.nextCoord == dwarfEntity.tileMovementStateComponent.navPath[0])
					{
						dwarfEntity.tileMovementStateComponent.nextCoord = dwarfEntity.tileMovementStateComponent.navPath[0];
						dwarfEntity.tileMovementStateComponent.navPath.RemoveAt(0);
						dwarfEntity.tileMovementStateComponent.directionChangeToBeCheckedOnUpdate = true;

						if (dwarfEntity.tileMovementStateComponent.navPath.Count > 0)
						{
							dwarfEntity.tileMovementStateComponent.nextCoord = dwarfEntity.tileMovementStateComponent.navPath[0];
							dwarfEntity.tileMovementStateComponent.navPath.RemoveAt(0);
							dwarfEntity.tileMovementStateComponent.directionChangeToBeCheckedOnUpdate = true;
						}
					}
					else
					{
						dwarfEntity.tileMovementStateComponent.nextCoord = dwarfEntity.tileMovementStateComponent.navPath[0];
						dwarfEntity.tileMovementStateComponent.navPath.RemoveAt(0);
						dwarfEntity.tileMovementStateComponent.directionChangeToBeCheckedOnUpdate = true;
					}
				}
			}
		}
	}
}
