﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCandidateActionByPathfindingSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	Vector2Int startingCoord;
	Vector2Int startingCoordSecondaryFromMovement;
	NavCoord[,] navArray;
	NavCoord[,] unreachableNavArray;
	List<Vector2Int> foundManualCoords;
	List<Vector2Int> candidateNavCoords;
	List<Vector2Int> foundMiningNodes;
	List<Vector2Int> foundFullFogs;
	List<Vector2Int> foundUnreachableEdges;
	bool[,] consideredMiningNodeAlready;
	bool[,] consideredFullFogAlready;
	bool[,] consideredUnreachableEdgeAlready;
	List<Vector2Int> reverseNavPathHolder;
	List<Vector2Int> attackRangePattern;

	public static bool CheckTileWalkableXY(LevelEntity levelEntity, Vector2Int coord)
	{
		// NOTE: Units assume tiles under fog are walkable
		if (levelEntity.fullFogTiles[coord.x, coord.y] != null)
			return true;

		if (!levelEntity.floorTiles[coord.x, coord.y].walkable || !levelEntity.floorTiles[coord.x, coord.y].passable)
			return false;

		if (levelEntity.rockTiles[coord.x, coord.y] != null && !levelEntity.rockTiles[coord.x, coord.y].passable)
			return false;

		return true;
	}

	public static bool CheckTileHPPenetrableXY(LevelEntity levelEntity, Vector2Int coord, Vector2Int prevCoord, double prevHPPenetrationLength, out double nextHPPenetrationLength)
	{
		nextHPPenetrationLength = prevHPPenetrationLength;

		// NOTE: Units assume tiles under fog are walkable/penetrable for free
		if (levelEntity.fullFogTiles[coord.x, coord.y] != null)
			return true;

		if (!levelEntity.floorTiles[coord.x, coord.y].walkable || !levelEntity.floorTiles[coord.x, coord.y].passable)
			return false;

		RockTileEntity rockTile = levelEntity.rockTiles[coord.x, coord.y];

		if (rockTile == null)
			return true;

		if (rockTile.passable)
			return true;

		if (!rockTile.destructible)
			return false;

		nextHPPenetrationLength += rockTile.HP;

		// NOTE: If there are 0 HP rock tiles that need to be whacked once, this code needs to change
		// You can't start penetrating at a reserved tile
		if (Mathf.Approximately((float)prevHPPenetrationLength, 0f))
			// NOTE: It's possible the freespacemover check here isn't necessary, but it makes sense. Don't penetrate from a reserved spot!
			if (levelEntity.reservedByAttackerTiles[prevCoord.x, prevCoord.y] != null || levelEntity.reservedByFreeSpaceMoverTiles[prevCoord.x, prevCoord.y] != null) // TODO: Currently a dwarf unreserves it's own reservation. Alternately, this code could just check, is it the own dwarf's
				return false;

		return true;
	}

	public static bool ConsiderCoordAndAddUnreachableNavCandidates(
		Vector2Int coord,
		Vector2Int previousCoord,
		float lengthToCoord,
		double prevHPPenetrationLengthToCoord,
		LevelEntity levelEntity,
		NavCoord[,] navArray,
		NavCoord[,] unreachableNavArray,
		List<Vector2Int> candidateFullFogNavCoords,
		List<Vector2Int> foundUnreachableEdges,
		bool[,] consideredUnreachableEdgeAlready
	)
	{
		bool candidateNavCoordAdded = false;

		if (!LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			return false;

		bool isDiagonalPath = Mathf.Abs(coord.x - previousCoord.x) == 1 && Mathf.Abs(coord.y - previousCoord.y) == 1;

		if (isDiagonalPath)
		{
			// Dwarves must be able to walk across the diagonal spots as well

			if (!CheckTileWalkableXY(levelEntity, new Vector2Int(coord.x, previousCoord.y)))
				return candidateNavCoordAdded; // false

			if (!CheckTileWalkableXY(levelEntity, new Vector2Int(previousCoord.x, coord.y)))
				return candidateNavCoordAdded; // false
		}

		double nextHPPenetrationLengthToCoord = prevHPPenetrationLengthToCoord;

		if (unreachableNavArray[coord.x, coord.y] == null || LogicHelpers.IsShorterPath(unreachableNavArray[coord.x, coord.y], lengthToCoord))
		{
			RockTileEntity unreachableIndestructibleCheckRockTile = levelEntity.rockTiles[coord.x, coord.y];
			FullFogEntity unreachableIndestructibleCheckFullFog = levelEntity.fullFogTiles[coord.x, coord.y];

			if (unreachableIndestructibleCheckFullFog == null && unreachableIndestructibleCheckRockTile != null && !unreachableIndestructibleCheckRockTile.destructible)
				nextHPPenetrationLengthToCoord += ValueHolder.INDESTRUCTIBLE_PENETRATION_LENGTH_FOR_PATHING_HACK;

			unreachableNavArray[coord.x, coord.y] = new NavCoord()
			{
				prevCoord = previousCoord,
				pathLength = lengthToCoord,
				HPPenetrationLength = nextHPPenetrationLengthToCoord,
			};

			// There's no need to keep going by adding a candidate if we've found a connection to the navArray
			if (navArray[coord.x, coord.y] == null)
			{
				candidateFullFogNavCoords.Add(coord); // NOTE: This needs to update candidate coords once again. Otherwise the unit test "NavPathAroundRocksToCopper()" fails and tries to go through a wall
				candidateNavCoordAdded = true;
			}
			else if (!consideredUnreachableEdgeAlready[coord.x, coord.y])
			{
				foundUnreachableEdges.Add(coord);
				consideredUnreachableEdgeAlready[coord.x, coord.y] = true;
			}
		}

		return candidateNavCoordAdded;
	}

	public static bool ConsiderCoordAndAddNavCandidates(
		Vector2Int coord, 
		Vector2Int previousCoord, 
		float lengthToCoord, 
		double prevHPPenetrationLengthToCoord, 
		bool canPenetrateDiagonally,
		LevelEntity levelEntity, 
		NavCoord[,] navArray, 
		List<Vector2Int> candidateNavCoords, 
		List<Vector2Int> foundMiningNodes, 
		List<Vector2Int> foundFullFogs, 
		bool[,] consideredMiningNodeAlready,
		bool[,] consideredFullFogAlready
		)
	{
		bool candidateNavCoordAdded = false;

		if (!LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			return false;

		bool isDiagonalPath = Mathf.Abs(coord.x - previousCoord.x) == 1 && Mathf.Abs(coord.y - previousCoord.y) == 1;
		bool isCoordWalkable = CheckTileWalkableXY(levelEntity, coord);
		bool intendsToPenetrate = false;

		if (isDiagonalPath)
		{
			// Dwarves must be able to walk across the diagonal spots as well

			if (!CheckTileWalkableXY(levelEntity, new Vector2Int(coord.x, previousCoord.y)))
				return candidateNavCoordAdded; // false

			if (!CheckTileWalkableXY(levelEntity, new Vector2Int(previousCoord.x, coord.y)))
				return candidateNavCoordAdded; // false
		}

		double nextHPPenetrationLengthToCoord = prevHPPenetrationLengthToCoord;

		if (!isCoordWalkable)
		{
			if (!isDiagonalPath || canPenetrateDiagonally)
			{
				if (!CheckTileHPPenetrableXY(levelEntity, coord, previousCoord, prevHPPenetrationLengthToCoord, out nextHPPenetrationLengthToCoord))
					return candidateNavCoordAdded; // false

				intendsToPenetrate = true;
			}
			else
				return candidateNavCoordAdded; // false
		}

		if (navArray[coord.x, coord.y] == null || LogicHelpers.IsShorterPathOrPenetration(navArray[coord.x, coord.y], lengthToCoord, nextHPPenetrationLengthToCoord))
		{
			navArray[coord.x, coord.y] = new NavCoord()
			{
				prevCoord = previousCoord,
				pathLength = lengthToCoord,
				HPPenetrationLength = nextHPPenetrationLengthToCoord
			};

			// TODO: this is really dirty, hoist it with a flag
			if (levelEntity.fullFogTiles[coord.x, coord.y] == null)
			{
				candidateNavCoords.Add(coord); // NOTE: This needs to update candidate coords once again. Otherwise the unit test "NavPathAroundRocksToCopper()" fails and tries to go through a wall
				candidateNavCoordAdded = true;
			}
		}

		if (levelEntity.fullFogTiles[coord.x, coord.y] == null)
		{
			if (intendsToPenetrate)
				if (!consideredMiningNodeAlready[coord.x, coord.y])
				{
					// NOTE: It's not ok to only add mining nodes if this is the first penetration (as in prevHPPenetrationLength == 0d).
					// Reasoning: Dwarves choose which rocks to penetrate based on possible goal nodes behind them

					// NOTE: No need to check reserved spots here, "intendsToPenetrate" already did that check
					foundMiningNodes.Add(coord);
					consideredMiningNodeAlready[coord.x, coord.y] = true;
				}
		}
		else
		{
			if (!(nextHPPenetrationLengthToCoord > 0d))
				if (!consideredFullFogAlready[coord.x, coord.y])
				{
					foundFullFogs.Add(coord);
					consideredFullFogAlready[coord.x, coord.y] = true;
				}
			// TODO: make an else here and fill a new array that's like "found
		}

		return candidateNavCoordAdded;
	}

	public static bool ConsiderAttackRangePatternCoordAndAddNavCandidates(
		Vector2Int coord,
		Vector2Int previousCoord,
		float lengthToCoord,
		double prevHPPenetrationLengthToCoord,
		LevelEntity levelEntity,
		NavCoord[,] navArray,
		List<Vector2Int> candidateNavCoords,
		List<Vector2Int> foundMiningNodes,
		bool[,] consideredMiningNodeAlready
		)
	{
		if (!LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			return false;

		double nextHPPenetrationLengthToCoord = prevHPPenetrationLengthToCoord;

		if (levelEntity.fullFogTiles[coord.x, coord.y] == null)
		{
			RockTileEntity rockTileEntity = levelEntity.rockTiles[coord.x, coord.y];
			if (rockTileEntity != null && rockTileEntity.destructible)
			{
				// NOTE: If penetrationLength is not increased here, it seems that sometimes the navArray coords left by this process are usurped for creating paths through rocks by the ranged dwarf.
				nextHPPenetrationLengthToCoord += rockTileEntity.HP;

				if (!consideredMiningNodeAlready[coord.x, coord.y])
					if (!(levelEntity.reservedByAttackerTiles[previousCoord.x, previousCoord.y] != null || levelEntity.reservedByFreeSpaceMoverTiles[previousCoord.x, previousCoord.y] != null))
					{
						if (navArray[coord.x, coord.y] == null || LogicHelpers.IsShorterPathOrPenetration(navArray[coord.x, coord.y], lengthToCoord, nextHPPenetrationLengthToCoord))
						{
							navArray[coord.x, coord.y] = new NavCoord()
							{
								prevCoord = previousCoord,
								pathLength = lengthToCoord,
								HPPenetrationLength = nextHPPenetrationLengthToCoord
							};

							// NOTE: Finding an attack spot does not generate new candidateNavCoords - it's not like the dwarf is teleporting to the spot
						}

						foundMiningNodes.Add(coord);
						consideredMiningNodeAlready[coord.x, coord.y] = true;

						// NOTE: Adding this allows the dwarf to keep constructing a deeper penetration plan in the following scenario:
						// A node on level 6 at 11, 14 is behind a node at 10, 14. If only 11, 14 is marked, the ranged dwarf will not attempt to penetrate at 10, 14 because it establishes no penetrative connection to 11, 14
						// Unless this line is in place:
						candidateNavCoords.Add(coord);

						return true;
					}
			}
		}

		return false;
	}

	public void PrepareNavigationComponent(DwarfEntity dwarfEntity)
	{
		// TODO: Consider using a path length based queue? Like Dijkstra? But we may want to find all mining nodes, not just nearest.
		// TODO: Changed to a shared pathing algorithm between dwarves. All points shortest path?

		// TODO: Maybe have one or more system create the nav information, and additional systems go through it

		// TODO: If there's only rocks to mine, and no clear fog: Dwarves should tend towards rocks that hide more of the map, instead of going for nearest rock (which often ends up being a stupid corner rock)
		// Since dwarves and maybe player don't know the extents of the map, it should probably based on surrounding amount of fog relative to rock
		// Or it can just be trying to get to fog with lowest "hp penetration"

		// TODO: Figure out why dwarves are sometimes staying on rock nodes after they finish an attack when there's higher value nodes being discovered

		navArray = dwarfEntity.navigationComponent.navArray;
		candidateNavCoords = dwarfEntity.navigationComponent.candidateNavCoords;
		foundManualCoords = dwarfEntity.navigationComponent.foundManualCoords;
		foundMiningNodes = dwarfEntity.navigationComponent.foundMiningNodes;
		foundFullFogs = dwarfEntity.navigationComponent.foundFullFogs;
		consideredMiningNodeAlready = dwarfEntity.navigationComponent.consideredMiningNodeAlready;
		consideredFullFogAlready = dwarfEntity.navigationComponent.consideredFullFogAlready;
		reverseNavPathHolder = dwarfEntity.navigationComponent.reverseNavPathHolder;
		attackRangePattern = dwarfEntity.navigationComponent.attackRangePattern;

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				navArray[x, y] = null;
				consideredMiningNodeAlready[x, y] = false;
				consideredFullFogAlready[x, y] = false;
			}

		candidateNavCoords.Clear();
		foundMiningNodes.Clear();
		foundFullFogs.Clear();

		if (dwarfEntity.unitStateComponent.unitState == UnitState.Moving)
		{
			startingCoord = dwarfEntity.coord;

			candidateNavCoords.Add(startingCoord);
			navArray[startingCoord.x, startingCoord.y] = new NavCoord()
			{
				prevCoord = startingCoord,
				pathLength = (LogicHelpers.PositionFromCoord(startingCoord) - dwarfEntity.gameObject.transform.position).magnitude + ValueHolder.BACKTRACK_TO_COORD_PATHING_PENALTY,
				HPPenetrationLength = 0f,
			};

			startingCoordSecondaryFromMovement = dwarfEntity.tileMovementStateComponent.nextCoord;

			candidateNavCoords.Add(startingCoordSecondaryFromMovement);
			navArray[startingCoordSecondaryFromMovement.x, startingCoordSecondaryFromMovement.y] = new NavCoord()
			{
				prevCoord = startingCoordSecondaryFromMovement,
				pathLength = (LogicHelpers.PositionFromCoord(startingCoordSecondaryFromMovement) - dwarfEntity.gameObject.transform.position).magnitude,
				HPPenetrationLength = 0f,
			};
		}
		else
		{
			startingCoord = dwarfEntity.coord;

			candidateNavCoords.Add(startingCoord);
			navArray[startingCoord.x, startingCoord.y] = new NavCoord()
			{
				prevCoord = startingCoord,
				pathLength = (LogicHelpers.PositionFromCoord(startingCoord) - dwarfEntity.gameObject.transform.position).magnitude,
				HPPenetrationLength = 0f,
			};
		}


		attackRangePattern.Clear();
		switch (dwarfEntity.possibleAttackingPosition)
		{
			case PossibleAttackingPosition.Melee:
				break;
			case PossibleAttackingPosition.GridAlignedWithRange:
				// TODO: Because of the particular code for checking if the projectile would be stopped along the way, it's not useful to pregenerate the pattern
				// An approach for this would be to make attackRangePatterns a Dictionary<{squential stoppable projectile pattern}, List<Vector2Int>>, and checking each sequence separately and stopping the check...
				// ... but this approach may be wrong, not general enough, and would certainly complicate things prematurely
				// LogicHelpers.GetPlusSignCoordsAroundCoordWithinLevel(Vector2Int.zero, (int)dwarfEntity.unitAttackingStateComponent.attackRange, attackRangePattern, levelEntity);
				break;
			case PossibleAttackingPosition.RadiusWithRange:
				LogicHelpers.GetDistanceCircleCoordsAroundCoordWithinLevel(Vector2Int.zero, dwarfEntity.unitAttackingStateComponent.attackRange, attackRangePattern, levelEntity);
				break;
			default:
				break;
		}

		//startingCoord = dwarfEntity.coord;

		//candidateNavCoords.Add(startingCoord);
		//navArray[startingCoord.x, startingCoord.y] = new NavCoord()
		//{
		//	prevCoord = startingCoord,
		//	pathLength = 0f,
		//	HPPenetrationLength = 0f,
		//};

		Vector2Int prevCoord;
		Vector2Int nextCandidateCoord;
		float prevBaseLength;
		float nextCandidateLength;
		double prevBaseHPPenetrationLength;

		int loopControl = 0;
		RockTileEntity attackProjectileCollisionCheckRockTileEntity;

		while (candidateNavCoords.Count > 0 && loopControl < ValueHolder.MAXIMUM_SEARCH_LOOPS)
		{
			loopControl++;

			// TODO: skip candidateNavCoords if a candidateMiningNode exists with a shorter path?
			// This doesn't make sense if we want dwarves to do a value assessment that also takes "better" penetrated nodes into account. But... we don't have to.
			// Or skip once K nodes are found for value comparison? Or skip significantly longer paths...
			// Or build value into the concept of "shortest path"

			prevCoord = candidateNavCoords[0];
			prevBaseLength = navArray[candidateNavCoords[0].x, candidateNavCoords[0].y].pathLength;
			prevBaseHPPenetrationLength = navArray[candidateNavCoords[0].x, candidateNavCoords[0].y].HPPenetrationLength;

			foreach (var movementDirection in ValueHolder.eightWayMovementDirections)
			{
				nextCandidateCoord = candidateNavCoords[0] + movementDirection;
				nextCandidateLength = prevBaseLength + movementDirection.magnitude;

				ConsiderCoordAndAddNavCandidates(
					nextCandidateCoord,
					prevCoord,
					nextCandidateLength,
					prevBaseHPPenetrationLength,
					dwarfEntity.canPenetrateDiagonally,
					levelEntity, 
					navArray, 
					candidateNavCoords, 
					foundMiningNodes, 
					foundFullFogs, 
					consideredMiningNodeAlready,
					consideredFullFogAlready
					);
			}

			// NOTE: The code below checks a wide grid of positions around some candidate position for detecting attackable nodes
			// This first check ensures the position isn't already behind the assumption of meleeing through some other node, since
			// The hack put in place for these ranged attacks would then let the dwarf ignore penetration and waltz through solid nodes (by creating such an invalid nav path)
			// This is because it ignores mechanisms related to the penetration length in ReevaluateAndShortenPreviousNavpathDueToNoNewBestCoordsFound
			if (Mathf.Approximately((float)prevBaseHPPenetrationLength, 0f))
			{
				switch (dwarfEntity.possibleAttackingPosition)
				{
					case PossibleAttackingPosition.Melee:
						break;
					case PossibleAttackingPosition.GridAlignedWithRange:
						bool projectileCollisionOccured;

						foreach (var projectileMovementDirection in ValueHolder.fourWayMovementDirections)
						{
							projectileCollisionOccured = false;

							nextCandidateLength = prevBaseLength;

							for (int r = 1; !projectileCollisionOccured && r <= (int)dwarfEntity.unitAttackingStateComponent.attackRange; r++)
							{
								nextCandidateCoord = candidateNavCoords[0] + projectileMovementDirection * r;

								ConsiderAttackRangePatternCoordAndAddNavCandidates(
										nextCandidateCoord,
										prevCoord,
										nextCandidateLength,
										prevBaseHPPenetrationLength,
										levelEntity,
										navArray,
										candidateNavCoords,
										foundMiningNodes,
										consideredMiningNodeAlready
										);

								if (LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, nextCandidateCoord))
								{
									attackProjectileCollisionCheckRockTileEntity = levelEntity.rockTiles[nextCandidateCoord.x, nextCandidateCoord.y];
									projectileCollisionOccured |= attackProjectileCollisionCheckRockTileEntity != null && !attackProjectileCollisionCheckRockTileEntity.passable;
								}
							}
						}
						break;
					case PossibleAttackingPosition.RadiusWithRange:

						nextCandidateLength = prevBaseLength;

						foreach (var attackRangePatternTile in attackRangePattern)
						{
							nextCandidateCoord = candidateNavCoords[0] + attackRangePatternTile;

							ConsiderAttackRangePatternCoordAndAddNavCandidates(
										nextCandidateCoord,
										prevCoord,
										nextCandidateLength,
										prevBaseHPPenetrationLength,
										levelEntity,
										navArray,
										candidateNavCoords,
										foundMiningNodes,
										consideredMiningNodeAlready
										);
						}
						break;
					default:
						break;
				}
			}

			candidateNavCoords.RemoveAt(0);
		}

		if (loopControl == ValueHolder.MAXIMUM_SEARCH_LOOPS)
		{
			throw new System.Exception("Infinite loop while building nav path.");
			// return;
		}
	}

	// NOTE: From a coord that's unreachable by the navArray, goes to the edge of its surroundings, until it reaches overlaps with the navArray
	// It only does this for fogs
	public static void ProcessManualCoordForUneachableManualNavigation(
		Vector2Int coord,
		LevelEntity levelEntity,
		NavCoord[,] navArray,
		NavCoord[,] unreachableNavArray,
		List<Vector2Int> candidateNavCoords,
		List<Vector2Int> foundUnreachableEdges,
		bool[,] consideredUnreachableEdgeAlready
		)
	{
		// If this manual coord is reachable, or cannot be apparently be walked on, it's not relevant
		if (navArray[coord.x, coord.y] != null
			|| !LogicHelpers.CheckTileMayBePassable(coord, levelEntity))
			return;

		Vector2Int prevCoord;
		Vector2Int nextCandidateCoord;
		float prevBaseLength;
		float nextCandidateLength;
		double prevBaseHPPenetrationLength;

		bool alreadyAccountedForTile = true;

		candidateNavCoords.Add(coord);

		foreach (var movementDirection in ValueHolder.eightWayMovementDirections)
		{
			Vector2Int testCoord = candidateNavCoords[0] + movementDirection;

			if (LogicHelpers.CoordinateIsInsideLevel(levelEntity, testCoord) && unreachableNavArray[testCoord.x, testCoord.y] == null)
				alreadyAccountedForTile = false;
		}

		if (!alreadyAccountedForTile)
		{
			candidateNavCoords.Clear();

			candidateNavCoords.Add(coord);
			unreachableNavArray[coord.x, coord.y] = new NavCoord()
			{
				prevCoord = coord,
				pathLength = 0f,
				HPPenetrationLength = 0d,
			};

			int loopControl = 0;

			while (candidateNavCoords.Count > 0 && loopControl < ValueHolder.MAXIMUM_SEARCH_LOOPS)
			{
				loopControl++;

				// NOTE: We do not skip candidates here since we want to fully mark the fogNavArray

				prevCoord = candidateNavCoords[0];
				prevBaseLength = unreachableNavArray[candidateNavCoords[0].x, candidateNavCoords[0].y].pathLength;
				prevBaseHPPenetrationLength = unreachableNavArray[candidateNavCoords[0].x, candidateNavCoords[0].y].HPPenetrationLength;

				foreach (var movementDirection in ValueHolder.eightWayMovementDirections)
				{
					nextCandidateCoord = candidateNavCoords[0] + movementDirection;
					nextCandidateLength = prevBaseLength + movementDirection.magnitude;

					ConsiderCoordAndAddUnreachableNavCandidates(
						nextCandidateCoord,
						prevCoord,
						nextCandidateLength,
						prevBaseHPPenetrationLength,
						levelEntity,
						navArray,
						unreachableNavArray,
						candidateNavCoords,
						foundUnreachableEdges,
						consideredUnreachableEdgeAlready
						);
				}

				candidateNavCoords.RemoveAt(0);
			}

			if (loopControl == ValueHolder.MAXIMUM_SEARCH_LOOPS)
				throw new System.Exception("Maximum loops reached while building full fog nav array from unreachable manually assigned coordinates.");
		}
	}

	public void PrepareLevelFogEdgeArrayForUnreachableManualNavigation(DwarfEntity dwarfEntity)
	{
		consideredUnreachableEdgeAlready = levelEntity.sharedConsideredUnreachableEdgeAlready;
		unreachableNavArray = levelEntity.sharedUnreachableNavArray;
		foundUnreachableEdges = levelEntity.sharedFoundUnreachableEdges;
		

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				consideredUnreachableEdgeAlready[x, y] = false;
				unreachableNavArray[x, y] = null;
			}
		foundUnreachableEdges.Clear(); // TODO: there's no point in this whole function since it's discarding everything we previously calculated
									   // HOWEVER, this causes the TwoDwarfInputUnreachableEdgeGetsEliminatedFromNavForSecondDwarf() test issue

		// NOTE: This system is simply detecting its ability to reach the best nav array. The 0th dwarf will have this.
		// TODO: Refactor this by making a bool map of where all the nav arrays reach
		// TODO: changed this from 0th dwarf to current dwarf, because of TwoDwarfInputUnreachableEdgeGetsEliminatedFromNavForSecondDwarf() test issue
		navArray = dwarfEntity.navigationComponent.navArray;
		candidateNavCoords = new List<Vector2Int>(); // TODO: this is a hack, figure out where to set these up / clear them

		List<Vector2Int> sharedManualCoords = new List<Vector2Int>(); // TODO: this is a hack, figure out where to set these up / clear them
		sharedManualCoords.AddRange(levelEntity.sharedManualCoordinates);

		foreach (var sharedManualCoord in sharedManualCoords)
		{
			candidateNavCoords.Clear();

			ProcessManualCoordForUneachableManualNavigation(
				sharedManualCoord,
				levelEntity,
				navArray,
				unreachableNavArray,
				candidateNavCoords,
				foundUnreachableEdges,
				consideredUnreachableEdgeAlready
				);
		}
	}

	public void PrepareDwarfFogEdgeArrayForUnreachableManualNavigation(DwarfEntity dwarfEntity)
	{

		unreachableNavArray = levelEntity.sharedUnreachableNavArray;
		consideredUnreachableEdgeAlready = levelEntity.sharedConsideredUnreachableEdgeAlready;

		// TODO: Add more checks whether dwarf needs to rebuild this map


		dwarfEntity.navigationComponent.foundUnreachableEdges.Clear();

		if (dwarfEntity.hasManualAssignment)
		{
			Vector2Int dwarfManualCoord = dwarfEntity.manuallyAssignedCoord;

			if (unreachableNavArray[dwarfManualCoord.x, dwarfManualCoord.y] != null)
			{
				// NOTE: This is probably very unsafe to use by reference
				dwarfEntity.navigationComponent.unreachableNavArray = unreachableNavArray;
			}
			else
			{
				// Rebuild the unreachable nav array for this dwarf

				for (int x = 0; x < levelEntity.xSize; x++)
					for (int y = 0; y < levelEntity.ySize; y++)
					{
						dwarfEntity.navigationComponent.consideredUnreachableEdgeAlready[x, y] = false;
						dwarfEntity.navigationComponent.unreachableNavArray[x, y] = null;
					}

				ProcessManualCoordForUneachableManualNavigation(
					dwarfEntity.manuallyAssignedCoord,
					levelEntity,
					navArray,
					dwarfEntity.navigationComponent.unreachableNavArray,
					candidateNavCoords,
					dwarfEntity.navigationComponent.foundUnreachableEdges,
					dwarfEntity.navigationComponent.consideredUnreachableEdgeAlready
					);
			}
		}
		else
		{
			for (int x = 0; x < levelEntity.xSize; x++)
				for (int y = 0; y < levelEntity.ySize; y++)
				{
					dwarfEntity.navigationComponent.consideredUnreachableEdgeAlready[x, y] = consideredUnreachableEdgeAlready[x, y];
					dwarfEntity.navigationComponent.unreachableNavArray[x, y] = unreachableNavArray[x, y];
				}

			foreach (var foundUnreachableEdge in levelEntity.sharedFoundUnreachableEdges)
			{
				dwarfEntity.navigationComponent.foundUnreachableEdges.Add(foundUnreachableEdge);
			}
		}
	}

	public void SetCandidateActionForBestDestination(DwarfEntity dwarfEntity)
	{
		dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking = true;
		dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = true;

		navArray = dwarfEntity.navigationComponent.navArray;
		unreachableNavArray = dwarfEntity.navigationComponent.unreachableNavArray;
		candidateNavCoords = dwarfEntity.navigationComponent.candidateNavCoords;
		foundManualCoords = dwarfEntity.navigationComponent.foundManualCoords;
		foundMiningNodes = dwarfEntity.navigationComponent.foundMiningNodes;
		foundFullFogs = dwarfEntity.navigationComponent.foundFullFogs;
		foundUnreachableEdges = dwarfEntity.navigationComponent.foundUnreachableEdges;
		consideredMiningNodeAlready = dwarfEntity.navigationComponent.consideredMiningNodeAlready;
		consideredFullFogAlready = dwarfEntity.navigationComponent.consideredFullFogAlready;
		reverseNavPathHolder = dwarfEntity.navigationComponent.reverseNavPathHolder;

		bool bestCoordFound = false;
		bool bestMiningNodeFound = false;

		Vector2Int bestCoord = Vector2Int.zero;

		float shortestPathLength = float.MaxValue;
		double shortestHPPenetrationPathLength = double.MaxValue;
		float shortestUnreachablePathLength = float.MaxValue;
		double shortestUnreachableHPPenetrationPathLength = double.MaxValue;
		double highestMiningValue = double.MinValue;
		bool shortestIsFreeSpace = false;
		bool shortestIsExit = false;
		NavCoord miningNodeNavCoord;
		RockTileEntity miningNodeRockTileEntity;

		// NEXT: Build out these next two if blocks for going manual

		// Checking dwarf going to tile marked specifically for it: "own-marked" reachable and unreachable
		if (!bestCoordFound)
		{
			if (dwarfEntity.hasManualAssignment)
			{
				Vector2Int manualCoord = dwarfEntity.manuallyAssignedCoord;
				NavCoord manualNavCoord = navArray[manualCoord.x, manualCoord.y];

				// if "own-marked" reachable
				if (manualNavCoord != null)
				{
					bestCoordFound = true;
					bestCoord = manualCoord;
				}
				else
				{
					manualNavCoord = unreachableNavArray[manualCoord.x, manualCoord.y];

					// if "own-marked" unreachable
					if (manualNavCoord != null)
					{
						bestCoordFound = true;
						bestCoord = manualCoord;
					}
					else
						throw new System.Exception("Dwarf has manual assignment, yet it's neither reachable nor unreachable.");
				}
			}
		}

		// Checking dwarf going to tiles marked on the level: "shared-marked" reachable
		if (!bestCoordFound && levelEntity.sharedManualCoordinates.Count > 0)
		{
			Vector2Int coord;
			NavCoord navCoord;
			RockTileEntity rockTileEntityAtCoord;
			FullFogEntity fullFogEntityAtCoord;

			// NOTE: When the user, let's say, taps a single spot - they assign a single shared manual coord.
			// If this code did not go "in depth" to find a spot for the dwarf, only a single dwarf of many would move to the tapped location.
			// Instead, dwarves make a best attempt to go near the shared manual coord. This is not the only way to do this logic. In fact it would be better to make a slew of nearby candidates from the initial shared manual coords.
			// TODO: Make a candidate creation algorithm from the input sharedManualCoord, so there's clear control over what the dwarf considers going to.
			for (int depth = 0; depth < ValueHolder.MAXIMUM_DWARVES_IN_PARTY && !bestCoordFound; depth++)
			{
				foreach (var sharedManualCoord in levelEntity.sharedManualCoordinates)
				{
					coord = sharedManualCoord;
					navCoord = navArray[coord.x, coord.y];

					// NOTE: This is the only real depth code. Everything else would just work as if there's no depth.
					for (int i = 0; i < depth && navCoord != null; i++)
					{
						coord = navCoord.prevCoord;
						navCoord = navArray[coord.x, coord.y];
					}

					fullFogEntityAtCoord = levelEntity.fullFogTiles[coord.x, coord.y];

					// TODO: This controls this mechanism from seeing rocks under fog tiles. Refactor it... the free space or higher mining value function below should just contemplate fog
					if (fullFogEntityAtCoord == null)
						rockTileEntityAtCoord = levelEntity.rockTiles[coord.x, coord.y];
					else
						rockTileEntityAtCoord = null;

					if (navCoord != null)
					{
						// TODO: This condition is inconsistent in the way other things are handled. But it's also absolute. This is the key useage of reservedByFreeSpaceMoverTiles
						// 
						if (
							(levelEntity.reservedByFreeSpaceMoverTiles[coord.x, coord.y] == null && levelEntity.reservedByFreeSpaceMoverTiles[coord.x, coord.y] == null) // Don't go on reserved tiles. This includes movement tiles.
							|| (rockTileEntityAtCoord != null && rockTileEntityAtCoord.isExit)) // Unless it's an exit tile - then all dwarves can go there
						{
							if (!LogicHelpers.IsExitOrFreeSpaceOrHigherMiningValueOrShorterPathOrPenetration(navCoord, rockTileEntityAtCoord, shortestIsExit, shortestIsFreeSpace, shortestPathLength, shortestHPPenetrationPathLength, highestMiningValue))
							{
								shortestIsExit = LogicHelpers.IsExitNavWise(navCoord, rockTileEntityAtCoord);
								shortestIsFreeSpace = LogicHelpers.IsFreeSpaceNavWise(navCoord, rockTileEntityAtCoord);
								shortestPathLength = navCoord.pathLength;
								shortestHPPenetrationPathLength = navCoord.HPPenetrationLength;
								if (rockTileEntityAtCoord != null)
									highestMiningValue = rockTileEntityAtCoord.miningValue;

								bestCoordFound = true;
								bestCoord = coord;
							}
						}
					}
				}
			}
		}

		// Checking dwarf going to tiles marked on the level: "shared-marked" unreachable
		if (!bestCoordFound && levelEntity.sharedManualCoordinates.Count > 0)
		{
			NavCoord navCoord;
			NavCoord unreachableNavCoord;
			RockTileEntity rockTileEntityAtCoord;
			FullFogEntity fullFogEntityAtCoord;

			// List<Vector2Int> sharedManualCoordinates;

			foreach (var coord in dwarfEntity.navigationComponent.foundUnreachableEdges)
			{
				navCoord = navArray[coord.x, coord.y];
				unreachableNavCoord = unreachableNavArray[coord.x, coord.y];
				rockTileEntityAtCoord = levelEntity.rockTiles[coord.x, coord.y];
				fullFogEntityAtCoord = levelEntity.fullFogTiles[coord.x, coord.y];

				if (navCoord == null || unreachableNavCoord == null)
					throw new System.Exception("An unreachable edge should be on both coordinate maps.");

				// TODO: This change here to also check freespacemovertiles was key at resolving the Tutorial Room 2 dwarves stacking in a narrow corridor issue
				if (
					(levelEntity.reservedByFreeSpaceMoverTiles[coord.x, coord.y] == null && levelEntity.reservedByFreeSpaceMoverTiles[coord.x, coord.y] == null) // Don't go on reserved tiles
					)
				{
					if (!LogicHelpers.IsShorterPathOrPenetrationWithUnreachable(navCoord, unreachableNavCoord, shortestPathLength, shortestHPPenetrationPathLength, shortestUnreachablePathLength, shortestUnreachableHPPenetrationPathLength))
					{
						shortestPathLength = navCoord.pathLength;
						shortestHPPenetrationPathLength = navCoord.HPPenetrationLength;
						shortestUnreachablePathLength = unreachableNavCoord.pathLength;
						shortestUnreachableHPPenetrationPathLength = unreachableNavCoord.HPPenetrationLength;
						// highestMiningValue = miningNodeRockTileEntity.miningValue;

						bestCoordFound = true;
						bestCoord = coord;
					}
				}
			}
		}

		if (!bestCoordFound && gameStateEntity.autoMine)
		{
			// First try to pick a valuable mining node
			foreach (var candidateMiningNode in foundMiningNodes)
			{
				miningNodeNavCoord = navArray[candidateMiningNode.x, candidateMiningNode.y];
				miningNodeRockTileEntity = levelEntity.rockTiles[candidateMiningNode.x, candidateMiningNode.y];

				if (!LogicHelpers.IsHigherMiningValueOrShorterPathOrPenetration(miningNodeNavCoord, miningNodeRockTileEntity, shortestPathLength, shortestHPPenetrationPathLength, highestMiningValue))
				{
					shortestPathLength = miningNodeNavCoord.pathLength;
					shortestHPPenetrationPathLength = miningNodeNavCoord.HPPenetrationLength;
					highestMiningValue = miningNodeRockTileEntity.miningValue;

					bestCoordFound = true;
					bestMiningNodeFound = true;
					bestCoord = candidateMiningNode;
				}
			}
		}

		// If auto mine is on AND (if no good destination found OR (if no valuable mining nodes found, then prefer to explore fog))
		if (gameStateEntity.autoMine &&
			(!bestCoordFound ||
			(
				bestMiningNodeFound 
				&& (Mathf.Approximately((float)highestMiningValue, 0f) || highestMiningValue < 0d))
			)
			)
		{
			if (foundFullFogs.Count > 0)
			{
				NavCoord fullFogNavCoord;

				// NOTE: This going to fogs over going to mining nodes already found mechanism requires these variables to be reset, since they may be dirty from looking for mining nodes
				shortestPathLength = float.MaxValue;
				shortestHPPenetrationPathLength = double.MaxValue; // NOTE: This value should go unused, all the fog penetration lengths should be 0 based on the ConsiderAndAddFullFog logic

				foreach (var candidateFullFog in foundFullFogs)
				{
					fullFogNavCoord = navArray[candidateFullFog.x, candidateFullFog.y];

					if (!LogicHelpers.IsShorterPathOrPenetration(fullFogNavCoord, shortestPathLength, shortestHPPenetrationPathLength))
					{
						shortestPathLength = fullFogNavCoord.pathLength;
						shortestHPPenetrationPathLength = fullFogNavCoord.HPPenetrationLength;

						bestCoordFound = true;
						bestCoord = candidateFullFog;
					}
				}
			}
		}

		if (bestCoordFound)
		{
			// NOTE: The below code creates the dwarf's intended navPath. Importantly, the nav path ends at the first spot the dwarf is trying to penetrate a mining node.
			reverseNavPathHolder.Clear();

			int loopControl = 0;

			Vector2Int prevReverseNavPathCoord;
			NavCoord prevNavArrayCoord;
			Vector2Int reverseNavPathCoord;
			NavCoord navArrayCoord;

			bool intendedAttackTargetCoordAssigned = false;

			prevReverseNavPathCoord = bestCoord;
			prevNavArrayCoord = navArray[prevReverseNavPathCoord.x, prevReverseNavPathCoord.y];

			// If the bestCoord looks walkable to, it can be the destination
			// TODO: refactor this, the loop should know to add the first coord... if it's a free path. prev should be redefined as current.
			if (Mathf.Approximately((float)prevNavArrayCoord.HPPenetrationLength, 0f) && LogicHelpers.CheckTileMayBePassable(prevReverseNavPathCoord, levelEntity))
				reverseNavPathHolder.Add(prevReverseNavPathCoord);

			reverseNavPathCoord = prevNavArrayCoord.prevCoord;
			navArrayCoord = navArray[reverseNavPathCoord.x, reverseNavPathCoord.y];

			// Reverse through the nav path to the desired mining node / full fog
			// Look for when the nav path starts trying to penetrate a wall, record that as the target, and then start recording the dwarf's nav path


			// NOTE: When this loop works in the full fog case, HPPenetrationLength should not come into play
			while (
				prevReverseNavPathCoord != startingCoord 
				&& (dwarfEntity.unitStateComponent.unitState != UnitState.Moving || prevReverseNavPathCoord != startingCoordSecondaryFromMovement)
				&& loopControl < ValueHolder.MAXIMUM_SEARCH_LOOPS)
			{
				loopControl++;

				if (Mathf.Approximately((float)prevNavArrayCoord.HPPenetrationLength, 0f))
				{
					reverseNavPathHolder.Add(reverseNavPathCoord);
				}
				// If the previous coordinate had HPPenetration, but the next one does not - then this is the point where this dwarf intends to penetrate
				else if(prevNavArrayCoord.HPPenetrationLength > 0d && Mathf.Approximately((float)navArrayCoord.HPPenetrationLength, 0f))
				{
					if (!intendedAttackTargetCoordAssigned)
					{
						intendedAttackTargetCoordAssigned = true;
						dwarfEntity.candidateUnitAttackingStateComponent.intendedAttackTargetCoord = prevReverseNavPathCoord;
					}

					reverseNavPathHolder.Add(reverseNavPathCoord);
				}

				prevReverseNavPathCoord = reverseNavPathCoord;
				prevNavArrayCoord = navArrayCoord;
				reverseNavPathCoord = prevNavArrayCoord.prevCoord;
				navArrayCoord = navArray[reverseNavPathCoord.x, reverseNavPathCoord.y];
			}

			if (loopControl == ValueHolder.MAXIMUM_SEARCH_LOOPS)
			{
				dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking = false;
				dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = false;
				throw new System.Exception("Infinite loop while reversing nav path to dwarf.");
				// return;
			}

			dwarfEntity.candidateTileMovementStateComponent.navPath.Clear();
			for (int i = 0; i < reverseNavPathHolder.Count; i++)
			{
				dwarfEntity.candidateTileMovementStateComponent.navPath.Add(reverseNavPathHolder[reverseNavPathHolder.Count - 1 - i]);
			}

			// If we've determined a point where the dwarf would penetrate, then the dwarf intends to attack and must reserve a spot
			if (intendedAttackTargetCoordAssigned)
			{
				Vector2Int spotToAttackFromCandidate;

				dwarfEntity.candidateUnitAttackingStateComponent.hasIntendedAttackTarget = true;

				if (dwarfEntity.candidateTileMovementStateComponent.navPath.Count > 0)
				{
					dwarfEntity.candidateTileMovementStateComponent.hasIntendedMovementTarget = true;
					spotToAttackFromCandidate = dwarfEntity.candidateTileMovementStateComponent.navPath[dwarfEntity.candidateTileMovementStateComponent.navPath.Count - 1];
				}
				else
				{
					dwarfEntity.candidateTileMovementStateComponent.hasIntendedMovementTarget = false;
					spotToAttackFromCandidate = dwarfEntity.coord;
				}

				dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord = spotToAttackFromCandidate;

				// TODO: kind of weird that we use the resultant tile movement state component to reserve. But it makes sense. That's guaranteed to be the reserve spot
				levelEntity.reservedByAttackerTiles[dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.y] = dwarfEntity.tileMovementStateComponent;
				// NOTE: This seemed to be necessary for fixing the narrow corridor in tutorial level 2 issue, but it's not clear why
				levelEntity.reservedByFreeSpaceMoverTiles[dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.y] = dwarfEntity.tileMovementStateComponent;
			}
			else
			{
				dwarfEntity.candidateUnitAttackingStateComponent.hasIntendedAttackTarget = false;
				dwarfEntity.candidateTileMovementStateComponent.hasIntendedMovementTarget = true;

				Vector2Int spotToFreeSpaceStandOnCandidate;

				if (dwarfEntity.candidateTileMovementStateComponent.navPath.Count > 0)
					spotToFreeSpaceStandOnCandidate = dwarfEntity.candidateTileMovementStateComponent.navPath[dwarfEntity.candidateTileMovementStateComponent.navPath.Count - 1];
				else
					spotToFreeSpaceStandOnCandidate = dwarfEntity.coord;

				dwarfEntity.candidateTileMovementStateComponent.spotToFreeSpaceStandOnCoord = spotToFreeSpaceStandOnCandidate;

				levelEntity.reservedByFreeSpaceMoverTiles[spotToFreeSpaceStandOnCandidate.x, spotToFreeSpaceStandOnCandidate.y] = dwarfEntity.tileMovementStateComponent;

				// NOTE: For now, just moving to a spot without intent to attack does not reserve spots
			}

			// TODO: there's a bug as seen in January 23rd screenshot on your desktop - multiple dwarves stacked on mining one copper. somehow the spot 14, 6 was not reserved, or it was ignored.
			// The unit test ThreeDwarvesOneAccessibleCopperSpot() was created to assess this but did not catch an issue.
		}
		else
		{
			// TODO: This is a quick fix. It says "something's gone wrong with my previous navigation plan. Let me try to keep using it and remove any final stretch I can't walk across anymore.
			ReevaluateAndShortenPreviousNavpathDueToNoNewBestCoordsFound(dwarfEntity);

			// Originally, here the code would just say that there isn't a candidate action primed for checking, both attack and movement wise. That's also wrong - changing board state mandates reevaluation
			// Simply stopping the dwarf in place is also silly - it's nicer that the dwarf goes up as far it could
			//dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking = false;
			//dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = false;
		}
	}

	void ReevaluateAndShortenPreviousNavpathDueToNoNewBestCoordsFound(DwarfEntity dwarfEntity)
	{
		dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking = false;

		bool canPathStill = true;
		bool unusablePortionFound = false;
		int beginningOfUnusablePortion = 0;
		bool isDiagonalPath;
		bool isCoordWalkable;
		Vector2Int coord;
		Vector2Int previousCoord;

		dwarfEntity.candidateTileMovementStateComponent.navPath.Clear();

		foreach (var item in dwarfEntity.tileMovementStateComponent.navPath)
		{
			dwarfEntity.candidateTileMovementStateComponent.navPath.Add(item);
		}

		if (dwarfEntity.candidateTileMovementStateComponent.navPath.Count > 0)
		{
			for (int i = dwarfEntity.candidateTileMovementStateComponent.navPath.Count - 1; i >= 1; i--)
			{
				coord = dwarfEntity.candidateTileMovementStateComponent.navPath[i];
				previousCoord = dwarfEntity.candidateTileMovementStateComponent.navPath[i - 1];

				isDiagonalPath = Mathf.Abs(coord.x - previousCoord.x) == 1 && Mathf.Abs(coord.y - previousCoord.y) == 1;
				isCoordWalkable = CheckTileWalkableXY(levelEntity, coord);

				canPathStill = true;

				if (isDiagonalPath)
				{
					// Dwarves must be able to walk across the diagonal spots as well

					if (!CheckTileWalkableXY(levelEntity, new Vector2Int(coord.x, previousCoord.y)))
						canPathStill = false;

					if (!CheckTileWalkableXY(levelEntity, new Vector2Int(previousCoord.x, coord.y)))
						canPathStill = false;
				}

				if (!isCoordWalkable)
					canPathStill = false;

				if (!canPathStill)
				{
					// dwarfEntity.candidateTileMovementStateComponent.navPath.RemoveAt(i);
					unusablePortionFound = true;
					beginningOfUnusablePortion = i;
				}
			}

			coord = dwarfEntity.candidateTileMovementStateComponent.navPath[0];
			isCoordWalkable = CheckTileWalkableXY(levelEntity, coord);

			if (!isCoordWalkable)
			{
				// dwarfEntity.candidateTileMovementStateComponent.navPath.RemoveAt(0);
				unusablePortionFound = true;
				beginningOfUnusablePortion = 0;
			}
		}

		if (!unusablePortionFound)
		{
			dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = false;
		}
		else
		{
			for (int i = dwarfEntity.candidateTileMovementStateComponent.navPath.Count - 1; i >= beginningOfUnusablePortion; i--)
				dwarfEntity.candidateTileMovementStateComponent.navPath.RemoveAt(i);

			dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = true;
			dwarfEntity.candidateTileMovementStateComponent.hasIntendedMovementTarget = true; // TODO: Needed?

			Vector2Int spotToFreeSpaceStandOnCandidate;
			if (dwarfEntity.candidateTileMovementStateComponent.navPath.Count > 0)
				spotToFreeSpaceStandOnCandidate = dwarfEntity.candidateTileMovementStateComponent.navPath[dwarfEntity.candidateTileMovementStateComponent.navPath.Count - 1];
			else
				spotToFreeSpaceStandOnCandidate = dwarfEntity.coord;

			dwarfEntity.candidateTileMovementStateComponent.spotToFreeSpaceStandOnCoord = spotToFreeSpaceStandOnCandidate;
			levelEntity.reservedByFreeSpaceMoverTiles[spotToFreeSpaceStandOnCandidate.x, spotToFreeSpaceStandOnCandidate.y] = dwarfEntity.tileMovementStateComponent;
		}
	}

	public override void SystemUpdate()
	{
		for (int i = 0; i < levelEntity.xSize; i++)
			for (int j = 0; j < levelEntity.ySize; j++)
			{
				levelEntity.reservedByAttackerTiles[i, j] = null;
				levelEntity.reservedByFreeSpaceMoverTiles[i, j] = null;
			}

		foreach (var dwarfEntity in dwarfEntities)
		{
			if (!dwarfEntity.tutorialDisabledCandidateActionProcessing && !dwarfEntity.hasExitedLevel)
			{
				if (dwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget)
				{
					// TODO: Hack to allow ranged dwarves to cooperate more nicely with melee dwarves. This would be a non-issue if ranged dwarves had ranged logic for identifying spots to attack from
					// if (dwarfEntity.unitAttackingStateComponent.attackRange <= 1 || dwarfEntity.unitStateComponent.unitState == UnitState.Attacking)
					{
						levelEntity.reservedByAttackerTiles[dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord.y] = dwarfEntity.tileMovementStateComponent;
						levelEntity.reservedByFreeSpaceMoverTiles[dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.y] = dwarfEntity.tileMovementStateComponent;
					}
					//else if (dwarfEntity.unitStateComponent.unitState == UnitState.Attacking)
					//{

					//}
				}
				else // if (dwarfEntity.tileMovementStateComponent.hasIntendedMovementTarget)
				{
					levelEntity.reservedByFreeSpaceMoverTiles[dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord.x, dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord.y] = dwarfEntity.tileMovementStateComponent;
				}

				// TODO: dwarves that don't intend to attack or move into fog, but do intend to "move out of the way" should reserve spots
				// Specifically, dwarves that want to move into fog should NOT reserve spots, since it's not really their destination

					// TODO: dwarves that don't have an intended attack target that are standing on a resreved tile need a new system that moves them out of the way if possible
					// It's likely that reservedByAttackerTiles is the wrong name. Every dwarf needs a place to stand at the end of things if at all possible.
			}
		}

		foreach (var dwarfEntity in dwarfEntities)
		{
			if (!dwarfEntity.tutorialDisabledCandidateActionProcessing && !dwarfEntity.hasExitedLevel)
			{
				if (
				(dwarfEntity.unitStateComponent.unitState == UnitState.Idle && (gameStateEntity.autoMine || levelEntity.sharedManualCoordinates.Count > 0 || dwarfEntity.navigationComponent.levelDataWasDirtied)) // TODO: This condition is hacky. But it can't be jsut "playerCoordinatesChanged" or dwarves knock down a node and then stand there
				|| (dwarfEntity.unitStateComponent.unitState == UnitState.Moving && (gameStateEntity.playerCoordinatesChanged || dwarfEntity.navigationComponent.levelDataWasDirtied))
				|| (dwarfEntity.unitStateComponent.unitState == UnitState.Attacking && dwarfEntity.unitAttackingStateComponent.attackBackswingFinishedPointPerformed && (gameStateEntity.playerCoordinatesChanged || dwarfEntity.navigationComponent.levelDataWasDirtied))
				)
				{
					// Unreserve this dwarf's spot for its own consideration. This makes dwarves not jaunt out of their own attacking spot if the user once again assigns to attack the same node.
					// NOTE: Careful, cannot just set it to null. Make sure someone else didn't reserve it already!
					TileMovementStateComponent reservedByAttackerTileOnDwarfsSpot = levelEntity.reservedByAttackerTiles[dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord.y];
					if (Object.ReferenceEquals(dwarfEntity.tileMovementStateComponent, reservedByAttackerTileOnDwarfsSpot))
					{
						levelEntity.reservedByAttackerTiles[dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord.y] = null;
						levelEntity.reservedByFreeSpaceMoverTiles[dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.x, dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord.y] = null;
					}

					// Same as above
					TileMovementStateComponent reservedByFreeSpaceMoverTileOnDwarfsSpot = levelEntity.reservedByFreeSpaceMoverTiles[dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord.x, dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord.y];
					if (Object.ReferenceEquals(dwarfEntity.tileMovementStateComponent, reservedByFreeSpaceMoverTileOnDwarfsSpot))
						levelEntity.reservedByFreeSpaceMoverTiles[dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord.x, dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord.y] = null;

					// TODO: see if there's a way to calculate some of this stuff just once, and not for all dwarves. Problem with unreachable is that attack reservations can make areas unreachable
					PrepareNavigationComponent(dwarfEntity);

					// NOTE: regardless of the level data dirtying source, it may be correct to recalculate the "unreachable" zone. Since after all, this zone does change from other sources of level dirtiness

					// TODO: Originally PrepareLevelFogEdgeArrayForUnreachableManualNavigation() was intended to be called first, but it relies on navArray of dwarfEntities[0] being populated already for identifying "foundUnreachableEdge"... all in all, not happy with this
					PrepareLevelFogEdgeArrayForUnreachableManualNavigation(dwarfEntity);

					PrepareDwarfFogEdgeArrayForUnreachableManualNavigation(dwarfEntity);

					SetCandidateActionForBestDestination(dwarfEntity);
					dwarfEntity.navigationComponent.levelDataWasDirtied = false;
				}
			}

			// TODO: This system can know the following: to find a coord that is permanently unreachable (but not due to mining issues, right? So off the first dwarf?
			// Actually, hard to determine permanently unreachable with current implementation, we'll see
			// Or should this... fade out such coordinates? Maybe the death of coordinates needs to always be a fade out with different timings

			// TODO: Prevntion of dwarves piling up while trying to get to an unreachable coord
		}
	}
}
