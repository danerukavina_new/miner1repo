﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDamageInstanceResolutionSystem : BaseSystem
{
	public List<AttackDamageInstance> attackDamageInstances;
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public PlayerStateEntity playerStateEntity;
	public List<DwarfEntity> dwarfEntities;
	public List<HPBarEntity> HPBarEntities;
	public HPBarEntity HPBarEntityPrefab;
	public GameObject unitProgressBars;
	public List<RockDamageOrDeathAnimationEntity> rockDeathAnimationEntities;
	public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDeathAnimationPrefabs;
	public GameObject rockDeathAnimationsObject;
	public List<RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationEntities;
	public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationPrefabs;
	public GameObject rockDamagedStageAnimationsObject;

	public List<DamageOrBuffTextEntity> damageOrBuffTextEntities;
	public Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs;
	public GameObject damageOrBuffTexts;

	Vector2Int coord;
	RockTileEntity damageRockTileEntity;
	DamageOutcome damageOutcome;
	AttackEntity ownerAttackEntity;

	public static DamageOutcome DamageFunction(AttackEntity attackEntity, RockTileEntity attackDamageTargetRockTile, GameStateEntity gameStateEntity)
	{
		if (!attackDamageTargetRockTile.destructible)
			return new DamageOutcome()
			{
				damageOutcomeType = DamageOutcomeType.ImmuneNoDamage,
				damage = 0d,
				canCauseProcs = false
			};

		UnitStatsComponent ownerUnitAttackStats = attackEntity.owner.unitStatsComponent;

		double damageBuilder = attackEntity.baseDamage * ownerUnitAttackStats.finalStats.damageFactor;

		// TODO: Ignoring armor should be a component on attacks, not inherit in the type
		switch (attackEntity.attackType)
		{
			case AttackType.Chain:
			case AttackType.BasicProjectile:
				break;
			default:
				damageBuilder -= (attackDamageTargetRockTile.toughnessArmor - attackDamageTargetRockTile.buffReceiverComponent.accumulatedBuffStats.toughnessArmorFlatReduction);
				break;
		}

		// TODO: The damageAmpBonus is used in two ways here. That's a little unclear. In fact the dwarf one is unused right now. It should be two different stats probably.
		damageBuilder *= (1d + ownerUnitAttackStats.finalStats.damageAmpBonus);

		damageBuilder *= (1d + attackDamageTargetRockTile.buffReceiverComponent.accumulatedBuffStats.damageAmpBonus);

		damageBuilder = StatsAndDamageHelpers.RoundWithPrecision(damageBuilder, ValueHolder.ATTACK_DAMAGE_DECIMAL_PRECISION);

		if (damageBuilder <= 0d)
		{
			return new DamageOutcome()
			{
				damageOutcomeType = DamageOutcomeType.ToughnessArmorNoDamage,
				damage = 0d,
				canCauseProcs = false
			};
		}

		float p = UnityEngine.Random.Range(0f, 1f);
		float pSum = 0f;

		if (attackDamageTargetRockTile.smeltable)
		{
			pSum += (float)ownerUnitAttackStats.finalStats.smeltChance;
			if (
				(gameStateEntity.tutorialSmeltControlSetting == TutorialSmeltControlSetting.None && p < pSum)
				|| (gameStateEntity.tutorialSmeltControlSetting == TutorialSmeltControlSetting.AlwaysSmeltIfSmeltable && ownerUnitAttackStats.finalStats.smeltChance > double.Epsilon)
				)
				return new DamageOutcome()
				{
					damageOutcomeType = DamageOutcomeType.Smelt,
					damage = damageBuilder,
					canCauseProcs = true
				};
		}

		pSum += (float)ownerUnitAttackStats.finalStats.critChance;
		if (p < pSum)
		{
			double critDamage = StatsAndDamageHelpers.RoundWithPrecision(damageBuilder * (1d + ownerUnitAttackStats.finalStats.critBonusDamageFactor), ValueHolder.ATTACK_DAMAGE_DECIMAL_PRECISION);

			return new DamageOutcome()
			{
				damageOutcomeType = DamageOutcomeType.CritDamage,
				damage = critDamage,
				canCauseProcs = true
			};
		}

		return new DamageOutcome()
		{
			damageOutcomeType = DamageOutcomeType.BasicDamage,
			damage = damageBuilder,
			canCauseProcs = true
		};
	}

	public void PerformProcLogic(ProcData procData, DwarfEntity ownerDwarfEntity, RockTileEntity damageRockTileEntity, int buffLevel)
	{
		switch (procData.procBuffApplicationType)
		{
			case ProcBuffApplicationType.Self:
				throw new System.Exception("Direct to self buff procs NYI");
			case ProcBuffApplicationType.AttackReceiver:
				// TODO: Why do I need a special constructor here for PrototypeBuff? Seems wrong
				damageRockTileEntity.buffReceiverComponent.receivedPrototypeBuffsThisFixedUpdate.Add(new PrototypeBuff(procData.possibleProcBuffType.Value, buffLevel)); 
				break;
			case ProcBuffApplicationType.BuffGiverAreaCreator:
				// TODO: This is inconsistent with the case above. Either the buffGiverArea also needs to contemplate level or whatever data, or the direct buff applyer shouldn't consider level.
				ownerDwarfEntity.buffGiverAreaCreatorComponent.buffGiverAreasToBeCreated.Add(procData.possibleProcBuffGiverAreaType.Value);
				break;
			default:
				break;
		}

		if (procData.hasInternalCooldown)
		{
			procData.isInternallyCoolingdown = true;
			procData.internalCooldownStartTime = Time.time;
		}
	}

	public void AttemptToCauseAttackProcs(DamageOutcome damageOutcome, DwarfEntity ownerDwarfEntity, AttackEntity ownerAttackEntity, RockTileEntity damageRockTileEntity)
	{
		foreach (var procData in ownerDwarfEntity.procOnAttackComponent.procDataOnAttack)
		{
			// TODO: This should probably be its own system
			if (procData.isInternallyCoolingdown && procData.internalCooldownStartTime + procData.internalCooldownDuration < Time.time)
				procData.isInternallyCoolingdown = false;

			if (
				!procData.isInternallyCoolingdown
				&& (procData.triggerRequiredDamageOutcomeTypeConstraints == null || procData.triggerRequiredDamageOutcomeTypeConstraints.Contains(damageOutcome.damageOutcomeType))
				&& (procData.triggerRequiredAttackTypeConstraints == null || procData.triggerRequiredAttackTypeConstraints.Contains(ownerAttackEntity.attackType))
				)

			{
				float p = UnityEngine.Random.Range(0f, 1f);

				if (p < procData.procChance)
				{
					PerformProcLogic(procData, ownerDwarfEntity, damageRockTileEntity, ValueHolder.TEMPORARY_FIXED_BUFF_LEVEL);
				}
			}
		}
	}

	private enum RockTileDamageOutcomeType
	{
		RegularDamage,
		DamagedStage,
		Death
	}

	private static RockTileDamageOutcomeType DamageOutcomeWillCauseDamagedStageOrDeath(DamageOutcome damageOutcome, RockTileEntity damageRockTileEntity)
	{
		if (damageOutcome.damageOutcomeType == DamageOutcomeType.Smelt)
		{
			if (damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds.Count > damageRockTileEntity.rockDamagedStageComponent.thresholdStage)
				return RockTileDamageOutcomeType.DamagedStage;

			return RockTileDamageOutcomeType.Death;
		}
			
		double newHP = damageRockTileEntity.HP - damageOutcome.damage;

		if (damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds.Count > damageRockTileEntity.rockDamagedStageComponent.thresholdStage)
		{
			if (newHP / damageRockTileEntity.maxHP <= damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds[damageRockTileEntity.rockDamagedStageComponent.thresholdStage])
				return RockTileDamageOutcomeType.DamagedStage;

			return RockTileDamageOutcomeType.RegularDamage;

		}

		if (newHP <= 0d)
			return RockTileDamageOutcomeType.Death;

		return RockTileDamageOutcomeType.RegularDamage;
	}

	private RockTileDamageOutcomeType ProcessRockTileDamageSetResourceGainsAndDetermineOutcomeType()
	{
		return RockTileDamageOutcomeType.DamagedStage;
	}

	public override void SystemUpdate()
	{
		// TODO: improve this by building a list of candidate entities to check death / that are known to have died. No reason to check everything.
		// In DOTS this would mean adding a death component possibly?

		bool someRockTileEntityDied = false;

		foreach (var attackDamageInstance in attackDamageInstances)
		{
			damageRockTileEntity = attackDamageInstance.damageRockTileEntity;

			if (damageRockTileEntity != null && damageRockTileEntity.destructible)
			{
				ownerAttackEntity = attackDamageInstance.ownerAttackEntity;

				damageOutcome = DamageFunction(ownerAttackEntity, damageRockTileEntity, gameStateEntity);

				EntityHelpers.AddDamageText(
					damageOutcome.damageOutcomeType,
					damageOutcome.damage,
					1f,
					ownerAttackEntity.coord,
					damageRockTileEntity.coord,
					damageOrBuffTextEntities,
					damageOrBuffTextPrefabs,
					damageOrBuffTexts
					);

				// TODO: This canCauseProcs check is probably bad code. The triggerRequiredDamageOutcomeTypeConstraints implementation is probably better and should assess the damageOutcome and decide.
				if (damageOutcome.canCauseProcs)
					AttemptToCauseAttackProcs(damageOutcome, ownerAttackEntity.owner, ownerAttackEntity, damageRockTileEntity);

				if (damageOutcome.damage > 0d)
				{
					RockTileDamageOutcomeType damageOutcomeWillCauseDamagedStageOrDeath = RockTileDamageOutcomeType.RegularDamage;

					// TODO: Add in logic for the resource gained based on if it's smelt or if it's regular damage threshold passing

					double outcomeHP = damageRockTileEntity.HP - damageOutcome.damage;

					if (damageOutcome.damageOutcomeType == DamageOutcomeType.Smelt)
					{
						if (damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds.Count > damageRockTileEntity.rockDamagedStageComponent.thresholdStage)
						{

							double outcomeHPCandidate = // - double.Epsilon +  // TODO: consider adding this epsilon so that the outcome HP ends below the threshold
								damageRockTileEntity.maxHP * damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds[damageRockTileEntity.rockDamagedStageComponent.thresholdStage];

							outcomeHP = outcomeHP < outcomeHPCandidate ? outcomeHP : outcomeHPCandidate;
						}
						else
						{
							outcomeHP = 0d;
						}
					}


					while (damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds.Count > damageRockTileEntity.rockDamagedStageComponent.thresholdStage
						&& outcomeHP / damageRockTileEntity.maxHP <= damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds[damageRockTileEntity.rockDamagedStageComponent.thresholdStage])
					{
						damageRockTileEntity.rockDamagedStageComponent.thresholdStage++;

						damageOutcomeWillCauseDamagedStageOrDeath = RockTileDamageOutcomeType.DamagedStage;

						playerStateEntity.playerResourcesComponent.candidateResourceAdditions.Add(new CandidateResourceAddition()
						{
							sourceRockTileType = damageRockTileEntity.rockTileType,
							coord = damageRockTileEntity.coord,
							isSmeltEffect = damageOutcome.damageOutcomeType == DamageOutcomeType.Smelt
						});
					}

					if (outcomeHP <= 0d)
					{
						damageRockTileEntity.HP = outcomeHP; // NOTE: Not needed to set HP in this case, since Death deletes it. But in case it becomes important, better to have it here already.

						damageOutcomeWillCauseDamagedStageOrDeath = RockTileDamageOutcomeType.Death;

						playerStateEntity.playerResourcesComponent.candidateResourceAdditions.Add(new CandidateResourceAddition()
						{
							sourceRockTileType = damageRockTileEntity.rockTileType,
							coord = damageRockTileEntity.coord,
							isSmeltEffect = damageOutcome.damageOutcomeType == DamageOutcomeType.Smelt
						});
					}
					else if (damageRockTileEntity.HP > outcomeHP) // TODO: Is this a redundant check to see if actual damage was done? Does this condition ever fail? See debug log below.
					{
						damageRockTileEntity.HP = outcomeHP;

						if (damageRockTileEntity.HPBarEntity == null)
							EntityHelpers.AddHPBar(damageRockTileEntity, HPBarEntities, HPBarEntityPrefab, unitProgressBars);

						damageRockTileEntity.HPBarEntity.HPBarFillXScaler.transform.localScale = new Vector3((float)(damageRockTileEntity.HP / damageRockTileEntity.maxHP), 1f, 1f);
					}
					else
					{
						Debug.Log("Unexpected 0 damage done even though that was already checked, investigate."); // See TODO above
					}

					switch (damageOutcomeWillCauseDamagedStageOrDeath)
					{
						case RockTileDamageOutcomeType.RegularDamage:
							break;
						case RockTileDamageOutcomeType.DamagedStage:
							EntityHelpers.AddRockDamageOrDeathAnimation(damageRockTileEntity.rockTileType, damageRockTileEntity.coord, rockDamagedStageAnimationEntities, rockDamagedStageAnimationPrefabs, rockDamagedStageAnimationsObject);

							damageRockTileEntity.spriteRenderer.sprite = damageRockTileEntity.rockDamagedStageComponent.rockDamagedStageSprites[damageRockTileEntity.rockDamagedStageComponent.thresholdStage];
							break;
						case RockTileDamageOutcomeType.Death:
							if (damageRockTileEntity.HPBarEntity != null)
								EntityHelpers.RemoveHPBar(damageRockTileEntity.HPBarEntity, HPBarEntities);

							EntityHelpers.AddRockDamageOrDeathAnimation(damageRockTileEntity.rockTileType, damageRockTileEntity.coord, rockDeathAnimationEntities, rockDeathAnimationPrefabs, rockDeathAnimationsObject);

							// TODO: Perhaps a separate system should be handling cleanup of player inputs
							// TODO: Should dirty the gamestate entity... maybe
							EntityHelpers.RemoveSharedManualCoordinate(new Vector2Int(damageRockTileEntity.coord.x, damageRockTileEntity.coord.y), levelEntity);

							EntityHelpers.RemoveRock(damageRockTileEntity.coord, levelEntity);

							someRockTileEntityDied = true;
							break;
						default:
							throw new System.Exception("Impossible damage instance result.");
					}
				}
			}
		}

		attackDamageInstances.Clear();

		if (someRockTileEntityDied)
		{
			foreach (var dwarfEntity in dwarfEntities)
			{
				dwarfEntity.navigationComponent.levelDataWasDirtied = true;
			}
		}
	}
}
