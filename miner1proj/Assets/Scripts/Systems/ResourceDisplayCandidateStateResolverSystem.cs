﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceDisplayCandidateStateResolverSystem : BaseSystem
{
	public PlayerStateEntity playerStateEntity;
	public Dictionary<ResourceType, Sprite> resourceSprites;
	public ResourceDisplayCanvasEntity resourceDisplayCanvasEntity;

	ResourceType candidateGainedResourceType;
	Dictionary<ResourceType, int> amountOfResourcesAdded;

	static void SetRsourceDisplayItemPanelEntity(ResourceDisplayItemPanelEntity resourceDisplayItemPanelEntity, ResourceEntity resourceEntity, Dictionary<ResourceType, Sprite> resourceSprites)
	{
		resourceDisplayItemPanelEntity.gameObject.SetActive(true);
		resourceDisplayItemPanelEntity.resourceSpriteRenderer.sprite = resourceSprites[resourceEntity.resourceType];
		resourceDisplayItemPanelEntity.amountTextMeshPro.SetText(resourceEntity.amount.ToString());
	}

	public override void SystemUpdate()
	{
		if (resourceDisplayCanvasEntity.attemptRefreshResourceDisplay)
		{
			resourceDisplayCanvasEntity.attemptRefreshResourceDisplay = false;

			bool updateResourceDisplayItemPanelEntities = false;

			// Always process dwarf upgrade
			// Supress resource addition if display state is currently in dwarf upgrade

			switch (resourceDisplayCanvasEntity.candidateResourceDisplayState)
			{
				case ResourceDisplayCanvasEntity.ResourceDisplayState.Hide:
					resourceDisplayCanvasEntity.resourceDisplayState = resourceDisplayCanvasEntity.candidateResourceDisplayState;
					resourceDisplayCanvasEntity.moveToShow = false;
					resourceDisplayCanvasEntity.entryStartTime = float.MinValue; //  TODO: This is a hack that makes the resource display start the hiding animation immediately, improve it. entryStartTime should not be carrying out the Hide state's job
					break;
				case ResourceDisplayCanvasEntity.ResourceDisplayState.ResourceAddition:
					if (resourceDisplayCanvasEntity.resourceDisplayState != ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgrade)
						resourceDisplayCanvasEntity.resourceDisplayState = resourceDisplayCanvasEntity.candidateResourceDisplayState;

					resourceDisplayCanvasEntity.moveToShow = true;
					resourceDisplayCanvasEntity.entryStartTime = Time.time;

					updateResourceDisplayItemPanelEntities = true;
					break;
				case ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgrade:
				case ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgradeResourceSubtraction:
					resourceDisplayCanvasEntity.resourceDisplayState = resourceDisplayCanvasEntity.candidateResourceDisplayState;
					resourceDisplayCanvasEntity.moveToShow = true;
					resourceDisplayCanvasEntity.entryStartTime = Time.time;

					updateResourceDisplayItemPanelEntities = true;
					break;
				default:
					break;
			}

			if (updateResourceDisplayItemPanelEntities)
			{
				int i = 0;
				ResourceEntity resourceEntity;

				switch (resourceDisplayCanvasEntity.resourceDisplayState)
				{
					case ResourceDisplayCanvasEntity.ResourceDisplayState.ResourceAddition:

						for (; i < resourceDisplayCanvasEntity.resourceDisplayItemPanelEntities.Length && i < playerStateEntity.playerResourcesComponent.recentlyAddedResources.Count(); i++)
						{
							if (i < playerStateEntity.playerResourcesComponent.recentlyAddedResources.Count())
							{
								resourceEntity = playerStateEntity.playerResourcesComponent.resources[
									playerStateEntity.playerResourcesComponent.recentlyAddedResources[i]
									];

								SetRsourceDisplayItemPanelEntity(resourceDisplayCanvasEntity.resourceDisplayItemPanelEntities[i], resourceEntity, resourceSprites);
							}
						}

						break;
					case ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgrade:
					case ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgradeResourceSubtraction:

						SetRsourceDisplayItemPanelEntity(resourceDisplayCanvasEntity.resourceDisplayItemPanelEntities[0], playerStateEntity.playerResourcesComponent.resources[ResourceType.Stone], resourceSprites);
						i++;

						break;
					case ResourceDisplayCanvasEntity.ResourceDisplayState.Hide:
					default:
						throw new System.Exception("Unexpected resource display mode when refreshing display of resources.");
				}

				for (; i < resourceDisplayCanvasEntity.resourceDisplayItemPanelEntities.Length; i++)
				{
					resourceDisplayCanvasEntity.resourceDisplayItemPanelEntities[i].gameObject.SetActive(false);
				}
			}
		}
	}
}
