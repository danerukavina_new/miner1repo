﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMovementFromTargetSystem : BaseSystem
{
	public List<AttackEntity> attackEntities;

	public override void SystemUpdate()
	{
		foreach (var attackEntity in attackEntities)
		{
			if (attackEntity.tileMovementStateComponent != null && attackEntity.tileMovementStateComponent.readyForNextCoord)
			{
				attackEntity.tileMovementStateComponent.readyForNextCoord = false;

				attackEntity.coord = attackEntity.tileMovementStateComponent.nextCoord;

				attackEntity.tileMovementStateComponent.nextCoord = LogicHelpers.NextCoordForAttack(attackEntity);
			}
		}
	}
}
