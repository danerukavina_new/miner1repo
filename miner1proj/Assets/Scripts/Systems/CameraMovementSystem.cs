﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public CameraInputCanvasEntity cameraInputCanvasEntity;
	public Sprite cameraArrowGridAlignedDeactivated;
	public Sprite cameraArrowGridAlignedActivated;
	public Sprite cameraArrowDiagonalDeactivated;
	public Sprite cameraArrowDiagonalActivated;
	public GameObject cameraIndicationCanvasEntity;

	Vector3 normalizedPlayerClickInputPositionDelta;
	Vector2Int inputDirection;
	Vector2 localPoint;

	private Vector2Int EightDirectionFromVector3(Vector3 delta)
	{
		float delX = delta.x;
		float delY = delta.y;

		// NOTE: Adding half of the camera angle in the following sense: the sector for the "right" direction is from -22.5 degrees to 22.5 degrees. The -22.5 degree that should be included is accounted for by this addition.
		float parameterAngle = (float)((Mathf.Atan2(delY, delX) / (2 * Mathf.PI))) + ValueHolder.PARAMETER_ANGLE_OF_CAMERA_DIRECTION / 2f;
		if (parameterAngle < 0f)
			parameterAngle += 1f;

		int index = Mathf.FloorToInt(parameterAngle * ValueHolder.eightWayMovementDirectionsRadiallyOrdered.Count);

		return ValueHolder.eightWayMovementDirectionsRadiallyOrdered[index];
	}

	private void InputStartLogic()
	{
		switch (gameStateEntity.cameraPressAndHoldControlType)
		{
			case CameraPressAndHoldControlType.None:
				break;
			case CameraPressAndHoldControlType.Joystick:
				RectTransformUtility.ScreenPointToLocalPointInRectangle(
					cameraInputCanvasEntity.ownRectTransform,
					gameStateEntity.playerClickInputStartScreenPosition,
					mainCamera,
					out localPoint
					);

				cameraInputCanvasEntity.cameraJoystickOrigin.anchoredPosition = localPoint;

				cameraInputCanvasEntity.gameObject.SetActive(true);
				break;
			case CameraPressAndHoldControlType.ClickAndDrag:

				cameraIndicationCanvasEntity.SetActive(true);
				break;
			default:
				throw new System.Exception("Unexpected camera control type.");
		}
	}

	private void InputUpdateLogic()
	{
		switch (gameStateEntity.cameraPressAndHoldControlType)
		{
			case CameraPressAndHoldControlType.None:
				break;
			case CameraPressAndHoldControlType.Joystick:
				RectTransformUtility.ScreenPointToLocalPointInRectangle(
				cameraInputCanvasEntity.ownRectTransform,
				gameStateEntity.playerClickInputCurrentScreenPosition,
				mainCamera,
				out localPoint
				);

				cameraInputCanvasEntity.cameraJoystickPosition.anchoredPosition = localPoint;

				normalizedPlayerClickInputPositionDelta = (gameStateEntity.playerClickInputCurrentScreenPosition - gameStateEntity.playerClickInputStartScreenPosition) / cameraInputCanvasEntity.ownRectTransform.rect.height;

				if (normalizedPlayerClickInputPositionDelta.sqrMagnitude >
					ValueHolder.CAMERA_MOVE_DEADZONE_SQUARED)
				{

					inputDirection = EightDirectionFromVector3(normalizedPlayerClickInputPositionDelta);

					mainCamera.transform.position = Vector3.MoveTowards(
						mainCamera.transform.position,
						mainCamera.transform.position + new Vector3(inputDirection.x, inputDirection.y),
						Time.deltaTime * gameStateEntity.baseCameraMovementSpeed
						);
				}
				else
					inputDirection = Vector2Int.zero;

				foreach (var directionArrowDirection in cameraInputCanvasEntity.directionArrows.Keys)
				{
					if (LogicHelpers.IsGridAligned(directionArrowDirection))
					{
						if (inputDirection == directionArrowDirection)
							cameraInputCanvasEntity.directionArrows[directionArrowDirection].sprite = cameraArrowGridAlignedActivated;
						else
							cameraInputCanvasEntity.directionArrows[directionArrowDirection].sprite = cameraArrowGridAlignedDeactivated;
					}
					else
					{
						if (inputDirection == directionArrowDirection)
							cameraInputCanvasEntity.directionArrows[directionArrowDirection].sprite = cameraArrowDiagonalActivated;
						else
							cameraInputCanvasEntity.directionArrows[directionArrowDirection].sprite = cameraArrowDiagonalDeactivated;
					}
				}
				break;
			case CameraPressAndHoldControlType.ClickAndDrag:
				Vector3 delta = mainCamera.ScreenToWorldPoint(gameStateEntity.playerClickInputCurrentScreenPosition) - mainCamera.ScreenToWorldPoint(gameStateEntity.playerClickInputStartScreenPosition);

				if (delta.sqrMagnitude > float.Epsilon)
					mainCamera.transform.position = gameStateEntity.playerClickInputStartCameraWorldPosition - delta;
				break;
			default:
				throw new System.Exception("Unexpected camera control type.");
		}
	}

	private void NoInputLogic()
	{
		switch (gameStateEntity.cameraPressAndHoldControlType)
		{
			case CameraPressAndHoldControlType.None:
				break;
			case CameraPressAndHoldControlType.Joystick:
				cameraInputCanvasEntity.gameObject.SetActive(false);
				break;
			case CameraPressAndHoldControlType.ClickAndDrag:
				cameraIndicationCanvasEntity.SetActive(false);
				break;
			default:
				throw new System.Exception("Unexpected camera control type.");
		}

		if (gameStateEntity.softCameraPositionBounds.Contains(mainCamera.transform.position))
			return;

		Vector3 pushCameraBackIntoBoundsDestination = gameStateEntity.softCameraPositionBounds.ClosestPoint(mainCamera.transform.position);

		mainCamera.transform.position = Vector3.MoveTowards(
			mainCamera.transform.position,
			pushCameraBackIntoBoundsDestination,
			Time.deltaTime * gameStateEntity.baseCameraMovementSpeed * ValueHolder.CAMERA_RETURN_TO_BOUNDS_MOVE_SPEED_FACTOR
			);
	}

	public override void SystemUpdate()
	{
		if (gameStateEntity.inputState == PlayerClickInputState.CameraMovement)
		{
			if (gameStateEntity.inputStateBecameCameraMovementThisUpdate)
			{
				gameStateEntity.inputStateBecameCameraMovementThisUpdate = false;

				InputStartLogic();
			}

			InputUpdateLogic();
		}
		else
		{
			NoInputLogic();
		}
	}
}
