﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBoundsCalculationSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	float widthFromAspectAndHeight, xMin, yMin, xMax, yMax;
	float centerX, centerY;
	Bounds candidateBounds;

	public override void SystemUpdate()
	{
		widthFromAspectAndHeight = Mathf.FloorToInt(mainCamera.aspect * ValueHolder.CAMERA_HEIGHT_IN_UNITS);

		yMin = -0.5f + ValueHolder.CAMERA_HEIGHT_IN_UNITS / 2;
		yMax = 0.5f + levelEntity.ySize - ValueHolder.CAMERA_HEIGHT_IN_UNITS / 2;
		centerY = (yMin + yMax) / 2f;

		if (yMin + ValueHolder.CAMERA_MINIMUM_LEVEL_BASED_BOUNDS > yMax)
		{
			yMin = centerY - ValueHolder.CAMERA_MINIMUM_LEVEL_BASED_BOUNDS / 2f;
			yMax = centerY + ValueHolder.CAMERA_MINIMUM_LEVEL_BASED_BOUNDS / 2f;
		}

		xMin = -0.5f + widthFromAspectAndHeight / 2;
		xMax = 0.5f + levelEntity.xSize - widthFromAspectAndHeight / 2;
		centerX = (xMin + xMax) / 2f;

		if (xMin + ValueHolder.CAMERA_MINIMUM_LEVEL_BASED_BOUNDS > xMax)
		{
			xMin = centerX - ValueHolder.CAMERA_MINIMUM_LEVEL_BASED_BOUNDS / 2f;
			xMax = centerX + ValueHolder.CAMERA_MINIMUM_LEVEL_BASED_BOUNDS / 2f;
		}

		candidateBounds = new Bounds(new Vector3(centerX, centerY, mainCamera.transform.position.z), new Vector3(xMax - xMin, yMax - yMin, 0f));

		if (gameStateEntity.tutorialsStateComponent.hasTutorialCameraBounds)
		{
			BoundsInt tutorialCameraBounds = gameStateEntity.tutorialsStateComponent.tutorialCameraBounds;

			xMin = tutorialCameraBounds.xMin;
			xMax = tutorialCameraBounds.xMax;
			yMin = tutorialCameraBounds.yMin;
			yMax = tutorialCameraBounds.yMax;

			centerX = (xMin + xMax) / 2f;
			centerY = (yMin + yMax) / 2f;

			candidateBounds = new Bounds(new Vector3(centerX, centerY, mainCamera.transform.position.z), new Vector3(xMax - xMin, yMax - yMin, 0f));
		}

		// Debug.Log("Candidate bounds = " + candidateBounds);

		gameStateEntity.softCameraPositionBounds = candidateBounds;
	}
}
