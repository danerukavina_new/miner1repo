﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateDwarfUpgradePanelEntitiesSystem : BaseSystem
{
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;
	public PlayerStateEntity playerStateEntity;
	public Dictionary<DwarfType, DwarfSpriteData> dwarfSprites;
	public Dictionary<ResourceType, Sprite> resourceSprites;

	DwarfUpgradePanelEntity dwarfUpgradePanelEntity;

	static void UpdateDwarfUpgradePanelEntity(
		DwarfUpgradePanelEntity dwarfUpgradePanelEntity, 
		DwarfEntity dwarfEntity, 
		PlayerResourcesComponent playerResourcesComponent, 
		Dictionary<DwarfType, DwarfSpriteData> dwarfSprites, 
		Dictionary<ResourceType, Sprite> resourceSprites
		)
	{
		dwarfUpgradePanelEntity.levelText.text = dwarfEntity.unitStatsComponent.level.ToString();

		dwarfUpgradePanelEntity.resourceDisplayItemPanelEntity.amountTextMeshPro.text = dwarfEntity.unitStatsComponent.finalStats.upgradeCost.ToString();
		dwarfUpgradePanelEntity.resourceDisplayItemPanelEntity.resourceSpriteRenderer.sprite = resourceSprites[ResourceType.Stone];

		dwarfUpgradePanelEntity.dwarfPortraitUIItem.portraitHeadImage.sprite = dwarfSprites[dwarfEntity.dwarfType].portraitHeadSprite;
		dwarfUpgradePanelEntity.dwarfPortraitUIItem.portraitBodyImage.sprite = dwarfSprites[dwarfEntity.dwarfType].portraitBodySprite;
	}

	public override void SystemUpdate()
	{
		// TODO: There's a dependency here on the panel map already being initialized, and this only occurs if LoadDwarves sets a flag that activates the ReinitializeDwarfUpgradePanelDataSystem

		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.belongsInPlayersParty)
			{
				dwarfUpgradePanelEntity = dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity];

				// NOTE: This cannot occur at the same time as isDirtyDueToReinitialization
				// TODO: Should a state enum be used instead to describe these cases?
				if (dwarfEntity.unitStatsComponent.showUpgradeStatsTextsAsResultOfSuccessfulLevelIncrease)
				{
					dwarfEntity.unitStatsComponent.showUpgradeStatsTextsAsResultOfSuccessfulLevelIncrease = false;

					dwarfUpgradePanelEntity.triggerUpgradeStatsText = true;

					if (dwarfUpgradePanelEntity.isDirtyDueToReinitialization)
						throw new System.Exception("Somehow a level up occurred at the same time the panel UI was meant to reinitialize - should be impossible.");
				}

				if (dwarfUpgradePanelEntity.isDirtyDueToReinitialization)
				{
					// NOTE: isDirtyDueToReinitialization is cleared later

					dwarfUpgradePanelEntity.triggerUpgradeStatsText = false;
					dwarfUpgradePanelEntity.upgradeStatsTMPText.gameObject.SetActive(false);
				}

				if (dwarfEntity.unitStatsComponent.updateUIAsResultOfRecalculatedLevelBasedStats || dwarfUpgradePanelEntity.isDirtyDueToReinitialization)
				{
					dwarfEntity.unitStatsComponent.updateUIAsResultOfRecalculatedLevelBasedStats = false;
					dwarfUpgradePanelEntity.isDirtyDueToReinitialization = false;

					UpdateDwarfUpgradePanelEntity(dwarfUpgradePanelEntity, dwarfEntity, playerStateEntity.playerResourcesComponent, dwarfSprites, resourceSprites);

					dwarfUpgradePanelEntity.upgradeButton.interactable = StatsAndDamageHelpers.CanAffordUpgradeCost(dwarfEntity.unitStatsComponent, playerStateEntity.playerResourcesComponent);
				}
				else if (dwarfUpgradeDisplayCanvasEntity.refreshUpgradeButtonsDueToResourceChange)
				{
					dwarfUpgradePanelEntity.upgradeButton.interactable = StatsAndDamageHelpers.CanAffordUpgradeCost(dwarfEntity.unitStatsComponent, playerStateEntity.playerResourcesComponent);
				}

				if (dwarfUpgradePanelEntity.hideDwarfUpgradeButton)
				{
					dwarfUpgradePanelEntity.hideDwarfUpgradeButton = false;
					dwarfUpgradePanelEntity.upgradeButton.gameObject.SetActive(false);
				}

				if (dwarfUpgradePanelEntity.reshowDwarfUpgradeButton)
				{
					dwarfUpgradePanelEntity.reshowDwarfUpgradeButton = false;
					dwarfUpgradePanelEntity.upgradeButton.gameObject.SetActive(true);
				}
			}
		}

		dwarfUpgradeDisplayCanvasEntity.refreshUpgradeButtonsDueToResourceChange = false; // TODO: Set this at all resource changes?
	}
}
