﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScreenControlSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public GameObject startScreenCanvasEntity;

	public override void SystemUpdate()
	{
		if (gameStateEntity.haveStartScreenAtStartOfGame && !gameStateEntity.testingSkipStartScreen && !gameStateEntity.testingSkipSomeTutorialLevels)
		{
			gameStateEntity.haveStartScreenAtStartOfGame = false;
			gameStateEntity.openStartScreen = true;
		}

		// TODO: Maybe these screens... and their control. Should be handled with a UIScreenState and a candidateUIScreenState, or something like that...
		if (gameStateEntity.openStartScreen)
		{
			gameStateEntity.openStartScreen = false;

			startScreenCanvasEntity.SetActive(true);
			gameStateEntity.onStartScreen = true;
			gameStateEntity.currentlyOpenMenusSubscription++;
		}

		if (gameStateEntity.closeStartScreen)
		{
			gameStateEntity.closeStartScreen = false;

			startScreenCanvasEntity.SetActive(false);
			gameStateEntity.onStartScreen = false;
			gameStateEntity.currentlyOpenMenusSubscription--;
		}
	}
}
