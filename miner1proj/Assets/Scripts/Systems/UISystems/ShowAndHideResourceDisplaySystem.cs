﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAndHideResourceDisplaySystem : BaseSystem
{
	public ResourceDisplayCanvasEntity resourceDisplayCanvasEntity;

	float candidatePosY;

	public override void SystemUpdate()
	{
		// TODO: Refactor this to be driven off of resourceDisplayCanvasEntity.resourceDisplayState

		if (resourceDisplayCanvasEntity.moveToShow)
		{
			if (Mathf.Approximately(resourceDisplayCanvasEntity.rectTransform.anchoredPosition.y, resourceDisplayCanvasEntity.fullyShownRectPositionY))
				resourceDisplayCanvasEntity.moveToShow = false; // Avoids messing with .position paramater whose dirtying may or may not impact performance

			candidatePosY = resourceDisplayCanvasEntity.rectTransform.anchoredPosition.y - resourceDisplayCanvasEntity.baseScreenMovementVelocity * Time.deltaTime;

			if (candidatePosY <= resourceDisplayCanvasEntity.fullyShownRectPositionY)
			{
				resourceDisplayCanvasEntity.moveToShow = false;
				resourceDisplayCanvasEntity.rectTransform.anchoredPosition = new Vector2(resourceDisplayCanvasEntity.rectTransform.anchoredPosition.x, resourceDisplayCanvasEntity.fullyShownRectPositionY);
			}
			else
				resourceDisplayCanvasEntity.rectTransform.anchoredPosition = new Vector2(resourceDisplayCanvasEntity.rectTransform.anchoredPosition.x, candidatePosY);
		}
		else
		{
			if (resourceDisplayCanvasEntity.preventAutoHideSubscribers <= 0 && resourceDisplayCanvasEntity.entryStartTime + resourceDisplayCanvasEntity.baseDelayBeforeHiding < Time.time)
			{
				if (Mathf.Approximately(resourceDisplayCanvasEntity.rectTransform.anchoredPosition.y, resourceDisplayCanvasEntity.fullyHiddenRectPosition))
					return;  // Avoids messing with .position paramater whose dirtying may or may not impact performance

				candidatePosY = resourceDisplayCanvasEntity.rectTransform.anchoredPosition.y + resourceDisplayCanvasEntity.baseScreenMovementVelocity * Time.deltaTime;

				if (candidatePosY >= resourceDisplayCanvasEntity.fullyHiddenRectPosition)
				{
					resourceDisplayCanvasEntity.rectTransform.anchoredPosition = new Vector2(resourceDisplayCanvasEntity.rectTransform.anchoredPosition.x, resourceDisplayCanvasEntity.fullyHiddenRectPosition);
				}
				else
					resourceDisplayCanvasEntity.rectTransform.anchoredPosition = new Vector2(resourceDisplayCanvasEntity.rectTransform.anchoredPosition.x, candidatePosY);
			}

			if (resourceDisplayCanvasEntity.preventAutoHideSubscribers < 0)
				throw new System.Exception("Resource display canvas entity somehow lost more prevent auto hide subscribers than it ever had.");
		}
	}
}
