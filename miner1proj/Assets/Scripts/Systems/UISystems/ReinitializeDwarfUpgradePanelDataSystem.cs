﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReinitializeDwarfUpgradePanelDataSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;

	DwarfUpgradePanelEntity dwarfUpgradePanelEntity;

	public override void SystemUpdate()
	{
		if (gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel)
		{
			gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = false;

			dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap.Clear();

			// int upgradePanelIndex

			for (int i = 0; i < dwarfEntities.Count; i++)
			{
				if (dwarfEntities[i].belongsInPlayersParty)
				{
					dwarfUpgradePanelEntity = dwarfUpgradeDisplayCanvasEntity.dwarfUpgradePanelEntities[i];

					dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap.Add(dwarfEntities[i], dwarfUpgradePanelEntity);
					dwarfUpgradePanelEntity.isDirtyDueToReinitialization = true;
					dwarfUpgradePanelEntity.triggerUpgradeStatsText = false;
					dwarfUpgradePanelEntity.upgradeClicked = false;
					dwarfUpgradePanelEntity.hideDwarfUpgradeButton = false;
					dwarfUpgradePanelEntity.reshowDwarfUpgradeButton = false;
					dwarfUpgradePanelEntity.gameObject.SetActive(true);
				}
			}

			// dwarfUpgradeDisplayCanvasEntity.refreshUpgradeButtonsDueToResourceChange = true;
		}
	}
}
