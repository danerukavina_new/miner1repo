﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenOrCloseDwarfUpgradesSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;
	public ResourceDisplayCanvasEntity resourceDisplayCanvasEntity;

	public override void SystemUpdate()
	{
		if (gameStateEntity.continueDwarfUpgradesClicked || gameStateEntity.closeDwarfUpgradesClicked)
		{
			if (gameStateEntity.continueDwarfUpgradesClicked && gameStateEntity.dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel)
				gameStateEntity.dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel = false;

			gameStateEntity.continueDwarfUpgradesClicked = gameStateEntity.closeDwarfUpgradesClicked = false;

			dwarfUpgradeDisplayCanvasEntity.gameObject.SetActive(false);
			gameStateEntity.currentlyOpenMenusSubscription--;

			resourceDisplayCanvasEntity.attemptRefreshResourceDisplay = true;
			resourceDisplayCanvasEntity.candidateResourceDisplayState = ResourceDisplayCanvasEntity.ResourceDisplayState.Hide;
			resourceDisplayCanvasEntity.preventAutoHideSubscribers--;
		}
		else if (gameStateEntity.openDwarfUpgradesClickedOrTriggered)
		{
			gameStateEntity.openDwarfUpgradesClickedOrTriggered = false;

			dwarfUpgradeDisplayCanvasEntity.gameObject.SetActive(true);
			gameStateEntity.currentlyOpenMenusSubscription++;

			dwarfUpgradeDisplayCanvasEntity.closeButton.gameObject.SetActive(gameStateEntity.openDwarfUpgradesWithCloseButton);
			dwarfUpgradeDisplayCanvasEntity.continueButton.gameObject.SetActive(gameStateEntity.openDwarfUpgradesWithContinueButton);

			resourceDisplayCanvasEntity.attemptRefreshResourceDisplay = true;
			resourceDisplayCanvasEntity.candidateResourceDisplayState = ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgrade;
			resourceDisplayCanvasEntity.preventAutoHideSubscribers++;
		}

		if (gameStateEntity.disableDwarfUpgradeContinueButton)
		{
			gameStateEntity.disableDwarfUpgradeContinueButton = false;

			dwarfUpgradeDisplayCanvasEntity.continueButton.interactable = false;
		}

		if (gameStateEntity.reenableDwarfUpgradeContinueButton)
		{
			gameStateEntity.reenableDwarfUpgradeContinueButton = false;

			dwarfUpgradeDisplayCanvasEntity.continueButton.interactable = true;
		}
	}
}
