﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarelyInteractiveInfoModalControlSystem : BaseSystem
{
	public BarelyInteractiveInfoModalCanvasEntity barelyInteractiveInfoModalCanvasEntity;

	string textToShow;
	int i;

	public override void SystemUpdate()
	{
		if (barelyInteractiveInfoModalCanvasEntity.openNow)
		{
			barelyInteractiveInfoModalCanvasEntity.openNow = false;

			if (barelyInteractiveInfoModalCanvasEntity.textsToShow.Count <= 0)
				throw new System.Exception("Barely Interactive Info Modal asked to show up without texts. It's not designed to support this appearance.");

			if (barelyInteractiveInfoModalCanvasEntity.textsToShow.Count > ValueHolder.MAXIMUM_BARELY_INTERACTIVE_INFO_MODAL_TEXTS_TO_SHOW)
				throw new System.Exception("Barely Interactive Info Modal asked to show way too many texts. Unexpected!");

			// TODO: Is the string code below causing GC?
			textToShow = string.Empty;
			i = 0;
			textToShow += barelyInteractiveInfoModalCanvasEntity.textsToShow[i];

			for (int i = 1; i < barelyInteractiveInfoModalCanvasEntity.textsToShow.Count; i++)
				textToShow += System.Environment.NewLine + barelyInteractiveInfoModalCanvasEntity.textsToShow[i];

			barelyInteractiveInfoModalCanvasEntity.textsToShow.Clear();

			barelyInteractiveInfoModalCanvasEntity.infoModalText.text = textToShow;
			barelyInteractiveInfoModalCanvasEntity.durationStartTime = Time.time;

			barelyInteractiveInfoModalCanvasEntity.gameObject.SetActive(true);

			barelyInteractiveInfoModalCanvasEntity.textSizer.RefreshThisUpdate();
		}

		if (barelyInteractiveInfoModalCanvasEntity.attemptClosingEarly && barelyInteractiveInfoModalCanvasEntity.durationStartTime + barelyInteractiveInfoModalCanvasEntity.ignoreCloseInputInitiallyDuration <= Time.time)
		{
			barelyInteractiveInfoModalCanvasEntity.attemptClosingEarly = false;
			barelyInteractiveInfoModalCanvasEntity.closeEarly = true;
		}

		if (
			barelyInteractiveInfoModalCanvasEntity.durationStartTime + barelyInteractiveInfoModalCanvasEntity.automaticallyBeginClosingDuration <= Time.time
			|| barelyInteractiveInfoModalCanvasEntity.closeEarly)
		{
			barelyInteractiveInfoModalCanvasEntity.closeEarly = false;
			barelyInteractiveInfoModalCanvasEntity.gameObject.SetActive(false);
		}
	}
}
