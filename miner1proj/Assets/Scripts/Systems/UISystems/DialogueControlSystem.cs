﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueControlSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public DialogueDisplayCanvasEntity dialogueDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;
	public Dictionary<DwarfType, DwarfSpriteData> dwarfSprites;

	DialogueStateComponent dialogueStateComponent;
	TutorialsStateComponent tutorialsStateComponent;
	int screenID;
	int? dwarfID;
	bool? highlightParticipant;
	DwarfType dwarfType;
	DwarfType? dwarfTypeNullable;

	void ReinitalizeAndEnableDialogueDisplayCanvasEntity()
	{
		// NOTE: This reinitialization also overrides some prefab color values used for nicely editing the prefab when needed. This is good.
		// Thus, changing this code may cause the prefab to look transparent/red in game. No big deal, just make all the prefab images opaque and white!

		for (screenID = 0; screenID < dialogueDisplayCanvasEntity.dwarfPortraitUIItems.Length; screenID++)
		{
			dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(false);
			dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(false);
			dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color = Color.white;
			dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.color = Color.white;
			dialogueDisplayCanvasEntity.speakerNameLeftSide.gameObject.SetActive(false);
			dialogueDisplayCanvasEntity.speakerNameRightSide.gameObject.SetActive(false);

			// TODO: Reset render order, once it's implemented
		}

		dialogueDisplayCanvasEntity.gameObject.SetActive(true);
	}

	void ExecuteDialogueStep(DialogueStepEntity dialogueStepEntity)
	{
		foreach (var dialogueStepComponentTag in dialogueStepEntity.dialogueStepComponentTags)
		{
			switch (dialogueStepComponentTag)
			{
				case DialogueStepComponentTag.ParticipantPortraitsFromIDs:
					for (screenID = 0; screenID < dialogueStepEntity.participantPortraitIDsForScreenIDs.Count; screenID++)
					{
						dwarfID = dialogueStepEntity.participantPortraitIDsForScreenIDs[screenID];

						if (dwarfID.HasValue)
						{
							dwarfType = dwarfEntities[dwarfID.Value].dwarfType;

							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.sprite = dwarfSprites[dwarfType].portraitHeadSprite;
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.sprite = dwarfSprites[dwarfType].portraitBodySprite;
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(true);
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(true);
						}
						else
						{
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(false);
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(false);
						}
					}

					for (; screenID < dialogueDisplayCanvasEntity.dwarfPortraitUIItems.Length; screenID++)
					{
						dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(false);
						dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(false);
					}
					break;
				case DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes:
					for (screenID = 0; screenID < dialogueStepEntity.participantDwarfTypesForScreenIDs.Count; screenID++)
					{
						dwarfTypeNullable = dialogueStepEntity.participantDwarfTypesForScreenIDs[screenID];

						if (dwarfTypeNullable.HasValue)
						{
							dwarfType = dwarfTypeNullable.Value;

							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.sprite = dwarfSprites[dwarfType].portraitHeadSprite;
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.sprite = dwarfSprites[dwarfType].portraitBodySprite;
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(true);
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(true);
						}
						else
						{
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(false);
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(false);
						}
					}

					for (; screenID < dialogueDisplayCanvasEntity.dwarfPortraitUIItems.Length; screenID++)
					{
						dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadObject.SetActive(false);
						dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyObject.SetActive(false);
					}
					break;
				case DialogueStepComponentTag.SpeakerName:
					if (!dialogueStepEntity.speakerNameIsOnRightSide)
					{
						dialogueDisplayCanvasEntity.speakerNameLeftSide.text = dwarfEntities[dialogueStepEntity.speakerNamingID].dwarfName;
						dialogueDisplayCanvasEntity.speakerNameLeftSide.gameObject.SetActive(true);
						dialogueDisplayCanvasEntity.speakerNameRightSide.gameObject.SetActive(false);
					}
					else
					{
						dialogueDisplayCanvasEntity.speakerNameRightSide.text = dwarfEntities[dialogueStepEntity.speakerNamingID].dwarfName;
						dialogueDisplayCanvasEntity.speakerNameRightSide.gameObject.SetActive(true);
						dialogueDisplayCanvasEntity.speakerNameLeftSide.gameObject.SetActive(false);
					}
					break;
				case DialogueStepComponentTag.CustomSpeakerName:
					if (!dialogueStepEntity.speakerNameIsOnRightSide)
					{
						dialogueDisplayCanvasEntity.speakerNameLeftSide.text = dialogueStepEntity.customSpeakerName;
						dialogueDisplayCanvasEntity.speakerNameLeftSide.gameObject.SetActive(true);
						dialogueDisplayCanvasEntity.speakerNameRightSide.gameObject.SetActive(false);
					}
					else
					{
						dialogueDisplayCanvasEntity.speakerNameRightSide.text = dialogueStepEntity.customSpeakerName;
						dialogueDisplayCanvasEntity.speakerNameRightSide.gameObject.SetActive(true);
						dialogueDisplayCanvasEntity.speakerNameLeftSide.gameObject.SetActive(false);
					}
					break;
				case DialogueStepComponentTag.HideSpeakerName:
					dialogueDisplayCanvasEntity.speakerNameLeftSide.gameObject.SetActive(false);
					dialogueDisplayCanvasEntity.speakerNameRightSide.gameObject.SetActive(false);
					break;
				case DialogueStepComponentTag.HighlightSingleParticipant:
					for (screenID = 0; screenID < dialogueDisplayCanvasEntity.dwarfPortraitUIItems.Length; screenID++)
						if (screenID == dialogueStepEntity.highlightSingleParticipantScreenID)
						{
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color = Color.white;
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.color = Color.white;
						}
						else
						{
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color = Color.gray;
							dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.color = Color.gray;
						}
					break;
				case DialogueStepComponentTag.HighlightedParticipants:
					for (screenID = 0; screenID < dialogueStepEntity.highlightedParticipantWithScreenIDs.Count; screenID++)
					{
						highlightParticipant = dialogueStepEntity.highlightedParticipantWithScreenIDs[screenID];

						if (highlightParticipant.HasValue)
						{
							if (highlightParticipant.Value)
							{
								if (dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color == Color.white)
									Debug.Log("A DialogueStep tries to emphasize a dwarf with highlight - but this dwarf was already highlighted. Something is amiss. Use null to skip dwarves that are already highlighted.");

								dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color = Color.white;
								dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.color = Color.white;
							}
							else
							{
								if (dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color == Color.gray)
									Debug.Log("A DialogueStep tries to deemphasize a dwarf by reducing highlight - but this dwarf was already de-highlighted. Something is amiss. Use null to skip dwarves that are already not highlighted.");

								dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitHeadImage.color = Color.gray;
								dialogueDisplayCanvasEntity.dwarfPortraitUIItems[screenID].portraitBodyImage.color = Color.gray;
							}
						}
					}
					break;
				case DialogueStepComponentTag.ParticipantRenderOrder:
					throw new System.Exception("NYI dialog step");
				case DialogueStepComponentTag.TwoButtonChoices:
					throw new System.Exception("NYI dialog step");
				case DialogueStepComponentTag.ThreeButtonChoices:
					throw new System.Exception("NYI dialog step");
				default:
					throw new System.Exception("NYI dialog step: " + dialogueStepComponentTag);
			}
		}

		// TODO: Finer control of text being displayed
		dialogueDisplayCanvasEntity.dialogueTextMeshPro.text = dialogueStepEntity.dialogueText;
		dialogueDisplayCanvasEntity.dialogueProceedIndicator.SetActive(true);
	}

	public override void SystemUpdate()
	{
		dialogueStateComponent = gameStateEntity.dialogueStateComponent;
		tutorialsStateComponent = gameStateEntity.tutorialsStateComponent;

		if (dialogueStateComponent.initiateInteractiveDialogue)
		{
			dialogueStateComponent.initiateInteractiveDialogue = false;

			tutorialsStateComponent.dialogueEnded = false; // TODO: this seems hacky, but it's very clear

			ReinitalizeAndEnableDialogueDisplayCanvasEntity();

			dialogueStateComponent.currentDialogueStep = 0;

			if (dialogueStateComponent.currentDialogueStep < dialogueStateComponent.dialogueStepEntities.Count)
				ExecuteDialogueStep(dialogueStateComponent.dialogueStepEntities[dialogueStateComponent.currentDialogueStep]);
			else
				throw new System.Exception("Dialogue system activated with no dialogue step entities.");
		}

		// NOTE: the start of interactive dialogue and the player inputting the next speech step are not expected to occur at the same time
		if (dialogueStateComponent.playerInputNextSpeechStep)
		{
			dialogueStateComponent.playerInputNextSpeechStep = false;

			dialogueStateComponent.currentDialogueStep++;

			if (dialogueStateComponent.currentDialogueStep < dialogueStateComponent.dialogueStepEntities.Count)
				ExecuteDialogueStep(dialogueStateComponent.dialogueStepEntities[dialogueStateComponent.currentDialogueStep]);
			else
			{
				dialogueDisplayCanvasEntity.gameObject.SetActive(false);

				tutorialsStateComponent.dialogueEnded = true; // TODO: this seems hacky, but it's very clear
			}
		}
	}
}
