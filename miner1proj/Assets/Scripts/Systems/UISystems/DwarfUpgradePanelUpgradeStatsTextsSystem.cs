﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfUpgradePanelUpgradeStatsTextsSystem : BaseSystem
{
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;

	DwarfUpgradePanelEntity dwarfUpgradePanelEntity;
	string upgradePanelStatsTextString;

	void UpdateDwarfUpgradePanelStatsTexts(
		DwarfUpgradePanelEntity dwarfUpgradePanelEntity, 
		UnitStatsComponent.UnitStatSet deltaFromUpgradeStats
		)
	{
		upgradePanelStatsTextString = string.Empty;

		if (deltaFromUpgradeStats.damageFactor > 0)
			upgradePanelStatsTextString += (upgradePanelStatsTextString.Length > 0 ? System.Environment.NewLine : string.Empty) + ValueHolder.PLUS_CHAR // + ValueHolder.SPACE_CHAR 
				+ deltaFromUpgradeStats.damageFactor.ToString() 
				+ ValueHolder.SPACE_CHAR + ValueHolder.UNIT_STAT_DAMAGE_FACTOR_STRING;

		if (deltaFromUpgradeStats.critChance > 0)
			upgradePanelStatsTextString += (upgradePanelStatsTextString.Length > 0 ? System.Environment.NewLine : string.Empty) + ValueHolder.PLUS_CHAR // + ValueHolder.SPACE_CHAR
				+ (deltaFromUpgradeStats.critChance * ValueHolder.PERCENTAGE_CONVERTER).ToString() + ValueHolder.PERCENTAGE_CHAR
				+ ValueHolder.SPACE_CHAR + ValueHolder.UNIT_STAT_CRIT_CHANCE_STRING;

		if (deltaFromUpgradeStats.critBonusDamageFactor > 0)
			upgradePanelStatsTextString += (upgradePanelStatsTextString.Length > 0 ? System.Environment.NewLine : string.Empty) + ValueHolder.PLUS_CHAR // + ValueHolder.SPACE_CHAR
				+ (deltaFromUpgradeStats.critBonusDamageFactor * ValueHolder.PERCENTAGE_CONVERTER).ToString() + ValueHolder.PERCENTAGE_CHAR
				+ ValueHolder.SPACE_CHAR + ValueHolder.UNIT_STAT_CRIT_BONUS_FACTOR_STRING;

		if (deltaFromUpgradeStats.smeltChance > 0)
			upgradePanelStatsTextString += (upgradePanelStatsTextString.Length > 0 ? System.Environment.NewLine : string.Empty) + ValueHolder.PLUS_CHAR // + ValueHolder.SPACE_CHAR
				+ (deltaFromUpgradeStats.smeltChance * ValueHolder.PERCENTAGE_CONVERTER).ToString() + ValueHolder.PERCENTAGE_CHAR
				+ ValueHolder.SPACE_CHAR + ValueHolder.UNIT_STAT_SMELT_CHANCE_FACTOR_STRING;

		// NOTE: Currently nothing triggers this
		if (deltaFromUpgradeStats.movementSpeed > 0)
			upgradePanelStatsTextString += (upgradePanelStatsTextString.Length > 0 ? System.Environment.NewLine : string.Empty) + ValueHolder.PLUS_CHAR // + ValueHolder.SPACE_CHAR 
				+ deltaFromUpgradeStats.movementSpeed.ToString()
				+ ValueHolder.SPACE_CHAR + ValueHolder.UNIT_STAT_MOVEMENT_SPEED_FACTOR_STRING;

		// NOTE: new stats like attack speed and damage amp, which may only ever come from buffs, NYI. They may not need to ever be implemented.

		if (upgradePanelStatsTextString.Length == 0)
			throw new System.Exception("Unexpected no change occurance after upgrade. All dwarf level ups should result in some change.");
		else
			upgradePanelStatsTextString.Remove(upgradePanelStatsTextString.Length - 1); // 

		dwarfUpgradePanelEntity.upgradeStatsTMPText.text = upgradePanelStatsTextString;

		dwarfUpgradePanelEntity.upgradeStatsTextDissipationStartTime = Time.time;
		dwarfUpgradePanelEntity.firstUpgradeStatsTextDissipationOccurred = false;
		dwarfUpgradePanelEntity.secondUpgradeStatsTextDissipationOccurred = false;

		dwarfUpgradePanelEntity.upgradeStatsTMPText.alpha = ValueHolder.INITIAL_UPGRADE_STATS_TEXT_DISSIPATION_ALPHA;

		dwarfUpgradePanelEntity.upgradeStatsTMPText.gameObject.SetActive(true);
	}

	public override void SystemUpdate()
	{
		// TODO: There's a dependency here on the panel map already being initialized, and this only occurs if LoadDwarves sets a flag that activates the ReinitializeDwarfUpgradePanelDataSystem

		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.belongsInPlayersParty)
			{
				dwarfUpgradePanelEntity = dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity];

				if (dwarfUpgradePanelEntity.triggerUpgradeStatsText)
				{
					dwarfUpgradePanelEntity.triggerUpgradeStatsText = false;

					UpdateDwarfUpgradePanelStatsTexts(dwarfUpgradePanelEntity, dwarfEntity.unitStatsComponent.deltaFromUpgradeStats);
				}

				if (!dwarfUpgradePanelEntity.firstUpgradeStatsTextDissipationOccurred && dwarfUpgradePanelEntity.upgradeStatsTextDissipationStartTime + ValueHolder.FIRST_UPGRADE_STATS_TEXT_DISSIPATION_DURATION <= Time.time)
				{
					dwarfUpgradePanelEntity.firstUpgradeStatsTextDissipationOccurred = true;
					dwarfUpgradePanelEntity.upgradeStatsTMPText.alpha = ValueHolder.FIRST_UPGRADE_STATS_TEXT_DISSIPATION_ALPHA;
				}
				else if (!dwarfUpgradePanelEntity.secondUpgradeStatsTextDissipationOccurred && dwarfUpgradePanelEntity.upgradeStatsTextDissipationStartTime + ValueHolder.SECOND_UPGRADE_STATS_TEXT_DISSIPATION_DURATION <= Time.time)
				{
					dwarfUpgradePanelEntity.secondUpgradeStatsTextDissipationOccurred = true;
					dwarfUpgradePanelEntity.upgradeStatsTMPText.gameObject.SetActive(false);
				}
			}
		}
	}
}
