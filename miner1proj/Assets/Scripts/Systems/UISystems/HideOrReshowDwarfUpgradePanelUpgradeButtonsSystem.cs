﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOrReshowDwarfUpgradePanelUpgradeButtonsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;

	DwarfEntity dwarfEntity;

	public override void SystemUpdate()
	{
		// TODO: should this system always set these variable to false?

		if (gameStateEntity.hideAllDwarfUpgradeButtons)
		{
			foreach (var dwarfEntity in dwarfEntities)
				if (dwarfEntity.belongsInPlayersParty)
					dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity].hideDwarfUpgradeButton = true;
		}
		else if (gameStateEntity.hideDwarfUpgradeButtons)
		{
			foreach (var dwarfID in gameStateEntity.hideDwarfUpgradeButtonDwarfIDs)
			{
				dwarfEntity = dwarfEntities[dwarfID];

				if (dwarfEntity.belongsInPlayersParty)
					dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity].hideDwarfUpgradeButton = true;
			}
		}

		if (gameStateEntity.reshowAllDwarfUpgradeButtons)
		{
			foreach (var dwarfEntity in dwarfEntities)
				if (dwarfEntity.belongsInPlayersParty)
					dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity].reshowDwarfUpgradeButton = true;
		}
		else if (gameStateEntity.reshowDwarfUpgradeButtons)
		{
			foreach (var dwarfID in gameStateEntity.reshowDwarfUpgradeButtonDwarfIDs)
			{
				dwarfEntity = dwarfEntities[dwarfID];

				if (dwarfEntity.belongsInPlayersParty)
					dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity].reshowDwarfUpgradeButton = true;
			}
		}

		gameStateEntity.hideDwarfUpgradeButtons = false;
		gameStateEntity.reshowDwarfUpgradeButtons = false;
		gameStateEntity.hideAllDwarfUpgradeButtons = false;
		gameStateEntity.reshowAllDwarfUpgradeButtons = false;
	}
}
