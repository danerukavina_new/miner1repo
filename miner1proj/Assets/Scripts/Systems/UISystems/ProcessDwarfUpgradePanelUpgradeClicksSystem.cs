﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessDwarfUpgradePanelUpgradeClicksSystem : BaseSystem
{
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public List<DwarfEntity> dwarfEntities;

	DwarfUpgradePanelEntity dwarfUpgradePanelEntity;

	public override void SystemUpdate()
	{
		// TODO: initialize this better - but basically this means initialization hasn't finished
		if (dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap.Count == 0)
			return;

		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.belongsInPlayersParty)
			{
				dwarfUpgradePanelEntity = dwarfUpgradeDisplayCanvasEntity.dwarfEntityToDwarfUpgradePanelEntityMap[dwarfEntity];

				if (dwarfUpgradePanelEntity.upgradeClicked)
				{
					dwarfUpgradePanelEntity.upgradeClicked = false;

					dwarfEntity.unitStatsComponent.attemptToPerformLevelUpgrade = true;
				}
			}
		}
	}
}
