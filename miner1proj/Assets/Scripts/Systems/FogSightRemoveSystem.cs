﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogSightRemoveSystem : BaseSystem
{
	public LevelEntity levelEntity;
	public List<DwarfEntity> dwarfEntities;

	List<Vector2Int> partialFogSightCoords;
	List<Vector2Int> fullFogSightCoords;

	Vector2Int coordToFogSightFrom;

	public FogSightRemoveSystem()
	{
		partialFogSightCoords = new List<Vector2Int>(
			LogicHelpers.TilesInRadius(ValueHolder.MAXIMUM_FOG_SIGHT_RADIUS)
			);

		fullFogSightCoords = new List<Vector2Int>(
			LogicHelpers.TilesInRadius(ValueHolder.MAXIMUM_FOG_SIGHT_RADIUS)
			);
	}

	public override void SystemUpdate()
	{
		bool someFullFogEntityRemoved = false;

		PartialFogEntity partialFogEntity;

		foreach (var dwarfEntity in dwarfEntities)
		{
			// TODO: Implement it to track subscribers to partial fog tiles, and control partial fog that way (it's more efficient)

			if (dwarfEntity.fogSightComponent.fogSightEnabled)
			{
				coordToFogSightFrom =  LogicHelpers.NearestCoordToPosition(dwarfEntity.gameObject.transform.position);

				// TODO: You can't check coord dirtied, because all fog is returned around all dwarves when any coord is dirtied. So one dwarf returns fog for all dwarves. Would need to check if any dwarf was dirtied.
				// TODO: Implement a reasonable way to check this, like one flag in levelEntity saying hey some fog was returned, now delete some
				// if (dwarfEntity.coordWasDirtiedThisFixedUpdate)
				{
					LogicHelpers.GetRectilinearCircleCoordsAroundCoord(
						coordToFogSightFrom,
						dwarfEntity.fogSightComponent.partialFogSightRadius,
						partialFogSightCoords
						);

					LogicHelpers.GetRectilinearCircleCoordsAroundCoord(
						coordToFogSightFrom,
						dwarfEntity.fogSightComponent.fullFogSightRadius,
						fullFogSightCoords
						);

					foreach (var partialFogSightCoord in partialFogSightCoords)
					{
						if (LogicHelpers.CoordinateIsInsideLevel(levelEntity, partialFogSightCoord))
						{
							partialFogEntity = levelEntity.partialFogTiles[partialFogSightCoord.x, partialFogSightCoord.y];

							if (partialFogEntity != null)
							{
								partialFogEntity.gameObject.SetActive(false);
							}
						}
					}

					foreach (var fullFogSightCoord in fullFogSightCoords)
					{
						if (LogicHelpers.CoordinateIsInsideLevel(levelEntity, fullFogSightCoord))
						{
							someFullFogEntityRemoved |= EntityHelpers.RemoveFullFog(fullFogSightCoord, levelEntity);
						}
					}
				}
			}
		}

		if (someFullFogEntityRemoved)
		{
			foreach (var dwarfEntity in dwarfEntities)
			{
				dwarfEntity.navigationComponent.levelDataWasDirtied = true;
			}
		}
	}
}
