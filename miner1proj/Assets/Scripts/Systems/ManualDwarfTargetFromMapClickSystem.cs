﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: find a place for this code... it's not useful right now

public class ManualDwarfTargetFromMapClickSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	public static bool CandidateIsBetterDwarfForTheJob(PlayerClickOrderType playerClickOrderType, bool currentBestDwarfIsBusy, float currentBestDwarfPowerLevel, bool candidateDwarfIsBusy, float candidatePowerLevel)
	{
		if (currentBestDwarfIsBusy)
		{
			if (!candidateDwarfIsBusy)
				return true;
		}
		if (candidatePowerLevel > currentBestDwarfPowerLevel)
			return true;

		return false;
	}

	

	public static bool CandidateIsBetterDwarfForTheJob(Vector2Int playerclickedTileCoord, PlayerClickOrderType playerClickOrderType, DwarfEntity currentBestDwarfEntity, DwarfEntity candidateDwarfEntity)
	{
		return true;
		//// TODO: Game design - it makes the most sense for the player to "lead" with the highest power level dwarf... but
		//// Maybe it would be more intuitive to use  nearest dwarf, or generally something that will shuffle around which dwarf you pull around when you tap

		//// TODO: consider making the "position" check more about nearness via nav. It could also still account for the position delta if that makes it better...

		//if (currentBestDwarfEntity == null)
		//	return true;

		//bool currentBestDwarfIsGoingToExplore = currentBestDwarfEntity.tileMovementStateComponent.hasIntendedMovementWithoutAttackingTarget;
		//bool candidateDwarfIsGoingToExplore = candidateDwarfEntity.tileMovementStateComponent.hasIntendedMovementWithoutAttackingTarget;
		//bool currentBestDwarfIsGoingToANode = currentBestDwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget && currentBestDwarfEntity.unitStateComponent.unitState != UnitState.Attacking;
		//bool candidateDwarfIsGoingToANode = candidateDwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget && currentBestDwarfEntity.unitStateComponent.unitState != UnitState.Attacking;
		//bool currentBestDwarfIsAttackingANode = currentBestDwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget && currentBestDwarfEntity.unitStateComponent.unitState != UnitState.Attacking;
		//bool candidateDwarfIsAttackingANode = candidateDwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget && currentBestDwarfEntity.unitStateComponent.unitState != UnitState.Attacking;

		//if (
		//	(currentBestDwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget && currentBestDwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord == playerclickedTileCoord)
		//	&& (candidateDwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget && candidateDwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord == playerclickedTileCoord)
		//	)
		//	return true;

		//if (currentBestDwarfIsAttackingANode && !candidateDwarfIsAttackingANode)
		//	return true;

		//if ((currentBestDwarfEntity.transform.position - LogicHelpers.PositionFromCoord(playerclickedTileCoord)).sqrMagnitude > (candidateDwarfEntity.transform.position - LogicHelpers.PositionFromCoord(playerclickedTileCoord)).sqrMagnitude)
		//	return true;

		//if (currentBestDwarfEntity.powerLevel < candidateDwarfEntity.powerLevel)
		//	return true;

		//if (currentBestDwarfIsGoingToANode && !candidateDwarfIsGoingToANode)
		//	return true;

		//if (currentBestDwarfIsGoingToExplore && !candidateDwarfIsGoingToExplore)
		//	return true;

		//return false;

		//// TODO: Remove this  player order type based logic if we find no design reasons for it
		////switch (playerClickOrderType)
		////{
		////	case PlayerClickOrderType.GoToEmptyTile:
		////		if (currentBestDwarfEntity.powerLevel < candidateDwarfEntity.powerLevel)
		////			return true;

		////		return BestForJobTieBreaker(currentBestDwarfEntity, candidateDwarfEntity);
		////	case PlayerClickOrderType.AttackNode:
		////		if (currentBestDwarfEntity.powerLevel < candidateDwarfEntity.powerLevel)
		////			return true;

		////		return BestForJobTieBreaker(currentBestDwarfEntity, candidateDwarfEntity);
		////	case PlayerClickOrderType.ExploreFog:
		////		if (currentBestDwarfEntity.powerLevel < candidateDwarfEntity.powerLevel)
		////			return true;

		////		return BestForJobTieBreaker(currentBestDwarfEntity, candidateDwarfEntity);
		////	default:
		////		throw new System.Exception("Unexpected kind of player order when determining best dwarf for the job.");
		////}
	}

	public override void SystemUpdate()
	{
		// TODO: There's a strong assumption in this system and future systems that all the dwarves start in the same connected region AND that all dwarves can move the same
		// Consider two disconnected rooms or dwarves that can hover over void tiles as an example of where this breaks down
		// Ultimately, because of full fog, dwarves should also be able to hot-potato the players order when one discovers it can't actually reach it (is stuck on an island). Or cancel the player's order
		
		// TODO: Canceling the players order, if candidate dwarves discover they can't reach somewhere? But they need to make an effort to stand near it... but which one?

		// TODO: This sort of doesn't make sense, the best dwarf depends on navigation, but navigation depends on the ordering of dwarves (which is basically best dwarf)

		if (gameStateEntity.playerClickedTile)
		{
			gameStateEntity.playerClickedTile = false;

			DwarfEntity bestDwarfForTheJob = null;

			foreach (var dwarfEntity in dwarfEntities)
			{
				if (CandidateIsBetterDwarfForTheJob(gameStateEntity.playerclickedTileCoord, gameStateEntity.playerClickOrderType, bestDwarfForTheJob, dwarfEntity))
				{
					bestDwarfForTheJob = dwarfEntity;
				}
			}

			bestDwarfForTheJob.hasManualAssignment = true;
			bestDwarfForTheJob.manuallyAssignedCoord = gameStateEntity.playerclickedTileCoord;
		}
	}
}
