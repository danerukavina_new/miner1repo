﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerformUpgradeForUnitStatsSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public PlayerStateEntity playerStateEntity;
	public ResourceDisplayCanvasEntity resourceDisplayCanvasEntity;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public BarelyInteractiveInfoModalCanvasEntity barelyInteractiveInfoModalCanvasEntity;

	UnitStatsComponent unitStatsComponent;
	int linearLevelUpgradeFactor;
	double exponentialLevelUpgradeFactor;
	UnitStatsComponent.UnitStatSet previousLevelStatSet;
	List<TemporaryLevelToProcUnlockData> dwarfProcLevelUnlockData;

	public static double LinearUpgrade(double baseVal, double growthVal, int linearLevelUpgradeFactor)
	{
		return baseVal + growthVal * linearLevelUpgradeFactor;
	}

	public static double ExponentialUpgrade(double baseVal, double growthVal, int linearLevelUpgradeFactor, double exponentialLevelUpgradeFactor)
	{
		return (baseVal + growthVal * linearLevelUpgradeFactor) * exponentialLevelUpgradeFactor;
	}

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			unitStatsComponent = dwarfEntity.unitStatsComponent;

			if (unitStatsComponent.attemptToPerformLevelUpgrade)
			{
				unitStatsComponent.attemptToPerformLevelUpgrade = false;

				if (StatsAndDamageHelpers.CanAffordUpgradeCost(dwarfEntity.unitStatsComponent, playerStateEntity.playerResourcesComponent))
				{
					playerStateEntity.playerResourcesComponent.resources[ResourceType.Stone].amount -= dwarfEntity.unitStatsComponent.finalStats.upgradeCost;

					dwarfUpgradeDisplayCanvasEntity.refreshUpgradeButtonsDueToResourceChange = true;

					resourceDisplayCanvasEntity.attemptRefreshResourceDisplay = true;
					resourceDisplayCanvasEntity.candidateResourceDisplayState = ResourceDisplayCanvasEntity.ResourceDisplayState.DwarfUpgradeResourceSubtraction;

					unitStatsComponent.level++;

					unitStatsComponent.levelIsDirty = true;
					unitStatsComponent.showUpgradeStatsTextsAsResultOfSuccessfulLevelIncrease = true;
				}
				else
					throw new System.Exception("Upgrade attempted when player couldn't afford it. Button should have been disabled.");
			}

			if (unitStatsComponent.levelIsDirty)
			{
				unitStatsComponent.levelIsDirty = false;

				previousLevelStatSet = unitStatsComponent.levelBasedStats;

				linearLevelUpgradeFactor = unitStatsComponent.level - 1;
				exponentialLevelUpgradeFactor = (double)Mathf.Pow(1.1f, (unitStatsComponent.level - 1));

				unitStatsComponent.levelBasedStats.damageFactor = LinearUpgrade(unitStatsComponent.baseStats.damageFactor, unitStatsComponent.growthStats.damageFactor, linearLevelUpgradeFactor);
				unitStatsComponent.levelBasedStats.critChance = LinearUpgrade(unitStatsComponent.baseStats.critChance, unitStatsComponent.growthStats.critChance, linearLevelUpgradeFactor);
				unitStatsComponent.levelBasedStats.critBonusDamageFactor = LinearUpgrade(unitStatsComponent.baseStats.critBonusDamageFactor, unitStatsComponent.growthStats.critBonusDamageFactor, linearLevelUpgradeFactor);
				unitStatsComponent.levelBasedStats.smeltChance = LinearUpgrade(unitStatsComponent.baseStats.smeltChance, unitStatsComponent.growthStats.smeltChance, linearLevelUpgradeFactor);
				unitStatsComponent.levelBasedStats.movementSpeed = unitStatsComponent.baseStats.movementSpeed;

				unitStatsComponent.levelBasedStats.attackSpeedBonus = unitStatsComponent.baseStats.attackSpeedBonus;
				unitStatsComponent.levelBasedStats.damageAmpBonus = unitStatsComponent.baseStats.damageAmpBonus;

				unitStatsComponent.levelBasedStats.upgradeCost = ExponentialUpgrade(unitStatsComponent.baseStats.upgradeCost, unitStatsComponent.growthStats.upgradeCost, linearLevelUpgradeFactor, exponentialLevelUpgradeFactor);

				unitStatsComponent.levelBasedStats = StatsAndDamageHelpers.RoundUnitStatSetWithAppropriatePrecisions(unitStatsComponent.levelBasedStats);

				unitStatsComponent.deltaFromUpgradeStats = StatsAndDamageHelpers.DeltaBetweenTwoStatSetsRoundedWithPrecision(previousLevelStatSet, unitStatsComponent.levelBasedStats);

				unitStatsComponent.statsWereDirtied = true;
				unitStatsComponent.updateUIAsResultOfRecalculatedLevelBasedStats = true;

				// Compute the next anticipated stats upgrade?

				// TODO: Perhaps move this to a separate system, or rename this system to attempting to level up and handling all updates related to it

				if (dwarfEntity.procOnAttackComponent.highestLevelAttained < dwarfEntity.unitStatsComponent.level)
				{
					dwarfProcLevelUnlockData = ValueHolder.dwarvesProcLevelUnlockData[dwarfEntity.dwarfType];

					foreach (var dwarfProcLevelUnlockDatum in dwarfProcLevelUnlockData)
						if (dwarfProcLevelUnlockDatum.unlockLevel > dwarfEntity.procOnAttackComponent.highestLevelAttained && dwarfProcLevelUnlockDatum.unlockLevel <= dwarfEntity.unitStatsComponent.level)
						{
							dwarfEntity.procOnAttackComponent.procDataOnAttack.Add(new ProcData(ValueHolder.procsBaseValues[dwarfProcLevelUnlockDatum.procType]));

							if (dwarfEntity.unitStatsComponent.level > ValueHolder.DONT_SHOW_LEVEL_1_INITIAL_COMPUTE_ABILITY_GAINS)
							{
								barelyInteractiveInfoModalCanvasEntity.textsToShow.Add(dwarfEntity.dwarfName + ValueHolder.ABILITY_LEARNED_MIDDLE_STRING + ValueHolder.procNames[dwarfProcLevelUnlockDatum.procType] + ValueHolder.SENTENCE_PERIOD);
								barelyInteractiveInfoModalCanvasEntity.openNow |= true;
							}
						}

					dwarfEntity.procOnAttackComponent.highestLevelAttained = dwarfEntity.unitStatsComponent.level;
				}
			}
		}
	}
}
