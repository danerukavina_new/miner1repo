﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackStateTransitionSystem : BaseSystem
{
	public List<AttackEntity> attackEntities;
	public LevelEntity levelEntity;

	public static bool ShouldAttackTrigger(AttackEntity attackEntity, LevelEntity levelEntity)
	{
		// NOTE: This logic purposely written based on components rather than case logic based on AttackType. Try to preserve this, instead of spreading around AttackType facets throughout the code

		if (attackEntity.tileMovementRangeComponent == null)
			return true;

		Vector2Int nextCoord = attackEntity.tileMovementStateComponent.nextCoord; // NOTE: These types of attacks have this component

		return levelEntity.rockTiles[nextCoord.x, nextCoord.y] != null
			&& (attackEntity.transform.position - LogicHelpers.PositionFromCoord(nextCoord)).magnitude < 0.375f
			;
	}

	public static bool ShouldAttackFizzleOut(AttackEntity attackEntity, LevelEntity levelEntity)
	{
		// NOTE: This logic purposely written based on components rather than case logic based on AttackType. Try to preserve this, instead of spreading around AttackType facets throughout the code

		if (attackEntity.tileMovementRangeComponent == null)
			return false;

		if (!LogicHelpers.IsInRadiusRange(attackEntity.tileMovementRangeComponent.originCoord, attackEntity.transform.position, attackEntity.tileMovementRangeComponent.movementRange))
			return true;

		if (attackEntity.chainTileBouncingComponent == null)
			return false;

		if (levelEntity.rockTiles[attackEntity.targetCoord.x, attackEntity.targetCoord.y] == null
					&& (attackEntity.transform.position - LogicHelpers.PositionFromCoord(attackEntity.targetCoord)).magnitude < 0.375f)
			return true;

		return false;
	}

	public static bool AssignNextBounceTargetToAttackEntityWithChainTileBouncingComponent(AttackEntity attackEntity, LevelEntity levelEntity)
	{
		LogicHelpers.GetDistanceCircleCoordsAroundCoordWithinLevel(
			attackEntity.targetCoord,
			attackEntity.chainTileBouncingComponent.bounceRange,
			attackEntity.chainTileBouncingComponent.candidateBounceTargets,
			levelEntity
			); // TODO: Is not filtering to be within the level

		int i;
		int randomizeViaStartingIndexOffset = UnityEngine.Random.Range(0, attackEntity.chainTileBouncingComponent.candidateBounceTargets.Count);
		Vector2Int coord;
		RockTileEntity rockTileEntityAtCoord;
		FullFogEntity fullFogEntityAtCoord;
		double highestMiningValue = double.MinValue;
		bool bestIsNotIndesctructible = false;
		bool bestIsNotFreeSpace = false;
		bool bestIsNotInFog = false;

		Vector2Int bestCoord = Vector2Int.zero;
		bool bestCoordFound = false;

		for (int unrandomized_i = 0; unrandomized_i < attackEntity.chainTileBouncingComponent.candidateBounceTargets.Count; unrandomized_i++)
		{
			i = (unrandomized_i + randomizeViaStartingIndexOffset) % attackEntity.chainTileBouncingComponent.candidateBounceTargets.Count;

			coord = attackEntity.chainTileBouncingComponent.candidateBounceTargets[i];

			if (attackEntity.chainTileBouncingComponent.repeatBounceTargetsAllowed || !attackEntity.chainTileBouncingComponent.bounceHistory.Contains(coord))
			{
				rockTileEntityAtCoord = levelEntity.rockTiles[coord.x, coord.y];
				fullFogEntityAtCoord = levelEntity.fullFogTiles[coord.x, coord.y];

				if (!LogicHelpers.IsHigherMiningValueOrOrNotIndestructibleOrNotFreeSpaceOrNotInFog(rockTileEntityAtCoord, fullFogEntityAtCoord, highestMiningValue, bestIsNotIndesctructible, bestIsNotFreeSpace, bestIsNotInFog))
				{
					if (rockTileEntityAtCoord != null)
					{
						highestMiningValue = rockTileEntityAtCoord.miningValue;
						bestIsNotIndesctructible = rockTileEntityAtCoord.destructible;
					}
					bestIsNotFreeSpace = rockTileEntityAtCoord != null;
					bestIsNotInFog = fullFogEntityAtCoord == null;

					bestCoordFound = true;
					bestCoord = coord;
				}
			}
		}

		if (bestCoordFound)
		{
			attackEntity.targetCoord = bestCoord;
			return true;
		}

		return false;
	}

	public override void SystemUpdate()
	{
		foreach (var attackEntity in attackEntities)
		{
			if (attackEntity.attackStateComponent.attackState == AttackState.Moving)
			{

				if (attackEntity.primedToDealDamage &&
					ShouldAttackTrigger(attackEntity, levelEntity))
				{
					attackEntity.begunDealingDamageThisFixedUpdate = true;

					bool shouldDealDamageAndDissipate = true;

					if (attackEntity.chainTileBouncingComponent != null
						&& attackEntity.chainTileBouncingComponent.bounces + 1 <= attackEntity.chainTileBouncingComponent.maxBounces)
					{
						attackEntity.chainTileBouncingComponent.bounces++;
						attackEntity.chainTileBouncingComponent.bounceHistory.Add(attackEntity.targetCoord);

						if (AssignNextBounceTargetToAttackEntityWithChainTileBouncingComponent(attackEntity, levelEntity))
						{
							attackEntity.tileMovementStateComponent.readyForNextCoord = true;
							attackEntity.tileMovementRangeComponent.originCoord = attackEntity.tileMovementStateComponent.nextCoord;

							shouldDealDamageAndDissipate = false;
						}
					}
					
					if (shouldDealDamageAndDissipate)
					{
						attackEntity.primedToDealDamage = false;

						attackEntity.attackStateComponent.attackState = AttackState.DealingDamage;
						attackEntity.attackStateComponent.changedToBeClearedOnUpdate = true;

						attackEntity.attackDissipationStartTime = Time.time;
					}
				}
				else if (ShouldAttackFizzleOut(attackEntity, levelEntity))
				{
					attackEntity.primedToDealDamage = false;

					attackEntity.attackStateComponent.attackState = AttackState.FizzlingOut;
					attackEntity.attackStateComponent.changedToBeClearedOnUpdate = true;

					attackEntity.attackDissipationStartTime = Time.time;
				}
			}
		}
	}
}
