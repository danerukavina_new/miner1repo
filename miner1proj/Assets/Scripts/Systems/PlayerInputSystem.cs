﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public PlayerInputIndicator playerInputIndicatorPrefab;
	public GameObject playerInputIndicatorObject;

	public TMPro.TextMeshProUGUI debugText1;
	public TMPro.TextMeshProUGUI debugText2;

	Vector2Int coord;
	Vector2Int coordInputStart;
	bool disabledCoordinateInput;
	bool disabledCameraInput;
	bool disableAllButExitInput;
	bool addManualCoordinateThisFrame;
	bool addManualCoordinateAtInputStartThisFrame;
	RockTileEntity rockTileEntity;
	bool allTouchesAreStarting;
	bool atLeastOneTouchIsEnding;
	bool alltouchesAreEnding;
	bool fakedGetMouseButtonDown0;
	bool fakedGetMouseButton0;
	bool fakedGetMouseButtonUp0;
	Vector3 fakedMousePosition;
	Touch touch;
	Vector3 fakedMousePositionAccumulator;

	public override void SystemUpdate()
	{
		// NOTE: If tutorial is forcing exit input, regular input must also be enabled initially
		disabledCoordinateInput =  gameStateEntity.tutorialDisabledManualCoordinateInput && !gameStateEntity.tutorialDisableAllButExitInput;
		disabledCameraInput = gameStateEntity.tutorialDisabledCameraInput;
		disableAllButExitInput = gameStateEntity.tutorialDisableAllButExitInput;

		addManualCoordinateAtInputStartThisFrame = false;
		addManualCoordinateThisFrame = false;

		if (Application.isMobilePlatform)
		{
			//if (Input.touchCount > 0)
			//{
			//	allTouchesAreStarting = true;
			//	atLeastOneTouchIsEnding = false;
			//	alltouchesAreEnding = true;
			//	fakedMousePositionAccumulator = Vector3.zero;

			//	for (int i = 0; i < Input.touchCount; i++)
			//	{
			//		touch = Input.GetTouch(i);
			//		switch (touch.phase)
			//		{
			//			case TouchPhase.Began:
			//				alltouchesAreEnding = false;
			//				break;
			//			case TouchPhase.Moved:
			//				allTouchesAreStarting = false;
			//				alltouchesAreEnding = false;
			//				break;
			//			case TouchPhase.Stationary:
			//				allTouchesAreStarting = false;
			//				alltouchesAreEnding = false;
			//				break;
			//			case TouchPhase.Ended:
			//				atLeastOneTouchIsEnding = true;
			//				allTouchesAreStarting = false;
			//				break;
			//			case TouchPhase.Canceled:
			//				atLeastOneTouchIsEnding = true;
			//				allTouchesAreStarting = false;
			//				break;
			//			default:
			//				throw new System.Exception("New kind of touch phase detected, that's not implemented");
			//		}

			//		fakedMousePositionAccumulator += new Vector3(touch.position.x, touch.position.y, 0f);
			//	}

			//	if (allTouchesAreStarting)
			//	{
			//		if (!fakedGetMouseButton0)
			//			fakedGetMouseButtonDown0 = true;
			//		else
			//			fakedGetMouseButtonDown0 = false;

			//		fakedGetMouseButton0 = true;

			//		fakedGetMouseButtonUp0 = false;
			//	}

			//	if (atLeastOneTouchIsEnding && alltouchesAreEnding)
			//	{
			//		fakedGetMouseButtonDown0 = false;

			//		fakedGetMouseButton0 = false;

			//		if (fakedGetMouseButton0)
			//			fakedGetMouseButtonUp0 = true;
			//		else
			//			fakedGetMouseButtonUp0 = false;
			//	}

			//	fakedMousePosition = fakedMousePositionAccumulator / Input.touchCount;
			//}
			//else
			//{
			//	fakedGetMouseButtonDown0 = false;

			//	fakedGetMouseButton0 = false;

			//	if (fakedGetMouseButton0)
			//		fakedGetMouseButtonUp0 = true;
			//	else
			//		fakedGetMouseButtonUp0 = false;
			//}

			if (Input.touchCount > 0)
			{
				touch = Input.GetTouch(0);
				switch (touch.phase)
				{
					case TouchPhase.Began:
						fakedGetMouseButtonDown0 = true;
						fakedGetMouseButton0 = true;
						fakedGetMouseButtonUp0 = false;
						break;
					case TouchPhase.Moved:
						fakedGetMouseButtonDown0 = false;
						fakedGetMouseButton0 = true;
						fakedGetMouseButtonUp0 = false;
						break;
					case TouchPhase.Stationary:
						fakedGetMouseButtonDown0 = false;
						fakedGetMouseButton0 = true;
						fakedGetMouseButtonUp0 = false;
						break;
					case TouchPhase.Ended:
						fakedGetMouseButtonDown0 = false;
						fakedGetMouseButton0 = false;
						fakedGetMouseButtonUp0 = true;
						break;
					case TouchPhase.Canceled:
						fakedGetMouseButtonDown0 = false;
						fakedGetMouseButton0 = false;
						fakedGetMouseButtonUp0 = true;
						break;
					default:
						throw new System.Exception("New kind of touch phase detected, that's not implemented");
				}

				fakedMousePosition = new Vector3(touch.position.x, touch.position.y, 0f);
			}
			else
			{
				fakedGetMouseButtonDown0 = false;
				fakedGetMouseButton0 = false;
				fakedGetMouseButtonUp0 = false;
			}
		}
		else
		{
			fakedGetMouseButtonDown0 = Input.GetMouseButtonDown(0);
			fakedGetMouseButton0 = Input.GetMouseButton(0);
			fakedGetMouseButtonUp0 = Input.GetMouseButtonUp(0);

			fakedMousePosition = Input.mousePosition;
		}

		// TODO: This code is crap. If a screen organically comes up after a user mouses down, mouseUp can happen much later.
		// gameStateEntity.openDwarfUpgradesClickedOrTriggered  perhaps didn't matter for mousedown,
		// But later in the code, gameStateEntity.openDwarfUpgradesClickedOrTriggered is used to prevent the mouse up from causing a coordinate to be placed...
		// Perhaps the best way to do this is to use raycast
		bool mouseDown0 = fakedGetMouseButtonDown0 && GUIUtility.hotControl == 0 && gameStateEntity.currentlyOpenMenusSubscription == 0; // && !gameStateEntity.openDwarfUpgradesClickedOrTriggered 
		bool mouse0 = fakedGetMouseButton0 && GUIUtility.hotControl == 0 && gameStateEntity.currentlyOpenMenusSubscription == 0; // && !gameStateEntity.openDwarfUpgradesClickedOrTriggered
		bool mouseUp0 = fakedGetMouseButtonUp0; // TODO: There needs to be more sophisticated logic here

		//debugText1.text = "gameStateEntity.playerClickInputCurrentScreenPosition: " + gameStateEntity.playerClickInputCurrentScreenPosition
		//	+ System.Environment.NewLine + "gameStateEntity.playerClickInputStartScreenPosition: " + gameStateEntity.playerClickInputStartScreenPosition
		//		+ System.Environment.NewLine + " ... pos: " + fakedMousePosition
		//		+ System.Environment.NewLine + " ... mouseDown0: " + mouseDown0
		//		+ System.Environment.NewLine + " ... mouse0: " + mouse0
		//		+ System.Environment.NewLine + " ... mouseUp0: " + mouseUp0;

		if (mouseDown0)
		{
			if (gameStateEntity.inputState == PlayerClickInputState.NoneOrTutorialCancelled)
			{
				gameStateEntity.inputState = PlayerClickInputState.ClickStartedAndAmbiguous;

				gameStateEntity.playerClickInputStartTime = Time.time;
				gameStateEntity.playerClickInputStartCameraWorldPosition = mainCamera.transform.position;
				gameStateEntity.playerClickInputStartWorldPosition = mainCamera.ScreenToWorldPoint(fakedMousePosition);
				gameStateEntity.playerClickInputStartScreenPosition = fakedMousePosition;

				//debugText2.text = "Start";
			}
		}

		if (mouse0 || mouseUp0)
		{
			gameStateEntity.playerClickInputCurrentWorldPosition = mainCamera.ScreenToWorldPoint(fakedMousePosition);
			gameStateEntity.playerClickInputCurrentScreenPosition = fakedMousePosition;
		}

		if (mouse0)
		{
			switch (gameStateEntity.inputState)
			{
				case PlayerClickInputState.ClickStartedAndAmbiguous:
					if ((gameStateEntity.playerClickInputCurrentWorldPosition - gameStateEntity.playerClickInputStartWorldPosition).sqrMagnitude > ValueHolder.PRESS_IN_PLACE_TOLERANCE_SQUARED)
					{
						//debugText2.text = "Tolerance exceeded";

						if (!disabledCoordinateInput)
						{
							gameStateEntity.inputState = PlayerClickInputState.ManualCoordinateInput;

							addManualCoordinateThisFrame = true;
							addManualCoordinateAtInputStartThisFrame = true;

							EntityHelpers.RemoveSharedManualCoordinates(levelEntity);
							gameStateEntity.playerCoordinatesChanged = true;
						}
						else
							gameStateEntity.inputState = PlayerClickInputState.NoneOrTutorialCancelled; // Can no longer be a camera input, since it left the tolerance
					}
					else if (gameStateEntity.playerClickInputStartTime + ValueHolder.LONG_UI_PRESS_DELAY < Time.time)
					{
						// debugText2.text = "Long press occurred";

						if (!gameStateEntity.tutorialDisabledCameraInput)
						{
							gameStateEntity.inputState = PlayerClickInputState.CameraMovement;

							gameStateEntity.inputStateBecameCameraMovementThisUpdate = true;
						}
						// NOTE: Purposely not transitioning to None here. Instead, the player has a longer chance to do Manual input effectivelt
					}
					break;
				case PlayerClickInputState.ManualCoordinateInput:
					// NOTE: No need to check tutorialDisabledManualCoordinateInput - can't reach this state during it
					if (disabledCoordinateInput)
					{
						// TODO: This can occur if dialogue starts during player input
						// throw new System.Exception("During Player Input processing, system believes it should add a coordinate in spite of logic that disables it. Check code.");
					}
					addManualCoordinateThisFrame = true;
					break;
				case PlayerClickInputState.CameraMovement:
					if (gameStateEntity.tutorialDisabledCameraInput)
						gameStateEntity.inputState = PlayerClickInputState.NoneOrTutorialCancelled;
					break;
				case PlayerClickInputState.NoneOrTutorialCancelled:
					// NOTE: Can occur when inputs are ignored or cancelled due to tutorial. Do nothing.
					break;
				default:
					throw new System.Exception("Unknown input state while clicked.");
			}
		}
		else if (mouseUp0)
		{
			if (gameStateEntity.inputState == PlayerClickInputState.ClickStartedAndAmbiguous)
			{
				// TODO: Another part ofthe clicks going through hack. gameStateEntity.currentlyOpenMenusSubscription == 0 check doesn't work here.
				// Ideally this would just check disabledCoordinateInput.
				// See notes above, but the solution is to determine mouse inputs via Raycast, probably
				if (!disabledCoordinateInput && !gameStateEntity.openDwarfUpgradesClickedOrTriggered) 
				{
					addManualCoordinateThisFrame = true;

					EntityHelpers.RemoveSharedManualCoordinates(levelEntity);
					gameStateEntity.playerCoordinatesChanged = true;
				}
			}

			gameStateEntity.inputState = PlayerClickInputState.NoneOrTutorialCancelled;
		}

		if (addManualCoordinateThisFrame)
		{
			// NOTE: No need to check tutorialDisabledManualCoordinateInput - variable can't become true to begin with
			if (!disabledCoordinateInput)
			{
				coord = LogicHelpers.CoordFromScreenPosition(
					gameStateEntity.playerClickInputCurrentWorldPosition
					);

				if (LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
				{
					rockTileEntity = levelEntity.rockTiles[coord.x, coord.y];

					if (!disableAllButExitInput || (rockTileEntity != null && rockTileEntity.isExit))
						EntityHelpers.AddValidatedSharedManualCoordinateAndUpdateGameState(gameStateEntity, PlayerClickOrderType.AttackNode, coord, levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);

					if (addManualCoordinateAtInputStartThisFrame)
					{
						coordInputStart = LogicHelpers.CoordFromScreenPosition(
							gameStateEntity.playerClickInputStartWorldPosition
							);


						if (coordInputStart != coord && LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
						{
							rockTileEntity = levelEntity.rockTiles[coordInputStart.x, coordInputStart.y];
							if (!disableAllButExitInput || (rockTileEntity != null && rockTileEntity.isExit))
								EntityHelpers.AddValidatedSharedManualCoordinateAndUpdateGameState(gameStateEntity, PlayerClickOrderType.AttackNode, coordInputStart, levelEntity, playerInputIndicatorPrefab, playerInputIndicatorObject);
						}
					}
				}
			}
			else
			{
				// TODO: This can occur if dialogue starts during player input
				// throw new System.Exception("During Player Input processing, system believes it should add a coordinate in spite of logic that disables it. Check code.");
			}
		}

		//if (Input.GetMouseButtonDown(1))
		//{
		//	coord = LogicHelpers.CoordFromScreenPosition(
		//		Camera.main.ScreenToWorldPoint(Input.mousePosition)
		//		);


		//	if (!LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, coord))
		//		return;

		//	gameStateEntity.playerClickedTile = true;
		//	gameStateEntity.playerclickedTileCoord = coord;

		//	if (levelEntity.fullFogTiles[coord.x, coord.y] != null)
		//		gameStateEntity.playerClickOrderType = PlayerClickOrderType.ExploreFog;
		//	else if (levelEntity.rockTiles[coord.x, coord.y] != null) // NOTE: It's intended that even indestructible tiles can be "ordered for attack" by the player
		//		gameStateEntity.playerClickOrderType = PlayerClickOrderType.AttackNode;
		//	else // NOTE: It's intended that the player can order a dwarf to go to an unwalkable tile
		//		gameStateEntity.playerClickOrderType = PlayerClickOrderType.GoToEmptyTile;

		//	// TODO: Dirtying the dwarf's data

		//	// Debug.Log("Coord = " + gameStateEntity.playerclickedTileCoord + " , pos = " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
		//}
	}
}
