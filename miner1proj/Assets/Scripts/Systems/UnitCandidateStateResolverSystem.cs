﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCandidateStateResolverSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking)
			{
				dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking = false;

				// TODO: improve "Equal" to work with direction rather than exact attackTarget. Matters in the multi tile penetrating attack case, like a polearm. Then the dwarf would be indifferent which tile was selected as long as it means the same thing for it based on coord.
				if (!CandidateUnitAttackingStateComponent.Equal(dwarfEntity.unitAttackingStateComponent, dwarfEntity.candidateUnitAttackingStateComponent))
				{
					dwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget = dwarfEntity.candidateUnitAttackingStateComponent.hasIntendedAttackTarget;
					dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord = dwarfEntity.candidateUnitAttackingStateComponent.intendedAttackTargetCoord;
					dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord = dwarfEntity.candidateUnitAttackingStateComponent.spotToAttackFromCoord;

					dwarfEntity.unitAttackingStateComponent.isRefreshedPreviousFixedUpdate = true;

					// dwarfEntity.unitStateComponent.unitState = UnitState.Attacking;
					dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;
				}
			}

			if (dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking)
			{
				dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = false;

				if (!CandidateTileMovementStateComponent.Equal(dwarfEntity.tileMovementStateComponent, dwarfEntity.candidateTileMovementStateComponent))
				{
					dwarfEntity.tileMovementStateComponent.hasIntendedMovementTarget = dwarfEntity.candidateTileMovementStateComponent.hasIntendedMovementTarget;
					dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord = dwarfEntity.candidateTileMovementStateComponent.spotToFreeSpaceStandOnCoord;

					dwarfEntity.tileMovementStateComponent.navPath.Clear();
					foreach (var navPathItem in dwarfEntity.candidateTileMovementStateComponent.navPath)
					{
						dwarfEntity.tileMovementStateComponent.navPath.Add(navPathItem);
					}

					// dwarfEntity.unitStateComponent.unitState = UnitState.Moving;
					// TODO: Dirty fix for moonwalking animation and not changing attack direction in the animator. What should be done instead is to only dirty if a change in direction occured. But the animator only reacts if a change in direction or unit state occurs so it's all good for now.
					dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;

					// Debug.Log("Candidate change accepted, dwarf: " + dwarfEntity.coord + " - nav: " + dwarfEntity.tileMovementStateComponent.navPath.CustomListOutput());
				}
			}
		}
	}
}
