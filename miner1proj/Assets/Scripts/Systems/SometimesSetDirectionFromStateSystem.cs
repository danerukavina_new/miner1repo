﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SometimesSetDirectionFromStateSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public List<AttackEntity> attackEntities;

	Vector3 positionDelta;
	Vector2Int coordDelta;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate || dwarfEntity.tileMovementStateComponent.directionChangeToBeCheckedOnUpdate)
			{
				// NOTE: The set animation system always clears these flags in the if above, do not clear them here

				// NOTE: Direction isn't always changed. Only if there's a reason to.

				switch (dwarfEntity.unitStateComponent.unitState)
				{
					case UnitState.Idle:
						break;
					case UnitState.Moving:
						positionDelta = LogicHelpers.PositionFromCoord(dwarfEntity.tileMovementStateComponent.nextCoord) - dwarfEntity.gameObject.transform.position;

						if (positionDelta.sqrMagnitude > Mathf.Epsilon)
							dwarfEntity.tileMovementStateComponent.normalizedGridAlignedDirection = LogicHelpers.NormalizedGridAlignedDirectionFromDeltas(positionDelta.x, positionDelta.y);
						break;
					case UnitState.Attacking:
						coordDelta = dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord - dwarfEntity.coord;

						if (coordDelta.sqrMagnitude > 0)
							dwarfEntity.tileMovementStateComponent.normalizedGridAlignedDirection = LogicHelpers.NormalizedGridAlignedDirectionFromDeltas(coordDelta.x, coordDelta.y);
						break;
					default:
						break;
				}
			}
		}

		foreach (var attackEntity in attackEntities)
		{
			if (attackEntity.attackStateComponent.changedToBeClearedOnUpdate)
			{
				// NOTE: The set animation system always clears these flag in the if above, do not clear them here

				if (attackEntity.tileMovementStateComponent != null)
				{
					// TODO: No need for this code yet. Also, AttackCreation, which sets the normalizedGridAlignedDirection, is in FixedUpdate. On the other hand, this system is in Update to correctly adjust dwarf "direction" for the sake of animation...
				}
			}
		}
	}
}
