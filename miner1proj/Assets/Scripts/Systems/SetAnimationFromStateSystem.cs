﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimationFromStateSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public List<AttackEntity> attackEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate || dwarfEntity.tileMovementStateComponent.directionChangeToBeCheckedOnUpdate)
			{
				dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = false;
				dwarfEntity.tileMovementStateComponent.directionChangeToBeCheckedOnUpdate = false;

				dwarfEntity.animator.SetInteger("UnitState", (int)dwarfEntity.unitStateComponent.unitState);

				switch (dwarfEntity.unitStateComponent.unitState)
				{
					case UnitState.Idle:
						dwarfEntity.animator.SetInteger("Direction", (int)LogicHelpers.FourDirectionsFromNormalziedGridAlignedDirection(dwarfEntity.tileMovementStateComponent.normalizedGridAlignedDirection));
						break;
					case UnitState.Moving:
						// Using exact positions prevents moonwalking and makes things snappy. Another way to do this would be to make navigation parameters consistent before this Update() occurs and go off the coords
						// TODO: Moving always implies having navPath... seems hacky
						dwarfEntity.animator.SetInteger("Direction", (int)LogicHelpers.FourDirectionsFromNormalziedGridAlignedDirection(dwarfEntity.tileMovementStateComponent.normalizedGridAlignedDirection));
						break;
					case UnitState.Attacking:
						dwarfEntity.animator.SetInteger("Direction", (int)LogicHelpers.FourDirectionsFromNormalziedGridAlignedDirection(dwarfEntity.tileMovementStateComponent.normalizedGridAlignedDirection));
						break;
					default:
						throw new System.Exception("Unexpected unit state in set animation from unit state.");
				}
			}
		}

		foreach (var attackEntity in attackEntities)
		{
			if (attackEntity.attackStateComponent.changedToBeClearedOnUpdate)
			{
				attackEntity.attackStateComponent.changedToBeClearedOnUpdate = false;

				attackEntity.animator.SetInteger("AttackState", (int)attackEntity.attackStateComponent.attackState);
			}
		}
	}
}
