﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandidateResourceAdditionResolutionSystem : BaseSystem
{
	public PlayerStateEntity playerStateEntity;
	public GameStateEntity gameStateEntity;
	public List<ResourceGainTextEntity> resourceGainTextEntities;
	public ResourceGainTextEntity resourceGainTextPrefab;
	public Dictionary<ResourceType, Sprite> resourceSprites;
	public GameObject resourceGainTexts;
	public ResourceDisplayCanvasEntity resourceDisplayCanvasEntity;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;

	ResourceType candidateGainedResourceType;
	Dictionary<ResourceType, int> amountOfResourcesAdded;

	private static ResourceType SmeltCertainResourceTypes(ResourceType resourceType)
	{
		switch (resourceType)
		{
			case ResourceType.CopperOre:
				return ResourceType.CopperBar;
			case ResourceType.TinOre:
				return ResourceType.TinBar;
			case ResourceType.SilverOre:
				return ResourceType.SilverBar;
			default:
				return resourceType;
		}
	}

	private static double IncreaseAmountForCertainResourceTypes(ResourceType resourceType, int difficultyTier)
	{
		switch (resourceType)
		{
			case ResourceType.Stone:
			case ResourceType.CopperOre:
			case ResourceType.TinOre:
			case ResourceType.SilverOre:
			case ResourceType.CopperBar:
				// Increase based on difficulty for these resources
				return 1d * difficultyTier;
			case ResourceType.Diamond:
				// No increase for these resources
				return 1d;
			case ResourceType.TinBar:
			case ResourceType.SilverBar:
			case ResourceType.BronzeBar:
			default:
				// Smelt effect should be checked after the increase is checked... conceptually you're smelting the amount of ore calculated. So bars should not be considered.
				throw new System.Exception("Increased amount for certain resource types asked for unexpected resource type.");
		}
	}

	private bool AttemptToAddCandidateResourcesToPlayerResourcesComponent(PlayerResourcesComponent playerResourcesComponent, CandidateResourceAddition candidateResourceAddition, int difficultyTier)
	{
		List<ValueHolder.RockTileDroptableItem> rockTileDropTable = ValueHolder.RockTileDropTables[candidateResourceAddition.sourceRockTileType];
		ResourceType finalizedResourceType;
		double finalizedAmountMultiplier;
		bool resourceAdded = false;

		foreach (var rockTileDrop in rockTileDropTable)
		{
			if (UnityEngine.Random.Range(0f, 1f) <= rockTileDrop.probability)
			{
				finalizedAmountMultiplier = IncreaseAmountForCertainResourceTypes(rockTileDrop.resourceType, difficultyTier);

				if (!candidateResourceAddition.isSmeltEffect)
					finalizedResourceType = rockTileDrop.resourceType;
				else
					finalizedResourceType = SmeltCertainResourceTypes(rockTileDrop.resourceType);

				// TODO: Is this unnecessary memory allocation?
				ResourceEntity resourceEntity = new ResourceEntity()
				{
					resourceType = finalizedResourceType,
					amount = finalizedAmountMultiplier * UnityEngine.Random.Range(rockTileDrop.baseMinAmount, rockTileDrop.baseMaxAmount),
				};

				playerResourcesComponent.resources[resourceEntity.resourceType].amount += resourceEntity.amount;

				playerResourcesComponent.recentlyAddedResources.AddOrMoveToEndOfSet(resourceEntity.resourceType);

				resourceAdded = true;

				EntityHelpers.AddResourceGainText(
					resourceEntity.resourceType, 
					resourceEntity.amount,
					1.0f, 
					candidateResourceAddition.coord, 
					0.3f * Random.Range(-0.5f, 0.5f) * Vector3.up, 
					resourceGainTextEntities, 
					resourceGainTextPrefab, 
					resourceSprites, 
					resourceGainTexts
					);
			}
		}

		return resourceAdded;
	}

	public override void SystemUpdate()
	{
		bool someResourcesAdded = false;

		if (!gameStateEntity.tutorialDisabledResourceGains)
			foreach (var candidateResourceAddition in playerStateEntity.playerResourcesComponent.candidateResourceAdditions)
			{
				if (AttemptToAddCandidateResourcesToPlayerResourcesComponent(playerStateEntity.playerResourcesComponent, candidateResourceAddition, gameStateEntity.difficultyTier))
					someResourcesAdded = true;
			}

		playerStateEntity.playerResourcesComponent.candidateResourceAdditions.Clear();

		if (someResourcesAdded)
		{
			//Debug.Log("Stone: " + playerStateEntity.playerResourcesComponent.resources[ResourceType.Stone].amount
			//	+ " ... " + "Copper ore: " + playerStateEntity.playerResourcesComponent.resources[ResourceType.CopperOre].amount
			//	+ " ... " + "Tin ore: " + playerStateEntity.playerResourcesComponent.resources[ResourceType.TinOre].amount
			//	+ " ... " + "Silver ore: " + playerStateEntity.playerResourcesComponent.resources[ResourceType.SilverOre].amount
			//	+ " ... " + "Diamond: " + playerStateEntity.playerResourcesComponent.resources[ResourceType.Diamond].amount
			//	);

			// TODO: This is not refreshing correctly when in dwarf mode but a resource addition occurred.
			resourceDisplayCanvasEntity.attemptRefreshResourceDisplay = true;
			resourceDisplayCanvasEntity.candidateResourceDisplayState = ResourceDisplayCanvasEntity.ResourceDisplayState.ResourceAddition;

			dwarfUpgradeDisplayCanvasEntity.refreshUpgradeButtonsDueToResourceChange = true;
		}
	}
}
