﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialStartStepSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	TutorialsStateComponent tutorialsStateComponent;

	public override void SystemUpdate()
	{
		tutorialsStateComponent = gameStateEntity.tutorialsStateComponent;

		if (!tutorialsStateComponent.tutorialStepIsActive && tutorialsStateComponent.currentOrNextTutorialStep < tutorialsStateComponent.tutorialStepEntities.Count)
		{
			if (tutorialsStateComponent.tutorialStepEntities[tutorialsStateComponent.currentOrNextTutorialStep].startTutorialStepCondition(gameStateEntity, levelEntity, dwarfEntities, Time.time, mainCamera))
			{
				tutorialsStateComponent.activateTutorialStepThisUpdate = true;
			}
		}
	}
}