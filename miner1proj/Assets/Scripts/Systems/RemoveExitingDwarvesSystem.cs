﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveExitingDwarvesSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	DwarfEntity dwarfEntity;
	RockTileEntity rockTileEntity;

	public override void SystemUpdate()
	{
		bool removalOccured = false;

		//for (int i = dwarfEntities.Count - 1; i >= 0 ; i--)
		//{
		//	dwarfEntity = dwarfEntities[i];
		//	rockTileEntity = levelEntity.rockTiles[dwarfEntity.coord.x, dwarfEntity.coord.y];

		//	if (rockTileEntity != null && rockTileEntity.rockTileType == RockTileType.Exit)
		//	{
		//		EntityHelpers.RemoveDwarf(dwarfEntity, dwarfEntities);
		//		removalOccured = true;
		//	}
		//}

		foreach (var dwarfEntity in dwarfEntities)
		{
			rockTileEntity = levelEntity.rockTiles[dwarfEntity.coord.x, dwarfEntity.coord.y];

			if (rockTileEntity != null && rockTileEntity.isExit)
			{
				dwarfEntity.hasExitedLevel = true;
				dwarfEntity.gameObject.SetActive(false);
				removalOccured = true;
			}
		}

		if (removalOccured)
		{
			foreach (var dwarfEntity in dwarfEntities)
			{
				dwarfEntity.navigationComponent.levelDataWasDirtied = true;
			}
		}
	}
}
