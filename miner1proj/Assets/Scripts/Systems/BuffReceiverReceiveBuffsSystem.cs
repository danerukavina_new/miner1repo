﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: This probably isn't needed, the receive buff system clears this
public class BuffReceiverReceiveBuffsSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;
	public Dictionary<BuffType, BuffEntity> buffPseudoprefabs;

	RockTileEntity rockTileEntity;
	BuffEntity incumbentBuffEntity;
	PrototypeBuff bestReceivedPrototypeBuffSoFar;
	Dictionary<BuffType, PrototypeBuff> bestReceivedPrototypeBuffs;

	public BuffReceiverReceiveBuffsSystem()
	{
		bestReceivedPrototypeBuffs = new Dictionary<BuffType, PrototypeBuff>(ValueHolder.MAXIMUM_BUFFS_TO_HAVE);
	}

	void SelectBestOfEachBuffTypeFromReceivedPrototypeBuffs(List<PrototypeBuff> receivedPrototypeBuffs, ref Dictionary<BuffType, PrototypeBuff> bestReceivedPrototypeBuffs)
	{
		bestReceivedPrototypeBuffs.Clear();

		foreach (var receivedPrototypeBuff in receivedPrototypeBuffs)
		{
			if (bestReceivedPrototypeBuffs.TryGetValue(receivedPrototypeBuff.buffType, out bestReceivedPrototypeBuffSoFar))
			{
				if (bestReceivedPrototypeBuffSoFar.buffLevel < receivedPrototypeBuff.buffLevel)
					bestReceivedPrototypeBuffs[receivedPrototypeBuff.buffType] = receivedPrototypeBuff;
			}
			else
				bestReceivedPrototypeBuffs.Add(receivedPrototypeBuff.buffType, receivedPrototypeBuff);
		}
	}

	void ProcessReceptionOfExistingDissipatingPrototypeBuff(PrototypeBuff receivedPrototypeBuff, BuffEntity incumbentBuffEntity, out bool changeOccurred)
	{
		incumbentBuffEntity.level = Mathf.Max(receivedPrototypeBuff.buffLevel, incumbentBuffEntity.level);

		// TODO: Concievably final stats would be recalculated here and then an reset call would be performed. For example dissipation duration would be tweaked

		incumbentBuffEntity.dissipationStartTime = Time.time;
		changeOccurred = true;
	}

	void ProcessReceptionOfExistingCountingDownChargesPrototypeBuff(PrototypeBuff receivedPrototypeBuff, BuffEntity incumbentBuffEntity, out bool changeOccurred)
	{
		// TODO: Implement a way for charged buffs to only receive refreshes from FRESH sources. Probably based on a GUID.
		changeOccurred = false;

		//incumbentBuffEntity.level = Mathf.Max(receivedPrototypeBuff.buffLevel, incumbentBuffEntity.level);

		//// TODO: Concievably final stats would be recalculated here and then an reset call would be performed. For example dissipation duration would be tweaked

		//incumbentBuffEntity.currentCharges = incumbentBuffEntity.finalStats.charges;
		//changeOccurred = true;
	}

	void ProcessReceptionOfExistingBuffGiverAreaDependentPrototypeBuff(PrototypeBuff receivedPrototypeBuff, BuffEntity incumbentBuffEntity, out bool changeOccurred)
	{
		// NOTE: Area dependent buffs always take the new level as the value
		if (incumbentBuffEntity.level < receivedPrototypeBuff.buffLevel)
		{
			incumbentBuffEntity.level = receivedPrototypeBuff.buffLevel;
			changeOccurred = true;
		}
		else
			changeOccurred = false;

		// TODO: Concievably final stats would be recalculated here and then an reset call would be performed. For example dissipation duration would be tweaked

		incumbentBuffEntity.buffAreaDependenceMaintainedThisFixedUpdate = true;
	}

	void BuffReceiverLogic(BuffReceiverComponent buffReceiverComponent)
	{
		bool changeOccurred = false;
		bool checkChangeOccurred = false;

		foreach (var buffEntity in buffReceiverComponent.buffEntities.Values)
			if (buffEntity.isBuffGiverAreaDependent)
				buffEntity.buffAreaDependenceMaintainedThisFixedUpdate = false;

		SelectBestOfEachBuffTypeFromReceivedPrototypeBuffs(buffReceiverComponent.receivedPrototypeBuffsThisFixedUpdate, ref bestReceivedPrototypeBuffs);

		buffReceiverComponent.receivedPrototypeBuffsThisFixedUpdate.Clear();

		foreach (var prototypeBuff in bestReceivedPrototypeBuffs.Values)
		{
			if (!buffReceiverComponent.buffEntities.TryGetValue(prototypeBuff.buffType, out incumbentBuffEntity))
			{
				EntityHelpers.AddBuffEntity(prototypeBuff, buffReceiverComponent.buffEntities, buffPseudoprefabs);
				changeOccurred = true;
			}
			else
			{
				if (buffPseudoprefabs[prototypeBuff.buffType].isDissipating)
				{
					ProcessReceptionOfExistingDissipatingPrototypeBuff(prototypeBuff, buffReceiverComponent.buffEntities[prototypeBuff.buffType], out checkChangeOccurred);
					changeOccurred |= checkChangeOccurred;
				}

				if (buffPseudoprefabs[prototypeBuff.buffType].isCountingDownCharges)
				{
					ProcessReceptionOfExistingCountingDownChargesPrototypeBuff(prototypeBuff, buffReceiverComponent.buffEntities[prototypeBuff.buffType], out checkChangeOccurred);
					changeOccurred |= checkChangeOccurred;
				}

				if (buffPseudoprefabs[prototypeBuff.buffType].isBuffGiverAreaDependent)
				{
					ProcessReceptionOfExistingBuffGiverAreaDependentPrototypeBuff(prototypeBuff, buffReceiverComponent.buffEntities[prototypeBuff.buffType], out checkChangeOccurred);
					changeOccurred |= checkChangeOccurred;
				}
			}
		}

		buffReceiverComponent.buffEntitiesListIsDirtyThisFixedUpdate |= changeOccurred;
	}

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
			BuffReceiverLogic(dwarfEntity.buffReceiverComponent);

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				rockTileEntity = levelEntity.rockTiles[x, y];
				if (rockTileEntity != null)
					BuffReceiverLogic(rockTileEntity.buffReceiverComponent);
			}
	}
}
