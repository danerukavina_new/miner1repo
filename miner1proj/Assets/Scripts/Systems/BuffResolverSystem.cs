﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: This probably isn't needed, the receive buff system clears this
public class BuffResolverSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	RockTileEntity rockTileEntity;
	List<BuffEntity> buffEntitiesForRemoval;

	public BuffResolverSystem()
	{
		buffEntitiesForRemoval = new List<BuffEntity>(ValueHolder.MAXIMUM_BUFFS_TO_HAVE);
	}

	BuffEntity.BuffStatSet AccumulateBuffStats(BuffEntity.BuffStatSet buffStatSetA, BuffEntity.BuffStatSet buffStatSetB)
	{
		return new BuffEntity.BuffStatSet()
		{
			attackSpeedBonus = buffStatSetA.attackSpeedBonus + buffStatSetB.attackSpeedBonus,
			movementSpeedBonus = buffStatSetA.movementSpeedBonus + buffStatSetB.movementSpeedBonus,
			damageAmpBonus = buffStatSetA.damageAmpBonus + buffStatSetB.damageAmpBonus,
			toughnessArmorFlatReduction = buffStatSetA.toughnessArmorFlatReduction + buffStatSetB.toughnessArmorFlatReduction
		};
	}

	void BuffReceiverLogic(BuffReceiverComponent buffReceiverComponent)
	{
		bool changeOccurred = false;

		buffEntitiesForRemoval.Clear();

		foreach (var buffEntity in buffReceiverComponent.buffEntities.Values)
			if (
				buffEntity.isDissipating && buffEntity.dissipationStartTime + buffEntity.finalStats.dissipationDuration <= Time.time
				|| buffEntity.isCountingDownCharges && buffEntity.currentCharges <= 0
				||buffEntity.isBuffGiverAreaDependent && !buffEntity.buffAreaDependenceMaintainedThisFixedUpdate
				)
			{
				buffEntitiesForRemoval.Add(buffEntity);
				changeOccurred = true;
			}

		EntityHelpers.RemoveBuffEntities(buffEntitiesForRemoval, buffReceiverComponent.buffEntities);

		buffReceiverComponent.buffEntitiesListIsDirtyThisFixedUpdate |= changeOccurred;

		// NOTE: The BuffStatSet is only recalculated if the buff list changed
		if (buffReceiverComponent.buffEntitiesListIsDirtyThisFixedUpdate)
		{
			buffReceiverComponent.accumulatedBuffStats = new BuffEntity.BuffStatSet();

			foreach (var buffEntity in buffReceiverComponent.buffEntities.Values)
			{
				buffReceiverComponent.accumulatedBuffStats = AccumulateBuffStats(buffReceiverComponent.accumulatedBuffStats, buffEntity.finalStats);
			}

			buffReceiverComponent.accumulatedBuffStatsIsDirty = true;
			buffReceiverComponent.buffEntitiesListIsDirtyAfterBuffResolver = true;
		}
	}

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
			BuffReceiverLogic(dwarfEntity.buffReceiverComponent);

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				rockTileEntity = levelEntity.rockTiles[x, y];
				if (rockTileEntity != null)
					BuffReceiverLogic(rockTileEntity.buffReceiverComponent);
			}
	}
}
