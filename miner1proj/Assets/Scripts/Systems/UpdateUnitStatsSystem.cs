﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateUnitStatsSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;

	UnitStatsComponent unitStatsComponent;
	BuffReceiverComponent buffReceiverComponent;
	float attackSpeedFactor;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			unitStatsComponent = dwarfEntity.unitStatsComponent;
			buffReceiverComponent = dwarfEntity.buffReceiverComponent;

			if (unitStatsComponent.statsWereDirtied || buffReceiverComponent.accumulatedBuffStatsIsDirty)
			{
				unitStatsComponent.statsWereDirtied = false;

				unitStatsComponent.finalStats.damageFactor = unitStatsComponent.levelBasedStats.damageFactor;
				unitStatsComponent.finalStats.critChance = unitStatsComponent.levelBasedStats.critChance;
				unitStatsComponent.finalStats.critBonusDamageFactor = unitStatsComponent.levelBasedStats.critBonusDamageFactor;
				unitStatsComponent.finalStats.smeltChance = unitStatsComponent.levelBasedStats.smeltChance;
				unitStatsComponent.finalStats.movementSpeed = unitStatsComponent.levelBasedStats.movementSpeed * (1 + buffReceiverComponent.accumulatedBuffStats.movementSpeedBonus);

				unitStatsComponent.finalStats.upgradeCost = unitStatsComponent.levelBasedStats.upgradeCost;

				// TODO: This needs to be rationalized to BuffEntity having a separate component for unit and node buffs, or buffs living in dynamic lists rather than in fixed structs
				// Why? Because a unit has only some of the buff stats, and that's hardcoded here in some sense. There's no public double toughnessArmorFlatReduction for example
				unitStatsComponent.finalStats.attackSpeedBonus = unitStatsComponent.levelBasedStats.attackSpeedBonus + buffReceiverComponent.accumulatedBuffStats.attackSpeedBonus;
				unitStatsComponent.finalStats.damageAmpBonus = unitStatsComponent.levelBasedStats.damageAmpBonus + buffReceiverComponent.accumulatedBuffStats.damageAmpBonus;

				dwarfEntity.tileMovementStateComponent.movementSpeed = unitStatsComponent.finalStats.movementSpeed;

				attackSpeedFactor = 1f / (1f + unitStatsComponent.finalStats.attackSpeedBonus);
				dwarfEntity.unitAttackingStateComponent.nextAttackStatSet.attackCreationPointTime = dwarfEntity.unitAttackingStateComponent.baseAttackStatSet.attackCreationPointTime * attackSpeedFactor;
				dwarfEntity.unitAttackingStateComponent.nextAttackStatSet.attackBackswingFinishedPointTime = dwarfEntity.unitAttackingStateComponent.baseAttackStatSet.attackBackswingFinishedPointTime * attackSpeedFactor;
				dwarfEntity.unitAttackingStateComponent.nextAttackStatSet.attackCompletionTime = dwarfEntity.unitAttackingStateComponent.baseAttackStatSet.attackCompletionTime * attackSpeedFactor;

				// TODO: This is a hack... basically attacks have to control their own animation speed since we're not recalculating attack duration in the middle of attacks. On the other hand, someone has to control animation speed in all other cases.
				// TODO: This animator control is not granular enough. Should only control the movement animations
				if (dwarfEntity.unitStateComponent.unitState != UnitState.Attacking)
				{
					// TODO: This should be controlled from SetAnimationFromStateSystem
					dwarfEntity.animator.speed = (1f + buffReceiverComponent.accumulatedBuffStats.movementSpeedBonus);
				}

				// Compute the next anticipated stats upgrade?
			}
		}
	}
}
