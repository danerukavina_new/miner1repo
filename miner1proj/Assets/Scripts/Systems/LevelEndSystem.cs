﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		if (!gameStateEntity.dwarvesShouldBeLoaded)
		{
			foreach (var dwarfEntity in dwarfEntities)
			{
				if (dwarfEntity.isRequiredToExitLevelToProceedToNextLevel && !dwarfEntity.hasExitedLevel)
					return;
			}

			gameStateEntity.indexToLoad++;

			if (gameStateEntity.dwarfUpgradesTriggersAtEndsOfLevels)
			{
				gameStateEntity.openDwarfUpgradesClickedOrTriggered = true;
				gameStateEntity.openDwarfUpgradesWithCloseButton = false;
				gameStateEntity.openDwarfUpgradesWithContinueButton = true;

				gameStateEntity.dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel = true;
			}

			gameStateEntity.levelShouldBeLoaded = true;
			gameStateEntity.dwarvesShouldBeLoaded = true;
		}
	}
}
