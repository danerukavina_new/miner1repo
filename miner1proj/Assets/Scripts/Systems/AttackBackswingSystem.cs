﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBackswingSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.unitStateComponent.unitState == UnitState.Attacking)
			{
				if (!dwarfEntity.unitAttackingStateComponent.attackBackswingFinishedPointPerformed && dwarfEntity.unitAttackingStateComponent.attackStartTime + dwarfEntity.unitAttackingStateComponent.currentAttackStatSet.attackBackswingFinishedPointTime <= Time.time)
				{
					dwarfEntity.unitAttackingStateComponent.attackBackswingFinishedPointPerformed = true;
				}
			}
		}
	}
}
