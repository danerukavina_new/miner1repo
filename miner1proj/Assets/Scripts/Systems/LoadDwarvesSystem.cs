﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Delete this system, roll it into LoadTerrainSystem
// Also, it sucks that this shares logic with TutorialValueHolder. Perhaps the values should coexist.
public class LoadDwarvesSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;
	public Dictionary<DwarfType, DwarfEntity> dwarfPrefabs;
	public GameObject dwarvesObject;

	List<Vector2Int> unfilteredEntranceCoords;
	List<Vector2Int> filteredEntranceCoords;
	List<Vector2Int> dwarfCoords;
	TileMovementStateComponent facingDirectionTileMovementStateComponent;

	public LoadDwarvesSystem()
	{
		unfilteredEntranceCoords = new List<Vector2Int>(
			LogicHelpers.TilesInRadius(ValueHolder.ENTRY_RADIUS_FROM_DWARF_COUNT[ValueHolder.MAXIMUM_DWARVES_IN_PARTY])
			);

		filteredEntranceCoords = new List<Vector2Int>(
			LogicHelpers.TilesInRadius(ValueHolder.ENTRY_RADIUS_FROM_DWARF_COUNT[ValueHolder.MAXIMUM_DWARVES_IN_PARTY]) / 2 // Dividing by two since entrances discard at least half the coordinates
			);

		dwarfCoords = new List<Vector2Int>(
			ValueHolder.MAXIMUM_DWARVES_IN_PARTY
			);
	}

	void LevelEntrySequenceForDwarves(int dwarvesToControl, Vector2Int initialFacingDirection)
	{
		// TODO: store this in the level perhaps. Then watch out, the helper function is destructive towards the source list
		LogicHelpers.GetRectilinearCircleCoordsAroundCoordExcludingCoordWithinLevelEdges(
			levelEntity.levelDwarfEntranceCoord,
			ValueHolder.ENTRY_RADIUS_FROM_DWARF_COUNT[dwarvesToControl],
			levelEntity,
			unfilteredEntranceCoords,
			filteredEntranceCoords
			);

		LogicHelpers.ChooseRandomCoordsFromListDestructive(dwarvesToControl, filteredEntranceCoords, dwarfCoords);

		LevelEntrySequenceForDwarvesWithPredefinedCoords(dwarvesToControl, dwarfCoords, initialFacingDirection);
	}

	void LevelEntrySequenceForDwarvesWithPredefinedCoords(int dwarvesToControl, List<Vector2Int> dwarfCoords,  Vector2Int initialFacingDirection)
	{
		LogicHelpers.ReenableAllDwarfCandidateActionProcessing(dwarfEntities);

		LogicHelpers.SetSharedManualCoordinates(dwarfCoords, gameStateEntity, levelEntity);

		for (int i = 0; i < dwarvesToControl; i++)
		{
			facingDirectionTileMovementStateComponent = dwarfEntities[i].tileMovementStateComponent;

			facingDirectionTileMovementStateComponent.normalizedGridAlignedDirection = initialFacingDirection; // TODO: For some reason, just when the game loads, this doesn't adjust the facing direction on time
			facingDirectionTileMovementStateComponent.directionChangeToBeCheckedOnUpdate = true;
		}
	}

	void BasicTestLevel()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		LevelEntrySequenceForDwarves(dwarfEntities.Count, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;

		//for (int i = 0; i < dwarfCoords.Count - 2; i++)
		//{
		//	EntityHelpers.AddDwarf(DwarfType.Basic, dwarfCoords[i], dwarfEntities, dwarfPrefabs, dwarvesObject);
		//}

		//EntityHelpers.AddDwarf(DwarfType.MeleeSpear, dwarfCoords[dwarfCoords.Count - 2], dwarfEntities, dwarfPrefabs, dwarvesObject);
		//EntityHelpers.AddDwarf(DwarfType.RangedChain, dwarfCoords[dwarfCoords.Count - 1], dwarfEntities, dwarfPrefabs, dwarvesObject);

		// TODO: the map click system depends on keeping the dwarves sorted descending by power level. This needs to be made more visible
		// dwarfEntities.Sort((a, b) => b.powerLevel.CompareTo(a.powerLevel));
	}

	void TutorialLevel0()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			// NOTE: Nothing really done when it's level 0
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;
		}

		EntityHelpers.AddDwarf(DwarfType.MeleeCleave, TutorialValueHolder.L0_dwarf0Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);

		// Dwarf just appears, no entry sequence

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
		// Do not sort dwarves for tutorial
	}

	void TutorialLevel1()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true);
		}

		EntityHelpers.AddDwarf(DwarfType.MeleeSpear, TutorialValueHolder.L1_dwarf1Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		EntityHelpers.AddDwarf(DwarfType.Ritual, TutorialValueHolder.L1_dwarf2Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "???", isRequiredToExitLevelToProceedToNextLevel: false, belongsInPlayersParty: false);
		EntityHelpers.AddDwarf(DwarfType.Ritual, TutorialValueHolder.L1_dwarf3Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "???", isRequiredToExitLevelToProceedToNextLevel: false, belongsInPlayersParty: false);
		EntityHelpers.AddDwarf(DwarfType.Ritual, TutorialValueHolder.L1_dwarf4Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "???", isRequiredToExitLevelToProceedToNextLevel: false, belongsInPlayersParty: false);
		EntityHelpers.AddDwarf(DwarfType.Ritual, TutorialValueHolder.L1_dwarf5Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "???", isRequiredToExitLevelToProceedToNextLevel: false, belongsInPlayersParty: false);
		EntityHelpers.AddDwarf(DwarfType.Ritual, TutorialValueHolder.L1_dwarf6Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "???", isRequiredToExitLevelToProceedToNextLevel: false, belongsInPlayersParty: false);

		dwarfCoords.Clear();
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(2, 0));

		LevelEntrySequenceForDwarvesWithPredefinedCoords(dwarfEntities.Count, dwarfCoords, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;

		// Do not sort dwarves for tutorial
	}

	void TutorialLevel2()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, TutorialValueHolder.L2_dwarf0Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, TutorialValueHolder.L2_dwarf1Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], TutorialValueHolder.L2_dwarf0Coord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], TutorialValueHolder.L2_dwarf1Coord, true, true);
			// NOTE: 4 Other dwarves are removed by tutorial steps
			EntityHelpers.RemoveDwarf(dwarfEntities[2], dwarfEntities);
		}

		// Custom entry sequence controlled by tutorial

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;

		// Do not sort dwarves for tutorial
	}

	void TutorialLevel4()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		dwarfCoords.Clear();
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(2, 0));

		LevelEntrySequenceForDwarvesWithPredefinedCoords(dwarfEntities.Count, dwarfCoords, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;

		// Do not sort dwarves for tutorial
	}

	void TutorialLevel5()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeHammer, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gonk", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[2], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		LevelEntrySequenceForDwarves(dwarfEntities.Count, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
	}

	void TutorialLevel6()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeHammer, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gonk", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[2], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		dwarfCoords.Clear();
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(0, 1));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 1));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(0, 2));

		LevelEntrySequenceForDwarvesWithPredefinedCoords(dwarfEntities.Count, dwarfCoords, Vector2Int.up);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
	}

	void TutorialLevel7()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeHammer, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gonk", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedChain, new Vector2Int(TutorialValueHolder.L7_R1OffX + 8, TutorialValueHolder.L7_R1OffY + 6), dwarfEntities, dwarfPrefabs, dwarvesObject, "Zim", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[2], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[3], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.AddDwarf(DwarfType.RangedChain, new Vector2Int(TutorialValueHolder.L7_R1OffX + 8, TutorialValueHolder.L7_R1OffY + 6), dwarfEntities, dwarfPrefabs, dwarvesObject, "Zim", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}

		dwarfCoords.Clear();
		// dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, -1));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 1));
		// dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(2, -1));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(2, 0));
		dwarfCoords.Add(levelEntity.levelDwarfEntranceCoord + new Vector2Int(2, 1));

		LevelEntrySequenceForDwarvesWithPredefinedCoords(dwarfEntities.Count, dwarfCoords, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
	}

	void TutorialLevel8()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeHammer, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gonk", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0), dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedChain, levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0), dwarfEntities, dwarfPrefabs, dwarvesObject, "Zim", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[1], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[2], levelEntity.levelDwarfEntranceCoord, true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[3], levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0), true, true);
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[4], levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0), true, true);
		}

		LevelEntrySequenceForDwarves(dwarfEntities.Count, Vector2Int.down);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
	}

	void TestLevel0()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, TutorialValueHolder.L0_dwarf0Coord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], TutorialValueHolder.L0_dwarf0Coord, true, true);
			EntityHelpers.RemoveDwarf(dwarfEntities[2], dwarfEntities);
			EntityHelpers.RemoveDwarf(dwarfEntities[2], dwarfEntities);
			EntityHelpers.RemoveDwarf(dwarfEntities[2], dwarfEntities);
			EntityHelpers.RemoveDwarf(dwarfEntities[2], dwarfEntities);
			EntityHelpers.RemoveDwarf(dwarfEntities[2], dwarfEntities);
		}

		// Dwarf just appears, no entry sequence

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
		// Do not sort dwarves for tutorial
	}

	void TestLevel1()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			// EntityHelpers.AddDwarf(DwarfType.MeleeCleave, new Vector2Int(1, 1), dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			// EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], new Vector2Int(1, 1), true, true);
		}

		// Dwarf just appears, no entry sequence

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
		// Do not sort dwarves for tutorial
	}

	void TestLevel2()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		dwarfEntities[0].unitStatsComponent.level = 3;
		dwarfEntities[0].unitStatsComponent.levelIsDirty = true;

		LevelEntrySequenceForDwarves(dwarfEntities.Count, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
		// Do not sort dwarves for tutorial
	}

	void TestLevel3()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zim", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		dwarfEntities[0].unitStatsComponent.level = 3;
		dwarfEntities[0].unitStatsComponent.levelIsDirty = true;

		LevelEntrySequenceForDwarves(dwarfEntities.Count, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
		// Do not sort dwarves for tutorial
	}

	void TestLevel4()
	{
		if (gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves)
		{
			gameStateEntity.testingSkipSomeTutorialLevelsForLoadDwarves = false;

			//EntityHelpers.AddDwarf(DwarfType.MeleeCleave, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Maglore", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			//EntityHelpers.AddDwarf(DwarfType.MeleeSpear, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gettan", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			//EntityHelpers.AddDwarf(DwarfType.MeleeHammer, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Gonk", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			//EntityHelpers.AddDwarf(DwarfType.RangedChain, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zim", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
			EntityHelpers.AddDwarf(DwarfType.RangedBasicProjectile, levelEntity.levelDwarfEntranceCoord, dwarfEntities, dwarfPrefabs, dwarvesObject, "Zam", isRequiredToExitLevelToProceedToNextLevel: true, belongsInPlayersParty: true);
		}
		else
		{
			EntityHelpers.ResetDwarfStateForNextLevel(dwarfEntities[0], levelEntity.levelDwarfEntranceCoord, true, true);
		}

		dwarfEntities[0].unitStatsComponent.level = 3;
		dwarfEntities[0].unitStatsComponent.levelIsDirty = true;

		LevelEntrySequenceForDwarves(dwarfEntities.Count, Vector2Int.right);

		gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
		// Do not sort dwarves for tutorial
	}

	public override void SystemUpdate()
	{
		if (gameStateEntity.dwarvesShouldBeLoaded && !gameStateEntity.dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel)
		{
			gameStateEntity.dwarvesShouldBeLoaded = false;

			switch (gameStateEntity.indexToLoad)
			{
				case 0:
					TutorialLevel0();
					break;
				case 1:
					TutorialLevel1();
					break;
				case 2:
					TutorialLevel2();
					break;
				case 3:
					BasicTestLevel();
					break;
				case 4:
					TutorialLevel4();
					break;
				case 5:
					TutorialLevel5();
					break;
				case 6:
					TutorialLevel6();
					break;
				case 7:
					TutorialLevel7();
					break;
				case 8:
					TutorialLevel8();
					break;
				case 100:
					TestLevel0();
					break;
				case 101:
					TestLevel1();
					break;
				case 102:
					TestLevel2();
					break;
				case 103:
					TestLevel3();
					break;
				case 104:
					TestLevel4();
					break;
				default:
					break;
			}
		}
	}
}
