﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStateTransitionSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	bool changedAttackSpeed;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			bool stateChosen = false;

			if (!stateChosen && dwarfEntity.unitStateComponent.unitState == UnitState.Attacking && !dwarfEntity.unitAttackingStateComponent.attackBackswingFinishedPointPerformed)
			{
				stateChosen = true; // NOTE: The dwarf being in backswing is a state it can't leave easily. Consider how stuns will work vs this...
			}

			if (!stateChosen && dwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget)
			{
				TileMovementStateComponent reservedByAttackerMovementStateComponent = levelEntity.reservedByAttackerTiles[dwarfEntity.coord.x, dwarfEntity.coord.y];

				// TODO: Logic needed here to make it imossible to interrupt dwarves mid movement and start attacking
				if (LogicHelpers.AttackTargetIsInAcceptablePosition(dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord, dwarfEntity)
					&& levelEntity.rockTiles[dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord.x, dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord.y] != null
					&& (reservedByAttackerMovementStateComponent == null || Object.ReferenceEquals(reservedByAttackerMovementStateComponent, dwarfEntity.tileMovementStateComponent)) // If the current tile can be attacked from. Don't just assume you can start attacking if you're in range.
					)
				{
					if (dwarfEntity.isCoordGridAligned &&
						(dwarfEntity.unitStateComponent.unitState != UnitState.Attacking || dwarfEntity.unitAttackingStateComponent.isRefreshedPreviousFixedUpdate))
					{
						dwarfEntity.unitAttackingStateComponent.isRefreshedPreviousFixedUpdate = false;

						dwarfEntity.unitStateComponent.unitState = UnitState.Attacking;
						dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;

						// TODO: This is used for dwarves that stop in place ahead of time to attack something. They need to note this spot as reserved. Then the UnitCandidateAction... system uses this data to correctly reserve pathfinding.
						// This approach of "surprise, start attacking" is a little too ad hoc. Either dwarves should know ahead of time exactly what they plan to attack - and should repath from the beginning based on any and all changing conditions
						// Or a more formal treatment of this dynamic attack start needs to be done, with its own system
						dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord = dwarfEntity.coord;
						// NOTE: Based on my analysis of existing code, attacking does not also set this variable. One day spotToAttackFromCoord should be unified with the standing spot coord
						// dwarfEntity.tileMovementStateComponent.spotToFreeSpaceStandOnCoord = dwarfEntity.coord;

						dwarfEntity.unitAttackingStateComponent.initiateAttackRotation = true;
					}
				}
				else if (dwarfEntity.unitStateComponent.unitState == UnitState.Attacking) // TODO: This is a hack, trying to express "if you're not aligned with your target and think you're attacking, you should not be attacking"
				{
					dwarfEntity.unitStateComponent.unitState = UnitState.Idle; // This is more like a placeholder state
					dwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget = false;
					dwarfEntity.tileMovementStateComponent.hasIntendedMovementTarget = false;
					dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;
				}

				stateChosen = dwarfEntity.unitStateComponent.unitState == UnitState.Attacking;
			}

			if (!stateChosen && dwarfEntity.tileMovementStateComponent.hasIntendedMovementTarget)
			{
				if (dwarfEntity.tileMovementStateComponent.navPath.Count > 0)
				{
					if (dwarfEntity.unitStateComponent.unitState != UnitState.Moving)
					{
						dwarfEntity.unitStateComponent.unitState = UnitState.Moving;
						dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;

						// TODO: the above violates dry. SetMovementFromNavPathSystem should solely be doing this
						dwarfEntity.tileMovementStateComponent.nextCoord = dwarfEntity.tileMovementStateComponent.navPath[0];
						dwarfEntity.tileMovementStateComponent.navPath.RemoveAt(0);
						dwarfEntity.tileMovementStateComponent.readyForNextCoord = false;
					}
				}
				else if (dwarfEntity.coord == dwarfEntity.tileMovementStateComponent.nextCoord) // TODO: This seems like a hack. It correctly represents the concept "did you finish the last leg of your journey now that you're out of navpath" ... but still
				{
					dwarfEntity.unitStateComponent.unitState = UnitState.Idle; // Shouldn't be idle, what if it's in position to attack now?
					dwarfEntity.tileMovementStateComponent.hasIntendedMovementTarget = false; // TODO: this seems like a hack
					dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;
				}

				stateChosen = dwarfEntity.unitStateComponent.unitState == UnitState.Moving;
			}

			if (!stateChosen && dwarfEntity.unitStateComponent.unitState != UnitState.Idle)
			{
				dwarfEntity.unitStateComponent.unitState = UnitState.Idle;
				dwarfEntity.unitStateComponent.changedToBeClearedOnUpdate = true;

				//stateResolved = dwarfEntity.unitStateComponent.unitState == UnitState.Idle; // No need
			}
		}
	}
}
