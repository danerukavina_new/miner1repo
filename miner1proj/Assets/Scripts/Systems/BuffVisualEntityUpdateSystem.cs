﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffVisualEntityUpdateSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;
	public Dictionary<BuffType, BuffVisualAnimationControllerData> buffVisualAnimationControllers;

	RockTileEntity rockTileEntity;
	List<BuffEntity> candidateTopNBuffsToVisualize;
	List<bool> candidateTopNBuffsMatchedToIncumbentBuffVisualEntity;
	BuffEntity candidateBuffEntity;
	BuffVisualEntity incumbentBuffVisualEntity;
	bool foundCandidateForIncumbentBuffVisualEntity;

	public static void AddSortedWithLimit(List<BuffEntity> list, BuffEntity item, int countLimit)
	{
		BuffEntity listItem;
		int i;

		for (i = 0; i < list.Count && i < countLimit; i++)
		{
			listItem = list[i];

			if (listItem.visualPriorityRating < item.visualPriorityRating)
				break;
		}

		if (i < countLimit)
			list.Insert(i, item);

		if (list.Count > countLimit)
			list.RemoveAt(list.Count - 1);
	}

	public BuffVisualEntityUpdateSystem()
	{
		candidateTopNBuffsToVisualize = new List<BuffEntity>(ValueHolder.MAXIMUM_BUFFS_TO_VISUALIZE);
		candidateTopNBuffsMatchedToIncumbentBuffVisualEntity = new List<bool>(ValueHolder.MAXIMUM_BUFFS_TO_VISUALIZE);
	}

	void SetBuffVisualEntitiy(BuffVisualEntity buffVisualEntity, BuffType buffType, int candidateSortedIndexForSpriteOrdering)
	{
		buffVisualEntity.buffType = buffType;
		buffVisualEntity.animator.runtimeAnimatorController = buffVisualAnimationControllers[buffType].runtimeAnimatorController;

		buffVisualEntity.spriteRenderer.sortingOrder = ValueHolder.BASE_ORDER_IN_LAYER_FOR_BUFF_VISUAL_ENTITY_SPRITE_RENDERER + candidateSortedIndexForSpriteOrdering;

		buffVisualEntity.hasValidBuff = true;
		buffVisualEntity.gameObject.SetActive(true);

		buffVisualEntity.wasUsedAlready = true;
	}

	void SetAlreadyValidBuffVisualEntityOrdering(BuffVisualEntity buffVisualEntity, int candidateSortedIndexForSpriteOrdering)
	{
		buffVisualEntity.spriteRenderer.sortingOrder = ValueHolder.BASE_ORDER_IN_LAYER_FOR_BUFF_VISUAL_ENTITY_SPRITE_RENDERER + candidateSortedIndexForSpriteOrdering;

		buffVisualEntity.wasUsedAlready = true;
	}

	void BuffReceiverLogic(List<BuffVisualEntity> incumbentBuffVisualEntities, BuffReceiverComponent buffReceiverComponent, GameObject buffVisualsObject)
	{
		if (buffReceiverComponent.buffEntitiesListIsDirtyAfterBuffResolver)
		{
			buffReceiverComponent.buffEntitiesListIsDirtyAfterBuffResolver = false;

			candidateTopNBuffsToVisualize.Clear();
			candidateTopNBuffsMatchedToIncumbentBuffVisualEntity.Clear();

			// Get buffs that should be displayed
			foreach (var buffEntity in buffReceiverComponent.buffEntities.Values)
				AddSortedWithLimit(candidateTopNBuffsToVisualize, buffEntity, incumbentBuffVisualEntities.Count);//buffReceiverComponent.numberOfTopNBuffsToVisualize);

			foreach (var candidateTopNBuffToVisualize in candidateTopNBuffsToVisualize)
			{
				candidateTopNBuffsMatchedToIncumbentBuffVisualEntity.Add(false);
			}

			// Match currently displayed buffs with those that should be displayed
			foreach (var incumbentBuffVisualEntity in incumbentBuffVisualEntities)
				if (incumbentBuffVisualEntity.hasValidBuff)
				{
					foundCandidateForIncumbentBuffVisualEntity = false;

					for (int j = 0; j < candidateTopNBuffsToVisualize.Count && !foundCandidateForIncumbentBuffVisualEntity; j++)
					{
						candidateBuffEntity = candidateTopNBuffsToVisualize[j];

						if (incumbentBuffVisualEntity.hasValidBuff && incumbentBuffVisualEntity.buffType == candidateBuffEntity.buffType)
						{
							foundCandidateForIncumbentBuffVisualEntity = true;
							candidateTopNBuffsMatchedToIncumbentBuffVisualEntity[j] = true;

							SetAlreadyValidBuffVisualEntityOrdering(incumbentBuffVisualEntity, j);
						}
					}

					if (!foundCandidateForIncumbentBuffVisualEntity)
					{
						incumbentBuffVisualEntity.hasValidBuff = false;
						incumbentBuffVisualEntity.becameInvalid = true;
					}
				}

			// Fill all available buff visual entites
			for (int j = 0; j < candidateTopNBuffsToVisualize.Count; j++)
			{
				for (int i = 0; !candidateTopNBuffsMatchedToIncumbentBuffVisualEntity[j] && i < incumbentBuffVisualEntities.Count; i++)
				{
					incumbentBuffVisualEntity = incumbentBuffVisualEntities[i];

					if (!incumbentBuffVisualEntity.hasValidBuff)
					{
						candidateTopNBuffsMatchedToIncumbentBuffVisualEntity[j] = true;

						SetBuffVisualEntitiy(incumbentBuffVisualEntity, candidateTopNBuffsToVisualize[j].buffType, j);
					}
				}

				// It's possible all the incumbent buff visual entities are busy, in that case override an arbitrary one that hasn't been marked already with "wasUsedAlready"
				for (int i = 0; !candidateTopNBuffsMatchedToIncumbentBuffVisualEntity[j] && i < incumbentBuffVisualEntities.Count; i++)
				{
					incumbentBuffVisualEntity = incumbentBuffVisualEntities[i];

					if (!incumbentBuffVisualEntity.wasUsedAlready)
					{
						candidateTopNBuffsMatchedToIncumbentBuffVisualEntity[j] = true;

						SetBuffVisualEntitiy(incumbentBuffVisualEntity, candidateTopNBuffsToVisualize[j].buffType, j);
					}
				}
			}

			// Deactivate unused entities and then clear flags
			foreach (var incumbentBuffVisualEntity in incumbentBuffVisualEntities)
			{
				if (!incumbentBuffVisualEntity.hasValidBuff && incumbentBuffVisualEntity.becameInvalid)
					incumbentBuffVisualEntity.gameObject.SetActive(false);

				incumbentBuffVisualEntity.becameInvalid = false;
				incumbentBuffVisualEntity.wasUsedAlready = false;
			}

		}
	}

	// TODO: Test if this system really behaves correctly if incumbentBuffVisualEntities.Count = 3 and there's 5 BuffEntity vying for the top spots. Does it correctly replace incumbent buffs or not.
	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
			BuffReceiverLogic(dwarfEntity.buffVisualEntities, dwarfEntity.buffReceiverComponent, dwarfEntity.buffVisualsObject);

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				rockTileEntity = levelEntity.rockTiles[x, y];
				if (rockTileEntity != null)
					BuffReceiverLogic(rockTileEntity.buffVisualEntities, rockTileEntity.buffReceiverComponent, rockTileEntity.buffVisualsObject);
			}
	}
}
