﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadTerrainSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs;
	public Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs;
	public Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs;
	public Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs;
	public GameObject floorObject;
	public GameObject rockObject;
	public GameObject partialFogObject;
	public GameObject fullFogObject;
	public List<HPBarEntity> HPBarEntities;
	public List<BuffGiverAreaEntity> buffGiverAreaEntities;

	List<Vector2Int> unfilteredEntranceCoords;
	List<Vector2Int> filteredEntranceCoords;
	List<Vector2Int> candidateCoords;

	public LoadTerrainSystem()
	{
		unfilteredEntranceCoords = new List<Vector2Int>(
			LogicHelpers.TilesInRadius(ValueHolder.ENTRANCE_CLEARING_RADIUS)
			);

		filteredEntranceCoords = new List<Vector2Int>(
			LogicHelpers.TilesInRadius(ValueHolder.ENTRANCE_CLEARING_RADIUS) / 2 // Dividing by two since entrances discard at least half the coordinates
			);

		candidateCoords = new List<Vector2Int>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT);
	}

	public static bool CheckTilePassableReachableXY(Vector2Int coord, LevelEntity levelEntity)
	{
		// At time of writing, different than pathfinding function because it doesn't consider full fog, and allows for the BlackInaccessible tiles that are intended for replacement

		if (levelEntity.floorTiles[coord.x, coord.y].floorTileType != FloorTileType.BlackInaccessible && (!levelEntity.floorTiles[coord.x, coord.y].walkable || !levelEntity.floorTiles[coord.x, coord.y].passable))
			return false;

		RockTileEntity rockTileEntity = levelEntity.rockTiles[coord.x, coord.y];

		if (
			rockTileEntity != null &&
			((!rockTileEntity.destructible && !rockTileEntity.passable) // If it's a wall that cannot be destroyed or passed
			|| rockTileEntity.isExit) // Or if it's a level exit
			)
			return false;

		return true;
	}

	public static bool ConsiderCoordAndAddCandidates(
		Vector2Int coord,
		LevelEntity levelEntity,
		List<Vector2Int> candidateNavCoords,
		Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs,
		GameObject floorObject,
		Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs,
		GameObject partialFogObject,
		Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs,
		GameObject fullFogObject
	)
	{
		if (!LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			return false;

		if (levelEntity.floorTiles[coord.x, coord.y].floorTileType == FloorTileType.BlackInaccessible)
		{
			EntityHelpers.RemoveFloor(coord, levelEntity);
			EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
			EntityHelpers.AddPartialFog(FogShapeType.Basic, coord, levelEntity, partialFogPrefabs, partialFogObject);
			EntityHelpers.AddFullFog(FogShapeType.Basic, coord, levelEntity, fullFogPrefabs, fullFogObject);

			if (CheckTilePassableReachableXY(coord, levelEntity))
			{
				candidateNavCoords.Add(coord);
				return true;
			}
		}

		return false;
	}

	// TODO: This is pretty hacky - you can't edit floors for a level until after this is called
	private void FillAllReachablePassableFloors(Vector2Int startCoord)
	{
		candidateCoords.Clear();

		ConsiderCoordAndAddCandidates(startCoord, levelEntity, candidateCoords, floorTilesPrefabs, floorObject, partialFogPrefabs, partialFogObject, fullFogPrefabs, fullFogObject);

		int loopControl = 0;
		Vector2Int nextCandidateCoord;

		while (candidateCoords.Count > 0 && loopControl < ValueHolder.MAXIMUM_SEARCH_LOOPS)
		{
			loopControl++;

			// NOTE: Currently, this has to be eight way, or corner indestructible walls don't get fog
			foreach (var movementDirection in ValueHolder.eightWayMovementDirections)
			{
				nextCandidateCoord = candidateCoords[0] + movementDirection;
				ConsiderCoordAndAddCandidates(nextCandidateCoord, levelEntity, candidateCoords, floorTilesPrefabs, floorObject, partialFogPrefabs, partialFogObject, fullFogPrefabs, fullFogObject);
			}

			candidateCoords.RemoveAt(0);
		}

		if (loopControl == ValueHolder.MAXIMUM_SEARCH_LOOPS)
		{
			throw new System.Exception("Infinite loop while loading terrain.");
			// return;
		}
	}

	void CreateEntranceForSixDwarves(Vector2Int entranceOriginCoord)
	{
		EntityHelpers.RemoveRock(entranceOriginCoord, levelEntity);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, entranceOriginCoord, levelEntity, rockTilesPrefabs, rockObject);

		LogicHelpers.GetRectilinearCircleCoordsAroundCoordExcludingCoordWithinLevelEdges(
			entranceOriginCoord,
			ValueHolder.ENTRANCE_CLEARING_RADIUS,
			levelEntity,
			unfilteredEntranceCoords,
			filteredEntranceCoords
			);

		foreach (var coord in filteredEntranceCoords)
		{
			EntityHelpers.RemoveFloor(coord, levelEntity);
			EntityHelpers.RemoveRock(coord, levelEntity);

			EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
		}
	}

	void CreateExitAndEnsureSomeWalkability(Vector2Int exitOriginCoord)
	{
		EntityHelpers.RemoveRock(exitOriginCoord, levelEntity);
		EntityHelpers.AddRock(RockTileType.Exit, exitOriginCoord, levelEntity, rockTilesPrefabs, rockObject);

		LogicHelpers.GetRectilinearCircleCoordsAroundCoordExcludingCoordWithinLevelEdges(
			exitOriginCoord,
			ValueHolder.EXIT_CLEARING_RADIUS,
			levelEntity,
			unfilteredEntranceCoords,
			filteredEntranceCoords
			);

		foreach (var coord in filteredEntranceCoords)
		{
			if (!LogicHelpers.CheckTileMayBePassableRegardlessOfFog(coord, levelEntity))
			{
				EntityHelpers.RemoveFloor(coord, levelEntity);
				EntityHelpers.RemoveRock(coord, levelEntity);

				EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
			}
		}

		EntityHelpers.RemoveRock(exitOriginCoord, levelEntity);
		EntityHelpers.AddRock(RockTileType.Exit, exitOriginCoord, levelEntity, rockTilesPrefabs, rockObject);
	}

	void BasicTestLevelWithoutEntranceAndExit(int xSize, int ySize)
	{
		EntityHelpers.ReinitializeLevel(levelEntity, xSize, ySize, HPBarEntities, buffGiverAreaEntities);
		EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

		int x; int y;
		Vector2Int coord;
		float p;

		for (x = 0; x < levelEntity.xSize; x++)
		{
			for (y = 0; y < levelEntity.ySize; y++)
			{
				coord = new Vector2Int(x, y);

				EntityHelpers.AddPartialFog(FogShapeType.Basic, coord, levelEntity, partialFogPrefabs, partialFogObject);
				EntityHelpers.AddFullFog(FogShapeType.Basic, coord, levelEntity, fullFogPrefabs, fullFogObject);
			}
		}

		for (x = 1; x < levelEntity.xSize - 1; x++)
		{
			for (y = 1; y < levelEntity.ySize - 1; y++)
			{
				coord = new Vector2Int(x, y);

				p = UnityEngine.Random.Range(0f, 1f);

				// if (p < 0.5f)
				if (p < 0.025f)
				{
					EntityHelpers.AddFloor(FloorTileType.Void, coord, levelEntity, floorTilesPrefabs, floorObject);
				}
				// else if (p < 1f)
				else if (p < 0.065f)
				{
					EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
					EntityHelpers.AddRock(RockTileType.IndestructibleWall, coord, levelEntity, rockTilesPrefabs, rockObject);
				}
				else if (p < 0.1f)
				{
					EntityHelpers.AddFloor(FloorTileType.MasonedFloor, coord, levelEntity, floorTilesPrefabs, floorObject);
				}
				// else if (p < 1f) // NOTE: for testing fog exploration
				else if (p < 0.35f)
				{
					EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
				}
				else if (p < 0.85f)
				{
					EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
					EntityHelpers.AddRock(RockTileType.Rock1, coord, levelEntity, rockTilesPrefabs, rockObject);
				}
				else if (p < 0.93f)
				{
					EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
					EntityHelpers.AddRock(RockTileType.Copper, coord, levelEntity, rockTilesPrefabs, rockObject);
				}
				else if (p < 0.98f)
				{
					EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
					EntityHelpers.AddRock(RockTileType.Tin, coord, levelEntity, rockTilesPrefabs, rockObject);
				}
				else
				{
					EntityHelpers.AddFloor(FloorTileType.Dirt, coord, levelEntity, floorTilesPrefabs, floorObject);
					EntityHelpers.AddRock(RockTileType.Silver, coord, levelEntity, rockTilesPrefabs, rockObject);
				}
			}
		}
	}

	void BasicTestLevelWithoutExit(int xSize, int ySize)
	{
		BasicTestLevelWithoutEntranceAndExit(xSize, ySize);

		levelEntity.levelDwarfEntranceCoord = LogicHelpers.WestEdgeRandomCoordNoCorners(levelEntity);
		CreateEntranceForSixDwarves(levelEntity.levelDwarfEntranceCoord);
	}

	// TODO: Unused, remove
	void BasicTestLevel(int xSize, int ySize)
	{
		BasicTestLevelWithoutExit(xSize, ySize);

		levelEntity.levelExitCoord = LogicHelpers.NorthEastSouthEdgeRandomCoordNoCorners(levelEntity);
		CreateExitAndEnsureSomeWalkability(levelEntity.levelExitCoord);
	}

	void ImprovedTestLevel(int xSize, int ySize)
	{
		BasicTestLevelWithoutExit(xSize, ySize);

		levelEntity.levelExitCoord = LogicHelpers.NorthEastSouthEdgeRandomCoordNoCornersNotNearEntrance(levelEntity, levelEntity.levelDwarfEntranceCoord);
		CreateExitAndEnsureSomeWalkability(levelEntity.levelExitCoord);
	}

	void LevelWithPredeterminedEntranceAndExit(int xSize, int ySize, Vector2Int entranceCoord, Vector2Int exitCoord)
	{
		BasicTestLevelWithoutEntranceAndExit(xSize, ySize);

		levelEntity.levelDwarfEntranceCoord = entranceCoord;
		CreateEntranceForSixDwarves(levelEntity.levelDwarfEntranceCoord);

		levelEntity.levelExitCoord = exitCoord;
		CreateExitAndEnsureSomeWalkability(levelEntity.levelExitCoord);
	}

	private void TutorialLevel0()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, 17, 9 + TutorialValueHolder.L0_R1OffY, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		xOff = 0;
		yOff = TutorialValueHolder.L0_R1OffY;

		// NOTE: Not setting a levelDwarfEntrance coord for this level, doing a custom entry sequence

		// Room 1
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.EggChamberOpen, new Vector2Int(xOff + 2, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 9, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);

		// Room 2
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + -2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + -1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		// EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 1), levelEntity, rockTilesPrefabs, rockObject); // NOTE: This one gets seen innappropriately, so hiding it. A little odd.

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 9, yOff + 0), levelEntity, rockTilesPrefabs, rockObject); // NOTE: This is a hack to make the void nicer while keeping the fill algorithm working
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + -2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + -1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 16, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 11, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 12, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 12, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 12, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 12, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, TutorialValueHolder.L0_copperCoord, levelEntity, rockTilesPrefabs, rockObject); // new Vector2Int(xOff + 13, yOff + 5)
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 14, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(TutorialValueHolder.L0_dwarf0Coord);

		// TODO: This is needed because the egg chamber, being impassible, blocks the tile filling algorithm. Maybe it should just treat IndestructibleWall, Entrances, Exits as the criterion for stopping tile fill?
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 1, yOff + 3), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(xOff + 1, yOff + 3), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.AddFullFog(FogShapeType.Basic, new Vector2Int(xOff + 1, yOff + 3), levelEntity, fullFogPrefabs, fullFogObject);

		// TODO: Replace with the bounds based functions
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 9, yOff + -2), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 9, yOff + -2), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 9, yOff + -1), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 9, yOff + -1), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 9, yOff + 0), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 9, yOff + 0), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 10, yOff + -2), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 10, yOff + -2), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 10, yOff + -1), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 10, yOff + -1), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 10, yOff + 0), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 10, yOff + 0), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 11, yOff + -2), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 11, yOff + -2), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 11, yOff + -1), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 11, yOff + -1), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 11, yOff + 0), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 11, yOff + 0), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 12, yOff + -2), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 12, yOff + -2), levelEntity, floorTilesPrefabs, floorObject);
		EntityHelpers.RemoveFloor(new Vector2Int(xOff + 12, yOff + -1), levelEntity);
		EntityHelpers.AddFloor(FloorTileType.Void, new Vector2Int(xOff + 12, yOff + -1), levelEntity, floorTilesPrefabs, floorObject);


	}

	private void TutorialLevel1()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, TutorialValueHolder.R1Width + TutorialValueHolder.R2Width, TutorialValueHolder.questionmarkOffestY + TutorialValueHolder.R1Height, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;


		// R1
		xOff = TutorialValueHolder.R1offX;
		yOff = TutorialValueHolder.R1offY;

		levelEntity.levelDwarfEntranceCoord = new Vector2Int(xOff + 0, yOff + 3);

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 3, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 7, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 7, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 8, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);


		//CreateEntranceForSixDwarves(levelEntity.levelDwarfEntranceCoord);

		// R2
		xOff = TutorialValueHolder.R2offX;
		yOff = TutorialValueHolder.R2offY;

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 10, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 1, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 2, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 7, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 7, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 8, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 8, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 9, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 9, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		// levelEntity.levelExitCoord = new Vector2Int(R1offX + 9, R1offY + 2); // TODO: temporary

		// R3
		xOff = TutorialValueHolder.R3offX;
		yOff = TutorialValueHolder.R3offY;

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 2, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 2, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 2, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 3, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 3, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 3, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 3, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 3, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 4, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 4, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);

		// R4
		xOff = TutorialValueHolder.R4offX;
		yOff = TutorialValueHolder.R4offY;

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 8, yOff + 3), levelEntity, rockTilesPrefabs, rockObject); // True exit
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 8, yOff + 4), levelEntity, rockTilesPrefabs, rockObject); // True exit
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		//EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 9, yOff + 3), levelEntity, rockTilesPrefabs, rockObject); // True exit // TODO: figure out a way to make a limited depth exit
		//EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 9, yOff + 4), levelEntity, rockTilesPrefabs, rockObject); // True exit
		//EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		//EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock2, TutorialValueHolder.L1_ritual00Coord, levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, TutorialValueHolder.L1_ritual01Coord, levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, TutorialValueHolder.L1_ritual10Coord, levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, TutorialValueHolder.L1_ritual11Coord, levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(TutorialValueHolder.L1_dwarf0Coord);

		//CreateEntranceForSixDwarves(levelEntity.levelExitCoord);
	}

	private void TutorialLevel2()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, 14, 10, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		xOff = 0;
		yOff = TutorialValueHolder.L2_R1OffY;

		// NOTE: Not setting a levelDwarfEntrance coord for this level, doing a custom entry sequence

		// Room 1
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + -2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + -1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + -2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + -3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + -2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + -1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 13, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 9, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 9, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 10, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 12, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(new Vector2Int(xOff + 1, yOff + 1));

		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(4, yOff + -2, 0, 5, 3, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(4, yOff + 3, 0, 5, 3, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.Dirt, new Vector2Int(4, yOff + -2), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.Dirt, new Vector2Int(8, yOff + 3), levelEntity, floorTilesPrefabs, floorObject);
	}

	void TutorialLevel3()
	{
		LevelWithPredeterminedEntranceAndExit(TutorialValueHolder.L3_Width, TutorialValueHolder.L3_Height, TutorialValueHolder.L3_EntranceCoord, TutorialValueHolder.L3_ExitCoord);
	}

	void TutorialLevel4()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, TutorialValueHolder.L4_Width, TutorialValueHolder.L4_Height, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		xOff = TutorialValueHolder.L4_R1OffX;
		yOff = TutorialValueHolder.L4_R1OffY;

		// Room 1

		levelEntity.levelDwarfEntranceCoord = new Vector2Int(xOff + 0, yOff + 2);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		// EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		// EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		// EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 12, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 13, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 13, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		// EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 16, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 8, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 11, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 11, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 11, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 12, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 12, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 12, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 12, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 12, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 13, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 14, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 14, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 14, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 14, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 14, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 15, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 15, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1,0));

		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 2, yOff + 4), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 3, yOff + 3), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 4, yOff + 3), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 5, yOff + 2), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 6, yOff + 2), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 10, yOff + 1), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 10, yOff + 2), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 10, yOff + 3), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 11, yOff + 2), levelEntity, floorTilesPrefabs, floorObject);
	}

	void TutorialLevel5()
	{
		ImprovedTestLevel(TutorialValueHolder.L5_Width, TutorialValueHolder.L5_Height);
	}

	void TutorialLevel6()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, TutorialValueHolder.L6_Width, TutorialValueHolder.L6_Height, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		xOff = TutorialValueHolder.L6_R1OffX;
		yOff = TutorialValueHolder.L6_R1OffY;

		// Room 1

		levelEntity.levelDwarfEntranceCoord = new Vector2Int(xOff + 2, yOff + 0);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 0, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 2, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 3, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 16), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 17), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 15), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 16), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 15), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 19), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 19), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 19), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 11, yOff + 19), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 19), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 19), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 15), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 15, yOff + 16), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 17), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 15, yOff + 18), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 3, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 4, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 4, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 4, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 9), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 5, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 6, yOff + 10), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 7, yOff + 15), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 9, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 10, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 10, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 11, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 11, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 12, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 12, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 13, yOff + 11), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 13, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 13, yOff + 13), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 15), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 14, yOff + 12), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 14, yOff + 14), levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(levelEntity.levelDwarfEntranceCoord + new Vector2Int(0, 1));

		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 5, yOff + 3, 0, 1, 3, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 6, yOff + 1, 0, 5, 11, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.Dirt, new Vector2Int(xOff + 6, yOff + 10), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.Dirt, new Vector2Int(xOff + 6, yOff + 11), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.Dirt, new Vector2Int(xOff + 10, yOff + 1), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 11, yOff + 3, 0, 1, 6, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 8, yOff + 12, 0, 3, 1, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 5, yOff + 16, 0, 8, 2, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.Dirt, new Vector2Int(xOff + 5, yOff + 16), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 9, yOff + 15, 0, 3, 1, 0), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 9, yOff + 18, 0, 4, 1, 0), levelEntity, floorTilesPrefabs, floorObject);

		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 2, yOff + 7), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 2, yOff + 8), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 3, yOff + 9), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 3, yOff + 10), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 4, yOff + 11), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 4, yOff + 12), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 5, yOff + 13), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 0, yOff + 4), levelEntity, floorTilesPrefabs, floorObject);
		ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 1, yOff + 4), levelEntity, floorTilesPrefabs, floorObject);
	}

	void TutorialLevel7()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, TutorialValueHolder.L7_Width, TutorialValueHolder.L7_Height, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		xOff = TutorialValueHolder.L7_R1OffX;
		yOff = TutorialValueHolder.L7_R1OffY;

		// Room 1

		levelEntity.levelDwarfEntranceCoord = new Vector2Int(xOff + 0, yOff + 2);

		EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject, addFloors:false);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 0, yOff + 2), levelEntity);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 0, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 0, yOff + 7), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 0, yOff + 8), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 1, yOff + 8), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 4, yOff + 0), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 13, yOff + 0), levelEntity);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 13, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 13, yOff + 8), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 14, yOff + 0), levelEntity);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 14, yOff + 0), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 14, yOff + 8), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 15, yOff + 6), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 15, yOff + 7), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 15, yOff + 8), levelEntity);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 2, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 3, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 6, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 9, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 10, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 12, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 13, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 14, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 3, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 4, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(xOff + 4, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 7, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 8, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 8, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 9, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 9, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 9, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 9, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 10, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 10, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 10, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 11, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 11, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 12, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 12, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 12, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 12, yOff + 6), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 13, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 13, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Tin, new Vector2Int(xOff + 13, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 14, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 14, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 14, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 14, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(levelEntity.levelDwarfEntranceCoord + new Vector2Int(1, 0));

		// ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 5, yOff + 3, 0, 1, 3, 0), levelEntity, floorTilesPrefabs, floorObject);
		// ReplaceFloor(FloorTileType.Dirt, new Vector2Int(xOff + 6, yOff + 10), levelEntity, floorTilesPrefabs, floorObject);

		// ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 2, yOff + 7), levelEntity, floorTilesPrefabs, floorObject);
	}

	void TutorialLevel8()
	{
		EntityHelpers.ReinitializeLevel(levelEntity, TutorialValueHolder.L8_Width, TutorialValueHolder.L8_Height, HPBarEntities, buffGiverAreaEntities);

		int x, y, xOff, yOff;

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.BlackInaccessible, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		xOff = TutorialValueHolder.L8_R1OffX;
		yOff = TutorialValueHolder.L8_R1OffY;

		// Room 1

		levelEntity.levelDwarfEntranceCoord = new Vector2Int(xOff + 1, yOff + 8);

		EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject, addFloors: false);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 0, yOff + 0), levelEntity);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 1, yOff + 8), levelEntity);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 1, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 2, yOff + 8), levelEntity);
		EntityHelpers.AddRock(RockTileType.EntranceImpassable, new Vector2Int(xOff + 2, yOff + 8), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 9, yOff + 1), levelEntity);
		EntityHelpers.AddRock(RockTileType.Exit, new Vector2Int(xOff + 9, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.RemoveRock(new Vector2Int(xOff + 9, yOff + 8), levelEntity);

		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 1, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 4, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 5, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.IndestructibleWall, new Vector2Int(xOff + 8, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 1, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 2, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 3, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 3, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 4, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 4, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 4, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 5, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 5, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 5, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 6, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 6, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 6, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 6, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 6, yOff + 7), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 7, yOff + 1), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 7, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.EggChamberClosed, new Vector2Int(TutorialValueHolder.L8_eggChamberOpenCoord.x, TutorialValueHolder.L8_eggChamberOpenCoord.y), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 7, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 7, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 8, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 8, yOff + 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock2, new Vector2Int(xOff + 8, yOff + 4), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Silver, new Vector2Int(xOff + 8, yOff + 5), levelEntity, rockTilesPrefabs, rockObject);

		// EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(xOff + 3, yOff + 2), levelEntity, rockTilesPrefabs, rockObject);

		FillAllReachablePassableFloors(levelEntity.levelDwarfEntranceCoord + new Vector2Int(0, -1));

		//ReplaceFloorInBounds(FloorTileType.Void, new BoundsInt(xOff + 5, yOff + 3, 0, 1, 3, 0), levelEntity, floorTilesPrefabs, floorObject);
		//ReplaceFloor(FloorTileType.Dirt, new Vector2Int(xOff + 6, yOff + 10), levelEntity, floorTilesPrefabs, floorObject);

		//ReplaceFloor(FloorTileType.MasonedFloor, new Vector2Int(xOff + 2, yOff + 7), levelEntity, floorTilesPrefabs, floorObject);
	}

	void TestLevel0()
	{
		TutorialLevel0();
	}

	void TestLevel1()
	{
		int x, y;

		EntityHelpers.InitializeLevelEntity(levelEntity, 9, 7);
		EntityHelpers.SetLevelEntityEdgesToIndestructibleRockTiles(levelEntity, floorTilesPrefabs, floorObject, rockTilesPrefabs, rockObject);

		for (x = 0; x < levelEntity.xSize; x++)
			for (y = 0; y < levelEntity.ySize; y++)
				EntityHelpers.AddFloor(FloorTileType.Dirt, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);

		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(1, 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Rock1, new Vector2Int(2, 2), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(1, 3), levelEntity, rockTilesPrefabs, rockObject);
		EntityHelpers.AddRock(RockTileType.Copper, new Vector2Int(7, 1), levelEntity, rockTilesPrefabs, rockObject);
	}

	void TestLevel2()
	{
		ImprovedTestLevel(7, 9);
	}

	void TestLevel3()
	{
		ImprovedTestLevel(7, 9);
	}

	void TestLevel4()
	{
		ImprovedTestLevel(24, 9);
	}

	static void ReplaceFloor(FloorTileType replacementFloorTileType, Vector2Int coord, LevelEntity levelEntity, Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs, GameObject floorObject)
	{
		EntityHelpers.RemoveFloor(coord, levelEntity);
		EntityHelpers.AddFloor(replacementFloorTileType, coord, levelEntity, floorTilesPrefabs, floorObject);
	}

	static void ReplaceFloorInBounds(FloorTileType replacementFloorTileType, BoundsInt bounds2d, LevelEntity levelEntity, Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs, GameObject floorObject)
	{
		for (int x = bounds2d.xMin; x < bounds2d.xMax; x++)
		{
			for (int y = bounds2d.yMin; y < bounds2d.yMax; y++)
			{
				ReplaceFloor(replacementFloorTileType, new Vector2Int(x, y), levelEntity, floorTilesPrefabs, floorObject);
			}
		}
	}

	public override void SystemUpdate()
	{
		if (gameStateEntity.levelShouldBeLoaded && !gameStateEntity.dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel)
		{
			gameStateEntity.levelShouldBeLoaded = false;

			switch (gameStateEntity.indexToLoad)
			{
				case 0:
					TutorialLevel0();
					break;
				case 1:
					TutorialLevel1();
					break;
				case 2:
					TutorialLevel2();
					break;
				case 3:
					TutorialLevel3();
					break;
				case 4:
					TutorialLevel4();
					break;
				case 5:
					TutorialLevel5();
					break;
				case 6:
					TutorialLevel6();
					break;
				case 7:
					TutorialLevel7();
					break;
				case 8:
					TutorialLevel8();
					break;
				case 100:
					TestLevel0();
					break;
				case 101:
					TestLevel1();
					break;
				case 102:
					TestLevel2();
					break;
				case 103:
					TestLevel3();
					break;
				case 104:
					TestLevel4();
					break;
				default:
					break;
			}
		}
	}
}

