﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareFixedUpdateFlagsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (gameStateEntity.playerCoordinatesChanged)
				dwarfEntity.navigationComponent.levelDataWasDirtied = true;
		}

		if (gameStateEntity.playerCoordinatesChanged)
			gameStateEntity.playerCoordinatesChanged = false;
	}
}
