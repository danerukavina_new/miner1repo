﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialExecuteStepSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;
	public Camera mainCamera;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity;
	public TutorialInputIndicatorEntity tutorialInputIndicatorEntity;
	public Dictionary<TutorialInputIndicatorType, TutorialInputIndicatorAnimationControllerData> tutorialInputIndicatorAnimationControllers;
	public GameObject openDwarfUpgradesButtonCanvasEntity;
	public Dictionary<DwarfType, DwarfEntity> dwarfPrefabs;
	public Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs;
	public GameObject dwarvesObject;
	public GameObject rocksObject;

	TutorialsStateComponent tutorialsStateComponent;
	TutorialStepEntity tutorialStepEntity;
	int removals;
	int removalAdjustedIndex;
	TileMovementStateComponent facingDirectionTileMovementStateComponent;
	DwarfEntity dwarfEntity;

	public override void SystemUpdate()
	{
		tutorialsStateComponent = gameStateEntity.tutorialsStateComponent;

		if (tutorialsStateComponent.activateTutorialStepThisUpdate)
		{
			tutorialsStateComponent.activateTutorialStepThisUpdate = false;

			tutorialsStateComponent.tutorialStepIsActive = true;
		}

		if (tutorialsStateComponent.tutorialStepIsActive)
		{
			if (tutorialsStateComponent.currentOrNextTutorialStep < tutorialsStateComponent.tutorialStepEntities.Count)
			{
				tutorialStepEntity = tutorialsStateComponent.tutorialStepEntities[tutorialsStateComponent.currentOrNextTutorialStep];

				foreach (var tutorialStepComponentTag in tutorialStepEntity.tutorialStepComponentTags)
				{
					switch (tutorialStepComponentTag)
					{
						case TutorialStepComponentTag.TimePauseWithDuration:
							throw new System.Exception("NYI tutorial step");
						case TutorialStepComponentTag.TimePause:
							Time.timeScale = 0f; // TODO: Untested
							break;
						case TutorialStepComponentTag.TimeUnpause:
							Time.timeScale = 1f; // TODO: Untested
							break;
						case TutorialStepComponentTag.DialogueSteps:

							// This is a readonly sort of situation. Can this be marked in code?
							gameStateEntity.dialogueStateComponent.initiateInteractiveDialogue = true;
							gameStateEntity.dialogueStateComponent.dialogueStepEntities = tutorialStepEntity.dialogueStepEntities;
							break;
						case TutorialStepComponentTag.SetTutorialCameraBoundsWhichMayPanCamera:
							tutorialsStateComponent.hasTutorialCameraBounds = true;
							tutorialsStateComponent.tutorialCameraBounds = tutorialStepEntity.tutorialCameraBounds;
							break;
						case TutorialStepComponentTag.RemoveTutorialCameraBounds:
							tutorialsStateComponent.hasTutorialCameraBounds = false;
							break;
						case TutorialStepComponentTag.TeleportCamera:
							mainCamera.transform.position = new Vector3(tutorialStepEntity.teleportCameraPosition.x, tutorialStepEntity.teleportCameraPosition.y, mainCamera.transform.position.z);
							break;
						case TutorialStepComponentTag.AssignManualCoordinates:
							LogicHelpers.SetSharedManualCoordinates(tutorialStepEntity.assignedManualCoordinates, gameStateEntity, levelEntity);
							break;
						case TutorialStepComponentTag.ClearManualCoordinates:
							EntityHelpers.RemoveSharedManualCoordinates(levelEntity);
							break;
						case TutorialStepComponentTag.DisableDwarfCandidateActionProcessing:
							foreach (var dwarfEntityID in tutorialStepEntity.disableDwarfCandidateActionProcessingIDs)
							{
								dwarfEntities[dwarfEntityID].tutorialDisabledCandidateActionProcessing = true;
							}
							break;
						case TutorialStepComponentTag.ReenableDwarfCandidateActionProcessing:
							foreach (var dwarfEntityID in tutorialStepEntity.reenableDwarfCandidateActionProcessingIDs)
							{
								dwarfEntities[dwarfEntityID].tutorialDisabledCandidateActionProcessing = false;
							}
							break;
						case TutorialStepComponentTag.ReenableAllDwarfCandidateActionProcessing:
							LogicHelpers.ReenableAllDwarfCandidateActionProcessing(dwarfEntities);
							break;
						case TutorialStepComponentTag.DisableDwarfFogSight:
							foreach (var dwarfEntityID in tutorialStepEntity.disableDwarfFogSightIDs)
							{
								dwarfEntities[dwarfEntityID].fogSightComponent.fogSightEnabled = false;
							}
							break;
						case TutorialStepComponentTag.ReenableDwarfFogSight:
							foreach (var dwarfEntityID in tutorialStepEntity.reenableDwarfFogSightIDs)
							{
								dwarfEntities[dwarfEntityID].fogSightComponent.fogSightEnabled = true;
							}
							break;
						case TutorialStepComponentTag.ReenableAllDwarfFogSight:
							foreach (var dwarfEntity in dwarfEntities)
							{
								dwarfEntity.fogSightComponent.fogSightEnabled = true;
							}
							break;
						case TutorialStepComponentTag.DisableManualCoordinateInput:
							gameStateEntity.tutorialDisabledManualCoordinateInput = true;
							break;
						case TutorialStepComponentTag.EnableManualCoordinateInput:
							gameStateEntity.tutorialDisabledManualCoordinateInput = false;
							break;
						case TutorialStepComponentTag.DisableCameraInput:
							gameStateEntity.tutorialDisabledCameraInput = true;
							break;
						case TutorialStepComponentTag.EnableCameraInput:
							gameStateEntity.tutorialDisabledCameraInput = false;
							break;
						case TutorialStepComponentTag.DisableAllButExitInput:
							gameStateEntity.tutorialDisableAllButExitInput = true;
							break;
						case TutorialStepComponentTag.RenableNonExitInput:
							gameStateEntity.tutorialDisableAllButExitInput = false;
							break;
						case TutorialStepComponentTag.SetRockHPs:
							// TODO: Unchecked HP removal like this is a little dirty, it would be better to handle it via an AttackDamageInstance
							for (int i = 0; i < tutorialStepEntity.rocksToSetHPsCoords.Count; i++)
								levelEntity.rockTiles[tutorialStepEntity.rocksToSetHPsCoords[i].x, tutorialStepEntity.rocksToSetHPsCoords[i].y].HP = tutorialStepEntity.rockHPsToBeSetValues[i];
							break;
						case TutorialStepComponentTag.CreateRocks:
							for (int i = 0; i < tutorialStepEntity.rocksToBeCreatedTypes.Count; i++)
								EntityHelpers.AddRock(tutorialStepEntity.rocksToBeCreatedTypes[i], tutorialStepEntity.rocksToBeCreatedCoords[i], levelEntity, rockTilesPrefabs, rocksObject);
							break;
						case TutorialStepComponentTag.RemoveRocks:
							foreach (var rocksToBeRemovedCoord in tutorialStepEntity.rocksToBeRemovedCoords)
								EntityHelpers.RemoveRock(rocksToBeRemovedCoord, levelEntity);
							break;
						case TutorialStepComponentTag.CreateDwarves:
							for (int i = 0; i < tutorialStepEntity.dwarvesToBeCreatedTypes.Count; i++)
							{
								EntityHelpers.AddDwarfAtPartyIndex(
									tutorialStepEntity.dwarvesToBeCreatedPartyInsertionIndexes[i],
									tutorialStepEntity.dwarvesToBeCreatedTypes[i], 
									tutorialStepEntity.dwarvesToBeCreatedCoords[i],
									dwarfEntities,
									dwarfPrefabs, 
									dwarvesObject, 
									tutorialStepEntity.dwarvesToBeCreatedNames[i], 
									tutorialStepEntity.dwarvesToBeCreatedExitLevelRequirement[i],
									tutorialStepEntity.dwarvesToBeCreatedBelongingInPlayersPartisanship[i]
									);
							}

							gameStateEntity.dwarfPartyRearrangeOccurredRefreshUpgradesPanel = true;
							break;
						case TutorialStepComponentTag.RemoveDwarves:
							removals = 0;
							foreach (var dwarfToBeRemovedID in tutorialStepEntity.dwarvesToBeRemovedIDs)
							{
								removalAdjustedIndex = dwarfToBeRemovedID - removals;
								GameObject.Destroy(dwarfEntities[removalAdjustedIndex].gameObject);
								dwarfEntities.RemoveAt(removalAdjustedIndex);
								removals++;
							}
							break;
						case TutorialStepComponentTag.CreateAttacks:
							throw new System.Exception("NYI tutorial step");
						case TutorialStepComponentTag.DisableSmelts:
							throw new System.Exception("NYI tutorial step");
						case TutorialStepComponentTag.EnableSmelts:
							throw new System.Exception("NYI tutorial step");
						case TutorialStepComponentTag.DisableResourceGains:
							gameStateEntity.tutorialDisabledResourceGains = true;
							break;
						case TutorialStepComponentTag.EnableResourceGains:
							gameStateEntity.tutorialDisabledResourceGains = false;
							break;
						case TutorialStepComponentTag.RemoveFullFog:
							foreach (var fullFogCoord in tutorialStepEntity.fullFogsToBeRemovedCoords)
								EntityHelpers.RemoveFullFog(fullFogCoord, levelEntity);
							break;
						case TutorialStepComponentTag.SetIdleDwarfFacingDirections:
							for (int i = 0; i < tutorialStepEntity.idleDwarvesToHaveFacingDirectionSetIDs.Count; i++)
							{
								facingDirectionTileMovementStateComponent = dwarfEntities[tutorialStepEntity.idleDwarvesToHaveFacingDirectionSetIDs[i]].tileMovementStateComponent;

								facingDirectionTileMovementStateComponent.normalizedGridAlignedDirection = tutorialStepEntity.idleDwarfFacingDirectionNormalizedGridAlignedDirections[i];
								facingDirectionTileMovementStateComponent.directionChangeToBeCheckedOnUpdate = true;
							}
							break;
						case TutorialStepComponentTag.ActivateDwarfUpgradeDisplayInContinueModeWithContinueDisabled:
							gameStateEntity.openDwarfUpgradesClickedOrTriggered = true;
							gameStateEntity.openDwarfUpgradesWithCloseButton = false;
							gameStateEntity.openDwarfUpgradesWithContinueButton = true;
							gameStateEntity.disableDwarfUpgradeContinueButton = true;
							break;
						case TutorialStepComponentTag.DisableDwarfUpgradeContinueButton:
							gameStateEntity.disableDwarfUpgradeContinueButton = true;
							break;
						case TutorialStepComponentTag.ReenableDwarfUpgradeContinueButton:
							gameStateEntity.reenableDwarfUpgradeContinueButton = true;
							break;
						case TutorialStepComponentTag.HideDwarfUpgradeButtons:
							gameStateEntity.hideDwarfUpgradeButtons = true;
							gameStateEntity.hideDwarfUpgradeButtonDwarfIDs.Clear();
							gameStateEntity.hideDwarfUpgradeButtonDwarfIDs.AddRange(tutorialStepEntity.hideDwarfUpgradeButtonDwarfIDs);
							break;
						case TutorialStepComponentTag.ReshowDwarfUpgradeButtons:
							gameStateEntity.reshowDwarfUpgradeButtons = true;
							gameStateEntity.reshowDwarfUpgradeButtonDwarfIDs.Clear();
							gameStateEntity.reshowDwarfUpgradeButtonDwarfIDs.AddRange(tutorialStepEntity.reshowDwarfUpgradeButtonDwarfIDs);
							break;
						case TutorialStepComponentTag.HideAllDwarfUpgradeButtons:
							gameStateEntity.hideAllDwarfUpgradeButtons = true;
							break;
						case TutorialStepComponentTag.ReshowAllDwarfUpgradeButtons:
							gameStateEntity.reshowAllDwarfUpgradeButtons = true;
							break;
						case TutorialStepComponentTag.DisableDwarfUpgradesTriggeringAtEndsOfLevels:
							gameStateEntity.dwarfUpgradesTriggersAtEndsOfLevels = false;
							break;
						case TutorialStepComponentTag.ReenableDwarfUpgradesTriggeringAtEndsOfLevels:
							gameStateEntity.dwarfUpgradesTriggersAtEndsOfLevels = true;
							break;
						case TutorialStepComponentTag.ReshowOpenDwarfUpgradesButton:
							openDwarfUpgradesButtonCanvasEntity.SetActive(true);
							break;
						case TutorialStepComponentTag.HideOpenDwarfUpgradesButton:
							openDwarfUpgradesButtonCanvasEntity.SetActive(false);
							break;
						case TutorialStepComponentTag.AddTutorialInputIndicatorWithPossibleText:
							tutorialInputIndicatorEntity.inputIndicatorAnimator.runtimeAnimatorController = tutorialInputIndicatorAnimationControllers[tutorialStepEntity.addedTutorialInputIndicatorType].runtimeAnimatorController;

							if (tutorialStepEntity.addedTutorialInputIndicatorOptionalText.Length > 0)
								tutorialInputIndicatorEntity.optionalText.text = tutorialStepEntity.addedTutorialInputIndicatorOptionalText;
							else
								tutorialInputIndicatorEntity.optionalText.text = string.Empty;

							tutorialInputIndicatorEntity.gameObject.transform.position = new Vector3(tutorialStepEntity.addedTutorialInputIndicatorPosition.x, tutorialStepEntity.addedTutorialInputIndicatorPosition.y, 0f);

							tutorialInputIndicatorEntity.gameObject.SetActive(true);
							break;
						case TutorialStepComponentTag.RemoveTutorialInputIndicatorWithPossibleText:
							tutorialInputIndicatorEntity.gameObject.SetActive(false);
							break;
						case TutorialStepComponentTag.AddTutorialInputMask:
							throw new System.Exception("NYI tutorial step: " + tutorialStepComponentTag);
						case TutorialStepComponentTag.RemoveTutorialInputMask:
							throw new System.Exception("NYI tutorial step: " + tutorialStepComponentTag);
						case TutorialStepComponentTag.ChangeTutorialSmeltControlSetting:
							gameStateEntity.tutorialSmeltControlSetting = tutorialStepEntity.tutorialSmeltControlSettingToBeChangedTo;
							break;
						case TutorialStepComponentTag.StopDwarfAttacksInPlaceAfterBackswing:
							// TODO: How does this clash with UnitCandidateActionByPathfindingSystem? Presumably the candidate action by pathfinding will override this value. Is that ok?
							foreach (var dwarfEntityID in tutorialStepEntity.stopDwarfAttacksInPlaceAfterBackswingIDs)
							{
								dwarfEntity = dwarfEntities[dwarfEntityID];

								dwarfEntity.candidateUnitAttackingStateComponent.hasIntendedAttackTarget = false;
								dwarfEntity.candidateUnitAttackingStateComponent.candidatePrimedForChecking = true;

								// TODO: This part doesn't seem to achieve anything
								dwarfEntity.candidateTileMovementStateComponent.hasIntendedMovementTarget = true;
								dwarfEntity.candidateTileMovementStateComponent.navPath.Clear();
								dwarfEntity.candidateTileMovementStateComponent.navPath.Add(dwarfEntity.coord);
								dwarfEntity.candidateTileMovementStateComponent.candidatePrimedForChecking = true;
							}
							break;
						default:
							throw new System.Exception("NYI tutorial step: " + tutorialStepComponentTag);
					}
				}

				tutorialsStateComponent.tutorialStepIsActive = false;
				tutorialsStateComponent.currentOrNextTutorialStep++;
				tutorialsStateComponent.endTimeofPreviousTutorialStep = Time.time;
			}
		}
	}
}