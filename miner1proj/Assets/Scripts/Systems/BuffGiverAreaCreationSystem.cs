﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffGiverAreaCreationSystem : BaseSystem
{
	public List<BuffGiverAreaEntity> buffGiverAreaEntities;
	public List<DwarfEntity> dwarfEntities;
	public LevelEntity levelEntity;

	public Dictionary<BuffGiverAreaType, BuffGiverAreaEntity> buffGiverAreaPrefabs;
	public GameObject buffGiverAreasObject;

	public List<DamageOrBuffTextEntity> damageOrBuffTextEntities;
	public Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs;
	public GameObject damageOrBuffTexts;

	RockTileEntity rockTileEntity;

	// NOTE: Currently all buffs are created on the dwarf/node
	void BuffGiverAreaCreatorLogic(BuffGiverAreaCreatorComponent buffGiverAreaCreatorComponent, Vector2Int originCoord)
	{
		foreach (var buffGiverAreaTypeToBeCreated in buffGiverAreaCreatorComponent.buffGiverAreasToBeCreated)
		{
			EntityHelpers.AddBuffGiverAreaEntity(buffGiverAreaTypeToBeCreated, originCoord, buffGiverAreaEntities, buffGiverAreaPrefabs, buffGiverAreasObject);

			EntityHelpers.AddBuffText(
				DamageOrBuffTextType.Buff,
				ValueHolder.buffGiverAreaBuffTexts[buffGiverAreaTypeToBeCreated],
				originCoord,
				damageOrBuffTextEntities,
				damageOrBuffTextPrefabs,
				damageOrBuffTexts
				);
		}

		buffGiverAreaCreatorComponent.buffGiverAreasToBeCreated.Clear();
	}

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
			BuffGiverAreaCreatorLogic(dwarfEntity.buffGiverAreaCreatorComponent, dwarfEntity.coord);

		for (int x = 0; x < levelEntity.xSize; x++)
			for (int y = 0; y < levelEntity.ySize; y++)
			{
				rockTileEntity = levelEntity.rockTiles[x, y];
				if (rockTileEntity != null)
					BuffGiverAreaCreatorLogic(rockTileEntity.buffGiverAreaCreatorComponent, rockTileEntity.coord);
			}
	}
}
