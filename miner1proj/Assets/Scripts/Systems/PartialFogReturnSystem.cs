﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartialFogReturnSystem : BaseSystem
{
	public LevelEntity levelEntity;
	public List<DwarfEntity> dwarfEntities;

	public override void SystemUpdate()
	{
		bool somethingThatCanCausePartialFogChanged = false;

		PartialFogEntity partialFogEntity;

		// TODO: Implement it to track subscribers to partial fog tiles, and control partial fog that way (it's more efficient)

		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.coordWasDirtiedThisFixedUpdate)
			{
				somethingThatCanCausePartialFogChanged = true;
			}
		}

		if (somethingThatCanCausePartialFogChanged)
		{
			for (int x = 0; x < levelEntity.xSize; x++)
			{
				for (int y = 0; y < levelEntity.ySize; y++)
				{
					partialFogEntity = levelEntity.partialFogTiles[x, y];

					if (partialFogEntity != null)
						partialFogEntity.gameObject.SetActive(true);
				}
			}
		}
	}
}
