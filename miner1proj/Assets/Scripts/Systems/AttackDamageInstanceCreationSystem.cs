﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDamageInstanceCreationSystem : BaseSystem
{
	public List<AttackEntity> attackEntities;
	public List<AttackDamageInstance> attackDamageInstances;
	public LevelEntity levelEntity;

	RockTileEntity damageRockTileEntity;

	void SetDamageTiles(List<DamageTileCoord> damageTileCoords, Vector2Int startingCoord, bool damageIsDirectionless, Vector2Int direction, List<PrototypeDamageTileCoord> prototypeDamageTileCoords, LevelEntity levelEntity)
	{
		damageTileCoords.Clear(); // TODO: Needed to clear leftover tiles for bouncing attacks

		DamageTileCoord damageTileCoord;
		Vector2Int coord;

		foreach (var prototypeDamageTileCoord in prototypeDamageTileCoords)
		{
			if (damageIsDirectionless)
			{
				coord = startingCoord + prototypeDamageTileCoord.relativeCoord;
			}
			else
				coord = startingCoord + LogicHelpers.RotateCoordWithDirection(prototypeDamageTileCoord.relativeCoord, direction);

			if (LogicHelpers.CoordinateIsInsideLevel(levelEntity, coord))
			{
				damageTileCoord = new DamageTileCoord(prototypeDamageTileCoord, coord, Time.time);

				damageTileCoords.Add(damageTileCoord);
			}
		}
	}

	public override void SystemUpdate()
	{
		foreach (var attackEntity in attackEntities)
		{
			//if (attackEntity.damageTileCoords.Count == 0
			//	|| attackEntity.chainTileBouncingComponent != null) // TODO: This is a hack. It's relying on the fact that damage tiles by chain attack last extremely short to re-clear them.
			//{
			if (attackEntity.begunDealingDamageThisFixedUpdate) // TODO: This is a hack. It's relying on the fact that damage tiles by chain attack last extremely short to re-clear them.
			{
				attackEntity.begunDealingDamageThisFixedUpdate = false;

				Vector2Int damageCoord;
				Vector2Int direction;
				if (attackEntity.tileMovementStateComponent != null)
				{
					damageCoord = attackEntity.tileMovementStateComponent.nextCoord;
					direction = attackEntity.tileMovementStateComponent.nextCoord - attackEntity.coord;
				}
				else
				{
					damageCoord = attackEntity.targetCoord;
					direction = attackEntity.targetCoord - attackEntity.coord;
				}

				SetDamageTiles(
					attackEntity.damageTileCoords,
					damageCoord,
					attackEntity.damageIsDirectionless,
					direction,
					ValueHolder.attackShapeTiles[attackEntity.attackType],
					levelEntity
					);
			}

			//if (attackEntity.attackStateComponent.attackState == AttackState.DealingDamage
			//	|| (attackEntity.chainTileBouncingComponent != null && attackEntity.chainTileBouncingComponent.bounceHistory.Count > 0)
			//	)

			// TODO: The fact that nothing uses AttackState.DealingDamage, and that instead its better defined by having damageTileCoords in the case of a bouncing chain attack... is a problem
			if (attackEntity.damageTileCoords.Count > 0)
			{
				foreach (var damageTileCoord in attackEntity.damageTileCoords)
				{
					if (!damageTileCoord.hasTriggered && damageTileCoord.startTime + damageTileCoord.triggerDelay <= Time.time)
					{
						damageTileCoord.hasTriggered = true;

						damageRockTileEntity = levelEntity.rockTiles[damageTileCoord.coord.x, damageTileCoord.coord.y];

						if (damageRockTileEntity != null)
						{
							attackDamageInstances.Add(new AttackDamageInstance()
							{
								ownerAttackEntity = attackEntity,
								damageRockTileEntity = damageRockTileEntity
							});
						}
					}
				}
			}
		}
	}
}
