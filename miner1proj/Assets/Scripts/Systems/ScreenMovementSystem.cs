﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMovementSystem : BaseSystem
{
	public List<DamageOrBuffTextEntity> damageOrBuffTextEntities;
	public List<ResourceGainTextEntity> resourceGainTextEntities;

	public override void SystemUpdate()
	{
		foreach (var damageOrBuffTextEntity in damageOrBuffTextEntities)
		{
			if (damageOrBuffTextEntity.screenMovementStateComponent.movementStartTime + damageOrBuffTextEntity.screenMovementStateComponent.lingerInPlaceDelay >= Time.time)
			{
				damageOrBuffTextEntity.transform.position = Vector3.MoveTowards(
					damageOrBuffTextEntity.transform.position,
					damageOrBuffTextEntity.transform.position + damageOrBuffTextEntity.screenMovementStateComponent.direction,
					Time.deltaTime * damageOrBuffTextEntity.screenMovementStateComponent.speed
					);
			}
		}

		foreach (var resourceGainTextEntities in resourceGainTextEntities)
		{
			if (resourceGainTextEntities.screenMovementStateComponent.movementStartTime + resourceGainTextEntities.screenMovementStateComponent.lingerInPlaceDelay >= Time.time)
			{
				resourceGainTextEntities.transform.position = Vector3.MoveTowards(
					resourceGainTextEntities.transform.position,
					resourceGainTextEntities.transform.position + resourceGainTextEntities.screenMovementStateComponent.direction,
					Time.deltaTime * resourceGainTextEntities.screenMovementStateComponent.speed
					);
			}
		}
	}
}
