﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCreationSystem : BaseSystem
{
	public List<DwarfEntity> dwarfEntities;
	public List<AttackEntity> attackEntities;
	public Dictionary<AttackType, AttackEntity> attackPrefabs;
	public GameObject attacksObject;

	public override void SystemUpdate()
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			if (dwarfEntity.unitStateComponent.unitState == UnitState.Attacking)
			{
				// TODO: implement performing multiple attacks in one timespan, in case time is ticking too slowly
				if (!dwarfEntity.unitAttackingStateComponent.attackCreationPointPerformed && dwarfEntity.unitAttackingStateComponent.attackStartTime + dwarfEntity.unitAttackingStateComponent.currentAttackStatSet.attackCreationPointTime <= Time.time)
				{
					if (LogicHelpers.AttackTargetIsInAcceptablePosition(dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord, dwarfEntity))
					{
						dwarfEntity.unitAttackingStateComponent.attackCreationPointPerformed = true;

						EntityHelpers.AddAttack(
							dwarfEntity.unitAttackingStateComponent.attackRotation[dwarfEntity.unitAttackingStateComponent.nextAttack],
							dwarfEntity.coord,
							dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord,
							dwarfEntity,
							attackEntities,
							attackPrefabs,
							attacksObject
							);

						// TODO: This can likely be optimized, either maintaining a list of "charged" buffs on a unit, or using an ECS approach and having a system that only processes buffs entities with charges against whether the dwarf attacked this frame
						foreach (var buffEntity in dwarfEntity.buffReceiverComponent.buffEntities.Values)
							if (buffEntity.isCountingDownCharges)
								buffEntity.currentCharges--;
					}
					else
					{
						// NOTE: Purposely not unsetting the attackCreationPointPerformed here in order to debug
						// dwarfEntity.unitAttackingStateComponent.attackCreationPointPerformed = true;

						Debug.Log(
							"Unacceptable position attack attempted by: " + dwarfEntity.dwarfName
							+ " ...from world pos: " + dwarfEntity.transform.position
							+ " ... and coord: " + dwarfEntity.coord
							+ " ... to target coord: " + dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord
							+ " ... //// with initiate attack rotation value: " + dwarfEntity.unitAttackingStateComponent.initiateAttackRotation
							+ " ... and spot to attack from: " + dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord
							+ " ... and hasIntendedAttackTarget value: " + dwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget
							+ " ... and unit state: " + dwarfEntity.unitStateComponent.unitState.ToString()
							+ " ... and navpath count: " + dwarfEntity.tileMovementStateComponent.navPath.Count);
					}
				}
			}
		}
	}
}
