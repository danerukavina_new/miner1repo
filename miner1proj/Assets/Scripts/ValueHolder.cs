﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FourDirections
{
	Left = 0,
	Up = 1,
	Right = 2,
	Down = 3
}

public static class ValueHolder
{
	public const int ENTRANCE_CLEARING_RADIUS = 3; // TODO: If truly 12 dwarves can be in the player's party at the same time, this needs to go higher to match the List below
	public const int MAXIMUM_DWARVES_IN_PARTY = 12;
	public static readonly List<int> ENTRY_RADIUS_FROM_DWARF_COUNT = new List<int>() { 0, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5 };
	public const int EXIT_CLEARING_RADIUS = 3;
	public const int RANDOM_LEVEL_MINIMUM_EXIT_OFFSET_ABSOLUTE = 3;
	public const float RANDOM_LEVEL_MINIMUM_EXIT_OFFSET_PROPORTIONATE = 0.333333f;
	public const int MAXIMUM_FOG_SIGHT_RADIUS = 10;
	public const int MAXIMUM_LEVEL_X_TILES = 48;//32;
	public const int MAXIMUM_LEVEL_Y_TILES = 36;//18;
	public const int MAXIMUM_LEVEL_TILE_COUNT = MAXIMUM_LEVEL_X_TILES * MAXIMUM_LEVEL_Y_TILES;
	public const int MAXIMUM_SIMULTANEOUS_ATTACKS = MAXIMUM_DWARVES_IN_PARTY * 3;
	public const int MAXIMUM_SIMULTANEOUS_DAMAGE_AND_BUFF_EFFECTS = MAXIMUM_SIMULTANEOUS_ATTACKS * 3;
	public static readonly int MAXIMUM_DAMAGE_TILES = Mathf.Max(MAXIMUM_LEVEL_X_TILES, MAXIMUM_LEVEL_Y_TILES);
	public const int MAXIMUM_CHAIN_TILE_BOUNCE_HISTORY = 16;
	public static readonly int MAXIUM_CHAIN_TILE_BOUNCE_ANDIDATE_TARGETS = Mathf.CeilToInt(2 * 3.142f * 8); // NOTE: 2 * pi * 8 radius as an upper bound to the number of targets
	public const int MAXIMUM_RANGE = 8;
	public static readonly int MAXIMUM_ATTACK_RANGE_PATTERN_TILES = LogicHelpers.TilesInRadius(MAXIMUM_RANGE);

	public const int MAXIMUM_SEARCH_LOOPS = MAXIMUM_LEVEL_TILE_COUNT;
	public static readonly Vector2Int leftUp = Vector2Int.left + Vector2Int.up;
	public static readonly Vector2Int upRight = Vector2Int.up + Vector2Int.right;
	public static readonly Vector2Int rightDown = Vector2Int.right + Vector2Int.down;
	public static readonly Vector2Int downLeft = Vector2Int.down + Vector2Int.left;

	public static readonly float SQRT_2_DIAGONAL_LENGTH = Mathf.Sqrt(2f);
	public static readonly float BACKTRACK_TO_COORD_PATHING_PENALTY = SQRT_2_DIAGONAL_LENGTH / 2f; // Makes dwarves reluctant to prefer backtracking when moving between two tiles when they repath. Instead they prefer to repath from the next tile as if it was this constant "shorter"
	public const double INDESTRUCTIBLE_PENETRATION_LENGTH_FOR_PATHING_HACK = 9999d;

	public const int MAXIMUM_DWARVES_IN_POSSESSION = 32;

	public const int ATTACK_DAMAGE_DECIMAL_PRECISION = 0;

	public const int UNIT_STAT_DAMAGE_FACTOR_DECIMAL_PRECISION = 0;
	public const int UNIT_STAT_CRIT_CHANCE_DECIMAL_PRECISION = 2;
	public const int UNIT_STAT_CRIT_BONUS_DAMAGE_DECIMAL_PRECISION = 2;
	public const int UNIT_STAT_SMELT_CHANCE_DECIMAL_PRECISION = 2;
	public const int UNIT_STAT_MOVEMENT_SPEED_DECIMAL_PRECISION = 3; // NOTE: Not in use currently
	public const int UNIT_STAT_UPGRADE_COST_DECIMAL_PRECISION = 0;

	public const int MAXIMUM_BUFFS_TO_HAVE = 32; // TODO: If buffs are unique, as during initial implementation... then this should equal the number of BuffTypes?
	public const int MAXIMUM_BUFFS_TO_VISUALIZE = 3;
	public const int MAXIMUM_BUFF_GIVER_AREA_CREATIONS_TO_STORE_PER_CREATOR = 4;
	public const int MAXIMUM_SIMULTANEOUS_PROC_TYPES = 16;
	public const int TEMPORARY_FIXED_BUFF_LEVEL = 1;
	public const int BASE_ORDER_IN_LAYER_FOR_BUFF_VISUAL_ENTITY_SPRITE_RENDERER = 42; // TODO: This can cause confusion, all other values for order in layer are set in the editor.

	// UI Constants
	public const int RECENT_RESOURCES_TO_DISPLAY = 4;
	public const float LONG_UI_PRESS_DELAY = 0.3f;
	public const float PRESS_IN_PLACE_TOLERANCE_SQUARED = 0.25f * 0.25f;
	public const float CAMERA_HEIGHT_IN_UNITS = 9;
	public const float CAMERA_MINIMUM_LEVEL_BASED_BOUNDS = 1.5f;
	public const float CAMERA_MOVE_SPEED = 2.5f;
	public const float CAMERA_RETURN_TO_BOUNDS_MOVE_SPEED_FACTOR = 4.5f;
	public const float CAMERA_MOVE_DEADZONE_SQUARED = 0.125f * 0.125f;
	public const float PARAMETER_ANGLE_OF_CAMERA_DIRECTION = 1f / 8f;
	public const float INITIAL_UPGRADE_STATS_TEXT_DISSIPATION_ALPHA = 1f; // TODO: should this and similar values actually be params on the DwarfUpgradePanelEntity prefab? This would be more consistent with the way damage number dissipation, etc. works.
	public const float FIRST_UPGRADE_STATS_TEXT_DISSIPATION_DURATION = 0.5f;
	public const float FIRST_UPGRADE_STATS_TEXT_DISSIPATION_ALPHA = 0.75f;
	public const float SECOND_UPGRADE_STATS_TEXT_DISSIPATION_DURATION = FIRST_UPGRADE_STATS_TEXT_DISSIPATION_DURATION + 2.5f;
	public const int MAXIMUM_BARELY_INTERACTIVE_INFO_MODAL_TEXTS_TO_SHOW = 8;
	public const int DONT_SHOW_LEVEL_1_INITIAL_COMPUTE_ABILITY_GAINS = 1;
	public const int TOTAL_DWARF_PORTRAITS = 6;
	public const int NUMBER_OF_DWARF_PORTRAITS_ON_LEFT_SIDE = TOTAL_DWARF_PORTRAITS / 2;

	public const string PERCENTAGE_CHAR = "%";
	public const string PLUS_CHAR = "+";
	public const string SPACE_CHAR = " ";

	public const string UNIT_STAT_DAMAGE_FACTOR_STRING = "DMG";
	public const string UNIT_STAT_CRIT_CHANCE_STRING = "CRIT";
	public const string UNIT_STAT_CRIT_BONUS_FACTOR_STRING = "CRITDMG";
	public const string UNIT_STAT_SMELT_CHANCE_FACTOR_STRING = "SMLT";
	public const string UNIT_STAT_MOVEMENT_SPEED_FACTOR_STRING = "MOVE"; // NOTE: Not in use currently
	public const string UNIT_STAT_UPGRADE_COST_FACTOR_STRING = "COST"; // NOTE: Not in use currently

	public const string ABILITY_LEARNED_MIDDLE_STRING = " learned ";

	public const int PERCENTAGE_CONVERTER = 100;
	public const string SENTENCE_PERIOD = ".";

	// TODO: This is not the correct way to group dwarf information, but will do for now
	public static readonly Dictionary<DwarfType, List<AttackType>> dwarfTypeAttackRotations = new Dictionary<DwarfType, List<AttackType>>()
	{
		{ DwarfType.MeleeCleave,
		new List<AttackType>()
		{
			AttackType.Basic, AttackType.Basic, AttackType.Cleave,
		}},
		{ DwarfType.MeleeSpear,
		new List<AttackType>()
		{
			AttackType.BasicDoubleHit, AttackType.BasicDoubleHit, AttackType.BasicDoubleHit, AttackType.BasicDoubleHit, AttackType.Spear
		}},
		{ DwarfType.RangedBasicProjectile,
		new List<AttackType>()
		{
			AttackType.BasicProjectile
		}},
		{ DwarfType.RangedChain,
		new List<AttackType>()
		{
			AttackType.Chain
		}},
		{ DwarfType.Ritual,
		new List<AttackType>()
		{
			AttackType.Chain
		}},
		{ DwarfType.MeleeHammer,
		new List<AttackType>()
		{
			AttackType.Basic,
		}},
	};

	// TODO: Likely this will turn into ability names
	public static readonly Dictionary<ProcType, string> procNames = new Dictionary<ProcType, string>()
	{
		{ ProcType.OreCryBuffGiverArea, "Ore Cry" },
		{ ProcType.OnNodeDamageAmp, "Amplify Damage" },
		{ ProcType.OnNodeToughnessArmorReduction, "Soften Stone" },
	};

	public static readonly Dictionary<BuffGiverAreaType, string> buffGiverAreaBuffTexts = new Dictionary<BuffGiverAreaType, string>()
	{
		{ BuffGiverAreaType.OreCryArea, "Ore Cry !" },
	};

	public static readonly Dictionary<DwarfType, List<TemporaryLevelToProcUnlockData>> dwarvesProcLevelUnlockData = new Dictionary<DwarfType, List<TemporaryLevelToProcUnlockData>>()
	{
		{ DwarfType.MeleeCleave,
		new List<TemporaryLevelToProcUnlockData>()
		{
			new TemporaryLevelToProcUnlockData() { unlockLevel = 4, procType = ProcType.OreCryBuffGiverArea }
		} },
		{ DwarfType.MeleeSpear,
		new List<TemporaryLevelToProcUnlockData>()
		{
			new TemporaryLevelToProcUnlockData() { unlockLevel = 4, procType = ProcType.OnNodeDamageAmp }
		} },
		{ DwarfType.RangedBasicProjectile,
		new List<TemporaryLevelToProcUnlockData>()
		{
			new TemporaryLevelToProcUnlockData() { unlockLevel = 4, procType = ProcType.OnNodeDamageAmp }
		} },
		{ DwarfType.RangedChain,
		new List<TemporaryLevelToProcUnlockData>()
		{
			new TemporaryLevelToProcUnlockData() { unlockLevel = 4, procType = ProcType.OnNodeToughnessArmorReduction }
		} },
		{ DwarfType.Ritual,
		new List<TemporaryLevelToProcUnlockData>()
		{
		} },
		{ DwarfType.MeleeHammer,
		new List<TemporaryLevelToProcUnlockData>()
		{
			new TemporaryLevelToProcUnlockData() { unlockLevel = 4, procType = ProcType.OnNodeToughnessArmorReduction }
		} },
	};

	public static readonly Dictionary<AttackType, List<PrototypeDamageTileCoord>> attackShapeTiles = new Dictionary<AttackType, List<PrototypeDamageTileCoord>>()
	{
		{ AttackType.Basic,
			new List<PrototypeDamageTileCoord>()
			{
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0f }
			} },
		{ AttackType.Spear, // NOTE: This now includes a second, delayed double hit effect to match the animation of the dwarf that uses AttackType.BasicDoubleHit. It was a quick fix, but that's ok.
			new List<PrototypeDamageTileCoord>()
			{
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0f },
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,1), triggerDelay = 0.15f },
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0.5f } // TODO: In some ways, this depends on the attack speed? Or the animation speed? So damage tile coords can sometimes scale with attack speed?
			} },
		{ AttackType.Cleave,
			new List<PrototypeDamageTileCoord>()
			{
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(1,0), triggerDelay = 0f },
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0.1f },
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(-1,0), triggerDelay = 0.2f },
			} },
		{ AttackType.BasicDoubleHit,
			new List<PrototypeDamageTileCoord>()
			{
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0f },
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0.5f } // TODO: In some ways, this depends on the attack speed? Or the animation speed? So damage tile coords can sometimes scale with attack speed?
			} },
		{ AttackType.Chain,
			new List<PrototypeDamageTileCoord>()
			{
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0f }
			} },
		{ AttackType.BasicProjectile,
			new List<PrototypeDamageTileCoord>()
			{
				new PrototypeDamageTileCoord() { relativeCoord = new Vector2Int(0,0), triggerDelay = 0.2f }
			} },
	};

	private static List<PrototypeBuffGiverAreaTileCoord> GetPrototypeBuffGiverAreaTileCoords(BuffType buffType, int buffLevel, int radius)
	{
		List<Vector2Int> recrilinearCircleCoords = new List<Vector2Int>();
		List<PrototypeBuffGiverAreaTileCoord> ret = new List<PrototypeBuffGiverAreaTileCoord>();

		LogicHelpers.GetRectilinearCircleCoordsAroundCoord(Vector2Int.zero, radius, recrilinearCircleCoords);

		foreach (var recrilinearCircleCoord in recrilinearCircleCoords)
			ret.Add(new PrototypeBuffGiverAreaTileCoord() { relativeCoord = recrilinearCircleCoord, buffType = buffType, buffLevel = buffLevel });

		return ret;
	}

	public static readonly Dictionary<BuffGiverAreaType, List<PrototypeBuffGiverAreaTileCoord>> buffGiverAreaShapeTiles = new Dictionary<BuffGiverAreaType, List<PrototypeBuffGiverAreaTileCoord>>()
	{
		{ BuffGiverAreaType.OreCryArea,
			GetPrototypeBuffGiverAreaTileCoords(BuffType.OreCry, ValueHolder.TEMPORARY_FIXED_BUFF_LEVEL, 2) }
	};

	// TODO: Calling this a "prefab" for consistency... refactor for clarity
	public static readonly Dictionary<BuffType, BuffEntity> buffPseudoprefabs = new Dictionary<BuffType, BuffEntity>()
	{
		{ BuffType.OreCry,
			new BuffEntity()
			{
				buffType = BuffType.OreCry,
				baseStats = new BuffEntity.BuffStatSet()
				{
					dissipationDuration = 15f,
					charges = 5,

					attackSpeedBonus = 3f,
					movementSpeedBonus = 0.7f
				},
				visualPriorityRating = 1000,

				isDissipating = true,
				isCountingDownCharges = true
			} },
		{ BuffType.ArmorBreakOnNode,
			new BuffEntity()
			{
				buffType = BuffType.ArmorBreakOnNode,
				baseStats = new BuffEntity.BuffStatSet()
				{
					dissipationDuration = 10f,

					toughnessArmorFlatReduction = 20d
				},
				visualPriorityRating = 800,

				isDissipating = true
			} },
		{ BuffType.DamageAmpOnNode,
			new BuffEntity()
			{
				buffType = BuffType.DamageAmpOnNode,
				baseStats = new BuffEntity.BuffStatSet()
				{
					dissipationDuration = 6f,

					damageAmpBonus = 0.5d
				},
				visualPriorityRating = 900,

				isDissipating = true
			} },
	};

	public static readonly Dictionary<ProcType, ProcData> procsBaseValues = new Dictionary<ProcType, ProcData>()
	{
		{ ProcType.OreCryBuffGiverArea,
			new ProcData()
			{
				procType = ProcType.OreCryBuffGiverArea,
				procBuffApplicationType = ProcBuffApplicationType.BuffGiverAreaCreator,
				possibleProcBuffGiverAreaType = BuffGiverAreaType.OreCryArea,
				possibleProcBuffType = null,
				procChance = 0.75f,
				triggerRequiredDamageOutcomeTypeConstraints = null,
				triggerRequiredAttackTypeConstraints = null, 
				hasInternalCooldown = true,
				internalCooldownDuration = 60f,
			} },
		{ ProcType.OnNodeToughnessArmorReduction,
			new ProcData()
			{
				procType = ProcType.OnNodeToughnessArmorReduction,
				procBuffApplicationType = ProcBuffApplicationType.AttackReceiver,
				possibleProcBuffGiverAreaType = null,
				possibleProcBuffType = BuffType.ArmorBreakOnNode,
				procChance = 1f,
				triggerRequiredDamageOutcomeTypeConstraints = new HashSet<DamageOutcomeType>() { DamageOutcomeType.CritDamage },
				triggerRequiredAttackTypeConstraints = null,
				hasInternalCooldown = false
			} },
		{ ProcType.OnNodeDamageAmp,
			new ProcData()
			{
				procType = ProcType.OnNodeDamageAmp,
				procBuffApplicationType = ProcBuffApplicationType.AttackReceiver,
				possibleProcBuffGiverAreaType = null,
				possibleProcBuffType = BuffType.DamageAmpOnNode,
				procChance = 0.25f,
				triggerRequiredDamageOutcomeTypeConstraints = null,
				triggerRequiredAttackTypeConstraints = null,
				hasInternalCooldown = false
			} },

	};

	public class RockTileDroptableItem
	{
		public ResourceType resourceType;
		public int baseMinAmount;
		public int baseMaxAmount;
		public float probability;
	}

	public static readonly Dictionary<RockTileType, List<RockTileDroptableItem>> RockTileDropTables = new Dictionary<RockTileType, List<RockTileDroptableItem>>()
	{
		{ RockTileType.Rock1,
			new List<RockTileDroptableItem>()
			{
				new RockTileDroptableItem() { resourceType = ResourceType.Stone,  baseMinAmount = 1, baseMaxAmount = 3, probability = 0.5f }
			} },
		{ RockTileType.Rock2,
			new List<RockTileDroptableItem>()
			{
				new RockTileDroptableItem() { resourceType = ResourceType.Stone,  baseMinAmount = 1, baseMaxAmount = 3, probability = 0.75f }
			} },
		{ RockTileType.Copper,
			new List<RockTileDroptableItem>()
			{
				new RockTileDroptableItem() { resourceType = ResourceType.CopperOre, baseMinAmount = 1, baseMaxAmount = 1, probability = 1f },
				new RockTileDroptableItem() { resourceType = ResourceType.Stone,  baseMinAmount = 1, baseMaxAmount = 2, probability = 0.5f },
				new RockTileDroptableItem() { resourceType = ResourceType.Diamond,  baseMinAmount = 1, baseMaxAmount = 1, probability = 0.05f }
			} },
		{ RockTileType.Tin,
			new List<RockTileDroptableItem>()
			{
				new RockTileDroptableItem() { resourceType = ResourceType.TinOre, baseMinAmount = 1, baseMaxAmount = 1, probability = 1f },
				new RockTileDroptableItem() { resourceType = ResourceType.Stone,  baseMinAmount = 1, baseMaxAmount = 2, probability = 0.4f },
				new RockTileDroptableItem() { resourceType = ResourceType.Diamond,  baseMinAmount = 1, baseMaxAmount = 1, probability = 0.1f }
			} },
		{ RockTileType.Silver,
			new List<RockTileDroptableItem>()
			{
				new RockTileDroptableItem() { resourceType = ResourceType.SilverOre, baseMinAmount = 1, baseMaxAmount = 1, probability = 1f },
				new RockTileDroptableItem() { resourceType = ResourceType.Stone,  baseMinAmount = 1, baseMaxAmount = 2, probability = 0.2f },
				new RockTileDroptableItem() { resourceType = ResourceType.Diamond,  baseMinAmount = 1, baseMaxAmount = 1, probability = 0.2f }
			} },
	};

	public static readonly List<Vector2Int> fourWayMovementDirections = new List<Vector2Int>()
	{
		Vector2Int.left,
		Vector2Int.up,
		Vector2Int.right,
		Vector2Int.down,
	};

	public static readonly List<Vector2Int> eightWayMovementDirections = new List<Vector2Int>()
	{
		Vector2Int.left,
		Vector2Int.up,
		Vector2Int.right,
		Vector2Int.down,
		ValueHolder.leftUp,
		ValueHolder.upRight,
		ValueHolder.rightDown,
		ValueHolder.downLeft
	};

	public static readonly List<Vector2Int> eightWayMovementDirectionsRadiallyOrdered = new List<Vector2Int>()
	{
		Vector2Int.right,
		ValueHolder.upRight,
		Vector2Int.up,
		ValueHolder.leftUp,
		Vector2Int.left,
		ValueHolder.downLeft,
		Vector2Int.down,
		ValueHolder.rightDown
	};
}
