﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavCoord
{
	public Vector2Int prevCoord;
	public float pathLength;
	public double HPPenetrationLength;
}

public class PrototypeDamageTileCoord
{
	public Vector2Int relativeCoord;
	public float triggerDelay;
}

public class DamageTileCoord
{
	// TODO: It's a little confusing that the prototype also has a relative coord which is ignored. How to make it less confusing though?
	public DamageTileCoord(PrototypeDamageTileCoord prototypeDamageTileCoord, Vector2Int coord, float startTime)
	{
		this.coord = coord;
		this.triggerDelay = prototypeDamageTileCoord.triggerDelay;
		this.hasTriggered = false;
		this.startTime = startTime;
	}

	public Vector2Int coord;
	public float triggerDelay;

	public bool hasTriggered;
	public float startTime;
}

public class PrototypeBuffGiverAreaTileCoord
{
	public Vector2Int relativeCoord;
	public BuffType buffType;
	public int buffLevel;
}

// TODO: It's not really a prototype buff, it's more a BuffType + PrototypeBuffState? the buff prototype is the buff prefab
public class PrototypeBuff
{
	public PrototypeBuff(PrototypeBuffGiverAreaTileCoord prototypeBuffGiverAreaTileCoord)
	{
		buffType = prototypeBuffGiverAreaTileCoord.buffType;
		buffLevel = prototypeBuffGiverAreaTileCoord.buffLevel;
	}

	public PrototypeBuff(BuffType buffType, int buffLevel)
	{
		this.buffType = buffType;
		this.buffLevel = buffLevel;
	}

	public BuffType buffType;
	public int buffLevel;
}

public class ProcData
{
	public ProcData()
	{

	}

	public ProcData(ProcData procData)
	{
		this.procType = procData.procType;
		this.procBuffApplicationType = procData.procBuffApplicationType;
		this.possibleProcBuffGiverAreaType = procData.possibleProcBuffGiverAreaType;
		this.possibleProcBuffType = procData.possibleProcBuffType;
		this.procChance = procData.procChance;
		this.triggerRequiredDamageOutcomeTypeConstraints = procData.triggerRequiredDamageOutcomeTypeConstraints;
		this.triggerRequiredAttackTypeConstraints = procData.triggerRequiredAttackTypeConstraints;
		this.hasInternalCooldown = procData.hasInternalCooldown;
		this.internalCooldownDuration = procData.internalCooldownDuration;

		this.isInternallyCoolingdown = false;
	}

	// prototype data
	public ProcType procType;
	public ProcBuffApplicationType procBuffApplicationType;
	public BuffGiverAreaType? possibleProcBuffGiverAreaType;
	public BuffType? possibleProcBuffType;
	public float procChance;
	public HashSet<DamageOutcomeType> triggerRequiredDamageOutcomeTypeConstraints; // TODO: Currently I blieve it's impossible to force procs on a immune no damage outcome, due to the damage outcome itself controlling if procs can occur. That's redundant, should be refactored
	public HashSet<AttackType> triggerRequiredAttackTypeConstraints; // TODO: Does it make more sense to earchitect and attach procs to attacks?
	public bool hasInternalCooldown;
	public float internalCooldownDuration; 

	// state data
	public bool isInternallyCoolingdown;
	public float internalCooldownStartTime;
}

public static class LogicHelpers
{
	public static Vector3 PositionFromCoord(Vector2Int coord)
	{
		return new Vector3((float)coord.x, (float)coord.y, 0f);
	}

	public static Vector2Int CoordFromScreenPosition(Vector3 pos)
	{
		return new Vector2Int(Mathf.FloorToInt(pos.x + 0.5f), Mathf.FloorToInt(pos.y + 0.5f));
	}

	public static bool IsPositionOnCoord(Vector3 position, Vector2Int coord)
	{
		Vector3 coordPosition = PositionFromCoord(coord);

		return Mathf.Approximately(position.x, coordPosition.x) && Mathf.Approximately(position.y, coordPosition.y);
	}

	public static Vector2Int NearestCoordToPosition(Vector3 position)
	{
		return new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
	}

	public static bool IsPositionOnAGridCoord(Vector3 position)
	{
		return Mathf.Approximately(position.x, Mathf.Round(position.x)) && Mathf.Approximately(position.y, Mathf.Round(position.y));
	}

	public static bool CoordinateIsInsideLevel(LevelEntity levelEntity, Vector2Int coord)
	{
		return coord.x >= 0 && coord.x < levelEntity.xSize && coord.y >= 0 && coord.y < levelEntity.ySize;
	}

	public static bool CoordinateIsInsideLevelEdges(LevelEntity levelEntity, Vector2Int coord)
	{
		return coord.x > 0 && coord.x < levelEntity.xSize - 1 && coord.y > 0 && coord.y < levelEntity.ySize - 1;
	}

	public static int TilesInRadius(int radius)
	{
		// NOTE: the inverse of this function is:
		// radius = Mathf.FloorToInt((Mathf.Sqrt(2 * index - 1) - 1) / 2) + 1;
		return 1 + 4 * radius * (radius + 1) / 2;
	}

	public static int RadiusForTileIndex(int index, out int indexOnRadius)
	{
		// 0 -> 0
		// 1 -> 1
		// 2 -> 1
		// 3 -> 1
		// 4 -> 1
		// 5 -> 2
		// 12 -> 2
		// 13 -> 3
		// 24 -> 3
		// 25 -> 4
		// 26 -> 4
		// 40 -> 4
		// 41 -> 5

		int radius = Mathf.FloorToInt((Mathf.Sqrt(2 * index - 1) - 1) / 2) + 1;
		indexOnRadius = index - TilesInRadius(radius - 1);

		return radius;
	}

	public static void GetCircleCoordsAroundCoordByIndex(Vector2Int coord, int index, List<Vector2Int> returnCoords)
	{
		int radius = RadiusForTileIndex(index, out int indexOnRadius);

		// NAH

	}

	public static int RectilinearDistance(Vector2Int coord1, Vector2Int coord2)
	{
		return Mathf.Abs(coord1.x - coord2.x) + Mathf.Abs(coord1.y - coord2.y);
	}

	public static float EightWayDistance(Vector2Int coord1, Vector2Int coord2)
	{
		int deltaX = Mathf.Abs(coord1.x - coord2.x);
		int deltaY = Mathf.Abs(coord1.y - coord2.y);
		return Mathf.Min(deltaX - deltaY) + Mathf.Min(deltaX, deltaY) * ValueHolder.SQRT_2_DIAGONAL_LENGTH;
	}

	public static float EightWayDistance(Vector3 pos1, Vector3 pos2)
	{
		float deltaX = Mathf.Abs(pos1.x - pos2.x);
		float deltaY = Mathf.Abs(pos1.y - pos2.y);
		return Mathf.Min(deltaX - deltaY) + Mathf.Min(deltaX, deltaY) * ValueHolder.SQRT_2_DIAGONAL_LENGTH;
	}

	public static void AddRectilinearCircleCoordsOnRadiusAroundCoord(Vector2Int coord, int rectilinearRadius, List<Vector2Int> returnCoords)
	{
		for (int i = 0; i < rectilinearRadius; i++)
		{
			returnCoords.Add(new Vector2Int(coord.x + i, coord.y + (rectilinearRadius - i))); // top to right
			returnCoords.Add(new Vector2Int(coord.x + (rectilinearRadius - i), coord.y - i)); // right to bottom
			returnCoords.Add(new Vector2Int(coord.x - i, coord.y - (rectilinearRadius - i))); // bottom to left
			returnCoords.Add(new Vector2Int(coord.x - (rectilinearRadius - i), coord.y + i)); // left to top
		}
	}

	public static void GetRectilinearCircleCoordsAroundCoordExcludingCoord(Vector2Int coord, int rectilinearRadius, List<Vector2Int> returnCoords)
	{
		returnCoords.Clear();

		for (int r = 1; r < rectilinearRadius + 1; r++)
		{
			for (int i = 0; i < r; i++)
			{
				AddRectilinearCircleCoordsOnRadiusAroundCoord(coord, r, returnCoords);
			}
		}
	}

	public static void GetRectilinearCircleCoordsAroundCoord(Vector2Int coord, int rectilinearRadius, List<Vector2Int> returnCoords)
	{
		GetRectilinearCircleCoordsAroundCoordExcludingCoord(coord, rectilinearRadius, returnCoords);
		returnCoords.Add(coord);
	}

	public static void AddDistanceCircleCoordsWithinRadiusAroundCoordWithinLevel(Vector2Int coord, int rectilinearRadius, float distanceRadius, List<Vector2Int> returnCoords, LevelEntity levelEntity)
	{
		float candidateSquaredDistanceRadiusOnRectilinearCircleSquared;
		Vector2Int candidateCoord;

		for (int i = 0; i < rectilinearRadius; i++)
		{
			candidateSquaredDistanceRadiusOnRectilinearCircleSquared = i * i + (rectilinearRadius - i) * (rectilinearRadius - i);

			if (candidateSquaredDistanceRadiusOnRectilinearCircleSquared <= distanceRadius * distanceRadius)
			{
				candidateCoord = new Vector2Int(coord.x + i, coord.y + (rectilinearRadius - i)); // top to right

				if (LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, candidateCoord))
					returnCoords.Add(candidateCoord);

				candidateCoord = new Vector2Int(coord.x + (rectilinearRadius - i), coord.y - i); // right to bottom

				if (LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, candidateCoord))
					returnCoords.Add(candidateCoord);

				candidateCoord = new Vector2Int(coord.x - i, coord.y - (rectilinearRadius - i)); // bottom to left

				if (LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, candidateCoord))
					returnCoords.Add(candidateCoord);

				candidateCoord = new Vector2Int(coord.x - (rectilinearRadius - i), coord.y + i); // left to top

				if (LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, candidateCoord))
					returnCoords.Add(candidateCoord);
			}
		}
	}

	public static void GetDistanceCircleCoordsAroundCoordWithinLevel(Vector2Int coord, float distanceRadius, List<Vector2Int> returnCoords, LevelEntity levelEntity)
	{
		int rectilinearRadius = Mathf.CeilToInt(distanceRadius / (ValueHolder.SQRT_2_DIAGONAL_LENGTH / 2f)); // NOTE: We are inscribing a circle in a square. The rectilinearRadius is the diagonal of the square. This is radius of circle / sqrt(2).

		returnCoords.Clear();

		for (int r = 1; r < rectilinearRadius + 1; r++)
		{
			for (int i = 0; i < r; i++)
			{
				AddDistanceCircleCoordsWithinRadiusAroundCoordWithinLevel(coord, r, distanceRadius, returnCoords, levelEntity);
			}
		}
	}

	public static bool IsGridAligned(Vector2Int coord1, Vector2Int coord2)
	{
		return coord1.x == coord2.x || coord1.y == coord2.y;
	}

	public static bool IsGridAligned(Vector2Int direction)
	{
		return direction.x == 0 || direction.y == 0;
	}

	// TODO: Commented this out because if a ranged dwarf stood inside an ethereal node, he may conceptually need to be able to fire and imeediately strike it. Using this would case an exception in AddAttack
	//public static bool DirectionIsGridAligned(Vector2Int direction)
	//{
	//	return direction.x == 0 ^ direction.y == 0;
	//}

	public static bool IsNextToCoord(Vector2Int coord1, Vector2Int coord2)
	{
		return (coord1 - coord2).sqrMagnitude == 1;
	}

	public static bool IsInRadiusRange(Vector2Int coord1, Vector2Int coord2, float radius)
	{
		return (coord1 - coord2).sqrMagnitude <= radius * radius;
	}

	public static bool IsInRadiusRange(Vector2Int coord, Vector3 pos, float radius)
	{
		return (pos - PositionFromCoord(coord)).sqrMagnitude <= radius * radius;
	}

	public static Vector2Int NormalizeGridAlignedDirection(Vector2Int gridAlignedDirection)
	{
		Vector2Int normalizedGridAlignedDirection = gridAlignedDirection;

		if (normalizedGridAlignedDirection.x != 0)
			normalizedGridAlignedDirection.x /= Mathf.Abs(normalizedGridAlignedDirection.x);
		else // != 0 y impled due to grid alignment function
			normalizedGridAlignedDirection.y /= Mathf.Abs(normalizedGridAlignedDirection.y);

		return normalizedGridAlignedDirection;
	}

	public static Vector2Int NextCoordForAttack(AttackEntity attackEntity)
	{

		switch (attackEntity.attackType)
		{
			case AttackType.Basic:
			case AttackType.Spear:
			case AttackType.Cleave:
			case AttackType.BasicDoubleHit:
			case AttackType.BasicProjectile:
				return attackEntity.coord + attackEntity.tileMovementStateComponent.normalizedGridAlignedDirection;
			case AttackType.Chain:
				return attackEntity.targetCoord;
			default:
				throw new System.Exception("Unexpected attack type when finding an attack's next coordinate.");
		}
	}

	public static bool AttackTargetIsInAcceptablePosition(Vector2Int targetCoord, DwarfEntity dwarfEntity)
	{
		// TODO: one day, this function will have to test for targets that are an array of coordinates

		switch (dwarfEntity.unitAttackingStateComponent.possibleAttackingPosition)
		{
			case PossibleAttackingPosition.Melee:
				return IsNextToCoord(targetCoord, dwarfEntity.coord);
			case PossibleAttackingPosition.GridAlignedWithRange:
				return IsGridAligned(targetCoord, dwarfEntity.coord) && IsInRadiusRange(targetCoord, dwarfEntity.coord, dwarfEntity.unitAttackingStateComponent.attackRange);
			case PossibleAttackingPosition.RadiusWithRange:
				return IsInRadiusRange(targetCoord, dwarfEntity.coord, dwarfEntity.unitAttackingStateComponent.attackRange);
			default:
				throw new System.Exception("Unrecognized possible attack position enum.");
		}
	}

	public static Vector2Int RotateCoordWithDirection(Vector2Int coord, Vector2Int direction)
	{
		if (direction.x != 0 && direction.y != 0)
			throw new System.Exception("Non-grid aligned direction supplied to RotateCoordWithDirection: Coord = " + coord + ", Direction = " + direction);

		if (direction.sqrMagnitude != 1)
			throw new System.Exception("Non unit length direction supplied to RotateCoordWithDirection: Coord = " + coord + ", Direction = " + direction);

		Vector2Int rotationMatrixRow1;
		Vector2Int rotationMatrixRow2;

		if (direction == Vector2Int.up)
		{
			return coord;
		}
		else if (direction == Vector2Int.left)
		{
			rotationMatrixRow1 = new Vector2Int(0, -1);
			rotationMatrixRow2 = new Vector2Int(1, 0);
		}
		else if (direction == Vector2Int.down)
		{
			rotationMatrixRow1 = new Vector2Int(-1, 0);
			rotationMatrixRow2 = new Vector2Int(0, -1);
		}
		else // right
		{
			rotationMatrixRow1 = new Vector2Int(0, 1);
			rotationMatrixRow2 = new Vector2Int(-1, 0);
		}

		return new Vector2Int(
			coord.x * rotationMatrixRow1.x + coord.y * rotationMatrixRow1.y,
			coord.x * rotationMatrixRow2.x + coord.y * rotationMatrixRow2.y
		);
	}

	public static void ChooseRandomCoordsFromListDestructive(int amount, List<Vector2Int> startCoordsThatAreDestroyed, List<Vector2Int> returnCoords)
	{
		returnCoords.Clear();

		int index;

		for (int i = 0; i < amount; i++)
		{
			index = UnityEngine.Random.Range(0, startCoordsThatAreDestroyed.Count);

			returnCoords.Add(startCoordsThatAreDestroyed[index]);

			startCoordsThatAreDestroyed.RemoveAt(index);
		}
	}

	public static Vector2Int RandomOnParimeterCoord(LevelEntity levelEntity)
	{
		int edgeWithoutTip = UnityEngine.Random.Range(0, 4);

		switch (edgeWithoutTip)
		{
			case 0: // tip is 0, ySize - 1
				return new Vector2Int(0, UnityEngine.Random.Range(0, levelEntity.ySize - 1));
			case 1: // tip is xSize - 1, 0
				return new Vector2Int(levelEntity.xSize - 1, UnityEngine.Random.Range(1, levelEntity.ySize));
			case 2: // tip is 0, 0
				return new Vector2Int(UnityEngine.Random.Range(1, levelEntity.xSize), 0);
			case 3: // tip is xSize - 1, ySize - 1
			default:
				return new Vector2Int(UnityEngine.Random.Range(0, levelEntity.xSize - 1), levelEntity.ySize - 1);
		}
	}

	public static Vector2Int WestEdgeRandomCoordNoCorners(LevelEntity levelEntity)
	{
		return new Vector2Int(0, UnityEngine.Random.Range(1, levelEntity.ySize - 1));
	}

	// TODO: Unused, too simple (only reference is also unused at time of writing)
	public static Vector2Int NorthEastSouthEdgeRandomCoordNoCorners(LevelEntity levelEntity)
	{
		int edge = UnityEngine.Random.Range(1, 4);

		switch (edge)
		{
			case 1:
				return new Vector2Int(levelEntity.xSize - 1, UnityEngine.Random.Range(1, levelEntity.ySize - 1));
			case 2:
				return new Vector2Int(UnityEngine.Random.Range(1 , levelEntity.xSize - 1), 0);
			case 3:
			default:
				return new Vector2Int(UnityEngine.Random.Range(1, levelEntity.xSize - 1), levelEntity.ySize - 1);
		}
	}

	public static Vector2Int NorthEastSouthEdgeRandomCoordNoCornersNotNearEntrance(LevelEntity levelEntity, Vector2Int entranceCoord)
	{
		int randomEdge = UnityEngine.Random.Range(1, 3);

		int xAtLeastKTilesAndPProportionAway = Mathf.Min(
			levelEntity.xSize - 1,
			Mathf.Max(
				ValueHolder.RANDOM_LEVEL_MINIMUM_EXIT_OFFSET_ABSOLUTE + 1,
				Mathf.FloorToInt(levelEntity.xSize * ValueHolder.RANDOM_LEVEL_MINIMUM_EXIT_OFFSET_PROPORTIONATE)
				)
				);

		// TODO: Improve this
		if (entranceCoord.y < levelEntity.ySize / 2f)
			switch (randomEdge)
			{
				case 1: // East
					return new Vector2Int(levelEntity.xSize - 1, UnityEngine.Random.Range(1, levelEntity.ySize - 1));
				case 2: // North
				default:
					return new Vector2Int(UnityEngine.Random.Range(xAtLeastKTilesAndPProportionAway, levelEntity.xSize - 1), levelEntity.ySize - 1);
			}
		else
			switch (randomEdge)
			{
				case 1: // East
					return new Vector2Int(levelEntity.xSize - 1, UnityEngine.Random.Range(1, levelEntity.ySize - 1));
				case 2: // South
				default:
					return new Vector2Int(UnityEngine.Random.Range(xAtLeastKTilesAndPProportionAway, levelEntity.xSize - 1), 0);
			}
	}

	public static void FilterCoordsToBeWithinLevelEdges(List<Vector2Int> unfilteredCoords, LevelEntity levelEntity, List<Vector2Int> returnCoords)
	{
		returnCoords.Clear();

		foreach (var unfilteredCoord in unfilteredCoords)
		{
			if (LogicHelpers.CoordinateIsInsideLevelEdges(levelEntity, unfilteredCoord))
			{
				returnCoords.Add(unfilteredCoord);
			}
		}
	}

	public static void GetRectilinearCircleCoordsAroundCoordExcludingCoordWithinLevelEdges(Vector2Int coord, int radius, LevelEntity levelEntity, List<Vector2Int> unfilteredCoordsHelper, List<Vector2Int> returnCoords)
	{
		// NOTE: Clears unfilteredCoordsHelper
		// TODO: This seems to add the same coords multiple times
		GetRectilinearCircleCoordsAroundCoordExcludingCoord(coord, radius, unfilteredCoordsHelper);

		// NOTE: Clears returnCoords
		FilterCoordsToBeWithinLevelEdges(unfilteredCoordsHelper, levelEntity, returnCoords);
	}

	public static bool IsShorterPath(NavCoord destinationNavCoord, float candidateShortestPath)
	{
		// TODO: This style led to confusion
		if (destinationNavCoord.pathLength <= candidateShortestPath)
			return false;
		return true;
	}

	public static bool IsShorterPathOrPenetrationWithUnreachable(NavCoord navCoord, NavCoord additionalUnreachableNavCoord, float candidateShortestPath, double candidateShortestHPPenetration, float candidateUnreachableShortestPath, double candidateUnreachableShortestHPPenetration)
	{
		float bestTotalPathLength = navCoord.pathLength + additionalUnreachableNavCoord.pathLength;
		float candidateTotalPathLength = candidateShortestPath + candidateUnreachableShortestPath;

		double bestTotalHPPenetration = navCoord.HPPenetrationLength + additionalUnreachableNavCoord.HPPenetrationLength;
		double candidateTotalHPPenetration = candidateShortestHPPenetration + candidateUnreachableShortestHPPenetration;

		if (bestTotalHPPenetration > 0d)
		{
			if (candidateTotalHPPenetration > 0d)
			{
				if (bestTotalHPPenetration < candidateTotalHPPenetration)
					return false;
				else if (Mathf.Approximately((float)bestTotalHPPenetration, (float)candidateTotalHPPenetration) && bestTotalPathLength <= candidateTotalPathLength)
					return false;
			}
		}
		else if (candidateTotalHPPenetration > 0d)
			return false;
		else if (bestTotalPathLength <= candidateTotalPathLength)
			return false;

		return true;
	}

	public static bool IsShorterPathOrPenetration(NavCoord destinationNavCoord, float candidateShortestPath, double candidateShortestHPPenetration)
	{
		if (destinationNavCoord.HPPenetrationLength > 0d)
		{
			if (candidateShortestHPPenetration > 0d)
			{
				if (destinationNavCoord.HPPenetrationLength < candidateShortestHPPenetration)
					return false;
				else if (Mathf.Approximately((float)destinationNavCoord.HPPenetrationLength, (float)candidateShortestHPPenetration) && destinationNavCoord.pathLength <= candidateShortestPath)
					return false;
			}
		}
		else if (candidateShortestHPPenetration > 0d)
			return false;
		else if (destinationNavCoord.pathLength <= candidateShortestPath)
			return false;

		return true;
	}

	public static bool IsHigherMiningValueOrShorterPathOrPenetration(NavCoord miningNodeNavCoord, RockTileEntity miningNodeRockTileEntity, float candidateShortestPath, double candidateShortestHPPenetration, double candidateHighestMiningValue)
	{
		if (miningNodeRockTileEntity.miningValue > candidateHighestMiningValue)
		{
			return false;
		}
		else if (Mathf.Approximately((float)miningNodeRockTileEntity.miningValue, (float)candidateHighestMiningValue))
		{
			if (!IsShorterPathOrPenetration(miningNodeNavCoord, candidateShortestPath, candidateShortestHPPenetration))
			{
				return false;
			}
		}

		return true;
	}

	// TODO: this function has undergone renaming, but it needs to probably return the opposite of its curent boolean values
	// TODO: this function should start considering fog as well as the rock tile entity
	public static bool IsExitOrFreeSpaceOrHigherMiningValueOrShorterPathOrPenetration(NavCoord candidateNavCoord, RockTileEntity candidateRockTileEntity, bool bestIsExit, bool bestIsFreeSpace, float bestShortestPath, double bestShortestHPPenetration, double bestHighestMiningValue)
	{
		// If it's a level exit that's not through a wall, while the best one isn't a level exit, or is a level exit through a wall
		if (
			!(bestIsExit && Mathf.Approximately((float)bestShortestHPPenetration, 0f))
			&& (IsExitNavWise(candidateNavCoord, candidateRockTileEntity) && Mathf.Approximately((float)candidateNavCoord.HPPenetrationLength, 0f)) // TODO: Is this checking hppenetration twice?
			)
			return false;

		// If it's a free space that's not through a wall, while the best one found isn't a free space, or is through a wall
		if (
			!(bestIsFreeSpace && Mathf.Approximately((float)bestShortestHPPenetration, 0f)) 
			&& (IsFreeSpaceNavWise(candidateNavCoord, candidateRockTileEntity) && Mathf.Approximately((float)candidateNavCoord.HPPenetrationLength, 0f)) // TODO: Is this checking hppenetration twice?
			)
			return false;

		if (candidateRockTileEntity != null && candidateRockTileEntity.miningValue > bestHighestMiningValue)
			return false;

		// TODO: This is a little odd, but correct. Removing this makes ManualCopperOverFreeSpaceBehindRock fail because it falls through "who has the shorter penetration"... which also seems like the wrong question
		if (bestHighestMiningValue > 0d)
			return true;

		if (candidateRockTileEntity != null && Mathf.Approximately((float)candidateRockTileEntity.miningValue, (float)bestHighestMiningValue))
		{
			if (!IsShorterPathOrPenetration(candidateNavCoord, bestShortestPath, bestShortestHPPenetration))
			{
				return false;
			}
		}

		if (candidateRockTileEntity == null && !IsShorterPathOrPenetration(candidateNavCoord, bestShortestPath, bestShortestHPPenetration))
			return false;

		return true;
	}

	// TODO: Again, I think the boolean logic here is reversed of what would be reasonable
	public static bool IsHigherMiningValueOrOrNotIndestructibleOrNotFreeSpaceOrNotInFog(RockTileEntity candidateRockTileEntity, FullFogEntity candidateFullFogEntity, double bestHighestMiningValue, bool bestNotIndestructible, bool bestNotFreeSpace, bool bestNotInFog)
	{
		if (candidateFullFogEntity != null) // NOTE: Being in fog is unnacceptable as a target
			return true;

		if (candidateFullFogEntity == null && !bestNotInFog)
			return false;

		if (candidateRockTileEntity != null && !bestNotFreeSpace)
			return false;

		if (candidateRockTileEntity != null && candidateRockTileEntity.destructible && !bestNotIndestructible)
			return false;

		if (candidateRockTileEntity != null && candidateRockTileEntity.miningValue > bestHighestMiningValue)
			return false;

		return true;
	}

	public static bool IsExitNavWise(NavCoord navCoord, RockTileEntity rockTileEntity)
	{
		return (rockTileEntity != null && rockTileEntity.isExit && Mathf.Approximately((float)navCoord.HPPenetrationLength, 0f));
	}

	public static bool IsFreeSpaceNavWise(NavCoord navCoord, RockTileEntity rockTileEntity)
	{
		return (rockTileEntity == null && Mathf.Approximately((float)navCoord.HPPenetrationLength, 0f));
	}

	public static bool CheckTileMayBePassableRegardlessOfFog(Vector2Int coord, LevelEntity levelEntity)
	{
		if (!levelEntity.floorTiles[coord.x, coord.y].walkable || !levelEntity.floorTiles[coord.x, coord.y].passable)
			return false;

		RockTileEntity rockTile = levelEntity.rockTiles[coord.x, coord.y];

		if (rockTile == null)
			return true;

		if (rockTile.passable)
			return true;

		if (!rockTile.destructible)
			return false;

		return true;
	}

	// Fog is assumed passable
	public static bool CheckTileMayBePassable(Vector2Int coord, LevelEntity levelEntity)
	{
		// NOTE: Units assume tiles under fog are walkable
		if (levelEntity.fullFogTiles[coord.x, coord.y] != null)
			return true;

		return CheckTileMayBePassableRegardlessOfFog(coord, levelEntity);
	}

	public static Vector2Int NormalizedGridAlignedDirectionFromDeltas(float deltaX, float deltaY)
	{
		if (Mathf.Abs(deltaY) > Mathf.Abs(deltaX))
		{
			if (deltaY > 0f)
				return Vector2Int.up;

			return Vector2Int.down;
		}
		else if (Mathf.Approximately(deltaX, 0))
		{
			// Debug.Log("Both 0");
			return Vector2Int.down; // In this case they're both 0, show "Down"
		}
		else if (Mathf.Approximately(Mathf.Abs(deltaY), Mathf.Abs(deltaX)) && deltaY < 0f)
		{
			return Vector2Int.down; // Prefer to show the "face" in case going SW or SE
		}
		else // NW, NE, W, E
		{
			if (deltaX > 0f)
				return Vector2Int.right;
			return Vector2Int.left;
		}
	}

	public static FourDirections FourDirectionsFromNormalziedGridAlignedDirection(Vector2Int direction)
	{
		if (direction == Vector2Int.left)
			return FourDirections.Left;
		else if (direction == Vector2Int.up)
			return FourDirections.Up;
		else if (direction == Vector2Int.right)
			return FourDirections.Right;
		else if (direction == Vector2Int.down)
			return FourDirections.Down;
		else
			throw new System.Exception("Four directions called with a non-normalized, gird aligned direction.");
	}

	public static bool DwarfIsWithinBounds(DwarfEntity dwarf, BoundsInt bounds)
	{
		return bounds.Contains(new Vector3Int(dwarf.coord.x, dwarf.coord.y, 0));
	}

	public static bool AllDwarvesAreWithinBounds(List<DwarfEntity> dwarfEntities, List<BoundsInt> boundsList)
	{
		foreach (var dwarfEntity in dwarfEntities)
		{
			foreach (var bounds in boundsList)
				if (!DwarfIsWithinBounds(dwarfEntity, bounds))
					return false;
		}

		return true;
	}

	public static bool AtLeastKDwarvesAreWithinBounds(int k, List<DwarfEntity> dwarfEntities, List<BoundsInt> boundsList)
	{
		int count = 0;
		bool dwarfPassedCountOnce;

		foreach (var dwarfEntity in dwarfEntities)
		{
			dwarfPassedCountOnce = false;

			for (int i = 0; !dwarfPassedCountOnce && i < boundsList.Count; i++)
				if (DwarfIsWithinBounds(dwarfEntity, boundsList[i]))
				{
					dwarfPassedCountOnce = true;
					count++;
				}
		}

		return count >= k;
	}

	public static bool CameraIsWithinBounds(Camera camera, BoundsInt bounds)
	{
		Bounds cameraBounds = new Bounds(new Vector3(bounds.center.x, bounds.center.y, camera.transform.position.z), bounds.size);
		return cameraBounds.Contains(camera.transform.position);
	}

	public static bool AssessDialogueEndAndResetFlag(GameStateEntity gameStateEntity)
	{
		if (gameStateEntity.tutorialsStateComponent.dialogueEnded)
		{
			gameStateEntity.tutorialsStateComponent.dialogueEnded = false;
			return true;
		}
		return false;
	}

	public static void EntityRemover<T>(T[,] entityArray) where T : MonoBehaviour
	{
		T gameObject;

		for (int x = entityArray.GetLength(0) - 1; x >= 0; x--)
			for (int y = entityArray.GetLength(1) - 1; y >= 0; y--)
			{
				gameObject = entityArray[x, y];
				if (gameObject != null)
				{
					GameObject.Destroy(gameObject.gameObject);
					entityArray[x, y] = null;
				}
			}
	}

	public static void EntityRemover<T>(List<T> entities) where T: MonoBehaviour
	{
		foreach (var entity in entities)
		{
			GameObject.Destroy(entity.gameObject);
		}

		entities.Clear();
	}
	public static void ComponentRemoverSetNull<T>(T[,] componentArray) where T : class
	{
		T component;

		for (int x = componentArray.GetLength(0) - 1; x >= 0; x--)
			for (int y = componentArray.GetLength(1) - 1; y >= 0; y--)
			{
				component = componentArray[x, y];
				if (component != null)
				{
					componentArray[x, y] = null;
				}
			}
	}

	// TODO: Not currently used
	public static void BoolClearer(List<bool> boolList)
	{
		for (int i = 0; i < boolList.Count; i++)
			boolList[i] = false;
	}

	public static void BoolClearer(bool[,] boolArray)
	{
		for (int x = boolArray.GetLength(0) - 1; x >= 0; x--)
			for (int y = boolArray.GetLength(1) - 1; y >= 0; y--)
				boolArray[x, y] = false;
	}

	public static bool CoordIsInBounds<T>(Vector2Int coord, T[,] array)
	{
		return (coord.x >= 0 && coord.x < array.GetLength(0))
			&& (coord.y >= 0 && coord.y < array.GetLength(1)); 
	}

	public static void ReenableAllDwarfCandidateActionProcessing(List<DwarfEntity> dwarfEntities)
	{
		foreach (var dwarfEntity in dwarfEntities)
			dwarfEntity.tutorialDisabledCandidateActionProcessing = false;
	}

	public static void SetSharedManualCoordinates(List<Vector2Int> assignedManualCoordinates, GameStateEntity gameStateEntity, LevelEntity levelEntity)
	{
		EntityHelpers.RemoveSharedManualCoordinates(levelEntity); // TODO: This is likely unused, at least the way I'm inputting TutorialValues

		foreach (var manualCoordinate in assignedManualCoordinates)
		{
			EntityHelpers.AddValidatedSharedManualCoordinateWithoutIndicatorAndUpdateGameState(gameStateEntity, PlayerClickOrderType.AttackNode, manualCoordinate, levelEntity);
		}
	}
}
