﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeDump
{
	//public void CreateNavigationToManualCoord(DwarfEntity dwarfEntity)
	//{
	//	if (!dwarfEntity.hasManualAssignment)
	//		return;

	//	Vector2Int manualCoord = dwarfEntity.manuallyAssignedCoord;
	//	NavCoord manualNavCoord = navArray[manualCoord.x, manualCoord.y];

	//	// NOTE: This needs to imply that it's cut off because of fog. In the future, we might not build a full nav anymore, so make sure this holds.
	//	if (manualNavCoord != null)
	//		return;

	//	int maxRadius = LogicHelpers.RectilinearDistance(dwarfEntity.coord, manualCoord);

	//	// NOTE: We want the dwarves to check "open" paths into the fog rather than busting through a wall
	//	bool circleSucceededAtConnectingNonHPPenetratingNav = false;
	//	bool circleSucceededAtConnecctingAnyNav = false;
	//	List<Vector2Int> onCircleCoords = new List<Vector2Int>();
	//	List<Vector2Int> foundFogDivingCoords = new List<Vector2Int>();

	//	const int MAX_CIRCLE_ITERATIONS_FOR_MANUAL = 3;
	//	int circleIterationsAfterConnectingAny = 0;

	//	for (int r = 1; r < maxRadius && circleIterationsAfterConnectingAny < MAX_CIRCLE_ITERATIONS_FOR_MANUAL; r++)
	//	{
	//		onCircleCoords.Clear();

	//		LogicHelpers.AddCircleCoordsOnRadiusAroundCoord(manualCoord, r, onCircleCoords);

	//		foreach (var onCircleCoord in onCircleCoords)
	//		{
	//			if (navArray[onCircleCoord.x, onCircleCoord.y] != null)
	//			{
	//				if (Mathf.Approximately((float)navArray[onCircleCoord.x, onCircleCoord.y].HPPenetrationLength, 0f))
	//					circleSucceededAtConnectingNonHPPenetratingNav = true;
	//				circleSucceededAtConnecctingAnyNav = true;

	//				foundFogDivingCoords.Add(onCircleCoord);
	//			}

	//			if (circleSucceededAtConnecctingAnyNav)
	//				circleIterationsAfterConnectingAny++;
	//		}
	//	}

	//	int xDelta;
	//	int yDelta;
	//	int xAdder;
	//	int yAdder;
	//	Vector2Int currentDivingCoord;
	//	Vector2Int prevDivingCoord;
	//	bool pathAlreadyDived = false;

	//	foreach (var foundFogDivingCoord in foundFogDivingCoords)
	//	{
	//		currentDivingCoord = foundFogDivingCoord;

	//		xDelta = manualCoord.x - currentDivingCoord.x;
	//		yDelta = manualCoord.y - currentDivingCoord.y;
	//		xAdder = xDelta > 0 ? 1 : -1;
	//		yAdder = yDelta > 0 ? 1 : -1;

	//		while (
	//			(xDelta != 0 || yDelta != 0)
	//			&& !pathAlreadyDived
	//			)
	//		{
	//			prevDivingCoord = currentDivingCoord;

	//			if (Mathf.Abs(xDelta) > Mathf.Abs(yDelta))
	//				currentDivingCoord.x += xAdder;
	//			else
	//				currentDivingCoord.y += yAdder;

	//			if (navArray[currentDivingCoord.x, currentDivingCoord.y] != null)
	//				pathAlreadyDived = true;

	//			navArray[currentDivingCoord.x, currentDivingCoord.y] = new NavCoord()
	//			{
	//				prevCoord = prevDivingCoord,
	//				pathLength = 0, // TODO: change this, might as well track the path length
	//				HPPenetrationLength = 0
	//			};

	//			xDelta = manualCoord.x - currentDivingCoord.x;
	//			yDelta = manualCoord.y - currentDivingCoord.y;
	//		}
	//	}

	//	// TODO: manual nav preference should be in this order:
	//	// Move to a full fog tile
	//	// Move to an empty tile
	//	// Penetrate an easy wall to get to a full fog blocked tile
	//	// Mine a node - because walls are easier than nodes (basically nodes don't make it better, it's just penetration length)


	//}

	// Not tested, but reasonable looking smelt code in the AttackDamageInstanceResolutionSystem

	//			if (damageOutcome.damageOutcomeType == DamageOutcomeType.Smelt)
	//			{
	//				if (damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds.Count > damageRockTileEntity.rockDamagedStageComponent.thresholdStage)
	//				{
	//					damageRockTileEntity.HP = damageRockTileEntity.maxHP* damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds[damageRockTileEntity.rockDamagedStageComponent.thresholdStage];

	//					damageRockTileEntity.rockDamagedStageComponent.thresholdStage++;

	//					damageOutcomeWillCauseDamagedStageOrDeath = Outcome.DamagedStage;
	//				}
	//				else
	//				{
	//					damageRockTileEntity.HP = 0d; // Probably not needed

	//					damageOutcomeWillCauseDamagedStageOrDeath = Outcome.Death;
	//				}
	//			}
	//			else
	//			{
	//				damageRockTileEntity.HP -= damageOutcome.damage;

	//				while (damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds.Count > damageRockTileEntity.rockDamagedStageComponent.thresholdStage
	//				 && damageRockTileEntity.HP / damageRockTileEntity.maxHP <= damageRockTileEntity.rockDamagedStageComponent.percentualRockDamagedStageThresholds[damageRockTileEntity.rockDamagedStageComponent.thresholdStage])
	//				{
	//					damageRockTileEntity.rockDamagedStageComponent.thresholdStage++;

	//					damageOutcomeWillCauseDamagedStageOrDeath = Outcome.DamagedStage;
	//				}

	//				if (damageRockTileEntity.HP <= 0d)
	//					damageOutcomeWillCauseDamagedStageOrDeath = Outcome.Death;
	//				else
	//					damageOutcomeWillCauseDamagedStageOrDeath = Outcome.Nothing;
	//			}


	//FloorTileEntity[,] floorTiles;
	//RockTileEntity[,] rockTiles;
	//PartialFogEntity[,] partialFogTiles;
	//FullFogEntity[,] fullFogTiles;
	//PlayerInputIndicator[,] playerInputIndicators;

	//TileMovementStateComponent[,] reservedByAttackerTiles; // TODO: this has problems, the MovementStateComponent needs to know who its parent entity is for the MovementComponent to be useful
	//TileMovementStateComponent[,] reservedByFreeSpaceMoverTiles; // TODO: This was very quickly implemented and probably sucks. Implemented by imitating reservedByAttackerTiles. Key idea is it's used in one place to discard sharedManualCoords by checking that it's not null
	//Vector2Int levelDwarfEntranceCoord;
	//Vector2Int levelExitCoord; // TODO: Barely used

	//List<Vector2Int> sharedManualCoordinates;
	//bool[,] isSharedManualCoordinate;
	//NavCoord[,] sharedUnreachableNavArray;
	//List<Vector2Int> sharedFoundUnreachableEdges;
	//bool[,] sharedConsideredUnreachableEdgeAlready;

	//floorTiles = levelEntity.floorTiles;
	//rockTiles = levelEntity.rockTiles;
	//partialFogTiles = levelEntity.partialFogTiles;
	//fullFogTiles = levelEntity.fullFogTiles;
	//playerInputIndicators = levelEntity.playerInputIndicators;

	//reservedByAttackerTiles = levelEntity.reservedByAttackerTiles; // TODO: this has problems, the MovementStateComponent needs to know who its parent entity is for the MovementComponent to be useful
	//reservedByFreeSpaceMoverTiles = levelEntity.reservedByFreeSpaceMoverTiles; // TODO: This was very quickly implemented and probably sucks. Implemented by imitating reservedByAttackerTiles. Key idea is it's used in one place to discard sharedManualCoords by checking that it's not null

	//sharedManualCoordinates = levelEntity.sharedManualCoordinates;
	//isSharedManualCoordinate = levelEntity.isSharedManualCoordinate;
	//sharedUnreachableNavArray = levelEntity.sharedUnreachableNavArray;
	//sharedFoundUnreachableEdges = levelEntity.sharedFoundUnreachableEdges;
	//sharedConsideredUnreachableEdgeAlready = levelEntity.sharedConsideredUnreachableEdgeAlready;
}
