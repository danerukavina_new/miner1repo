﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TestHelpers
{
	public static string CustomListOutput(this List<Vector2Int> list)
	{
		string ret = "Count = " + list.Count + " ... ";

		foreach (var item in list)
		{
			ret += "( " + item.x + ", " + item.y + "), ";
		}

		return ret;
	}
}
