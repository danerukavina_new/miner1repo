﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitedQueuedSet<T>
{
	private List<T> orderedSet;
	private int limitedSize;

	public LimitedQueuedSet(int limitedSize)
	{
		this.limitedSize = limitedSize;
		orderedSet = new List<T>(limitedSize);
	}

	public void AddOrMoveToEndOfSet(T item)
	{
		int index = orderedSet.IndexOf(item);
		
		if (index >= 0)
		{
			//orderedSet.RemoveAt(index);
			//orderedSet.Add(item);
		}
		else
		{
			orderedSet.Add(item);

			// NOTE: Assumes set only grows by 1, so one check/removal is enough
			if (orderedSet.Count > limitedSize)
				orderedSet.RemoveAt(0);
		}
	}

	public T this[int i] => orderedSet[i];

	public int Count() => orderedSet.Count;
}
