﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemManager : MonoBehaviour
{
	public GameStateEntity gameStateEntity;
	public PlayerStateEntity playerStateEntity;
	public LevelEntity levelEntity;
	public List<DwarfEntity> dwarfEntities;
	public List<AttackEntity> attackEntities;
	public List<AttackDamageInstance> attackDamageInstances; // Not an entity at all? Components of what?
	public List<DamageOrBuffTextEntity> damageOrBuffTextEntities; // Not destroyed in destory all yet
	public List<ResourceGainTextEntity> resourceGainTextEntities; // Not destroyed in destory all yet
	public List<HPBarEntity> HPBarEntities; // Not destroyed in destory all yet
	public List<RockDamageOrDeathAnimationEntity> rockDeathAnimationEntities; // Not destroyed in destory all yet
	public List<RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationEntities;
	public List<BuffGiverAreaEntity> buffGiverAreaEntities;

	// Gameplay Systems
	public LoadTerrainSystem loadTerrainSystem;
	public LoadDwarvesSystem loadDwarvesSystem;
	public PrepareFixedUpdateFlagsSystem prepareFixedUpdateFlagsSystem;
	public ClearFixedUpdateFlagsUsedByMultipleSystems clearFixedUpdateFlagsUsedByMultipleSystems;
	public UnitCandidateActionByPathfindingSystem dwarfCandidateActionByPathfindingSystem;
	public SetMovementFromNavPathSystem setMovementFromNavPathSystem;
	public SetMovementFromTargetSystem setMovementFromTargetSystem;
	public TileMovementSystem tileMovementSystem;
	public AttackCreationSystem attackCreationSystem;
	public AttackBackswingSystem attackBackswingSystem;
	public AttackCompletionAndRotationSystem attackCompletionAndRotationSystem;
	public AttackStateTransitionSystem attackStateTransitionSystem;
	public EntityDissipationSystem entityDissipationSystem;
	public AttackDamageInstanceResolutionSystem attackDamageInstanceResolutionSystem;
	public PartialFogReturnSystem partialFogReturnSystem;
	public FogSightRemoveSystem fogSightRemoveSystem;
	public PlayerInputSystem playerInputSystem;
	public RemoveCompletedPlayerInputSystem removeCompletedPlayerInputSystem;
	public ScreenMovementSystem screenMovementSystem;
	public SometimesSetDirectionFromStateSystem sometimesSetDirectionFromStateSystem;
	public SetAnimationFromStateSystem setAnimationFromUnitStateSystem;
	public UnitCandidateStateResolverSystem unitCandidateStateResolverSystem;
	public UnitStateTransitionSystem unitStateTransitionSystem;
	public AttackDamageInstanceCreationSystem attackDamageInstanceCreationSystem;
	public PerformUpgradeForUnitStatsSystem performUpgradeForUnitStatsSystem;
	public UpdateUnitStatsSystem updateUnitStatsSystem;
	public CandidateResourceAdditionResolutionSystem candidateResourceAdditionResolutionSystem;
	public CameraMovementSystem cameraMovementSystem;
	public CameraBoundsCalculationSystem cameraBoundsCalculationSystem;
	public TutorialStartStepSystem tutorialStartStepSystem;
	public TutorialExecuteStepSystem tutorialExecuteStepSystem;
	public RemoveExitingDwarvesSystem removeExitingDwarvesSystem;
	public LevelEndSystem levelEndSystem;
	public BuffGiverAreaCreationSystem buffGiverAreaCreationSystem;
	public BuffGiverAreaGiveBuffsSystem buffGiverAreaGiveBuffsSystem;
	public BuffReceiverReceiveBuffsSystem buffReceiverReceiveBuffsSystem;
	public BuffResolverSystem buffResolverSystem;
	public BuffVisualEntityUpdateSystem buffVisualEntityUpdateSystem;

	// UI Systems
	public StartScreenControlSystem startScreenControlSystem;
	public ResourceDisplayCandidateStateResolverSystem resourceDisplayCandidateStateResolverSystem;
	public ShowAndHideResourceDisplaySystem showAndHideResourceDisplaySystem;
	public DialogueControlSystem dialogueControlSystem;
	public ReinitializeDwarfUpgradePanelDataSystem reinitializeDwarfUpgradePanelDataSystem;
	public OpenOrCloseDwarfUpgradesSystem openOrCloseDwarfUpgradesSystem;
	public UpdateDwarfUpgradePanelEntitiesSystem updateDwarfUpgradePanelEntitiesSystem;
	public ProcessDwarfUpgradePanelUpgradeClicksSystem processDwarfUpgradePanelUpgradeClicksSystem;
	public DwarfUpgradePanelUpgradeStatsTextsSystem dwarfUpgradePanelUpgradeStatsTextsSystem;
	public HideOrReshowDwarfUpgradePanelUpgradeButtonsSystem hideOrReshowDwarfUpgradePanelUpgradeButtonsSystem;
	public BarelyInteractiveInfoModalControlSystem barelyInteractiveInfoModalControlSystem;

	// Monosystems
	public SkipSomeTutorialLevelsMonosystem skipSomeTutorialLevelsMonosystem;

	public GameObject floorObject;
	public GameObject rocksObject;
	public GameObject dwarvesObject;
	public GameObject attacksObject;
	public GameObject partialFogObject;
	public GameObject fullFogObject;
	public GameObject playerInputIndicatorsObject;
	public GameObject damageOrBuffTexts;
	public GameObject resourceGainTexts;
	public GameObject unitProgressBars;
	public GameObject rockDeathAnimationsObject;
	public GameObject rockDamagedStageAnimationsObject;
	public GameObject buffGiverAreasObject;

	// TODO: dictionaries keyed on enums are not good - look here: https://docs.unity3d.com/560/Documentation/Manual/BestPracticeUnderstandingPerformanceInUnity4-1.html

	public FloorTileEntity[] editorFloorTilesPrefabs;
	public Dictionary<FloorTileType, FloorTileEntity> floorTilesPrefabs;

	public RockTileEntity[] editorRockTilesPrefabs;
	public Dictionary<RockTileType, RockTileEntity> rockTilesPrefabs;

	public DwarfEntity[] editorDwarfPrefabs;
	public Dictionary<DwarfType, DwarfEntity> dwarfPrefabs;

	public AttackEntity[] editorAttackPrefabs;
	public Dictionary<AttackType, AttackEntity> attackPrefabs;

	public PartialFogEntity[] editorPartialFogEntities;
	public Dictionary<FogShapeType, PartialFogEntity> partialFogPrefabs;

	public FullFogEntity[] editorFullFogEntities;
	public Dictionary<FogShapeType, FullFogEntity> fullFogPrefabs;

	public PlayerInputIndicator playerInputIndicatorPrefab;

	public DamageOrBuffTextEntity[] editorDamageOrBuffTextEntities;
	public Dictionary<DamageOrBuffTextType, DamageOrBuffTextEntity> damageOrBuffTextPrefabs;

	public ResourceGainTextEntity resourceGainTextPrefab;

	public BuffGiverAreaEntity[] editorBuffGiverAreaPrefabs;
	public Dictionary<BuffGiverAreaType, BuffGiverAreaEntity> buffGiverAreaPrefabs;

	public BuffVisualAnimationControllerData[] editorBuffVisualAnimationControllers;
	public Dictionary<BuffType, BuffVisualAnimationControllerData> buffVisualAnimationControllers;

	public TutorialInputIndicatorAnimationControllerData[] editorTutorialInputIndicatorAnimationControllers;
	public Dictionary<TutorialInputIndicatorType, TutorialInputIndicatorAnimationControllerData> tutorialInputIndicatorAnimationControllers;

	// TODO: Sprite doesn't know it's ResourceType so associating these two is done in a janky way: by casting int to enum
	// Find a nicer way to store these together. One way is to declare a new very pointless class that has ResourceType and Sprite in it and make it Serializable
	public ResourceTypeSpriteData[] editorResourceSprites;
	public Dictionary<ResourceType, Sprite> resourceSprites; // TODO: Consider changing this to having values of ResourceTypeSpriteData, in case there's multiple sprites per resource

	public Sprite cameraArrowGridAlignedDeactivated;
	public Sprite cameraArrowGridAlignedActivated;
	public Sprite cameraArrowDiagonalDeactivated;
	public Sprite cameraArrowDiagonalActivated;

	public HPBarEntity HPBarEntityPrefab;

	public RockDamageOrDeathAnimationEntity[] editorRockDeathAnimationEntities;
	public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDeathAnimationPrefabs;

	public RockDamageOrDeathAnimationEntity[] editorRockDamagedStageAnimationEntities;
	public Dictionary<RockTileType, RockDamageOrDeathAnimationEntity> rockDamagedStageAnimationPrefabs;

	public DwarfSpriteData[] editorDwarfSprites;
	public Dictionary<DwarfType, DwarfSpriteData> dwarfSprites;



	// UI Entities
	public GameObject startScreenCanvasEntity;
	public ResourceDisplayCanvasEntity resourceDisplayCanvasEntity;
	public CameraInputCanvasEntity cameraInputCanvasEntity;
	public GameObject cameraIndicationCanvasEntity;
	public DialogueDisplayCanvasEntity dialogueDisplayCanvasEntity;
	public DwarfUpgradeDisplayCanvasEntity dwarfUpgradeDisplayCanvasEntity; // TODO: This is going into too many systems. A CandidateResourceAddition (and subtraction) system should be adjusting its flags, and it should do so in Update() not FixedUpdate(). An example is the PerformUpgradeForUnitStatsSystem.
	public DwarfUpgradePanelEntity dwarfUpgradePanelPrefab;
	public BarelyInteractiveInfoModalCanvasEntity barelyInteractiveInfoModalCanvasEntity;
	public TutorialInputIndicatorEntity tutorialInputIndicatorEntity;
	public GameObject openDwarfUpgradesButtonCanvasEntity;

	// Debugging Entities
	public GameObject debugCanvasEntity1;
	public TMPro.TextMeshProUGUI debugText1;
	public GameObject debugCanvasEntity2;
	public TMPro.TextMeshProUGUI debugText2;

	// Start is called before the first frame update
	void Start()
	{
		gameStateEntity = new GameStateEntity
		{
			haveStartScreenAtStartOfGame = true,

			levelShouldBeLoaded = true,
			dwarvesShouldBeLoaded = true,
			indexToLoad = 0,
			baseCameraMovementSpeed = ValueHolder.CAMERA_MOVE_SPEED,
			cameraPressAndHoldControlType = CameraPressAndHoldControlType.ClickAndDrag,
			cameraEdgeOfScreenControlEnabledNYI = false,

			autoMine = false,
			difficultyTier = 1,

			inputState = PlayerClickInputState.NoneOrTutorialCancelled,
			tutorialDisabledManualCoordinateInput = false,
			tutorialDisabledCameraInput = false,
			tutorialDisableAllButExitInput = false,

			tutorialsStateComponent = new TutorialsStateComponent()
			{
				tutorialStepEntities = TutorialValueHolder.tutorialStepEntities,
				tutorialStepIsActive = false,
				currentOrNextTutorialStep = 0,
			},
			tutorialDisabledResourceGains = false,

			dialogueStateComponent = new DialogueStateComponent()
			{
				initiateInteractiveDialogue = false,
			},

			currentlyOpenMenusSubscription = 0,

			openDwarfUpgradesClickedOrTriggered = false,
			closeDwarfUpgradesClicked = false,
			continueDwarfUpgradesClicked = false,
			dwarfPartyRearrangeOccurredRefreshUpgradesPanel = false,
			disableDwarfUpgradeContinueButton = false,
			reenableDwarfUpgradeContinueButton = false,
			hideDwarfUpgradeButtons = false,
			hideAllDwarfUpgradeButtons = false,
			reshowDwarfUpgradeButtons = false,
			reshowAllDwarfUpgradeButtons = false,
			hideDwarfUpgradeButtonDwarfIDs = new List<int>(ValueHolder.MAXIMUM_DWARVES_IN_PARTY),
			reshowDwarfUpgradeButtonDwarfIDs = new List<int>(ValueHolder.MAXIMUM_DWARVES_IN_PARTY),
			dwarfUpgradesTriggersAtEndsOfLevels = true,
			dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel = false,

			tutorialSmeltControlSetting = TutorialSmeltControlSetting.None,

			testingSkipStartScreen = false,
			testingSkipSomeTutorialLevels = false,
			testingLevelOfTutorialToSkipTo = 0,
		};

		debugCanvasEntity1.SetActive(false);
		debugCanvasEntity2.SetActive(false);

		levelEntity = new LevelEntity();
		EntityHelpers.InitializeLevelEntity(levelEntity);
		dwarfEntities = new List<DwarfEntity>(ValueHolder.MAXIMUM_DWARVES_IN_POSSESSION);
		attackEntities = new List<AttackEntity>(ValueHolder.MAXIMUM_SIMULTANEOUS_ATTACKS);
		attackDamageInstances = new List<AttackDamageInstance>(ValueHolder.MAXIMUM_SIMULTANEOUS_DAMAGE_AND_BUFF_EFFECTS);
		damageOrBuffTextEntities = new List<DamageOrBuffTextEntity>(ValueHolder.MAXIMUM_SIMULTANEOUS_DAMAGE_AND_BUFF_EFFECTS);
		HPBarEntities = new List<HPBarEntity>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT);
		rockDeathAnimationEntities = new List<RockDamageOrDeathAnimationEntity>(ValueHolder.MAXIMUM_LEVEL_TILE_COUNT);
		rockDamagedStageAnimationEntities = new List<RockDamageOrDeathAnimationEntity>(ValueHolder.MAXIMUM_SIMULTANEOUS_DAMAGE_AND_BUFF_EFFECTS);
		playerStateEntity = new PlayerStateEntity()
		{
			playerResourcesComponent = new PlayerResourcesComponent()
			{
				resources = new Dictionary<ResourceType, ResourceEntity>()
				{
					{ ResourceType.Stone, new ResourceEntity() { resourceType = ResourceType.Stone, amount = 20d} },
					{ ResourceType.CopperOre, new ResourceEntity() { resourceType = ResourceType.CopperOre, amount = 0d} },
					{ ResourceType.TinOre, new ResourceEntity() { resourceType = ResourceType.TinOre, amount = 0d} },
					{ ResourceType.SilverOre, new ResourceEntity() { resourceType = ResourceType.SilverOre, amount = 0d} },
					{ ResourceType.CopperBar, new ResourceEntity() { resourceType = ResourceType.CopperBar, amount = 0d} },
					{ ResourceType.TinBar, new ResourceEntity() { resourceType = ResourceType.TinBar, amount = 0d} },
					{ ResourceType.SilverBar, new ResourceEntity() { resourceType = ResourceType.SilverBar, amount = 0d} },
					{ ResourceType.BronzeBar, new ResourceEntity() { resourceType = ResourceType.BronzeBar, amount = 0d} },
					{ ResourceType.Diamond, new ResourceEntity() { resourceType = ResourceType.Diamond, amount = 0d} }
				},
				candidateResourceAdditions = new List<CandidateResourceAddition>(ValueHolder.MAXIMUM_SIMULTANEOUS_ATTACKS),
				recentlyAddedResources = new LimitedQueuedSet<ResourceType>(ValueHolder.RECENT_RESOURCES_TO_DISPLAY)
			}
		};

		// TODO: this crappy initalization section below should be better handled
		resourceDisplayCanvasEntity.moveToShow = false;
		resourceDisplayCanvasEntity.preventAutoHideSubscribers = 0;

		dwarfUpgradeDisplayCanvasEntity.dwarfUpgradePanelPrefab = dwarfUpgradePanelPrefab;

		barelyInteractiveInfoModalCanvasEntity.openNow = false;
		barelyInteractiveInfoModalCanvasEntity.textsToShow = new List<string>(ValueHolder.MAXIMUM_BARELY_INTERACTIVE_INFO_MODAL_TEXTS_TO_SHOW);

		EntityHelpers.DictionerifyEditorArrays(
			editorFloorTilesPrefabs,
			ref floorTilesPrefabs,

			editorRockTilesPrefabs,
			ref rockTilesPrefabs,

			editorDwarfPrefabs,
			ref dwarfPrefabs,

			editorAttackPrefabs,
			ref attackPrefabs,

			editorPartialFogEntities,
			ref partialFogPrefabs,

			editorFullFogEntities,
			ref fullFogPrefabs,

			editorDamageOrBuffTextEntities,
			ref damageOrBuffTextPrefabs,

			editorResourceSprites,
			ref resourceSprites,

			editorRockDeathAnimationEntities,
			ref rockDeathAnimationPrefabs,

			editorRockDamagedStageAnimationEntities,
			ref rockDamagedStageAnimationPrefabs,

			editorDwarfSprites,
			ref dwarfSprites,

			editorBuffGiverAreaPrefabs,
			ref buffGiverAreaPrefabs,

			editorBuffVisualAnimationControllers,
			ref buffVisualAnimationControllers
			);

		// TODO: Move more non-unit-test related dictionaries here, maybe?
		EntityHelpers.DictionerifyAdditionalEditorArrays(
			editorTutorialInputIndicatorAnimationControllers,
			ref tutorialInputIndicatorAnimationControllers
		);

		loadTerrainSystem = new LoadTerrainSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			floorTilesPrefabs = floorTilesPrefabs,
			rockTilesPrefabs = rockTilesPrefabs,
			partialFogPrefabs = partialFogPrefabs,
			fullFogPrefabs = fullFogPrefabs,
			floorObject = floorObject,
			rockObject = rocksObject,
			partialFogObject = partialFogObject,
			fullFogObject = fullFogObject,
			HPBarEntities = HPBarEntities,
			buffGiverAreaEntities = buffGiverAreaEntities
		};

		loadDwarvesSystem = new LoadDwarvesSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity,
			dwarfPrefabs = dwarfPrefabs,
			dwarvesObject = dwarvesObject
		};

		dwarfCandidateActionByPathfindingSystem = new UnitCandidateActionByPathfindingSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		setMovementFromNavPathSystem = new SetMovementFromNavPathSystem()
		{
			dwarfEntities = dwarfEntities,
		};

		setMovementFromTargetSystem = new SetMovementFromTargetSystem()
		{
			attackEntities = attackEntities,
		};

		tileMovementSystem = new TileMovementSystem()
		{
			dwarfEntities = dwarfEntities,
			attackEntities = attackEntities
		};

		attackCreationSystem = new AttackCreationSystem()
		{
			dwarfEntities = dwarfEntities,
			attackEntities = attackEntities,
			attackPrefabs = attackPrefabs,
			attacksObject = attacksObject
		};

		attackBackswingSystem = new AttackBackswingSystem()
		{
			dwarfEntities = dwarfEntities
		};

		attackCompletionAndRotationSystem = new AttackCompletionAndRotationSystem()
		{
			dwarfEntities = dwarfEntities
		};

		attackStateTransitionSystem = new AttackStateTransitionSystem()
		{
			attackEntities = attackEntities,
			levelEntity = levelEntity
		};

		entityDissipationSystem = new EntityDissipationSystem()
		{
			attackEntities = attackEntities,
			damageOrBuffTextEntities = damageOrBuffTextEntities,
			resourceGainTextEntities = resourceGainTextEntities,
			rockDeathAnimationEntities = rockDeathAnimationEntities,
			rockDamageStageAnimationEntities = rockDamagedStageAnimationEntities,
			buffGiverAreaEntities = buffGiverAreaEntities
		};

		prepareFixedUpdateFlagsSystem = new PrepareFixedUpdateFlagsSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfEntities = dwarfEntities
		};

		clearFixedUpdateFlagsUsedByMultipleSystems = new ClearFixedUpdateFlagsUsedByMultipleSystems()
		{
			dwarfEntities = dwarfEntities
		};

		attackDamageInstanceResolutionSystem = new AttackDamageInstanceResolutionSystem()
		{
			attackDamageInstances = attackDamageInstances,
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			playerStateEntity = playerStateEntity,
			dwarfEntities = dwarfEntities,
			HPBarEntities = HPBarEntities,
			rockDeathAnimationEntities = rockDeathAnimationEntities,
			rockDeathAnimationPrefabs = rockDeathAnimationPrefabs,
			rockDeathAnimationsObject = rockDeathAnimationsObject,
			rockDamagedStageAnimationEntities = rockDamagedStageAnimationEntities,
			rockDamagedStageAnimationPrefabs = rockDamagedStageAnimationPrefabs,
			rockDamagedStageAnimationsObject = rockDamagedStageAnimationsObject,
			damageOrBuffTextEntities = damageOrBuffTextEntities,
			damageOrBuffTextPrefabs = damageOrBuffTextPrefabs,
			damageOrBuffTexts = damageOrBuffTexts,
			HPBarEntityPrefab = HPBarEntityPrefab,
			unitProgressBars = unitProgressBars
		};

		partialFogReturnSystem = new PartialFogReturnSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		fogSightRemoveSystem = new FogSightRemoveSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		playerInputSystem = new PlayerInputSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			playerInputIndicatorPrefab = playerInputIndicatorPrefab,
			playerInputIndicatorObject = playerInputIndicatorsObject,

			debugText1 = debugText1,
			debugText2 = debugText2
		};

		removeCompletedPlayerInputSystem = new RemoveCompletedPlayerInputSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		screenMovementSystem = new ScreenMovementSystem()
		{
			damageOrBuffTextEntities = damageOrBuffTextEntities,
			resourceGainTextEntities = resourceGainTextEntities
		};

		sometimesSetDirectionFromStateSystem = new SometimesSetDirectionFromStateSystem()
		{
			dwarfEntities = dwarfEntities,
			attackEntities = attackEntities
		};

		setAnimationFromUnitStateSystem = new SetAnimationFromStateSystem()
		{
			dwarfEntities = dwarfEntities,
			attackEntities = attackEntities
		};

		unitCandidateStateResolverSystem = new UnitCandidateStateResolverSystem()
		{
			dwarfEntities = dwarfEntities
		};

		unitStateTransitionSystem = new UnitStateTransitionSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		attackDamageInstanceCreationSystem = new AttackDamageInstanceCreationSystem()
		{
			attackEntities = attackEntities,
			attackDamageInstances = attackDamageInstances,
			levelEntity = levelEntity
		};

		performUpgradeForUnitStatsSystem = new PerformUpgradeForUnitStatsSystem()
		{
			dwarfEntities = dwarfEntities,
			playerStateEntity = playerStateEntity,
			resourceDisplayCanvasEntity = resourceDisplayCanvasEntity,
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			barelyInteractiveInfoModalCanvasEntity = barelyInteractiveInfoModalCanvasEntity
		};

		updateUnitStatsSystem = new UpdateUnitStatsSystem()
		{
			dwarfEntities = dwarfEntities
		};

		candidateResourceAdditionResolutionSystem = new CandidateResourceAdditionResolutionSystem()
		{
			playerStateEntity = playerStateEntity,
			gameStateEntity = gameStateEntity,
			resourceGainTextEntities = resourceGainTextEntities,
			resourceGainTextPrefab = resourceGainTextPrefab,
			resourceSprites = resourceSprites,
			resourceGainTexts = resourceGainTexts,
			resourceDisplayCanvasEntity = resourceDisplayCanvasEntity,
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity
		};

		cameraMovementSystem = new CameraMovementSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			cameraInputCanvasEntity = cameraInputCanvasEntity,
			cameraArrowGridAlignedDeactivated = cameraArrowGridAlignedDeactivated,
			cameraArrowGridAlignedActivated = cameraArrowGridAlignedActivated,
			cameraArrowDiagonalDeactivated = cameraArrowDiagonalDeactivated,
			cameraArrowDiagonalActivated = cameraArrowDiagonalActivated,
			cameraIndicationCanvasEntity = cameraIndicationCanvasEntity
		};

		startScreenControlSystem = new StartScreenControlSystem()
		{
			gameStateEntity = gameStateEntity,
			startScreenCanvasEntity = startScreenCanvasEntity
		};

		resourceDisplayCandidateStateResolverSystem = new ResourceDisplayCandidateStateResolverSystem()
		{
			playerStateEntity = playerStateEntity,
			resourceSprites = resourceSprites,
			resourceDisplayCanvasEntity = resourceDisplayCanvasEntity
		};

		showAndHideResourceDisplaySystem = new ShowAndHideResourceDisplaySystem()
		{
			resourceDisplayCanvasEntity = resourceDisplayCanvasEntity
		};

		cameraBoundsCalculationSystem = new CameraBoundsCalculationSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity
		};

		tutorialStartStepSystem = new TutorialStartStepSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		tutorialExecuteStepSystem = new TutorialExecuteStepSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity,
			mainCamera = Camera.main,
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			tutorialInputIndicatorEntity = tutorialInputIndicatorEntity,
			tutorialInputIndicatorAnimationControllers = tutorialInputIndicatorAnimationControllers,
			openDwarfUpgradesButtonCanvasEntity = openDwarfUpgradesButtonCanvasEntity,
			dwarfPrefabs = dwarfPrefabs,
			rockTilesPrefabs = rockTilesPrefabs,
			dwarvesObject = dwarvesObject,
			rocksObject = rocksObject
		};

		dialogueControlSystem = new DialogueControlSystem()
		{
			gameStateEntity = gameStateEntity,
			dialogueDisplayCanvasEntity = dialogueDisplayCanvasEntity,
			dwarfEntities = dwarfEntities,
			dwarfSprites = dwarfSprites
		};

		removeExitingDwarvesSystem = new RemoveExitingDwarvesSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		levelEndSystem = new LevelEndSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			dwarfEntities = dwarfEntities
		};

		reinitializeDwarfUpgradePanelDataSystem = new ReinitializeDwarfUpgradePanelDataSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			dwarfEntities = dwarfEntities
		};

		openOrCloseDwarfUpgradesSystem = new OpenOrCloseDwarfUpgradesSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			dwarfEntities = dwarfEntities,
			resourceDisplayCanvasEntity = resourceDisplayCanvasEntity
		};

		updateDwarfUpgradePanelEntitiesSystem = new UpdateDwarfUpgradePanelEntitiesSystem()
		{
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			dwarfEntities = dwarfEntities,
			playerStateEntity = playerStateEntity,
			dwarfSprites = dwarfSprites,
			resourceSprites = resourceSprites
		};

		hideOrReshowDwarfUpgradePanelUpgradeButtonsSystem = new HideOrReshowDwarfUpgradePanelUpgradeButtonsSystem()
		{
			gameStateEntity = gameStateEntity,
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			dwarfEntities = dwarfEntities
		};

		processDwarfUpgradePanelUpgradeClicksSystem = new ProcessDwarfUpgradePanelUpgradeClicksSystem()
		{
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			dwarfEntities = dwarfEntities
		};

		dwarfUpgradePanelUpgradeStatsTextsSystem = new DwarfUpgradePanelUpgradeStatsTextsSystem()
		{
			dwarfUpgradeDisplayCanvasEntity = dwarfUpgradeDisplayCanvasEntity,
			dwarfEntities = dwarfEntities
		};

		buffGiverAreaCreationSystem = new BuffGiverAreaCreationSystem()
		{
			buffGiverAreaEntities = buffGiverAreaEntities,
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity,
			buffGiverAreaPrefabs = buffGiverAreaPrefabs,
			buffGiverAreasObject = buffGiverAreasObject,
			damageOrBuffTextEntities = damageOrBuffTextEntities,
			damageOrBuffTextPrefabs = damageOrBuffTextPrefabs,
			damageOrBuffTexts = damageOrBuffTexts
		};

		buffGiverAreaGiveBuffsSystem = new BuffGiverAreaGiveBuffsSystem()
		{
			buffGiverAreaEntities = buffGiverAreaEntities,
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		buffReceiverReceiveBuffsSystem = new BuffReceiverReceiveBuffsSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity,
			buffPseudoprefabs = ValueHolder.buffPseudoprefabs
		};

		buffResolverSystem = new BuffResolverSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity
		};

		buffVisualEntityUpdateSystem = new BuffVisualEntityUpdateSystem()
		{
			dwarfEntities = dwarfEntities,
			levelEntity = levelEntity,
			buffVisualAnimationControllers = buffVisualAnimationControllers
		};

		barelyInteractiveInfoModalControlSystem = new BarelyInteractiveInfoModalControlSystem()
		{
			barelyInteractiveInfoModalCanvasEntity = barelyInteractiveInfoModalCanvasEntity
		};

		dwarfUpgradeDisplayCanvasEntity.Initialize(); // TODO: Why isn't this together with its other initialization buddies up at the start?

		skipSomeTutorialLevelsMonosystem = new SkipSomeTutorialLevelsMonosystem()
		{
			gameStateEntity = gameStateEntity
		};

		ExecuteMonosystems();
	}

	private void ExecuteMonosystems()
	{
		skipSomeTutorialLevelsMonosystem.ExecuteOnce();
	}

	// TODO: The layout group and canvas size fitter found on ResourceDisplayItemPanel look buggy, but I can't get them to play nice. However disabling and enabling the panel works so no need to do anything.
	//private void Awake()
	//{
	//	// TODO: This doesn't work!
	//	foreach (var layourGroupRectTransform in dwarfUpgradeDisplayCanvas.resourceDisplayItemPanelRectTransforms)
	//	{
	//		UnityEngine.UI.LayoutRebuilder.MarkLayoutForRebuild(layourGroupRectTransform);
	//	}
	//}

	static void TestingAddBuffIfItsMissing(DwarfEntity dwarfEntity, BuffType buffType)
	{
		bool control = false;
		foreach (var item in dwarfEntity.buffVisualEntities)
		{
			if (item.hasValidBuff && item.buffType == buffType)
				control = true;
		}

		if (!control)
		{
			Debug.Log("Input applying buff: " + buffType);
			dwarfEntity.buffReceiverComponent.receivedPrototypeBuffsThisFixedUpdate.Add(new PrototypeBuff(buffType, ValueHolder.TEMPORARY_FIXED_BUFF_LEVEL));
		}
	}

	private void FixedUpdate()
	{
		startScreenControlSystem.SystemUpdate(); // TODO: In order to set the gameStateEntity.onStartScreen, which is checked by an early TutorialValueHolder step, this was put in both systems just in case. Seems silly...

		loadTerrainSystem.SystemUpdate();
		loadDwarvesSystem.SystemUpdate();

		tutorialStartStepSystem.SystemUpdate();

		tutorialExecuteStepSystem.SystemUpdate();

		dialogueControlSystem.SystemUpdate(); // Consider if this might better be suited for Update()

		cameraBoundsCalculationSystem.SystemUpdate();

		prepareFixedUpdateFlagsSystem.SystemUpdate();

		performUpgradeForUnitStatsSystem.SystemUpdate();
		updateUnitStatsSystem.SystemUpdate();

		// TODO: Previous position of the attackBasckswingSystem was here, before pathfinding. See it's TODO
		// TODO: the map click system depends on keeping the dwarves sorted descending by power level. This needs to be made more visible. See LoadDwarvesSystem
		dwarfCandidateActionByPathfindingSystem.SystemUpdate();

		unitCandidateStateResolverSystem.SystemUpdate();

		// TODO: If backswing time is the same as the complation time, the dwarf ignores orders for some reason.
		// TODO: Summary of attack time issues:
		// If attack backswing is same as completion, dwarf ignores orders
		// If attack creation is same as backswing, and backswing occurs before pathfinding, which occurs before creation... then the dwarf's target changes and it can fire diagnoally. All of these can be seen on the DwarfRangedBasicProjectile
		attackCompletionAndRotationSystem.SystemUpdate(); // NOTE: Beneficial to have this after the candidate unit state resolver, since it can set initiateAttackRotation

		attackCreationSystem.SystemUpdate(); // TODO: This must go after the attackCompletionAndRotationSystem, otherwise it will use the Attacking state and an "old" attackStartTime and immediately fire off an attack. Improve it to be agnostic to execution order.

		attackBackswingSystem.SystemUpdate(); // TODO: It would be beneficial to have this before the pahtfinding, since that gets checked only for dwarves that finish their backswing
		// TODO: However, placing it before causes pathfinding to change the intendedAttackTarget, and then attackCreation... if it occurs on the same frame... creates an attack for some crazy coord. This should be handled better by some kind of state data.
		// But instead, just moved the backswing system.

		attackStateTransitionSystem.SystemUpdate();

		attackDamageInstanceCreationSystem.SystemUpdate();

		attackDamageInstanceResolutionSystem.SystemUpdate();
		if (Input.GetKey(KeyCode.B))
			TestingAddBuffIfItsMissing(dwarfEntities[0], BuffType.DamageAmpOnNode);
		if (Input.GetKey(KeyCode.N))
			TestingAddBuffIfItsMissing(dwarfEntities[0], BuffType.OreCry);
		buffGiverAreaCreationSystem.SystemUpdate();
		buffGiverAreaGiveBuffsSystem.SystemUpdate();
		buffReceiverReceiveBuffsSystem.SystemUpdate();
		buffResolverSystem.SystemUpdate();
		buffVisualEntityUpdateSystem.SystemUpdate();

		candidateResourceAdditionResolutionSystem.SystemUpdate();

		// TODO: shouldn't these be in Update() because of movement? More resource intense if they're expensive
		partialFogReturnSystem.SystemUpdate();

		fogSightRemoveSystem.SystemUpdate();

		// Makes sense to run after the fog removal system as well as any dwarf movement
		removeCompletedPlayerInputSystem.SystemUpdate();

		clearFixedUpdateFlagsUsedByMultipleSystems.SystemUpdate();
	}

	// Update is called once per frame
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.Space))
			PlayerInputNextSpeechStep();

		startScreenControlSystem.SystemUpdate(); // TODO: In order to set the gameStateEntity.onStartScreen, which is checked by an early TutorialValueHolder step, this was put in both systems just in case. Seems silly...

		barelyInteractiveInfoModalControlSystem.SystemUpdate();

		playerInputSystem.SystemUpdate();
		reinitializeDwarfUpgradePanelDataSystem.SystemUpdate();
		openOrCloseDwarfUpgradesSystem.SystemUpdate();
		updateDwarfUpgradePanelEntitiesSystem.SystemUpdate();
		hideOrReshowDwarfUpgradePanelUpgradeButtonsSystem.SystemUpdate();
		processDwarfUpgradePanelUpgradeClicksSystem.SystemUpdate();
		// TODO: Somewhere in here, it would probably make sense to process resource spending changes -
		// - in a future state of the codebase where candidate changes to player's resources are closely tied to a buttery quick upgrade click response (instead of waiting on Fixed update and somehow locking the UI)
		dwarfUpgradePanelUpgradeStatsTextsSystem.SystemUpdate();

		cameraMovementSystem.SystemUpdate();

		tileMovementSystem.SystemUpdate(); // TODO: There's also need for setMovementFromNavPathSystem to be called before this... instead there's repetition in the UnitCandidateStateResolver
		setMovementFromNavPathSystem.SystemUpdate();
		setMovementFromTargetSystem.SystemUpdate();

		removeExitingDwarvesSystem.SystemUpdate();

		levelEndSystem.SystemUpdate();

		// NOTE: Performed in update to catch the moment the unit is grid aligned for the attacking transition.
		// Otherwise a unit may become unaligned again before FixedUpdate hits and the check will fail. Or the check would pass and the unit would start attacking from an unaligned position.
		// On the other hand, waiting for fixed update before letting the unit proceed by moving setMovementFrom...() into FixedUpdate would work but would cause jagged motion (or maybe that's desired?)
		// Having the tileMovement and setMovementFrom... live in both update and fixedupdate is not a solution, I think it would still skip the spot in Update
		// Walking the unit back to grid aligned to attack would cause some jitter
		unitStateTransitionSystem.SystemUpdate(); 

		sometimesSetDirectionFromStateSystem.SystemUpdate();

		screenMovementSystem.SystemUpdate();

		entityDissipationSystem.SystemUpdate();

		setAnimationFromUnitStateSystem.SystemUpdate();

		resourceDisplayCandidateStateResolverSystem.SystemUpdate();
		showAndHideResourceDisplaySystem.SystemUpdate();

		if (Input.GetKeyDown(KeyCode.R))
		{
			EntityHelpers.DestroyEverything(levelEntity, dwarfEntities, attackEntities);

			gameStateEntity.levelShouldBeLoaded = true;
			gameStateEntity.dwarvesShouldBeLoaded = true;
		}

		if (Input.GetKeyDown(KeyCode.X))
		{
			foreach (var dwarfEntity in dwarfEntities)
			{
				// TODO: This hasn't been tested recently
				dwarfEntity.unitStateComponent.unitState = UnitState.Idle;
				dwarfEntity.unitAttackingStateComponent.hasIntendedAttackTarget = false;
				dwarfEntity.tileMovementStateComponent.hasIntendedMovementTarget = false;
				//dwarfEntity.unitAttackingStateComponent.intendedAttackTargetCoord = Vector2Int.zero;
				//dwarfEntity.unitAttackingStateComponent.spotToAttackFromCoord = Vector2Int.zero;
			}
		}
	}

	public void PlayerInputNextSpeechStep()
	{
		gameStateEntity.dialogueStateComponent.playerInputNextSpeechStep = true;
	}

	public void DwarfUpgradesOpen()
	{
		gameStateEntity.openDwarfUpgradesClickedOrTriggered = true;
		gameStateEntity.openDwarfUpgradesWithCloseButton = true;
		gameStateEntity.openDwarfUpgradesWithContinueButton = false;
	}

	public void DwarfUpgradesContinue()
	{
		gameStateEntity.continueDwarfUpgradesClicked = true;
	}

	public void DwarfUpgradesClose()
	{
		gameStateEntity.closeDwarfUpgradesClicked = true;
	}

	public void StartScreenClose()
	{
		gameStateEntity.closeStartScreen = true;
	}
}
