﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RockDamageOrDeathAnimationEntity : MonoBehaviour
{
	public RockTileType originRockTileType;
	public float dissipationDuration;

	//[HideInInspector]
	public float dissipationStartTime;
}
