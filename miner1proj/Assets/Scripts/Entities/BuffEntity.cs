﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffType
{
	OreCry,
	DamageAmpOnNode,
	ArmorBreakOnNode,
}

public enum ProcBuffApplicationType
{
	Self, // NOTE: Currently not used
	AttackReceiver,
	BuffGiverAreaCreator
}

public struct BuffTypeAttributes
{
	public bool isDissipating;
	public bool isCountingDownCharges;
	public bool isBuffGiverAreaDependent;
}

public class BuffEntity
{
	[System.Serializable]
	public struct BuffStatSet
	{
		// TODO: Perhaps a buff stat set should be component based
		public float dissipationDuration;
		public int charges;

		public float attackSpeedBonus;
		public float movementSpeedBonus;
		public double damageAmpBonus;
		public double toughnessArmorFlatReduction;
	}

	// Prototype data

	public BuffType buffType;
	public BuffStatSet baseStats;
	public int visualPriorityRating;
	public bool isDissipating;
	public bool isCountingDownCharges;
	public bool isBuffGiverAreaDependent;

	// State

	//[HideInInspector]
	public int level; // TODO: Not really used currently

	public BuffStatSet finalStats;

	// TODO: There's clear overlap here between the BuffStatSet and these... subvalues. All the stats should instead be components in this case perhaps?
	//[HideInInspector]
	public float dissipationStartTime;
	//[HideInInspector]
	public int currentCharges;
	//[HideInInspector]
	public bool buffAreaDependenceMaintainedThisFixedUpdate;
}
