﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffVisualAnimationControllerData
{
	public BuffType buffType;
	public RuntimeAnimatorController runtimeAnimatorController;
}

public class BuffVisualEntity : MonoBehaviour
{
	public BuffType buffType;
	public Animator animator;
	public SpriteRenderer spriteRenderer;

	// State
	public bool hasValidBuff;
	public bool becameInvalid; // Cleared at end of processing in system. // TODO: Used for triggering .gameObject.SetActive(false) only when needed. Does it matter performance wise?
	public bool wasUsedAlready; // Cleared at end of processing in system. Used for marking buffVisualEntities that got a buff assigned to them already in the BuffVisualEntityUpdateSystem
}
