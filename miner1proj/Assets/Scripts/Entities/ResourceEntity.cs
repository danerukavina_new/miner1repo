﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType
{
	// Stones mined from the game
	Stone,

	// Ores mined from the game
	CopperOre,
	TinOre,
	SilverOre,

	// Bars crafted from ores or Smelt effect from the game
	CopperBar,
	TinBar,
	SilverBar,

	// Exclusively craftable bars
	BronzeBar,

	// Other resources
	Diamond
}

public class ResourceEntity
{
	public ResourceType resourceType;
	public double amount;
}
