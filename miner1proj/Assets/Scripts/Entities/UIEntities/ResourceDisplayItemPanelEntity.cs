﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ResourceDisplayItemPanelEntity : MonoBehaviour
{
	public Image resourceSpriteRenderer; // Image, not SpriteRenderer, right?
	public TextMeshProUGUI amountTextMeshPro;
	public int positionLeftToRight;
}
