﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[System.Serializable]
public class TutorialInputIndicatorAnimationControllerData
{
	public TutorialInputIndicatorType tutorialInputIndicatorType;
	public RuntimeAnimatorController runtimeAnimatorController;
}

public class TutorialInputIndicatorEntity : MonoBehaviour
{
	// Prototype
	public Animator inputIndicatorAnimator;
	public TextMeshPro optionalText;
}