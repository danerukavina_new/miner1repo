﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DwarfUpgradePanelEntity : MonoBehaviour
{
	public ResourceDisplayItemPanelEntity resourceDisplayItemPanelEntity;
	public TextMeshProUGUI levelText;
	public Button upgradeButton; // TODO: Control its clickability
	public RectTransform resourceDisplayItemPanelLayoutGroupRectTransform;  // TODO: This is for forcing a layout group recalculation, but it doesn't work. It's intended to prevent the resource display text from bugging out due to its layout group
	public DwarfPortraitUItem dwarfPortraitUIItem;
	public TextMeshProUGUI upgradeStatsTMPText;
	
	public bool isDirtyDueToReinitialization; // NOTE: These aren't being clearly initialized to true/false
	public bool triggerUpgradeStatsText;
	public bool upgradeClicked;
	public bool hideDwarfUpgradeButton;
	public bool reshowDwarfUpgradeButton;

	public float upgradeStatsTextDissipationStartTime;
	public bool firstUpgradeStatsTextDissipationOccurred;
	public bool secondUpgradeStatsTextDissipationOccurred;

	public void UpgradeClicked()
	{
		upgradeClicked = true;
	}
}