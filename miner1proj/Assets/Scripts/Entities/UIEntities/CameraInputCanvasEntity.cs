﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInputCanvasEntity : MonoBehaviour
{
	[System.Serializable]
	public class CameraInputDirectionArrowData
	{
		public Vector2Int direction;
		public UnityEngine.UI.Image image;
	}

	public RectTransform ownRectTransform;
	public RectTransform cameraJoystickOrigin;
	public RectTransform cameraJoystickPosition;
	public CameraInputDirectionArrowData[] editorDirectionArrows;
	public Dictionary<Vector2Int, UnityEngine.UI.Image> directionArrows;

	private void Awake()
	{
		directionArrows = new Dictionary<Vector2Int, UnityEngine.UI.Image>(editorDirectionArrows.Length);

		foreach (var item in editorDirectionArrows)
		{
			directionArrows.Add(item.direction, item.image);
		}
	}
}