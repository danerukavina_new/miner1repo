﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DwarfUpgradeDisplayCanvasEntity : MonoBehaviour
{
	public List<DwarfUpgradePanelEntity> dwarfUpgradePanelEntities;
	public Button continueButton;
	public Button closeButton;
	public List<RectTransform> resourceDisplayItemPanelRectTransforms; // TODO: This is for forcing a layout group recalculation, but it doesn't work. It's intended to prevent the resource display text from bugging out due to its layout group
	public DwarfUpgradePanelEntity dwarfUpgradePanelPrefab;
	public GameObject dwarfGridPanel;

	// TODO: Not just this is State. Clearly mark whether dwarfUpgradePanelEntities is considered Stateful
	public Dictionary<DwarfEntity, DwarfUpgradePanelEntity> dwarfEntityToDwarfUpgradePanelEntityMap;

	// [HideInInspector]
	public bool refreshUpgradeButtonsDueToResourceChange;

	public void Initialize()
	{
		DwarfUpgradePanelEntity dwarfUpgradePanel;
		for (int i = 0; i < ValueHolder.MAXIMUM_DWARVES_IN_POSSESSION; i++)
		{
			dwarfUpgradePanel = GameObject.Instantiate(dwarfUpgradePanelPrefab, dwarfGridPanel.transform);
			dwarfUpgradePanel.gameObject.SetActive(false);
			dwarfUpgradePanelEntities.Add(dwarfUpgradePanel);

			resourceDisplayItemPanelRectTransforms.Add(dwarfUpgradePanel.resourceDisplayItemPanelLayoutGroupRectTransform);
		}

		dwarfEntityToDwarfUpgradePanelEntityMap = new Dictionary<DwarfEntity, DwarfUpgradePanelEntity>(ValueHolder.MAXIMUM_DWARVES_IN_POSSESSION);

		refreshUpgradeButtonsDueToResourceChange = false;
	}
}