﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialogueDisplayCanvasEntity : MonoBehaviour
{
	public DwarfPortraitUItem[] dwarfPortraitUIItems;
	public TextMeshProUGUI dialogueTextMeshPro;
	public RectTransform rectTransform;
	public GameObject dialogueProceedIndicator;
	public Animator dialogueProceedIndicatorAnimator; // TODO: May not be needed, I guess it should always be animating when enabled?
	public TextMeshProUGUI speakerNameLeftSide;
	public TextMeshProUGUI speakerNameRightSide;
}

[System.Serializable]
public class DwarfSpriteData
{
	public DwarfType dwarfType;
	public Sprite portraitHeadSprite;
	public Sprite portraitBodySprite;
}

[System.Serializable]
public class DwarfPortraitUItem
{
	public GameObject portraitHeadObject;
	public GameObject portraitBodyObject;
	public Image portraitHeadImage;
	public Image portraitBodyImage;
}