﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceDisplayCanvasEntity : MonoBehaviour
{
	public enum ResourceDisplayState
	{
		Hide,
		ResourceAddition,
		DwarfUpgrade,
		DwarfUpgradeResourceSubtraction
	}

	public RectTransform rectTransform;
	public float baseScreenMovementVelocity;
	public float baseDelayBeforeHiding;
	public float fullyHiddenRectPosition;
	public float fullyShownRectPositionY;
	public ResourceDisplayItemPanelEntity[] resourceDisplayItemPanelEntities;

	// [HideInInspector]
	public bool attemptRefreshResourceDisplay;
	public ResourceDisplayState resourceDisplayState;
	public ResourceDisplayState candidateResourceDisplayState;
	public bool moveToShow;
	public int preventAutoHideSubscribers;

	// [HideInInspector]
	public float entryStartTime;
}
