﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BarelyInteractiveInfoModalCanvasEntity : MonoBehaviour
{
	// Prototype
	public TextMeshProUGUI infoModalText;
	public float ignoreCloseInputInitiallyDuration;
	public float automaticallyBeginClosingDuration;
	public TextSizer textSizer;

	// [HideInInspector]
	public bool openNow;
	public List<string> textsToShow;
	public float durationStartTime;
	public bool attemptClosingEarly;
	public bool closeEarly;

	public void BarelyInteractiveInfoModalAtteptClosingEarly()
	{
		attemptClosingEarly = true;
	}
}