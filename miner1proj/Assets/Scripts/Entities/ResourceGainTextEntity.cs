﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceGainTextEntity : MonoBehaviour
{
	public TextMeshPro textComponent;
	public SpriteRenderer spriteComponent;

	public float resourceGainOffsetFromSource;
	public float dissipationDuration;
	public float baseFontSize;
	public float baseScreenMovementSpeed;
	public float baseLingerInPlaceDelay;

	//[HideInInspector]
	public float initialImpactfullness; // TODO: Doesn't do anything yet, just recorded if we want to grow buffs up later?
	//[HideInInspector]
	public bool isDissipating;
	//[HideInInspector]
	public float dissipationStartTime;
	//[HideInInspector]
	public ScreenMovementStateComponent screenMovementStateComponent;
}

[System.Serializable]
public class ResourceTypeSpriteData
{
	public ResourceType resourceType;
	public Sprite sprite;
}
