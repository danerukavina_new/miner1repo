﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum FogShapeType
{
	Basic,
}

public class FullFogEntity : MonoBehaviour
{
	// Stats
	public FogShapeType fogShapeType;
	public float fadeTime;

	// [HideInInspector]
	public Vector2Int coord;
	// [HideInInspector]
	public bool isFading;
	// [HideInInspector]
	public float fadeStartTime;
}
