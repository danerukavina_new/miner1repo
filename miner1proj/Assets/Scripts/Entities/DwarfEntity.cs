﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DwarfType
{
	MeleeCleave,
	MeleeSpear,
	RangedBasicProjectile,
	RangedChain,
	Ritual,
	MeleeHammer,
	EggChamberClosed = 100,
	EggChamberOpen = 101,
}

public class DwarfEntity : MonoBehaviour
{
	public DwarfType dwarfType;
	public PossibleAttackingPosition possibleAttackingPosition;
	public float baseAttackRange;
	public bool canPenetrateDiagonally;
	public float powerLevel; // TODO: This is for testing map click system, flesh out more later. Should be a derived value from the dwarf's stats in the end state

	public UnitStatsComponent.UnitStatSet baseStats;
	public UnitStatsComponent.UnitStatSet growthStats;

	public Animator animator;
	public GameObject buffVisualsObject;
	public List<BuffVisualEntity> buffVisualEntities;

	//[HideInInspector]
	public UnitStateComponent unitStateComponent;
	//[HideInInspector]
	public bool tutorialDisabledCandidateActionProcessing; // TODO: Combine this and has exited level to make a better disabled state
	//[HideInInspector]
	public bool hasExitedLevel;
	//[HideInInspector]
	public bool isRequiredToExitLevelToProceedToNextLevel;
	//[HideInInspector]
	public bool belongsInPlayersParty;

	//[HideInInspector]
	public string dwarfName;

	//[HideInInspector]
	public Vector2Int coord; // TODO: Or should this be TilePositionStateComponent?
	//[HideInInspector]
	public bool coordWasDirtiedThisFixedUpdate;
	//[HideInInspector]
	public bool isCoordGridAligned;

	//[HideInInspector]
	public TileMovementStateComponent tileMovementStateComponent;
	//[HideInInspector]
	public CandidateTileMovementStateComponent candidateTileMovementStateComponent;

	//[HideInInspector]
	public UnitAttackingStateComponent unitAttackingStateComponent;
	//[HideInInspector]
	public CandidateUnitAttackingStateComponent candidateUnitAttackingStateComponent;

	//[HideInInspector]
	public FogSightComponent fogSightComponent;

	//[HideInInspector]
	public NavigationComponent navigationComponent;

	//[HideInInspector]
	public bool hasManualAssignment; //TODO: Or should this be ManualTileAssignmentComponent
	//[HideInInspector]
	public Vector2Int manuallyAssignedCoord;

	//[HideInInspector]
	public UnitStatsComponent unitStatsComponent;

	//[HideInInspector]
	public BuffGiverAreaCreatorComponent buffGiverAreaCreatorComponent;
	//[HideInInspector]
	public BuffReceiverComponent buffReceiverComponent;
	//[HideInInspector]
	public ProcOnAttackComponent procOnAttackComponent;
}
