﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum DamageOrBuffTextType
{
	BasicDamage,
	CritDamage,
	Smelt,
	ImmuneNoDamage,
	ToughnessArmorNoDamage,
	Buff
}

public class DamageOrBuffTextEntity : MonoBehaviour
{
	public DamageOrBuffTextType damageOrBuffTextType;
	public TextMeshPro textComponent;

	public float damageOrBuffOffsetFromSource;
	public float dissipationDuration;
	public float baseFontSize;
	public float baseScreenMovementSpeed;
	public float baseLingerInPlaceDelay;

	//[HideInInspector]
	public float initialImpactfullness; // TODO: Doesn't do anything yet, just recorded if we want to grow buffs up later?
	//[HideInInspector]
	public bool isDissipating;
	//[HideInInspector]
	public float dissipationStartTime;
	//[HideInInspector]
	public ScreenMovementStateComponent screenMovementStateComponent;
}
