﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FloorTileType
{
	Impassible,
	Void,
	Dirt,
	MasonedFloor,
	BlackInaccessible
}

public class FloorTileEntity : MonoBehaviour
{
	public static FloorTileEntity GetFloorTileEntity(FloorTileType floorTileType)
	{
		FloorTileEntity floorTileEntity = new FloorTileEntity();

		floorTileEntity.floorTileType = floorTileType;

		return floorTileEntity;
	}

	public FloorTileType floorTileType;

	public bool walkable;

	public bool passable;
}
