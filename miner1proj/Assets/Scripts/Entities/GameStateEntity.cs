﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerClickInputState
{
	NoneOrTutorialCancelled,
	ClickStartedAndAmbiguous,
	ManualCoordinateInput,
	CameraMovement
}

public enum CameraPressAndHoldControlType
{
	None,
	Joystick,
	ClickAndDrag
}

public class GameStateEntity
{
	public bool onStartScreen; // TODO: Redundant with start screen game object "active" state
	public bool haveStartScreenAtStartOfGame;
	public bool openStartScreen; // TODO: Maybe these screens... and their control. Should be handled with a UIScreenState and a candidateUIScreenState, or something like that...
	public bool closeStartScreen;

	public bool levelShouldBeLoaded;
	public bool dwarvesShouldBeLoaded;
	public int indexToLoad;
	public float baseCameraMovementSpeed;
	public CameraPressAndHoldControlType cameraPressAndHoldControlType;
	public bool cameraEdgeOfScreenControlEnabledNYI; // TODO: Implement this

	public bool playerClickedTile;
	public Vector2Int playerclickedTileCoord;
	public PlayerClickOrderType playerClickOrderType;

	public bool autoMine;
	public int difficultyTier; // TODO: Intended to scale up HP and resource rewards, among other things, every few rooms

	public PlayerClickInputState inputState;
	public bool tutorialDisabledManualCoordinateInput;
	public bool tutorialDisabledCameraInput;
	public bool tutorialDisableAllButExitInput; // TODO: Not used
	public bool inputStateBecameCameraMovementThisUpdate;
	public float playerClickInputStartTime;
	public Vector3 playerClickInputStartScreenPosition;
	public Vector3 playerClickInputCurrentScreenPosition;
	public Vector3 playerClickInputStartWorldPosition;
	public Vector3 playerClickInputCurrentWorldPosition;
	public Vector3 playerClickInputStartCameraWorldPosition;
	public bool playerCoordinatesChanged;

	public Bounds softCameraPositionBounds;

	public TutorialsStateComponent tutorialsStateComponent;
	public bool tutorialDisabledResourceGains;

	public DialogueStateComponent dialogueStateComponent;

	// TODO: This seems like a good cause for encapsulation or something. Maybe a menu Entity that just receives requests for opening / closing menues, controls a stack, etc.
	public int currentlyOpenMenusSubscription; // NOTE: Handle dialogue separately for now, some dialogue might not need to block input

	// TODO: There is no variable that represents the DwarfUpgrades menu is open... only the above subscription variable (vaguely) and the enabled/disabled state of the unity object
	public bool openDwarfUpgradesClickedOrTriggered;
	public bool closeDwarfUpgradesClicked;
	public bool continueDwarfUpgradesClicked;
	public bool dwarfPartyRearrangeOccurredRefreshUpgradesPanel;
	public bool disableDwarfUpgradeContinueButton;
	public bool reenableDwarfUpgradeContinueButton;
	public bool openDwarfUpgradesWithCloseButton; // Does not require initial state, sits behind other logic
	public bool openDwarfUpgradesWithContinueButton; // Does not require initial state, sits behind other logic
	public bool hideDwarfUpgradeButtons;
	public bool hideAllDwarfUpgradeButtons;
	public bool reshowDwarfUpgradeButtons;
	public bool reshowAllDwarfUpgradeButtons;
	public List<int> hideDwarfUpgradeButtonDwarfIDs;
	public List<int> reshowDwarfUpgradeButtonDwarfIDs;
	public bool dwarfUpgradesTriggersAtEndsOfLevels;
	public bool dwarfUpgradesHoldingNextLevelLoadAtEndOfLevel;

	public TutorialSmeltControlSetting tutorialSmeltControlSetting;

	public bool testingSkipStartScreen;
	public bool testingSkipSomeTutorialLevels;
	public int testingLevelOfTutorialToSkipTo;
	public bool testingSkipSomeTutorialLevelsForLoadDwarves; // Does not require initial state, sits behind other logic

	// public bool fakedGetMouseButton0; // TODO: This is a hack to adapt my current code to not need to use Input.GetMouseButton(0) which doesn't work on mobile
}
