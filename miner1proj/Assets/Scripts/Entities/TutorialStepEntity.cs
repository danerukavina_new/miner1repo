﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TutorialStepComponentTag
{
	TimePause,
	TimeUnpause,
	TimePauseWithDuration,
	DialogueSteps,
	SetTutorialCameraBoundsWhichMayPanCamera,
	RemoveTutorialCameraBounds,
	TeleportCamera,
	AssignManualCoordinates,
	ClearManualCoordinates,
	DisableDwarfCandidateActionProcessing,
	ReenableDwarfCandidateActionProcessing,
	ReenableAllDwarfCandidateActionProcessing,
	DisableDwarfFogSight,
	ReenableDwarfFogSight,
	ReenableAllDwarfFogSight,
	DisableManualCoordinateInput,
	EnableManualCoordinateInput,
	DisableCameraInput,
	EnableCameraInput,
	DisableAllButExitInput,
	RenableNonExitInput,
	SetRockHPs,
	CreateRocks,
	RemoveRocks,
	CreateDwarves,
	RemoveDwarves,
	CreateAttacks,
	DisableSmelts,
	EnableSmelts,
	DisableResourceGains,
	EnableResourceGains,
	RemoveFullFog,
	SetIdleDwarfFacingDirections,
	ActivateDwarfUpgradeDisplayInContinueModeWithContinueDisabled,
	DisableDwarfUpgradeContinueButton,
	ReenableDwarfUpgradeContinueButton,
	HideDwarfUpgradeButtons,
	ReshowDwarfUpgradeButtons,
	HideAllDwarfUpgradeButtons,
	ReshowAllDwarfUpgradeButtons,
	DisableDwarfUpgradesTriggeringAtEndsOfLevels,
	ReenableDwarfUpgradesTriggeringAtEndsOfLevels,
	ReshowOpenDwarfUpgradesButton,
	HideOpenDwarfUpgradesButton,
	AddTutorialInputIndicatorWithPossibleText,
	RemoveTutorialInputIndicatorWithPossibleText,
	AddTutorialInputMask,
	RemoveTutorialInputMask,
	ChangeTutorialSmeltControlSetting,
	StopDwarfAttacksInPlaceAfterBackswing
}

public enum TutorialInputIndicatorType
{
	TapInput,
	TapAndDragInput,
	PressAndHoldInput
}

public enum TutorialSmeltControlSetting
{
	None,
	PreventSmelting,
	AlwaysSmeltIfSmeltable
}

// TODO: Refactor this to use polymorphism, it fits. But I don't think each kind of tutorial step is a component - don't want to pollute 
public class TutorialStepEntity
{
	public TutorialStepEntity(
		int gameLevel,
		string documentationalDescriptionOfStep,
		List<TutorialStepComponentTag> tutorialStepComponentTags,
		StartTutorialStepCondition startTutorialStepCondition
		)
	{
		this.gameLevel = gameLevel;
		this.documentationalDescriptionOfStep = documentationalDescriptionOfStep;
		this.tutorialStepComponentTags = tutorialStepComponentTags;
		this.startTutorialStepCondition = startTutorialStepCondition;
	}

	public delegate bool StartTutorialStepCondition(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera);

	public int gameLevel;
	public string documentationalDescriptionOfStep;
	public List<TutorialStepComponentTag> tutorialStepComponentTags;
	public StartTutorialStepCondition startTutorialStepCondition;

	// TODO: Define entry condition logic, possibly as a function of gameStateEntity, levelEntity, dwarfEntities, and whatever else that may be relevant... Time.time?

	// State
	public bool tutorialStepIsResolved;

	public float timePauseDuration;

	public List<DialogueStepEntity> dialogueStepEntities;
	// State
	public int currentSpeechStep;

	public BoundsInt tutorialCameraBounds;

	public Vector2 teleportCameraPosition;

	public List<Vector2Int> assignedManualCoordinates;

	//public bool clearManualCoordinates;

	public List<int> disableDwarfCandidateActionProcessingIDs;

	public List<int> reenableDwarfCandidateActionProcessingIDs;

	public List<int> disableDwarfFogSightIDs;

	public List<int> reenableDwarfFogSightIDs;

	//public bool enableAllDwarfCandidateActionProcessing;

	//public bool disableManualCoordinateInput;

	//public bool enableManualCoordinateInput;

	//public bool disableCameraInput;

	//public bool enableCameraInput;

	public List<Vector2Int> rocksToSetHPsCoords;
	public List<double> rockHPsToBeSetValues;

	public List<RockTileType> rocksToBeCreatedTypes;
	public List<Vector2Int> rocksToBeCreatedCoords;

	public List<Vector2Int> rocksToBeRemovedCoords;

	public List<int> dwarvesToBeCreatedPartyInsertionIndexes;
	public List<DwarfType> dwarvesToBeCreatedTypes;
	public List<Vector2Int> dwarvesToBeCreatedCoords;
	public List<string> dwarvesToBeCreatedNames;
	public List<bool> dwarvesToBeCreatedExitLevelRequirement;
	public List<bool> dwarvesToBeCreatedBelongingInPlayersPartisanship;

	public List<int> dwarvesToBeRemovedIDs;

	public List<AttackType> attacksToBeCreated;
	public List<Vector2Int> attacksToBeCreatedStartingCoords;
	public List<Vector2Int> attacksToBeCreatedTargetCoords;

	public List<Vector2Int> fullFogsToBeRemovedCoords;

	public List<int> idleDwarvesToHaveFacingDirectionSetIDs;
	public List<Vector2Int> idleDwarfFacingDirectionNormalizedGridAlignedDirections;

	//public bool disableSmelts;

	//public bool enableSmelts;

	public List<int> hideDwarfUpgradeButtonDwarfIDs;

	public List<int> reshowDwarfUpgradeButtonDwarfIDs;

	public TutorialInputIndicatorType addedTutorialInputIndicatorType;
	public string addedTutorialInputIndicatorOptionalText;
	public Vector2 addedTutorialInputIndicatorPosition;

	public Bounds addedTutorialInputMaskBounds; // TODO: NYI

	public TutorialSmeltControlSetting tutorialSmeltControlSettingToBeChangedTo;

	public List<int> stopDwarfAttacksInPlaceAfterBackswingIDs;

	public static TutorialStepEntity EndOfDialogueTutorialStep(int gameLevel, string documentationalDescriptionOfStep)
	{
		return new TutorialStepEntity(
			gameLevel,
			documentationalDescriptionOfStep,
			new List<TutorialStepComponentTag>(),
			(GameStateEntity gameStateEntity, LevelEntity levelEntity, List<DwarfEntity> dwarfEntities, float time, Camera mainCamera) =>
			{
				return LogicHelpers.AssessDialogueEndAndResetFlag(gameStateEntity);
			}
			);
	}
}