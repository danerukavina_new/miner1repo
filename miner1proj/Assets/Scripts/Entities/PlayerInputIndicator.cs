﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerClickOrderType
{
	GoToEmptyTile,
	AttackNode,
	ExploreFog
}

public class PlayerInputIndicator : MonoBehaviour
{
	public PlayerClickOrderType playerClickOrderType;
	public bool isShared;

	public Vector2Int coord;
}
