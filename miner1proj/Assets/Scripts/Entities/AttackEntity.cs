﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType
{
	Basic,
	Spear,
	Cleave,
	Chain,
	BasicProjectile,
	BasicDoubleHit
}

public class AttackEntity : MonoBehaviour
{
	public AttackType attackType;
	public float attackStartOffsetFromDwarf;
	public double baseDamage;
	public float baseMovementSpeed;
	public float baseMovementRange;
	public int baseMaxBounces;
	public bool baseRepeatBounceTargetsAllowed;
	public float baseBounceRange;
	public float attackDealingDamageDissipationDuration; // TODO: For the Basic Double Hit Attack, this is coupled with the attack speed in a way. Annoying.
	public float attackFizzlingOutDissipationDuration;
	public bool damageIsDirectionless;
	public Animator animator;

	//[HideInInspector]
	public AttackStateComponent attackStateComponent;

	//[HideInInspector]
	public TileMovementStateComponent tileMovementStateComponent;

	//[HideInInspector]
	public TileMovementRangeComponent tileMovementRangeComponent;

	//[HideInInspector]
	public ChainTileBouncingComponent chainTileBouncingComponent;

	//[HideInInspector]
	public List<DamageTileCoord> damageTileCoords;

	//[HideInInspector]
	public DwarfEntity owner;
	//[HideInInspector]
	public Vector2Int coord;
	//[HideInInspector]
	public bool isCoordGridAligned;
	//[HideInInspector]
	public Vector2Int targetCoord;
	//[HideInInspector]
	public bool primedToDealDamage;
	//[HideInInspector]
	public bool begunDealingDamageThisFixedUpdate;
	//[HideInInspector]
	public float attackDissipationStartTime;
}
