﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEntity
{
	public int xSize;
	public int ySize;
	public FloorTileEntity[,] floorTiles;
	public RockTileEntity[,] rockTiles;
	public PartialFogEntity[,] partialFogTiles;
	public FullFogEntity[,] fullFogTiles;
	public PlayerInputIndicator[,] playerInputIndicators;

	// TODO: Should there just be one kind of reservation? If the dwarves reserve in order and we want to avoid dwarves standing on top of each other always...
	// I believe the original idea of only reserving in the attack case didn't make sense
	public TileMovementStateComponent[,] reservedByAttackerTiles; // TODO: this has problems, the MovementStateComponent needs to know who its parent entity is for the MovementComponent to be useful
	public TileMovementStateComponent[,] reservedByFreeSpaceMoverTiles; // TODO: This was very quickly implemented and probably sucks. Implemented by imitating reservedByAttackerTiles. Key idea is it's used in one place to discard sharedManualCoords by checking that it's not null
	public Vector2Int levelDwarfEntranceCoord;
	public Vector2Int levelExitCoord; // TODO: Barely used

	public List<Vector2Int> sharedManualCoordinates;
	public bool[,] isSharedManualCoordinate;
	public NavCoord[,] sharedUnreachableNavArray;
	public List<Vector2Int> sharedFoundUnreachableEdges;
	public bool[,] sharedConsideredUnreachableEdgeAlready;

	public bool isDirty;
}