﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DialogueStepComponentTag
{
	ParticipantPortraitsFromIDs,
	ParticipantPortraitsFromDwarfTypes,
	SpeakerName,
	CustomSpeakerName,
	HideSpeakerName,
	HighlightSingleParticipant,
	HighlightedParticipants,
	ParticipantRenderOrder,
	TwoButtonChoices,
	ThreeButtonChoices
}

public class DialogueStepEntity
{
	public DialogueStepEntity(
		string speech,
		List<DialogueStepComponentTag> dialogueStepComponentTags
		)
	{
		this.dialogueText = speech;
		this.dialogueStepComponentTags = dialogueStepComponentTags;
	}

	public string dialogueText;
	public List<DialogueStepComponentTag> dialogueStepComponentTags;

	public List<int?> participantPortraitIDsForScreenIDs;

	public List<DwarfType?> participantDwarfTypesForScreenIDs;

	public bool speakerNameIsOnRightSide; // NOTE: Shared for SpeakerName and CustomSpeakerName

	public int speakerNamingID;

	public string customSpeakerName;

	public int highlightSingleParticipantScreenID;

	public List<bool?> highlightedParticipantWithScreenIDs;

	public List<int> participantRenderOrderWithScreenIDs;

	public List<string> twoButtonChoiceTexts;

	public List<string> threeButtonChoiceTexts;

	// TODO: Will this amount of allocation in creating tutorial steps be a problem?
	public static DialogueStepEntity ContinueConversationWithAnotherString(string speechStep)
	{
		return new DialogueStepEntity(speechStep, new List<DialogueStepComponentTag>());
	}

	// TODO: Will this amount of allocation in creating tutorial steps be a problem?
	public static DialogueStepEntity HighlightSpeakerAndHaveThemSaySomething(int dwarfID, int screenID, string speechStep)
	{
		return new DialogueStepEntity(speechStep,
			new List<DialogueStepComponentTag>()
			{
				DialogueStepComponentTag.SpeakerName,
				DialogueStepComponentTag.HighlightSingleParticipant
			})
		{
			speakerNamingID = dwarfID,
			speakerNameIsOnRightSide = screenID >= ValueHolder.NUMBER_OF_DWARF_PORTRAITS_ON_LEFT_SIDE,
			highlightSingleParticipantScreenID = screenID
		};
	}

	// TODO: Will this amount of allocation in creating tutorial steps be a problem?
	public static List<DialogueStepEntity> SingleDwarfSpeaking(int dwarfID, List<string> speechSteps)
	{
		List<DialogueStepEntity> ret = new List<DialogueStepEntity>();

		if (speechSteps.Count <= 0)
			throw new System.Exception("SingleDwarfSpeaking expected at least one element in its input list of things to say.");

		ret.Add(
			new DialogueStepEntity(speechSteps[0],
					new List<DialogueStepComponentTag>() { DialogueStepComponentTag.ParticipantPortraitsFromIDs, DialogueStepComponentTag.SpeakerName })
			{
				participantPortraitIDsForScreenIDs = new List<int?>() { dwarfID },
				speakerNamingID = dwarfID,
				speakerNameIsOnRightSide = false
			}
			);

		for (int i = 1; i < speechSteps.Count; i++)
		{
			ret.Add(
			new DialogueStepEntity(speechSteps[i],
					new List<DialogueStepComponentTag>())
			);
		}

		return ret;
	}

	public static List<DialogueStepEntity> SingleDwarfSpeaking(DwarfType dwarfType, string customSpeakerName, List<string> speechSteps)
	{
		List<DialogueStepEntity> ret = new List<DialogueStepEntity>();

		if (speechSteps.Count <= 0)
			throw new System.Exception("SingleDwarfSpeaking expected at least one element in its input list of things to say.");

		ret.Add(
			new DialogueStepEntity(speechSteps[0],
					new List<DialogueStepComponentTag>() { DialogueStepComponentTag.ParticipantPortraitsFromDwarfTypes, DialogueStepComponentTag.CustomSpeakerName })
			{
				participantDwarfTypesForScreenIDs = new List<DwarfType?>() { dwarfType },
				customSpeakerName = customSpeakerName,
				speakerNameIsOnRightSide = false
			}
			);

		for (int i = 1; i < speechSteps.Count; i++)
		{
			ret.Add(
			new DialogueStepEntity(speechSteps[i],
					new List<DialogueStepComponentTag>())
			);
		}

		return ret;
	}

	public static List<DialogueStepEntity> SingleCustomSpeaking(string customSpeakerName, List<string> speechSteps)
	{
		List<DialogueStepEntity> ret = new List<DialogueStepEntity>();

		if (speechSteps.Count <= 0)
			throw new System.Exception("SingleDwarfSpeaking expected at least one element in its input list of things to say.");

		ret.Add(
			new DialogueStepEntity(speechSteps[0],
					new List<DialogueStepComponentTag>() { DialogueStepComponentTag.ParticipantPortraitsFromIDs, DialogueStepComponentTag.CustomSpeakerName })
			{
				participantPortraitIDsForScreenIDs = new List<int?>() { },
				customSpeakerName = customSpeakerName,
				speakerNameIsOnRightSide = false
			}
			);

		for (int i = 1; i < speechSteps.Count; i++)
		{
			ret.Add(
			new DialogueStepEntity(speechSteps[i],
					new List<DialogueStepComponentTag>())
			);
		}

		return ret;
	}
}