﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffGiverAreaType
{
	OreCryArea,
}

[System.Serializable]
public class BuffGiverAreaEntityData
{
	public BuffGiverAreaType buffGiverAreaType;
	public Sprite portraitHeadSprite;
	public Sprite portraitBodySprite;
}

public class BuffGiverAreaEntity : MonoBehaviour
{
	// Prototype data
	public BuffGiverAreaType buffGiverAreaType;
	public BuffReceiverComponent.ReceiverUnitTypeTag[] intendedReceiverUnitTypeTags;
	public float dissipationDuration; // TODO: This should be a component, only certain BuffGiverAreas that are based on dissipation need this. Others might be based on other conditions
	// public float attackStartOffsetFromDwarf; // If needed, this is where an offset component might go
	public int baseRangeFactor; // TODO: This should be a component, only certain BuffGiverAreas that are procedural need this. Others might need a wholly different component
	// public Animator animator;

	//[HideInInspector]
	public float dissipationStartTime;

	// TODO: Look at primedToDealDamage in AttackEnitity for buffs that shouldn't start going into effect immediately

	// TODO: This can be improved to use bounds, instead of the full level size
	//[HideInInspector]
	public PrototypeBuff[,] prototypeBuffTiles; // TODO: Should this be a component?

	//[HideInInspector]
	public Vector2Int coord;
}
