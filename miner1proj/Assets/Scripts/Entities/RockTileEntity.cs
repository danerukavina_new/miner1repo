﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RockTileType
{
	IndestructibleWall,
	EntranceImpassable,
	Rock1,
	Rock2,
	Copper,
	Tin,
	Silver,
	Exit,
	RockRitualDarkness,
	RockRitualDarknessSpawnEffect,
	EggChamberClosed = 100,
	EggChamberOpen = 101,
}

public class RockTileEntity : MonoBehaviour
{
	public RockTileType rockTileType;

	public double miningValue;
	public bool destructible;
	public bool passable;
	public bool isExit;
	public bool isMiningNode;
	public bool smeltable;
	public double maxHP;
	public double toughnessArmor;
	public SpriteRenderer spriteRenderer;
	public List<Sprite> editorRockDestructionStageSprites;
	public GameObject buffVisualsObject;
	public List<BuffVisualEntity> buffVisualEntities;

	// [HideInInspector]
	public Vector2Int coord;
	// [HideInInspector]
	public double HP;
	// [HideInInspector]

	// TODO: determine if RockTileEntity should simply know its HP bar and control it
	// [HideInInspector]
	public HPBarEntity HPBarEntity;
	// [HideInInspector]
	public RockDamagedStageComponent rockDamagedStageComponent;

	//[HideInInspector]
	public BuffGiverAreaCreatorComponent buffGiverAreaCreatorComponent;
	//[HideInInspector]
	public BuffReceiverComponent buffReceiverComponent;

	// NOTE: The resources "availble" in the rock are entirely determined by the rockDamageStageComponent's "stage" count, rockTileType, and game state. 
	// The RockTileEntity does not have predetermined resources in it. For this sort of gameplay, implement a RockResourcesComponent.

	// list of properties
	// list of buffs
}
